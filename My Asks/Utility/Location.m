//
//  Location.m
//  My Asks
//
//  Created by MY PC on 4/24/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "Location.h"

@implementation Location

+ (Location *) sharedManager
{
    static Location * sharedManager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[Location alloc] init];
        //        [sharedManager setUpPubnubClientWithConfiguration];
    });
    return sharedManager;
}



@end
