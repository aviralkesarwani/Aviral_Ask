//
//  APIClient.h
//  My Asks
//
//  Created by Mac on 01/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <AFNetworking.h>
#import "AppData.h"
#import "Constants.h"

@interface APIClient : NSObject

+ (APIClient *) sharedManager;

#pragma mark - SignUp

- (void) CreateUser : (NSDictionary *) param onCompletion:(void (^)(NSDictionary * resultData, bool success) )result;

#pragma marl - VarifyUser

- (void) VerifyUser : (NSString *) param onCompletion:(void (^)(NSDictionary * resultData, bool success) )result;

#pragma mark - Save Device Token

- (void) updateDeviceToken : (NSString *) param onCompletion:(void (^)(NSDictionary * resultData, bool success) )result;

#pragma mark - InsertUpdateContacts

- (void) InsertUpdateUserContacts : (NSDictionary *) param onCompletion:(void (^)(NSDictionary * resultData, bool success) )result;

#pragma mark - SearchPlaces

- (void) SearchPlaces : (NSDictionary *) param onCompletion:(void (^)(NSDictionary * resultData, bool success) )result;
- (void) PlaceDetails : (NSDictionary *) param onCompletion:(void (^)(NSDictionary * resultData, bool success) )result;
- (void) GetPlacesFilter:(NSString *)param indexOf : (NSInteger) index onCompletion:(void (^)(NSDictionary *, bool,NSInteger))result;
- (void) PlaceFavorite : (NSDictionary *) param onCompletion:(void (^)(NSDictionary * resultData, bool success) )result;


#pragma mark - FriendsList

- (void) FriendsList : (NSString *) param onCompletion:(void (^)(NSDictionary * resultData, bool success) )result;
- (void) GetFriendProfile : (NSString *) friendID onCompletion:(void (^)(NSDictionary * resultData, bool success) )result;

#pragma mark - InviteFriends

- (void) InviteFriends : (NSString *) param onCompletion:(void (^)(NSDictionary * resultData, bool success) )result;

#pragma mark - Ask

- (void) createAsk : (NSDictionary *) param onCompletion:(void (^)(NSDictionary* resultData, bool success) )result;

- (void) updateAsk : (NSDictionary *) param onCompletion:(void (^)(NSDictionary* resultData, bool success) )result;

- (void) saveASKBackgoundImage : (UIImage *) image onCompletion:(void (^)(NSDictionary* resultData, bool success) )result;

- (void) getUserAsks : (NSString *) userID onCompletion:(void (^)(NSArray * resultData, bool success) )result;

- (void) getAskById : (NSString *) askId userId : (NSString *) userId onCompletion:(void (^)(NSDictionary * resultData, bool success) )result;

- (void) addPlaceToAsk : (NSArray *) param onCompletion:(void (^)(NSDictionary* resultData, bool success) )result;

#pragma mark - Chat

- (void) getChannelHistory : (NSString *) channelName onCompletion:(void (^)(NSArray * resultData, bool success) )result;
- (void) insertAskMessage : (NSDictionary *) param onCompletion:(void (^)(NSArray * resultData, bool success) )result;
- (void) sendAskOfflineNotification : (NSDictionary *) param onCompletion:(void (^)(NSDictionary * resultData, bool success) )result;
- (void) insertAskInvite : (NSDictionary *) param onCompletion:(void (^)(NSDictionary* resultData, bool success) )result;

#pragma mark - Settings

- (void) getMyProfile : (NSString *) userID onCompletion:(void (^)(NSDictionary * resultData, bool success) )result;
- (void) updateUserProfile : (NSDictionary *) param onCompletion:(void (^)(NSDictionary * resultData, bool success) )result;

#pragma mark - Lists

- (void) getSearchPlaces : (NSDictionary *) param onCompletion:(void (^)(NSArray * resultData, bool success) )result;
- (void) getPlaces : (NSDictionary *) param onCompletion:(void (^)(NSDictionary * resultData, bool success) )result;
- (void) getListDetailsById : (NSDictionary *) param onCompletion:(void (^)(NSDictionary * resultData, bool success) )result;
- (void) getUserLists : (NSString *) userID onCompletion:(void (^)(NSArray * resultData, bool success) )result;
- (void) insertUpdateUserList : (NSDictionary *) param onCompletion:(void (^)(NSDictionary* resultData, bool success) )result;
- (void) AddPlaceToUserList : (NSMutableDictionary *) param onCompletion:(void (^)(NSDictionary * resultData, bool success) )result;

#pragma mark -

- (void) updateQBChatDialogId : (NSDictionary *) param onCompletion:(void (^)(NSDictionary * resultData, bool success) )result;
- (void) getAskByDialog : (NSDictionary *) param onCompletion:(void (^)(NSDictionary * resultData, bool success) )result;


@end
