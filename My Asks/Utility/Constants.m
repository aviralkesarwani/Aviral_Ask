//
//  Constants.m
//  My Asks
//
//  Created by Mac on 01/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "Constants.h"

@implementation Constants

NSString* const MABaseURL = @"http://rexlife.azurewebsites.net/api/";

NSString* const MAUserURL = @"User/";

NSString* const MACreateUserURL = @"CreateUser";

NSString* const MAVarifyUserURL = @"VerifyUser";

NSString* const MAUpdateDeviceTokenURL = @"UpdateDeviceToken";

NSString* const MAInsertUpdateUserContactsURL = @"InsertUpdateUserContacts";

NSString* const MAFriendsListURL = @"FriendsList";

NSString* const MAInviteFriendsURL = @"InviteFriends";

NSString* const MAUserAsksURL = @"UserAsks";

NSString* const MACreateAskURL = @"CreateAsk";

NSString* const MAUpdateAskURL = @"UpdateAsk";

NSString* const MAAskByIdURL = @"AskById";

NSString* const MAInsertAskInvitesURL = @"InsertAskInvites";

NSString* const MAAddPlaceToAskURL = @"AddPlaceToAsk";

NSString* const MASaveAskBackgroungImageURL = @"SaveImageToServer";

NSString* const MAGetChannelHistoryURL = @"GetChannelHistory";

NSString* const MAInsertAsksMessageURL = @"InsertAskMessages";

NSString* const MAUserListsURL = @"GetUserLists";

NSString* const MAGetAskByDialog = @"GetAskByDialog";



//NSString* const MASearchPlacesURL = @"SearchPlaces";
NSString* const MASearchPlacesURL = @"SearchGooglePlaces";

//NSString* const MAPlaceDetailsURL = @"PlaceDetails";
NSString* const MAPlaceDetailsURL = @"SearchGooglePlaceByPlaceId";
NSString* const MAPlaceFavoriteURL = @"IsFavouritePlace";

NSString* const GetMyProfileURL = @"GetMyProfile";

NSString* const MAAddPlaceToUserListURL = @"AddPlaceToUserList";

NSString* const MASearchFriendsListOrPlaces = @"SearchFriendsListOrPlaces";

NSString* const MAInsertUpdateUserList = @"InsertUpdateUserList";

NSString* const MAListDetailsById = @"ListDetailsById";

NSString* const MASendAskOfflineNotification = @"SendAskOfflineNotification";

NSString* const MAGetFriendProfileURL = @"GetFriendProfile";

NSString* const MAUpdateUserURL = @"UpdateUser";

NSString* const MAUpdateQBChatDialogId = @"UpdateQBChatDialogId";

NSString* const kSenderID = @"SenderID";
NSString* const kSenderName = @"SenderName";
NSString* const kMessage = @"Message";
NSString* const kMessageType = @"MessageType";
NSString* const kMessageDateTime = @"DateTime";
NSString* const kChannelName = @"ChannelName";

// QuickBlox
NSString* const MAQBPassword = @"QBPassword";
NSString* const MAQBUserName = @"QBUserName";

NSString* const MAUserID = @"UserID";
NSString* const MAUserName = @"UserName";
NSString* const MAUserEmail = @"UserEmail";
NSString* const MAUserMobileNumber = @"UserMobileNumber";
NSString* const MAUserCoutryCode = @"UserCoutryCode";
NSString* const MAUserProfilePicture = @"UserProfilePicture";
NSString* const MAUserIsVerified = @"UserIsVerified";
NSString* const MAUserDeviceToken = @"UserDeviceToken";
NSString* const MAUserDialingCode = @"UserDialingCode";

NSString* const MAUserDeviceName = @"UserDeviceName";

// Upload Background Image   http://rexlife.azurewebsites.net/api/User/SaveAskBackgroungImage

@end
