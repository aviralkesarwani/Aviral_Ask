//
//  Location.h
//  My Asks
//
//  Created by MY PC on 4/24/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface Location : NSObject<CLLocationManagerDelegate>

@property (retain ,nonatomic) CLLocationManager *locationManager;

@end
