//
//  ChatClient.m
//  My Asks
//
//  Created by Mac on 13/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "ChatClient.h"

@implementation ChatClient

+ (ChatClient *) sharedManager
{
    static ChatClient * sharedManager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[ChatClient alloc] init];
//        [sharedManager setUpPubnubClientWithConfiguration];
    });
    return sharedManager;
}

- (void) setUpPubnubClientWithConfiguration {
    PNConfiguration *configuration = [PNConfiguration configurationWithPublishKey:@"pub-c-76b2b26e-5be3-4e7c-b213-1b62f61e9483"
                                                                     subscribeKey:@"sub-c-037418c0-b0f4-11e5-ae71-02ee2ddab7fe"];
    configuration.uuid = [[AppData sharedManager] getUserMobileNumber];
    configuration.presenceHeartbeatValue = 120;
    configuration.presenceHeartbeatInterval = 30;

//    PNConfiguration *configuration = [PNConfiguration configurationWithPublishKey:@"pub-c-97549910-1939-4dc6-a994-654ed689ada2"
//                                                                     subscribeKey:@"sub-c-ab70baba-f672-11e5-a492-02ee2ddab7fe"];
    self.client = [PubNub clientWithConfiguration:configuration];
//    [self.client addListener:self];
//    [self.client subscribeToChannels: @[@"sunil"] withPresence:YES];
}

- (void) checkConnectivityWithOrigin {
    [self.client timeWithCompletion:^(PNTimeResult *result, PNErrorStatus *status) {
        // Check whether request successfully completed or not.
        if (!status.isError) {
            // Handle downloaded server time token using: result.data.timetoken
        }
        // Request processing failed.
        else {
            // Handle tmie token download error. Check 'category' property to find
            // out possible issue because of which request did fail.
            //
            // Request can be resent using: [status retry];
        }
    }];
}

- (void) publishMessageToChannel : (NSString *) channel message : (NSDictionary *) message {
    [self.client publish: message toChannel: channel storeInHistory:YES
          withCompletion:^(PNPublishStatus *status) {
              
              // Check whether request successfully completed or not.
              if (!status.isError) {
                  
                  // Message successfully published to specified channel.
              }
              // Request processing failed.
              else {
                  // Handle message publish error. Check 'category' property to find out possible issue
                  // because of which request did fail.
                  //
                  // Request can be resent using: [status retry];
              }
          }];
}

- (void) checkHereNowForChannel : (NSString *) channel {
    
    [self.client hereNowForChannel: @"my_channel" withVerbosity:PNHereNowUUID
                        completion:^(PNPresenceChannelHereNowResult *result,
                                     PNErrorStatus *status) {
                            
                            // Check whether request successfully completed or not.
                            if (!status.isError) {
                                
                                // Handle downloaded presence information using:
                                //   result.data.uuids - list of uuids.
                                //   result.data.occupancy - total number of active subscribers.
                            }
                            // Request processing failed.
                            else {
                                
                                // Handle presence audit error. Check 'category' property to find
                                // out possible issue because of which request did fail.
                                //
                                // Request can be resent using: [status retry];
                            }
                        }];
}

- (void) getHistoryForChannel : (NSString *) channel {
    
    [self.client historyForChannel: channel start:nil end:nil limit:100
                    withCompletion:^(PNHistoryResult *result, PNErrorStatus *status) {
                        
                        // Check whether request successfully completed or not.
                        if (!status.isError) {
                            
                            // Handle downloaded history using:
                            //   result.data.start - oldest message time stamp in response
                            //   result.data.end - newest message time stamp in response
                            //   result.data.messages - list of messages
                            NSLog(@"%@",result);
                        }
                        // Request processing failed.
                        else {
                            
                            // Handle message history download error. Check 'category' property to find
                            // out possible issue because of which request did fail.
                            //
                            // Request can be resent using: [status retry];
                        }
                    }];
}

- (void) subscribeToChannel : (NSArray *) channelArray {
    [self.client subscribeToChannels:channelArray withPresence:YES];
}

- (void) unsubscribeFromChannel : (NSArray *) channelArray {
    [self.client unsubscribeFromChannels: channelArray withPresence:YES];
}

//#pragma mark - PubNub Listner Methods
//
//- (void)client:(PubNub *)client didReceiveMessage:(PNMessageResult *)message {
//    
//    // Handle new message stored in message.data.message
//    if (message.data.actualChannel) {
//        
//        // Message has been received on channel group stored in
//        // message.data.subscribedChannel
//    }
//    else {
//        
//        // Message has been received on channel stored in
//        // message.data.subscribedChannel
//    }
//    NSLog(@"Received message: %@ on channel %@ at %@", message.data.message,
//          message.data.subscribedChannel, message.data.timetoken);
//    NSDictionary *messageDict = message.data.message;
//    
//    NSLog(@"Sender=%@m Message=%@",[messageDict valueForKey:kSenderName],[messageDict valueForKey:kMessage]);
//}
//
//- (void)client:(PubNub *)client didReceiveStatus:(PNSubscribeStatus *)status {
//    
//    if (status.category == PNUnexpectedDisconnectCategory) {
//        // This event happens when radio / connectivity is lost
//    }
//    
//    else if (status.category == PNConnectedCategory) {
//        
//        // Connect event. You can do stuff like publish, and know you'll get it.
//        // Or just use the connected event to confirm you are subscribed for
//        // UI / internal notifications, etc
//        
//        //        [self.client publish: @"Hello from the PubNub Objective-C SDK" toChannel: @"my_channel"
//        //              withCompletion:^(PNPublishStatus *status) {
//        //
//        //                  // Check whether request successfully completed or not.
//        //                  if (!status.isError) {
//        //
//        //                      // Message successfully published to specified channel.
//        //                  }
//        //                  // Request processing failed.
//        //                  else {
//        //
//        //                      // Handle message publish error. Check 'category' property to find out possible issue
//        //                      // because of which request did fail.
//        //                      //
//        //                      // Request can be resent using: [status retry];
//        //                  }
//        //              }];
//    }
//    else if (status.category == PNReconnectedCategory) {
//        
//        // Happens as part of our regular operation. This event happens when
//        // radio / connectivity is lost, then regained.
//    }
//    else if (status.category == PNDecryptionErrorCategory) {
//        
//        // Handle messsage decryption error. Probably client configured to
//        // encrypt messages and on live data feed it received plain text.
//    }
//}
//
//- (void)client:(PubNub *)client didReceivePresenceEvent:(PNPresenceEventResult *)event {
//    
//    //    // Handle presence event event.data.presenceEvent (one of: join, leave, timeout,
//    //    // state-change).
//    //    if (message.data.actualChannel) {
//    //
//    //        // Presence event has been received on channel group stored in
//    //        // event.data.subscribedChannel
//    //    }
//    //    else {
//    //
//    //        // Presence event has been received on channel stored in
//    //        // event.data.subscribedChannel
//    //    }
//    NSLog(@"Did receive presence event: %@", event.data.presenceEvent);
//}


@end
