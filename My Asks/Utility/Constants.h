//
//  Constants.h
//  My Asks
//
//  Created by Mac on 01/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Constants : NSObject

#define NilCheckOfNSString(x) ([x isKindOfClass:[NSString class]] ? x : @"")

extern NSString* const MABaseURL;
extern NSString* const MAUserURL;
extern NSString* const MACreateUserURL;
extern NSString* const MAVarifyUserURL;
extern NSString* const MAUpdateDeviceTokenURL;
extern NSString* const MAUserAsksURL;
extern NSString* const MACreateAskURL;
extern NSString* const MAUpdateAskURL;
extern NSString* const MASaveAskBackgroungImageURL;
extern NSString* const MAGetChannelHistoryURL;
extern NSString* const MAInsertAsksMessageURL;
extern NSString* const MAUserListsURL;
extern NSString* const MAInsertAskInvitesURL;
extern NSString* const MAAddPlaceToAskURL;

extern NSString* const MAAskByIdURL;

extern NSString* const MASearchPlacesURL;
extern NSString* const MAPlaceDetailsURL;
extern NSString* const MAPlaceFavoriteURL;

extern NSString* const GetMyProfileURL;

extern NSString* const MAInsertUpdateUserContactsURL;
extern NSString* const MAFriendsListURL;
extern NSString* const MAInviteFriendsURL;

extern NSString* const MAGetFriendProfileURL;

extern NSString* const kSenderID;
extern NSString* const kSenderName;
extern NSString* const kMessage;
extern NSString* const kMessageType;
extern NSString* const kMessageDateTime;
extern NSString* const kChannelName;

extern NSString* const MASearchFriendsListOrPlaces;
extern NSString* const MAInsertUpdateUserList;
extern NSString* const MAUpdateUserURL;

extern NSString* const MAUpdateQBChatDialogId;

extern NSString* const MAGetAskByDialog;

// QuickBlox
extern NSString* const MAQBPassword;
extern NSString* const MAQBUserName;

extern NSString* const MAUserID;
extern NSString* const MAUserName;
extern NSString* const MAUserEmail;
extern NSString* const MAUserMobileNumber;
extern NSString* const MAUserCoutryCode;
extern NSString* const MAUserDeviceToken;
extern NSString* const MAUserDeviceName;

extern NSString* const MAUserDialingCode;

extern NSString* const MAUserProfilePicture;
extern NSString* const MAUserIsVerified;

extern NSString* const MAAddPlaceToUserListURL;
extern NSString* const MAListDetailsById;

extern NSString* const MASendAskOfflineNotification;
extern NSString* const MAUserDeviceToken;
@end
