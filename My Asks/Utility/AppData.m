//
//  AppData.m
//  My Asks
//
//  Created by Mac on 01/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "AppData.h"

@implementation AppData

+ (AppData *) sharedManager
{
    static AppData * sharedManager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[AppData alloc] init];
    });
    
    return sharedManager;
}


- (NSString *)encodeToBase64String:(UIImage *)image {
    return [UIImagePNGRepresentation(image) base64EncodedStringWithOptions:NSDataBase64EncodingEndLineWithCarriageReturn];
}

- (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData {
    
    NSData *data = [[NSData alloc]initWithBase64EncodedString:strEncodeData options:NSDataBase64DecodingIgnoreUnknownCharacters];
    return [UIImage imageWithData:data];
}

-(UIImage *)compressImage:(UIImage *)image{
    
    float actualHeight = image.size.height;
    float actualWidth = image.size.width;
    float maxHeight = 600.0;
    float maxWidth = 800.0;
    float imgRatio = actualWidth/actualHeight;
    float maxRatio = maxWidth/maxHeight;
    float compressionQuality = 0.5;//50 percent compression
    
    if (actualHeight > maxHeight || actualWidth > maxWidth){
        if(imgRatio < maxRatio){
            //adjust width according to maxHeight
            imgRatio = maxHeight / actualHeight;
            actualWidth = imgRatio * actualWidth;
            actualHeight = maxHeight;
        }
        else if(imgRatio > maxRatio){
            //adjust height according to maxWidth
            imgRatio = maxWidth / actualWidth;
            actualHeight = imgRatio * actualHeight;
            actualWidth = maxWidth;
        }
        else{
            actualHeight = maxHeight;
            actualWidth = maxWidth;
        }
    }
    
    CGRect rect = CGRectMake(0.0, 0.0, actualWidth, actualHeight);
    UIGraphicsBeginImageContext(rect.size);
    [image drawInRect:rect];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    NSData *imageData = UIImageJPEGRepresentation(img, compressionQuality);
    UIGraphicsEndImageContext();
    
    return [UIImage imageWithData:imageData];
}


- (UIImage *)imageFromView:(UIView *) view
{
    if ([[UIScreen mainScreen] respondsToSelector:@selector(scale)]) {
        UIGraphicsBeginImageContextWithOptions(view.frame.size, NO, [[UIScreen mainScreen] scale]);
    } else {
        UIGraphicsBeginImageContext(view.frame.size);
    }
    [view.layer renderInContext: UIGraphicsGetCurrentContext()];
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}

- (BOOL) validateEmail: (NSString *) candidate {
    
    NSString *emailRegex =
    @"(?:[a-z0-9!#$%\\&'*+/=?\\^_`{|}~-]+(?:\\.[a-z0-9!#$%\\&'*+/=?\\^_`{|}"
    @"~-]+)*|\"(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21\\x23-\\x5b\\x5d-\\"
    @"x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])*\")@(?:(?:[a-z0-9](?:[a-"
    @"z0-9-]*[a-z0-9])?\\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\\[(?:(?:25[0-5"
    @"]|2[0-4][0-9]|[01]?[0-9][0-9]?)\\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-"
    @"9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\\x01-\\x08\\x0b\\x0c\\x0e-\\x1f\\x21"
    @"-\\x5a\\x53-\\x7f]|\\\\[\\x01-\\x09\\x0b\\x0c\\x0e-\\x7f])+)\\])";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES[c] %@", emailRegex];
    
    return [emailTest evaluateWithObject:candidate];
}

- (NSDate *) getLocalDateFrmGMT : (NSString *) createdDateString {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    NSDate *sourceDate = [formatter dateFromString:createdDateString];
    NSLog(@"%@",sourceDate);
    
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
    dateFormat.timeZone = [NSTimeZone systemTimeZone];
    [dateFormat setDateFormat:@"yyyy/MM/dd hh:mm a"];
    NSString* localTime = [dateFormat stringFromDate:sourceDate];
    
    NSDate *localDate = [dateFormat dateFromString:localTime];
    return localDate;
//    NSLog(@"localTime:%@", localTime);
//    return localTime;
}

- (NSString *) getYesterdayOrDateStringFromDate : (NSDate *) date {
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    
    NSUInteger unitFlags = NSCalendarUnitMonth | NSCalendarUnitDay;
    
    NSDateComponents *components = [gregorian components:unitFlags fromDate:date toDate:[NSDate date] options:0];
    NSInteger days = [components day];
    
    NSString *tmpValue = nil;
    
    if ( days == 0 )
        tmpValue = @"Today";
    else if(days == 1)
            tmpValue = @"Yesterday";
    else{
        
        NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
        dateFormat.timeZone = [NSTimeZone systemTimeZone];
        [dateFormat setDateFormat:@"yyyy/MM/dd"];//hh:mm a

        tmpValue = [dateFormat stringFromDate:date];
    }
    
    return tmpValue;
}


- (void) saveUserInformation : (NSDictionary *) userInfo {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:[userInfo valueForKey:MAUserID] forKey:MAUserID];
    [userDefaults setValue:[userInfo valueForKey:MAUserName] forKey:MAUserName];
    [userDefaults setValue:[userInfo valueForKey:MAUserEmail] forKey:MAUserEmail];
    [userDefaults setValue:[userInfo valueForKey:MAUserMobileNumber] forKey:MAUserMobileNumber];
    [userDefaults setValue:[userInfo valueForKey:MAUserCoutryCode] forKey:MAUserCoutryCode];
    
    [userDefaults synchronize];
}



#pragma mark -

// QuichBlox

- (NSString *) qbPassword {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ([userDefaults valueForKey:MAQBPassword] != nil) {
        
        return [userDefaults valueForKey:MAQBPassword];
    }
    return @"";
}
- (void) setQbPassword : (NSString *) qbPassword {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:qbPassword forKey:MAQBPassword];
    [userDefaults synchronize];

}
- (NSString *) qbUserName {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ([userDefaults valueForKey:MAQBUserName] != nil) {
        return [userDefaults valueForKey:MAQBUserName];
    }
    return @"";
}
- (void) setQbUserName : (NSString *) qbUserName{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:qbUserName forKey:MAQBUserName];
    [userDefaults synchronize];
}



- (NSString *) getCurrentUserID {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ([userDefaults valueForKey:MAUserID] != nil) {
        return [userDefaults valueForKey:MAUserID];
    }
    return @"";
}

- (void) setCurrentUserID : (NSString *) userID {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:userID forKey:MAUserID];
    [userDefaults synchronize];

}


-(void)getCurrentCountryCode:(NSString *)countryCode{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:countryCode forKey:MAUserCoutryCode];
    [userDefaults synchronize];

}

-(NSString *)getCurrentCountryCode{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ([userDefaults valueForKey:MAUserCoutryCode] != nil) {
        return [userDefaults valueForKey:MAUserCoutryCode];
    }
    return @"";
}

-(void)setCurrentDialingCode:(NSString *)countryCode{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:countryCode forKey:MAUserDialingCode];
    [userDefaults synchronize];

}

-(NSString *)getCurrentDialingCode{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ([userDefaults valueForKey:MAUserDialingCode] != nil) {
        return [userDefaults valueForKey:MAUserDialingCode];
    }
    return @"";
}

- (NSString *) getUserMobileNumber {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ([userDefaults valueForKey:MAUserMobileNumber] != nil) {
        
        NSString *mobileNumber = [NSString stringWithFormat:@"%@%@",[userDefaults valueForKey:MAUserCoutryCode],[userDefaults valueForKey:MAUserMobileNumber]];
        
        return mobileNumber;
    }
    return @"";
}

- (void) saveUserProfileImage : (UIImage *) image {
    [[NSUserDefaults standardUserDefaults] setObject:UIImagePNGRepresentation(image) forKey:MAUserProfilePicture];
}

- (UIImage *) getUserProfileImage {
    if ([[NSUserDefaults standardUserDefaults] objectForKey:MAUserProfilePicture] != nil) {
        NSData* imageData = [[NSUserDefaults standardUserDefaults] objectForKey:MAUserProfilePicture];
        UIImage* image = [UIImage imageWithData:imageData];
        return image;
    }
    else {
        UIImage* image = [UIImage imageNamed:@"Photo"];
        return image;
    }
}

- (NSString *) getDeviceName {
    
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if ([userDefaults valueForKey:MAUserDeviceName] != nil) {
        return [userDefaults valueForKey:MAUserDeviceName];
    }
    return @"";
}
- (void) setDeviceName: (NSString*)deviceName
{
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    [userDefaults setValue:deviceName forKey:MAUserDeviceName];
    [userDefaults synchronize];
}

#pragma mark - Class method

+ (NSString *) getLocalDateStringFrmGMT : (NSString *) createdDateString {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    NSDate *sourceDate = [formatter dateFromString:createdDateString];
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
    dateFormat.timeZone = [NSTimeZone systemTimeZone];
    [dateFormat setDateFormat:@"yyyy/MM/dd hh:mm a"];
    NSString* localTime = [dateFormat stringFromDate:sourceDate];
    
    return localTime;
}

+ (void) checkDeviceToken : (NSString *) deviceToken {
    NSUserDefaults *userDefaults = [NSUserDefaults standardUserDefaults];
    if (![[[AppData sharedManager] getCurrentUserID]  isEqual: @""]) {
        if ([userDefaults valueForKey:MAUserDeviceToken] == nil || [[userDefaults valueForKey:MAUserDeviceToken]  isEqual: @""]) {
            [userDefaults setValue:deviceToken forKey:MAUserDeviceToken];
            
            NSString *paramString = [NSString stringWithFormat:@"userId=%@&deviceToken=%@",[[AppData sharedManager] getCurrentUserID],deviceToken];
            
            [[APIClient sharedManager] updateDeviceToken:paramString onCompletion:^(NSDictionary *resultData, bool success) {
                
            }];
        }
        else if ([userDefaults valueForKey:MAUserDeviceToken] != deviceToken) {
            [userDefaults setValue:deviceToken forKey:MAUserDeviceToken];
            
            NSString *paramString = [NSString stringWithFormat:@"userId=%@&deviceToken=%@",[[AppData sharedManager] getCurrentUserID],deviceToken];
            
            [[APIClient sharedManager] updateDeviceToken:paramString onCompletion:^(NSDictionary *resultData, bool success) {
                
            }];

        }
    }
}

+ (void) startNetworkIndicator {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = true;
}

+ (void) stopNetworkIndicator {
    [UIApplication sharedApplication].networkActivityIndicatorVisible = false;
}

+(void)configurePushNotifications{
    
    if ([[[UIDevice currentDevice] systemVersion]floatValue] >=8.0) {
        
        [[UIApplication sharedApplication]registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        
        [[UIApplication sharedApplication]registerForRemoteNotifications];
    }
    else
    {
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIRemoteNotificationTypeNewsstandContentAvailability|UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
    }
}

@end
