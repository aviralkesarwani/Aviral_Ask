//
//  APIClient.m
//  My Asks
//
//  Created by Mac on 01/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "APIClient.h"

@implementation APIClient

+ (APIClient *) sharedManager
{
    static APIClient * sharedManager = nil;
    
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedManager = [[APIClient alloc] init];
    });
    
    return sharedManager;
}

#pragma mark - Sign Up


- (void) CreateUser : (NSDictionary *) param onCompletion:(void (^)(NSDictionary * resultData, bool success) )result {
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@",MABaseURL,MAUserURL,MACreateUserURL];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@",urlString] parameters:nil error:nil];
    
    req.timeoutInterval= [[[NSUserDefaults standardUserDefaults] valueForKey:@"timeoutInterval"] longValue];
    [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //        NSLog(@"Data is not nil");
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:param options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        
        if (!error) {
            NSLog(@"Reply JSON: %@", responseObject);
            result(responseObject,true);
        } else {
            
            
            
            NSLog(@"Error: %@, %@, %@", error, response, responseObject);
            result(error.userInfo,false);
            
            
        }
    }] resume];
}

#pragma mark - Varify User

- (void) VerifyUser : (NSString *) param onCompletion:(void (^)(NSDictionary * resultData, bool success) )result {
    
    NSString *userID = [AppData sharedManager].userID;
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@?userId=%@&code=%@",MABaseURL,MAUserURL,MAVarifyUserURL,userID,param];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager GET:urlString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        result(responseObject,true);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        result(@{@"error":error.userInfo},false);
    }];
}

#pragma mark - Save Device Token

- (void) updateDeviceToken : (NSString *) param onCompletion:(void (^)(NSDictionary * resultData, bool success) )result {
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@?%@",MABaseURL,MAUserURL,MAUpdateDeviceTokenURL,param];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager  POST:urlString parameters:@{} progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@",responseObject);
        result(responseObject,true);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",error.userInfo);
        result(@{@"error":error.userInfo},false);
    }];
}

#pragma mark - SearchPlaces

- (void) SearchPlaces : (NSDictionary *) param onCompletion:(void (^)(NSDictionary * resultData, bool success) )result {
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@",MABaseURL,MAUserURL,MASearchPlacesURL];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager  POST:urlString parameters:param progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@",responseObject);
        result(responseObject,true);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",error.userInfo);
        result(@{@"error":error.userInfo},false);
    }];
}


-(void)PlaceDetails:(NSDictionary *)param onCompletion:(void (^)(NSDictionary *, bool))result{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@",MABaseURL,MAUserURL,MAPlaceDetailsURL];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager  POST:urlString parameters:param progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@",responseObject);
        result(responseObject,true);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",error.userInfo);
        result(@{@"error":error.userInfo},false);
    }];
}

-(void)GetPlacesFilter:(NSString *)param indexOf : (NSInteger) index onCompletion:(void (^)(NSDictionary *, bool,NSInteger))result{
    
    //NSString *urlString = [NSString stringWithFormat:@"%@%@%@",MABaseURL,MAUserURL,MAPlaceDetailsURL];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager  POST:param parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@",responseObject);
        result(responseObject,true,index);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",error.userInfo);
        result(@{@"error":error.userInfo},false,100);
    }];
}

-(void)PlaceFavorite:(NSDictionary *)param onCompletion:(void (^)(NSDictionary *, bool))result{
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@",MABaseURL,MAUserURL,MAPlaceFavoriteURL];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager GET:urlString parameters:param progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@",responseObject);
        result(responseObject,true);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",error.userInfo);
        result(@{@"error":error.userInfo},false);
    }];
}


#pragma mark - InsertUpdateUserContacts


-(void) InsertUpdateUserContacts : (NSDictionary *) param onCompletion:(void (^)(NSDictionary * resultData, bool success) )result {
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@",MABaseURL,MAUserURL,MAInsertUpdateUserContactsURL];
    

    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@",urlString] parameters:nil error:nil];
    
    req.timeoutInterval= [[[NSUserDefaults standardUserDefaults] valueForKey:@"timeoutInterval"] longValue];
    [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //        NSLog(@"Data is not nil");
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:param options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        
        if (!error) {
            NSLog(@"Reply JSON: %@", responseObject);
            result(responseObject,true);
        } else {
            NSLog(@"Error: %@, %@, %@", error, response, responseObject);
            result(error.userInfo,false);
        }
    }] resume];
}

#pragma mark - FriendList

- (void) FriendsList : (NSString *) param onCompletion:(void (^)(NSDictionary * resultData, bool success) )result {
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@?userId=%@",MABaseURL,MAUserURL,MAFriendsListURL,param];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager GET:urlString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        result(responseObject,true);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        result(@{@"error":error.userInfo},false);
    }];
}

#pragma mark - InviteFriends

- (void) InviteFriends : (NSString *) param onCompletion:(void (^)(NSDictionary * resultData, bool success) )result {
    
    NSString *userID = [[AppData sharedManager]getCurrentUserID];
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@?contacts=%@&userId=%@",MABaseURL,MAUserURL,MAInviteFriendsURL,param,userID];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager  POST:urlString parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@",responseObject);
        result(responseObject,true);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",error.userInfo);
        result(@{@"error":error.userInfo},false);
    }];
    
}

#pragma mark - Ask

- (void) createAsk : (NSDictionary *) param onCompletion:(void (^)(NSDictionary* resultData, bool success) )result {
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@",MABaseURL,MAUserURL,MACreateAskURL];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@",urlString] parameters:nil error:nil];
    
    req.timeoutInterval= [[[NSUserDefaults standardUserDefaults] valueForKey:@"timeoutInterval"] longValue];
    [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
//        NSLog(@"Data is not nil");
        NSData *jsonData = [NSJSONSerialization dataWithJSONObject:param options:0 error:nil];
        NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
        

    [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        
        if (!error) {
            NSLog(@"Reply JSON: %@", responseObject);
            result(responseObject,true);
        } else {
            NSLog(@"Error: %@, %@, %@", error, response, responseObject);
            result(error.userInfo,false);
        }
    }] resume];
}

- (void) updateAsk : (NSDictionary *) param onCompletion:(void (^)(NSDictionary* resultData, bool success) )result {
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@",MABaseURL,MAUserURL,MAUpdateAskURL];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"PUT" URLString:[NSString stringWithFormat:@"%@",urlString] parameters:nil error:nil];
    
    req.timeoutInterval= [[[NSUserDefaults standardUserDefaults] valueForKey:@"timeoutInterval"] longValue];
    [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //        NSLog(@"Data is not nil");
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:param options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        
        if (!error) {
            NSLog(@"Reply JSON: %@", responseObject);
            result(responseObject,true);
        } else {
            NSLog(@"Error: %@, %@, %@", error, response, responseObject);
            result(error.userInfo,false);
        }
    }] resume];
}

- (void) saveASKBackgoundImage : (UIImage *) image onCompletion:(void (^)(NSDictionary* resultData, bool success) )result {

    NSString *urlString = [NSString stringWithFormat:@"%@%@%@",MABaseURL,MAUserURL,MASaveAskBackgroungImageURL];
    
    NSData *data = UIImageJPEGRepresentation(image, 0.8);
    
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *documentsDirectory = [paths objectAtIndex:0];
    
    NSString *imagePath =[documentsDirectory stringByAppendingPathComponent:[NSString stringWithFormat:@"%@.png",@"signature1"]];
    
    NSLog((@"pre writing to file"));
    if (![data writeToFile:imagePath atomically:NO])
    {
        NSLog(@"Failed to cache image data to disk");
    }
    else
    {
        NSLog(@"the cachedImagedPath is %@",imagePath);
    }
    
    NSMutableURLRequest *request = [[AFHTTPRequestSerializer serializer] multipartFormRequestWithMethod:@"POST" URLString:urlString parameters:nil constructingBodyWithBlock:^(id<AFMultipartFormData>  _Nonnull formData) {
        [formData appendPartWithFileData:data name:@"" fileName:@"BackgroundImage.jpg" mimeType:@"image/jpeg"];
        [formData appendPartWithFileURL:[NSURL fileURLWithPath:imagePath] name:@"" fileName:@"filename.jpg" mimeType:@"image/jpeg" error:nil];
    } error:nil];
    
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSURLSessionUploadTask *uploadTask;
    uploadTask = [manager
                  uploadTaskWithStreamedRequest:request
                  progress:^(NSProgress * _Nonnull uploadProgress) {
                      // This is not called back on the main queue.
                      // You are responsible for dispatching to the main queue for UI updates
                      dispatch_async(dispatch_get_main_queue(), ^{
                          //Update the progress view
                          //                          [progressView setProgress:uploadProgress.fractionCompleted];
                      });
                  }
                  completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
                      if (error) {
                          NSLog(@"Error: %@", error);
                      } else {
                          NSLog(@"%@",responseObject);
                          NSDictionary *responseDict = responseObject;
                          if ([[responseDict valueForKey:@"Success"] integerValue] == 1) {
                              if (![[responseDict valueForKey:@"Request_ID"]  isEqual: @""]) {
                                  result(responseDict,true);
                              }
                              else {
                                  result(@{},false);
                              }
                          }
                          else {
                              result(@{},false);
                          }
                      }
                  }];
    
    [uploadTask resume];
}


- (void) getUserAsks : (NSString *) userID onCompletion:(void (^)(NSArray * resultData, bool success) )result {
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@?userId=%@",MABaseURL,MAUserURL,MAUserAsksURL,userID];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];

    [manager GET:urlString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        result(responseObject,true);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        result(@[error.localizedDescription],false);
    }];

}

- (void) getAskById : (NSString *) askId userId : (NSString *) userId onCompletion:(void (^)(NSDictionary * resultData, bool success) )result {
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@?askId=%@&UserId=%@",MABaseURL,MAUserURL,MAAskByIdURL,askId,userId];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager GET:urlString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        result(responseObject,true);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        result(@{@"error":error.localizedDescription},false);
    }];
    
}

- (void) addPlaceToAsk : (NSArray *) param onCompletion:(void (^)(NSDictionary* resultData, bool success) )result {
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@",MABaseURL,MAUserURL,MAAddPlaceToAskURL];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@",urlString] parameters:nil error:nil];
    
    req.timeoutInterval= [[[NSUserDefaults standardUserDefaults] valueForKey:@"timeoutInterval"] longValue];
    [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //        NSLog(@"Data is not nil");
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:param options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        
        if (!error) {
            NSLog(@"Reply JSON: %@", responseObject);
            result(responseObject,true);
        } else {
            NSLog(@"Error: %@, %@, %@", error, response, responseObject);
            result(error.userInfo,false);
        }
    }] resume];
}



#pragma mark - Chat

- (void) getChannelHistory : (NSString *) channelName onCompletion:(void (^)(NSArray * resultData, bool success) )result {
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@?channelName=%@",MABaseURL,MAUserURL,MAGetChannelHistoryURL,channelName];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    [manager GET:urlString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        result(responseObject,true);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        result(@[error.localizedDescription],false);
    }];
}

- (void) insertAskMessage : (NSDictionary *) param onCompletion:(void (^)(NSArray * resultData, bool success) )result {
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@",MABaseURL,MAUserURL,MAInsertAsksMessageURL];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager  POST:urlString parameters:param progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@",responseObject);
        
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",error.userInfo);
    }];
}


//http://rexlife.azurewebsites.net/api/User/SendAskOfflineNotification?askId=23&message=919900062090: Hey, this is test msg from server&contacts=919900062090




// SendAskOfflineNotification
- (void) sendAskOfflineNotification : (NSDictionary *) param onCompletion:(void (^)(NSDictionary * resultData, bool success) )result {
    
    NSString *askId = [param valueForKey:@"askId"];
    NSString *message = [param valueForKey:@"message"];
    NSString *contacts = [param valueForKey:@"contacts"];
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@?askId=%@&message=%@&contacts=%@",MABaseURL,MAUserURL,MASendAskOfflineNotification,askId,message,contacts];
    
    NSString* encodedUrl = [urlString stringByAddingPercentEscapesUsingEncoding:
                            NSUTF8StringEncoding];

    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager GET:encodedUrl parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        result(responseObject,true);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        result(@{@"error":error.localizedDescription},false);
    }];
}

- (void) insertAskInvite : (NSDictionary *) param onCompletion:(void (^)(NSDictionary* resultData, bool success) )result {
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@",MABaseURL,MAUserURL,MAInsertAskInvitesURL];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@",urlString] parameters:nil error:nil];
    
    req.timeoutInterval= [[[NSUserDefaults standardUserDefaults] valueForKey:@"timeoutInterval"] longValue];
    [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //        NSLog(@"Data is not nil");
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:param options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    
    [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        if (!error) {
            NSLog(@"Reply JSON: %@", responseObject);
            result(@{@"":@""},true);
        } else {
            NSLog(@"Error: %@, %@, %@", error, response, responseObject);
            result(error.userInfo,false);
        }
    }] resume];
}


#pragma mark - Lists

- (void) getUserLists : (NSString *) userID onCompletion:(void (^)(NSArray * resultData, bool success) )result {
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@?userId=%@",MABaseURL,MAUserURL,MAUserListsURL,userID];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager GET:urlString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        result(responseObject,true);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        result(@[error.localizedDescription],false);
    }];
}

- (void) getPlaces : (NSDictionary *) param onCompletion:(void (^)(NSDictionary * resultData, bool success) )result {
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@",MABaseURL,MAUserURL,MASearchPlacesURL];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager  POST:urlString parameters:param progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@",responseObject);
        result(responseObject,true);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",error.userInfo);
        result(@{@"error":error.userInfo},false);
    }];
}

- (void) getListDetailsById : (NSDictionary *) param onCompletion:(void (^)(NSDictionary * resultData, bool success) )result {
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@",MABaseURL,MAUserURL,MAListDetailsById];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager  POST:urlString parameters:param progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@",responseObject);
        result(responseObject,true);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",error.userInfo);
        result(@{@"error":error.userInfo},false);
    }];
}

- (void) getSearchPlaces : (NSDictionary *) param onCompletion:(void (^)(NSArray * resultData, bool success) )result {
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@",MABaseURL,MAUserURL,MASearchFriendsListOrPlaces];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager  POST:urlString parameters:param progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@",responseObject);
        result(responseObject,true);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",error.userInfo);
        result(@[error.userInfo],false);
    }];
}

- (void) insertUpdateUserList : (NSDictionary *) param onCompletion:(void (^)(NSDictionary * resultData, bool success) )result {
    
//    NSString *urlString = [NSString stringWithFormat:@"%@%@%@",MABaseURL,MAUserURL,MAInsertUpdateUserList];
//    
//    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//    
//    [manager  POST:urlString parameters:param progress:^(NSProgress * _Nonnull uploadProgress) {
//        
//    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//        NSLog(@"%@",responseObject);
//        result(responseObject,true);
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        NSLog(@"%@",error.userInfo);
//        result(@{@"error":error.userInfo},false);
//    }];
    
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@",MABaseURL,MAUserURL,MAInsertUpdateUserList];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@",urlString] parameters:nil error:nil];
    
    req.timeoutInterval= [[[NSUserDefaults standardUserDefaults] valueForKey:@"timeoutInterval"] longValue];
    [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //        NSLog(@"Data is not nil");
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:param options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        
        if (!error) {
            NSLog(@"Reply JSON: %@", responseObject);
            result(responseObject,true);
        } else {
            NSLog(@"Error: %@, %@, %@", error, response, responseObject);
            result(error.userInfo,false);
        }
    }] resume];
}

#pragma mark - AddPlaceToUserList

- (void) AddPlaceToUserList : (NSMutableDictionary *) param onCompletion:(void (^)(NSDictionary * resultData, bool success) )result {
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@",MABaseURL,MAUserURL,MAAddPlaceToUserListURL];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager  POST:urlString parameters:param progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@",responseObject);
        result(responseObject,true);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",error.userInfo);
        result(@{@"error":error.userInfo},false);
    }];
}
#pragma mark - Settings

- (void) getMyProfile : (NSString *) userID onCompletion:(void (^)(NSDictionary * resultData, bool success) )result {
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@?userId=%@",MABaseURL,MAUserURL,GetMyProfileURL,userID];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager GET:urlString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
//        NSLog(@"JSON: %@", responseObject);
        result(responseObject,true);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        result(@{@"error":error.localizedDescription},false);
    }];
}

- (void) updateUserProfile : (NSDictionary *) param onCompletion:(void (^)(NSDictionary * resultData, bool success) )result {
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@",MABaseURL,MAUserURL,MAUpdateUserURL];
    
    AFURLSessionManager *manager = [[AFURLSessionManager alloc] initWithSessionConfiguration:[NSURLSessionConfiguration defaultSessionConfiguration]];
    
    NSMutableURLRequest *req = [[AFJSONRequestSerializer serializer] requestWithMethod:@"POST" URLString:[NSString stringWithFormat:@"%@",urlString] parameters:nil error:nil];
    
    req.timeoutInterval= [[[NSUserDefaults standardUserDefaults] valueForKey:@"timeoutInterval"] longValue];
    [req setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [req setValue:@"application/json" forHTTPHeaderField:@"Accept"];
    //        NSLog(@"Data is not nil");
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:param options:0 error:nil];
    NSString *jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    [req setHTTPBody:[jsonString dataUsingEncoding:NSUTF8StringEncoding]];
    
    [[manager dataTaskWithRequest:req completionHandler:^(NSURLResponse * _Nonnull response, id  _Nullable responseObject, NSError * _Nullable error) {
        if (!error) {
            NSLog(@"Reply JSON: %@", responseObject);
            result(responseObject,true);
        } else {
            NSLog(@"Error: %@, %@, %@", error, response, responseObject);
            result(error.userInfo,false);
        }
    }] resume];
}


#pragma mark - GetFriendProfile

- (void) GetFriendProfile : (NSString *) friendID onCompletion:(void (^)(NSDictionary * resultData, bool success) )result {
    
    NSString *userID = [[AppData sharedManager]getCurrentUserID];
    
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@?userId=%@&friendId=%@",MABaseURL,MAUserURL,MAGetFriendProfileURL,userID,friendID];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager GET:urlString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        //        NSLog(@"JSON: %@", responseObject);
        result(responseObject,true);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        result(@{@"error":error.localizedDescription},false);
    }];
}

#pragma mark - UpdateQBChatDialogId

- (void) updateQBChatDialogId : (NSDictionary *) param onCompletion:(void (^)(NSDictionary * resultData, bool success) )result {
    
//http://rexlife.azurewebsites.net/api/User/UpdateQBChatDialogId?askId=53&dialog=987654321

    
    NSString *urlString = [NSString stringWithFormat:@"%@%@%@?askId=%@&dialog=%@",MABaseURL,MAUserURL,MAUpdateQBChatDialogId,param[@"askId"],param[@"dialog"]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
    [manager  POST:urlString parameters:nil progress:^(NSProgress * _Nonnull uploadProgress) {
        
    } success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@",responseObject);
        result(responseObject,true);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@",error.userInfo);
        result(@{@"error":error.localizedDescription},false);
    }];
}

- (void) getAskByDialog : (NSDictionary *) param onCompletion:(void (^)(NSDictionary * resultData, bool success) )result {

    NSString *urlString = [NSString stringWithFormat:@"%@%@%@?dialog=%@&UserId=%@",MABaseURL,MAUserURL,MAGetAskByDialog,param[@"dialog"],param[@"UserId"]];
    
    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
    
//    http://rexlife.azurewebsites.net/api/User/GetAskByDialog?UserId=15&dialog=57c4911ea0eb479ae9000023,\
    
    [manager GET:urlString parameters:nil progress:nil success:^(NSURLSessionTask *task, id responseObject) {
        NSLog(@"JSON: %@", responseObject);
        
        result(responseObject,true);
    } failure:^(NSURLSessionTask *operation, NSError *error) {
        NSLog(@"Error: %@", error);
        result(@{@"error":error.localizedDescription},false);
    }];
    
}

@end
