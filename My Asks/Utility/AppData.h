//
//  AppData.h
//  My Asks
//
//  Created by Mac on 01/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "MAUserAsksModel.h"
#import "Constants.h"
#import "APIClient.h"

@interface AppData : NSObject

+ (AppData *) sharedManager;

@property(strong,nonatomic)NSString *userID;
@property(strong,nonatomic)NSString *userName;
@property(strong,nonatomic)NSString *userEmail;
@property(strong,nonatomic)NSString *userCountryCode;
@property(strong,nonatomic)NSString *userMobileNumber;
@property(strong,nonatomic)NSString *userProfilePicture;

// QuickBlox
@property(strong,nonatomic)NSString *qbPassword;
@property(strong,nonatomic)NSString *qbUserName;

@property(strong,nonatomic)NSString *latitude;
@property(strong,nonatomic)NSString *longitude;
@property(strong,nonatomic)NSString *deviceToken;

@property (strong, nonatomic) MAUserAsksModel *userAskModelForSideMenu;

@property (strong, nonatomic) NSMutableArray *invitedFriendsGlobalArray;

//QuickBlox
- (NSString *) qbPassword;
- (void) setQbPassword : (NSString *) qbPassword;
- (NSString *) qbUserName;
- (void) setQbUserName : (NSString *) qbUserName;


- (NSString *) getDeviceName;
- (void) setDeviceName : (NSString*)deviceName;
- (NSString *) getCurrentUserID;
- (void) setCurrentUserID : (NSString *) userID;
- (void) saveUserInformation : (NSDictionary *) userInfo;
- (NSString *) getUserMobileNumber;
- (void) saveUserProfileImage : (UIImage *) image;
- (UIImage *) getUserProfileImage;
- (NSString *)encodeToBase64String:(UIImage *)image;
- (UIImage *)decodeBase64ToImage:(NSString *)strEncodeData;
- (UIImage *)compressImage:(UIImage *)image;
- (UIImage *)imageFromView:(UIView *) view;
- (BOOL) validateEmail: (NSString *) candidate;
- (NSDate *) getLocalDateFrmGMT : (NSString *) createdDateString;
- (NSString *)getCurrentDialingCode;
- (void) setCurrentDialingCode : (NSString *) countryCode;
- (NSString *)getCurrentCountryCode;
- (void) setCurrentCountryCode : (NSString *) countryCode;

- (NSString *) getYesterdayOrDateStringFromDate : (NSDate *) date;

+ (NSString *) getLocalDateStringFrmGMT : (NSString *) createdDateString;
+ (void) checkDeviceToken : (NSString *) deviceToken;
+ (void) startNetworkIndicator;
+ (void) stopNetworkIndicator;
+ (void) configurePushNotifications;

@end

//
//NSString* const MAUserID = @"UserID";
//NSString* const MAUserName = @"UserName";
//NSString* const MAUserEmail = @"UserEmail";
//NSString* const MAUserMobileNumber = @"UserMobileNumber";
//NSString* const MAUserProfilePicture = @"UserProfilePicture";
//NSString* const MAUserIsVerified = @"UserIsVerified";
