//
//  ChatClient.h
//  My Asks
//
//  Created by Mac on 13/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <PubNub/PubNub.h>
#import "Constants.h"
#import "AppData.h"

@interface ChatClient : NSObject //<PNObjectEventListener>

+ (ChatClient *) sharedManager;

@property (nonatomic) PubNub *client;

- (void) setUpPubnubClientWithConfiguration;

- (void) checkConnectivityWithOrigin;

//- (void) subscribeToChannel;

- (void) publishMessageToChannel : (NSString *) channel message : (NSDictionary *) message;
- (void) checkHereNowForChannel : (NSString *) channel;
- (void) getHistoryForChannel : (NSString *) channel;
- (void) subscribeToChannel : (NSArray *) channelArray;
- (void) unsubscribeFromChannel : (NSArray *) channelArray;

@end
