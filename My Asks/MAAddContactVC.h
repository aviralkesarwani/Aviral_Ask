//
//  MAAddContactVC.h
//  My Asks
//
//  Created by Rahul on 9/14/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MAFriendListModel.h"


@protocol MAAddContactVCDelegate <NSObject>
- (void)addedContact:(MAFriendListModel*)friendListModel;
@end

@interface MAAddContactVC : UIViewController

@property (nonatomic, weak) id<MAAddContactVCDelegate> delegate;


@end
