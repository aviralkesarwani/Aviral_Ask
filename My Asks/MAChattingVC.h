//
//  MAChattingVC.h
//  My Asks
//
//  Created by _ on 8/30/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "JSQMessagesViewController.h"
#import "JSQMessages.h"
#import <QuickBlox/Quickblox.h>

#import "MAChatScreenVC.h"

@interface MAChattingVC : JSQMessagesViewController

@property (strong, nonatomic) QBChatDialog *selectedChatDialod;
@property (weak) MAChatScreenVC *parentVC;

@end
