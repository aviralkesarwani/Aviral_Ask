//
//  AppDelegate.h
//  My Asks
//
//  Created by artis on 30/03/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>
#import <PubNub/PubNub.h>
#import <IQKeyboardManager.h>
#import <GoogleMaps/GoogleMaps.h>
#import <QuartzCore/QuartzCore.h>
#import <PSLocation/PSlocation.h>

#import "MARightSideMenuVC.h"
#import "MFSideMenuContainerViewController.h"
#import "ChatClient.h"
#import "AppData.h"
#import "MAChatScreenVC.h"
#import "MAUserListsModel.h"
#import "MAPlacesDetailVC.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate , UITabBarControllerDelegate, CLLocationManagerDelegate>

{
    NSMutableArray *placesSearchArray;
}


@property (strong, nonatomic)UIWindow *window;
@property (strong, nonatomic) NSMutableArray *userPlacesArray;
@property (strong, nonatomic) MAUserListsModel *userlistData;
@property (nonatomic, retain)UITabBarController *tabBarController;
@property (strong, nonatomic) MFSideMenuContainerViewController *container;
@property (strong, nonatomic) UINavigationController *mainNavigationCntl;
@property (nonatomic, retain)UIImageView *imgV;

@property (retain ,nonatomic) CLLocationManager *locationManager;


- (void) slideMenuOpenWithPan : (BOOL ) flag;
- (void) openRightSlideDrawer;
- (void) changeRootControllOfNavigation:(UIViewController*)vc;
@end

