//
//  MAPlacesSearchVC.m
//  My Asks
//
//  Created by Mac on 20/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "MAPlacesSearchVC.h"

@interface MAPlacesSearchVC ()

@end

@implementation MAPlacesSearchVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.tblPlacesSearch.delegate = self;
    self.tblPlacesSearch.dataSource = self;
    
    self.placeSearchTextField.placeSearchDelegate                 = self;
    self.placeSearchTextField.strApiKey                           = @"AIzaSyCyL6A9-IRxfUqept-WCEOC3Za2v_-27zI";
    self.placeSearchTextField.superViewOfList                     = self.autoComplete;  // View, on which Autocompletion list should be appeared.
    self.placeSearchTextField.autoCompleteShouldHideOnSelection   = YES;
    self.placeSearchTextField.maximumNumberOfAutoCompleteRows     = 10;
    
//    self.placeSearchTextField.autoCompleteTableFrame = CGRectMake(10, 0, self.autoComplete.frame.size.width - 20, self.autoComplete.frame.size.height);
    
    searchArray = [[NSArray alloc]initWithObjects:@"Restaurant",@"Doctor",@"Gym",@"Spa",@"Shopping mall",@"Night club", nil];
    categoryArray = [[NSArray alloc]initWithObjects:@"restaurant",@"doctor",@"gym",@"spa",@"shopping_mall",@"night_club", nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Tableview Delegate/DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [searchArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MAPlacesSearchTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MAPlacesSearchTableCell"];
    
    NSLog(@"%@",searchArray[indexPath.row]);
    cell.lblCategoryName.text = searchArray[indexPath.row];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    [self.delegate selectedSearchCategory:categoryArray[indexPath.row]];
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Button Action

#pragma mark - Custom Methods
-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Place search Textfield Delegates

-(void)placeSearchResponseForSelectedPlace:(NSMutableDictionary*)responseDict{
    
    [self.view endEditing:YES];
    
    // NSLog(@"%@",responseDict);
    //    [placesSearchArray removeAllObjects];
    
    self.latitude = [[[[responseDict objectForKey:@"result"] objectForKey:@"geometry"] objectForKey:@"location"]valueForKey:@"lat"];
    self.longitude = [[[[responseDict objectForKey:@"result"] objectForKey:@"geometry"] objectForKey:@"location"]valueForKey:@"lng"];
    
    self.restaurantName = [[responseDict objectForKey:@"result"] objectForKey:@"formatted_address"];
    
    [_autoComplete setHidden:YES];
    
    [self.delegate selectedPlaceLatLong:self.latitude :self.longitude];
    
    [self.navigationController popViewControllerAnimated:YES];
    
//    [self getPlacesList];
}

-(void)placeSearchWillShowResult{
    
    [_autoComplete setHidden:NO];
}

-(void)placeSearchWillHideResult{
    
}

-(void)placeSearchResultCell:(UITableViewCell *)cell withPlaceObject:(PlaceObject *)placeObject atIndex:(NSInteger)index{
    
    if(index%2==0){
        cell.contentView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
    }else{
        cell.contentView.backgroundColor = [UIColor whiteColor];
    }
}

- (IBAction)btnMultipasClicked:(id)sender {
    MAPlacesFilterVC *filterVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MAPlacesFilterVC"];
    
    [self.navigationController pushViewController:filterVC animated:true];
}
- (IBAction)btnBackClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)btnClearSeachClicked:(id)sender {
    [self.delegate clearSelectedSearchCategoryAndPlace];
    [self.navigationController popViewControllerAnimated:YES];
}
@end
