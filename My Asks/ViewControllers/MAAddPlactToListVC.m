//
//  MAAddPlactToListVC.m
//  My Asks
//
//  Created by MY PC on 4/19/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "MAAddPlactToListVC.h"

@interface MAAddPlactToListVC ()

@end

@implementation MAAddPlactToListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.userPlacesArray = [[NSMutableArray alloc] init];
    self.userListsArray = [[NSMutableArray alloc] init];
    
    // Do any additional setup after loading the view.
    self.mainView.layer.cornerRadius = 10.0;
    self.tblAddPlaceToList.dataSource = self;
    self.tblAddPlaceToList.delegate = self;

    self.lblPlaceName.text = self.RestaurantName;
    self.lblAddress.text = self.placeAddress;
    NSLog(@"%@",self.PlaceID);
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {
    
    NSLog(@"%@",[AppData sharedManager].longitude);
    NSLog(@"%@",[AppData sharedManager].latitude);
    [self getLists];
    
}

- (void) getLists {
    
//    [KVNProgress show];
    [AppData startNetworkIndicator];

    [self.userListsArray removeAllObjects];
    
    NSString *userId = [[AppData sharedManager]getCurrentUserID];
    [[APIClient sharedManager] getUserLists:userId onCompletion:^(NSArray *resultData, bool success) {
        [AppData stopNetworkIndicator];
        //            [KVNProgress dismiss];

        if (success == true) {
            NSLog(@"%@",resultData);
            for (NSDictionary *userLists in resultData) {
                
                MAUserListsModel *userListsModel = [MAUserListsModel new];
                
                userListsModel.UserListID = [userLists valueForKey:@"UserListID"];
                userListsModel.UserId = [userLists valueForKey:@"UserId"];
                userListsModel.Name = [userLists valueForKey:@"Name"];
                userListsModel.Description = [userLists valueForKey:@"Description"];
                userListsModel.LikesCount = [userLists valueForKey:@"LikesCount"];
                userListsModel.PicturesCount = [userLists valueForKey:@"PicturesCount"];
                userListsModel.CommmentsCount = [userLists valueForKey:@"CommmentsCount"];
                NSString *modifiedDateString = [userLists valueForKey:@"LastmodifiedDate"];
                userListsModel.LastmodifiedDate = [self getLocalDateStringFrmGMT:modifiedDateString];
                
                userListsModel.IsPublic = [[userLists valueForKey:@"IsPublic"] integerValue];
                userListsModel.isSelected = NO;
                
                userListsModel.Pictures = [userLists valueForKey:@"Pictures"];
                NSArray *listsPlacesArray = [userLists valueForKey:@"Places"];
                NSMutableArray *listsPlacesMutableArray = [[NSMutableArray alloc] init];
                
                for (NSDictionary *places in listsPlacesArray) {
                    
                    MAUserListsPlacesModel *userListsPlacesModel = [MAUserListsPlacesModel new];
                    userListsPlacesModel.ID = [places valueForKey:@"ID"];
                    userListsPlacesModel.Name = [places valueForKey:@"Name"];
                    userListsPlacesModel.Address = [places valueForKey:@"Address"];
                    userListsPlacesModel.City = [places valueForKey:@"City"];
                    userListsPlacesModel.State = [places valueForKey:@"State"];
                    userListsPlacesModel.Latitude = [places valueForKey:@"Latitude"];
                    userListsPlacesModel.Longitude = [places valueForKey:@"Longitude"];
                    userListsPlacesModel.IsUserFavorite = [places valueForKey:@"IsUserFavorite"];
                    userListsPlacesModel.DistanceFromUserLocation = [places valueForKey:@"DistanceFromUserLocation"];
                    userListsPlacesModel.PlaceID = [places valueForKey:@"PlaceID"];
                    userListsPlacesModel.PhoneNumber = [places valueForKey:@"PhoneNumber"];
                    userListsPlacesModel.OpenHours = [places valueForKey:@"OpenHours"];
                    userListsPlacesModel.UserId = [places valueForKey:@"UserId"];
                    userListsPlacesModel.ListId = [places valueForKey:@"ListId"];
                    userListsPlacesModel.PlaceImage = [places valueForKey:@"PlaceImage"];
                    [listsPlacesMutableArray addObject:userListsPlacesModel];
                }
                userListsModel.Places = listsPlacesMutableArray;
                [self.userListsArray addObject:userListsModel];
            }
            [self.tblAddPlaceToList reloadData];
        }
        else {
            
            [KVNProgress showError];
            NSLog(@"Error");
        }
    }];
}

- (NSString *) getLocalDateStringFrmGMT : (NSString *) createdDateString {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    NSDate *sourceDate = [formatter dateFromString:createdDateString];
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
    dateFormat.timeZone = [NSTimeZone systemTimeZone];
    [dateFormat setDateFormat:@"yyyy/MM/dd hh:mm a"];
    NSString* localTime = [dateFormat stringFromDate:sourceDate];
    
    return localTime;
}

- (NSString *) getDateStringFromDate : (NSDate *) createdDateWithTime {
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *createdDateComp = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:createdDateWithTime];
    NSDate *createdDate = [calendar dateFromComponents:createdDateComp];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy/MM/dd";
    return [dateFormatter stringFromDate:createdDate];
}

- (NSString *) getTimeStringFromDate : (NSDate *) createdDateWithTime {
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *createdDateComp = [calendar components:NSCalendarUnitHour | NSCalendarUnitMinute fromDate:createdDateWithTime];
    NSDate *createdDate = [calendar dateFromComponents:createdDateComp];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"hh:mm a";
    return [dateFormatter stringFromDate:createdDate];
}

#pragma mark - TableView Delegate method

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.userListsArray count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MAListsTableCell * listsCell = [tableView dequeueReusableCellWithIdentifier:@"MAListsTableCell" forIndexPath: indexPath];
    
    [listsCell.btnAdd addTarget:self action:@selector(btnAddSelectClicked:) forControlEvents:UIControlEventTouchUpInside];
    listsCell.btnAdd.tag = indexPath.row;
    
    MAUserListsModel *userListsModel = [self.userListsArray objectAtIndex:indexPath.row];
    
    if (userListsModel.isSelected) {
        
        [listsCell.btnAdd setBackgroundImage:[UIImage imageNamed:@"Checkbox Selected.png"] forState:UIControlStateNormal];
    }
    else {
        
        [listsCell.btnAdd setBackgroundImage:[UIImage imageNamed:@"Add to list plus.png"] forState:UIControlStateNormal];
    }
    
    if(userListsModel.Description == (id)[NSNull null]) {
        
        listsCell.lblPlace.text = [NSString stringWithFormat:@""];
    }
    else {
        
        listsCell.lblPlace.text = [NSString stringWithFormat:@"%@",userListsModel.Description];
    }
    if(userListsModel.PicturesCount == (id)[NSNull null]) {
        
        listsCell.lblPlaceCount.text = [NSString stringWithFormat:@""];
    }
    else {
        
        listsCell.lblPlaceCount.text = [NSString stringWithFormat:@"%@",userListsModel.PicturesCount];
    }
    if(userListsModel.LikesCount == (id)[NSNull null]) {
        
        listsCell.lblLikeCount.text = @"";
    }
    else {
        
        listsCell.lblLikeCount.text = [NSString stringWithFormat:@"%@",userListsModel.LikesCount];
    }
    if(userListsModel.CommmentsCount == (id)[NSNull null]) {
        
        listsCell.lblCommentCount.text = @"";
    }
    else {
        
        listsCell.lblCommentCount.text = [NSString stringWithFormat:@"%@",userListsModel.CommmentsCount];
    }
    if ( userListsModel.Pictures.count == 0 ) {
        
        listsCell.imgBackground.image = [UIImage imageNamed:@""];
    }
    else{
        
        listsCell.imgBackground.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@", userListsModel.Pictures.firstObject]];
    }
    if(userListsModel.Name == (id)[NSNull null]) {
        
        //listsCell.lblName.text = @"";
        listsCell.lblPlace.text = @"";
    }
    else {
         //listsCell.lblName.text = [NSString stringWithFormat:@"%@",userListsModel.Name];
         listsCell.lblPlace.text = [NSString stringWithFormat:@"%@",userListsModel.Name];
    }
    
    return listsCell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
   // NSInteger index = sender.tag;
    
    MAUserListsModel *userListsModel = [self.userListsArray objectAtIndex:indexPath.row];
    if (userListsModel.isSelected) {
        
        //[sender setBackgroundImage:[UIImage imageNamed:@"Add to list plus.png"] forState:UIControlStateNormal];
        userListsModel.isSelected = NO;
    }
    else {
        
        //[sender setBackgroundImage:[UIImage imageNamed:@"Checkbox Selected.png"] forState:UIControlStateNormal];
        userListsModel.isSelected = YES;
    }
    
    NSMutableDictionary *param = [[NSMutableDictionary alloc]init];
    NSString *userId = [[AppData sharedManager]getCurrentUserID];
    
    [param setValue:self.RestaurantName forKey:@"RestaurantName"];
    [param setValue:self.Latitude forKey:@"Latitude"];
    [param setValue:self.Longitude forKey:@"Longitude"];
    [param setValue:userId forKey:@"UserID"];
    [param setValue:self.PlaceID forKey:@"PlaceId"];
    [param setValue:userListsModel.UserListID forKey:@"ListId"];
    
    NSLog(@"%@",param);
    [KVNProgress show];
    
    [[APIClient sharedManager]AddPlaceToUserList:param onCompletion:^(NSDictionary *resultData, bool success) {
        
        [KVNProgress dismiss];
        
        if (resultData == (id)[NSNull null] || success) {
            
            NSLog(@"%@",param);
            
            NSLog(@"success");
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
                                                            message:@"Place added to list"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            [self dismissViewControllerAnimated:true completion:nil];
            NSLog(@"%@",self.PlaceID);
            
            [self.delegate selected:self.PlaceID];
            
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
                                                            message:@"Something went wrong"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            [self dismissViewControllerAnimated:true completion:nil];
        }
        NSLog(@"%@",resultData);
        
        NSLog(@"%@",param);
    }];
    
}
#pragma mark - Button Action

//- (void)btnAddSelectClicked:(UIButton*)sender{
//    
//    NSInteger index = sender.tag;
//    
//    MAUserListsModel *userListsModel = [self.userListsArray objectAtIndex:index];
//    if (userListsModel.isSelected) {
//        
//        [sender setBackgroundImage:[UIImage imageNamed:@"Add to list plus.png"] forState:UIControlStateNormal];
//        userListsModel.isSelected = NO;
//    }
//    else {
//        
//        [sender setBackgroundImage:[UIImage imageNamed:@"Checkbox Selected.png"] forState:UIControlStateNormal];
//        userListsModel.isSelected = YES;
//    }
//    
//    NSMutableDictionary *param = [[NSMutableDictionary alloc]init];
//    NSString *userId = [[AppData sharedManager]getCurrentUserID];
//    
//    //[param setValue:self.RestaurantName forKey:@"RestaurantName"];
//    [param setValue:self.Latitude forKey:@"Latitude"];
//    [param setValue:self.Longitude forKey:@"Longitude"];
//    [param setValue:userId forKey:@"UserID"];
//    [param setValue:userListsModel.UserListID forKey:@"ListId"];
//    
//    NSLog(@"%@",param);
//    [KVNProgress show];
//    
//    [[APIClient sharedManager]AddPlaceToUserList:param onCompletion:^(NSDictionary *resultData, bool success) {
//        
//        [KVNProgress dismiss];
//        
//        if (resultData == (id)[NSNull null] || success) {
//            
//            NSLog(@"%@",param);
//            
//            NSLog(@"success");
//            
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
//                                                            message:@"Place added to list"
//                                                           delegate:self
//                                                  cancelButtonTitle:@"OK"
//                                                  otherButtonTitles:nil];
//            [alert show];
//            [self dismissViewControllerAnimated:true completion:nil];
//            [self.delegate selected:self.PlaceID];
//
//        }
//        else
//        {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
//                                                            message:@"Something went wrong"
//                                                           delegate:self
//                                                  cancelButtonTitle:@"OK"
//                                                  otherButtonTitles:nil];
//            [alert show];
//            [self dismissViewControllerAnimated:true completion:nil];
//        }
//        NSLog(@"%@",resultData);
//        
//        NSLog(@"%@",param);
//    }];
//    
//}

- (IBAction)btnDismissClicked:(id)sender {
    
    [self dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)btnAddListClicked:(id)sender {
    
    
    [self.delegate addListsWithId:self.PlaceID];
}
@end
