//
//  MAInviteFriendsVC.h
//  My Asks
//
//  Created by artis on 01/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <Contacts/Contacts.h>
#import <QuartzCore/QuartzCore.h>
#import <CZPicker/CZPicker.h>
#import "MAInviteFriendsModel.h"
#import "BIZPopupViewController.h"
#import "UIImageView+Letters.h"

@protocol MAInviteFriendsDelegate <NSObject>

@required
- (void)inviteFriends:(NSMutableArray *)contactsArray;
@end

@interface MAInviteFriendsVC : UIViewController <BIZPopupViewControllerDelegate,CZPickerViewDataSource,CZPickerViewDelegate,UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate>

{
    NSMutableArray *autocompleteContacts;
    NSMutableArray *autocompleteContactsName;
    BOOL isInviteAll;
    //CNContactStore *contactsStrore;
    NSMutableArray *contactDetails;
    MAInviteFriendsModel *inviteFriendsModel;
}

@property (weak, nonatomic) IBOutlet UIView *inviteFriendsView;
@property (weak, nonatomic) IBOutlet UITableView *inviteFriendsTableView;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectDeselectAll;
@property (weak, nonatomic) IBOutlet UITextField *txtSearchContacts;
@property (strong, nonatomic) UITableView *tblContactSearchResultTable;
@property (strong, nonatomic) NSMutableArray *searchContactArray;
@property (assign, nonatomic) BOOL isEdit;

@property (assign, nonatomic) BOOL isSearching;

@property (nonatomic, assign) id<MAInviteFriendsDelegate> delegate;

- (IBAction)btnDismissClicked:(id)sender;
- (IBAction)btnSelectDeselectAllClicked:(id)sender;
- (IBAction)btnInviteFriendsClicked:(id)sender;

@end
