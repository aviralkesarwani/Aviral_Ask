//
//  FAGoToScreenVC.h
//  My Asks
//
//  Created by MAC on 22/06/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MAPlacesFilterVC.h"
#import "AsksVC.h"
#import "AppData.h"

@interface FAGoToScreenVC : UIViewController


@property (strong,nonatomic)NSString *user;

@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UIButton *btnPlaces;
@property (weak, nonatomic) IBOutlet UIButton *btnAskFriends;

- (IBAction)btnStartPlaces:(id)sender;
- (IBAction)btnAskFriends:(id)sender;

@end
