//
//  AsksVC.h
//  My Asks
//
//  Created by artis on 30/03/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <KVNProgress.h>
#import "MAAsksTableCell.h"
#import "CreateNewAskVC.h"
//#import "MAUserAsksModel.h"
//#import "MAAskDetailsModel.h"
#import "AppData.h"
#import "MAChatScreenVC.h"
#import "MARightSideMenuVC.h"
#import "AppDelegate.h"
#import "ChatClient.h"
#import "BWTitlePagerView.h"
#import "MAAskTableViewCell.h"
#import "AwesomeMenu.h"
#import "DCPathButton.h"
#import "NewAskScreenVC.h"

@interface AsksVC : UIViewController <UITableViewDataSource, UITableViewDelegate,UISearchBarDelegate,UIScrollViewDelegate,AwesomeMenuDelegate,DCPathButtonDelegate,UIGestureRecognizerDelegate,UIActionSheetDelegate,QBChatDelegate>

#pragma mark - Variables

@property (strong, nonatomic) NSMutableArray *userAsksArray;
@property (strong, nonatomic) NSMutableArray *friendsAsksArray;
@property (strong, nonatomic) NSMutableArray *allAsksArray;
@property (strong, nonatomic) NSMutableArray *searchAsksArray;
@property (assign, nonatomic) BOOL isSearching;
@property (assign, nonatomic) BOOL isFriendsAsk;

@property (strong, nonatomic) UITableView *allAskTableView;
@property (strong, nonatomic) UITableView *myAskTableView;
@property (strong, nonatomic) UITableView *friendsAskTableView;

#pragma mark - Outlets

@property (weak, nonatomic) IBOutlet UIPageControl *askPageController;
@property (weak, nonatomic) IBOutlet UIScrollView *askScrollView;
@property (weak, nonatomic) IBOutlet UIView *askTableBackgroundView;
@property (weak, nonatomic) IBOutlet UIImageView *imgTabBackground;
@property (weak, nonatomic) IBOutlet UIButton *btnMyAsks;
@property (weak, nonatomic) IBOutlet UIButton *btnFriendAsks;
@property (strong, nonatomic) IBOutlet UIView *TopTabBarView;
@property (weak, nonatomic) IBOutlet UIView *topTabBarContainView;
@property (weak, nonatomic) IBOutlet UITableView *tblAsk;
@property (weak, nonatomic) IBOutlet UISearchBar *askSearchBar;
@property (weak, nonatomic) IBOutlet UILabel *lblAskType;


- (IBAction)btnMyAsksClicked:(id)sender;
- (IBAction)btnFriendAsksClicked:(id)sender;
- (IBAction)askPageControllerChanged:(id)sender;


@end
