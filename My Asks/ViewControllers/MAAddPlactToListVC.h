//
//  MAAddPlactToListVC.h
//  My Asks
//
//  Created by MY PC on 4/19/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MAListsTableCell.h"
#import "BIZPopupViewController.h"
#import <KVNProgress.h>
#import "BIZPopupViewController.h"
#import "AppDelegate.h"
#import "AppData.h"
#import "MAUserListsModel.h"
#import "MAUserListsPlacesModel.h"
#import "APIClient.h"
#import "MACreateNewListVC.h"
#import "MASearchPlacesModel.h"



@protocol MAAddPlactToListDelegate <NSObject>

@required

- (void)selected:(NSString *)placeId;
- (void)addListsWithId:(NSString *)placeId;
@end

@interface MAAddPlactToListVC : UIViewController<BIZPopupViewControllerDelegate, UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UILabel *lblPlaceName;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet UITableView *tblAddPlaceToList;
@property (weak, nonatomic) IBOutlet UITextField *txtSearchPlace;
@property (weak, nonatomic) IBOutlet UIView *mainView;

@property (nonatomic, assign) id<MAAddPlactToListDelegate> delegate;

@property (strong, nonatomic) MAUserListsModel *userlistData;
@property (strong, nonatomic) MAUserListsPlacesModel *userlistplacesData;
@property (strong, nonatomic)  MASearchPlacesModel *placesModel;

@property (strong, nonatomic) NSMutableArray *userPlacesArray;
@property (strong, nonatomic) NSMutableArray *userListsArray;

@property (strong, nonatomic)NSString *from;
@property (strong, nonatomic)NSString *RestaurantName;
@property (strong, nonatomic)NSString *placeAddress;
@property (strong, nonatomic)NSString *Latitude;
@property (strong, nonatomic)NSString *Longitude;
@property (strong, nonatomic)NSString *PlaceID;

- (IBAction)btnDismissClicked:(id)sender;
- (IBAction)btnAddListClicked:(id)sender;

@end
