//
//  MAPeopleInviteFriendsVC.h
//  My Asks
//
//  Created by artis on 19/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <KVNProgress.h>
#import "MAPeopleInviteFriendsTableCell.h"
#import "MAFriendListModel.h"
#import "MAPeopleInviteModel.h"
#import "APIClient.h"

@interface MAPeopleInviteFriendsVC : UIViewController<UITableViewDelegate,UITableViewDataSource,UISearchBarDelegate>
{
    MAPeopleInviteModel *inviteModel;
    NSMutableArray *contactsToinvite;
    NSString *ContactList;
}

#pragma Outlets
@property (weak, nonatomic) IBOutlet UITableView *tblFriendsList;
@property (strong, nonatomic) IBOutlet UISearchBar *friendsSearchBar;

@property (strong, nonatomic)NSMutableArray *ContactsArray;
@property (strong, nonatomic)NSMutableArray *filteredContactsArray;
@property (strong, nonatomic)NSString *selectedNumber;
@property (strong, nonatomic) NSMutableArray *searchContactArray;
@property (assign, nonatomic) BOOL isSearching;

#pragma Actions

- (IBAction)btnBackClicked:(id)sender;
@property (strong, nonatomic) IBOutlet UIButton *btnInvite;

@end
