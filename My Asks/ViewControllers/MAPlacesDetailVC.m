//
//  MAPlacesDetailVC.m
//  My Asks
//
//  Created by Mac on 21/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "MAPlacesDetailVC.h"
#import "MAAddPlactToListVC.h"
#import "MACreateNewListVC.h"

@interface MAPlacesDetailVC () <MAAddPlactToListDelegate, MACreateNewListDelegate>

@end

@implementation MAPlacesDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
//    if (self.PlaceImageUrl == (id)[NSNull null] || self.PlaceImageUrl.length == 0) {
//        
//        NSLog(@"there is no image");
//    }
//    else{
//        self.imgPlace.imageURL =   [NSURL URLWithString:self.PlaceImageUrl];
//    }
    
    NSLog(@"%@",self.PlaceSubLocality);
    NSLog(@"%@",self.PlaceLongitude);
    NSLog(@"%@",self.PlaceLatitude);
    NSLog(@"%@",self.PlaceID);
    
//    self.lblPlaeName.text = self.PlaceName;
//    self.lblPlaceAddress.text = self.PlaceAddress;
//    self.lblPlaceDistance.text = self.PlaceDistanceInMiles;
//    self.lblPlaceCityState.text = self.PlaceSubLocality;
    
//    if (self.isFavorite) {
//        [self.btnPlusClicked setImage:[UIImage imageNamed:@"Checkbox Selected"] forState:UIControlStateNormal];
//    }
    
    [self getPlaceDetails];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)getPlaceDetails{
    
    NSString *userID = [[AppData sharedManager]getCurrentUserID];
    NSMutableDictionary *param = [[NSMutableDictionary alloc]init];
    
    [param setValue:userID forKey:@"UserID"];
    [param setValue:self.PlaceID forKey:@"PlaceId"];
    
//    [KVNProgress show];
    [AppData startNetworkIndicator];
    
    [[APIClient sharedManager]PlaceDetails:param onCompletion:^(NSDictionary *resultData, bool success) {
        NSLog(@"%@",resultData);
//        [KVNProgress dismiss];
        [AppData stopNetworkIndicator];

        if (success) {
            if ([resultData count] == 0) {
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
                                                                message:@"Place details not available"
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
            else{
                
                self.lblPlaceName.text = [resultData valueForKey:@"PlaceName"];
                self.lblPlaceAddress.text = [resultData valueForKey:@"PlaceAddress"];
                self.lblPlaceCityState.text = self.PlaceSubLocality;
                self.PlaceLatitude = [resultData valueForKey:@"PlaceLatitude"];
                self.PlaceLongitude = [resultData valueForKey:@"PlaceLongitude"];
                self.isFavorite = [[resultData valueForKey:@"isFavorite"]boolValue];
                self.PlaceImageUrl = [resultData valueForKey:@"PlaceImageURL"];
                
                NSLog(@"%@",[resultData valueForKey:@"DistanceinMiles"]);
                
                NSArray *timings = [resultData valueForKey:@"OpenHoursAraay"];
                // NSLog(@"%lu",(unsigned long)[timings count]);
                
                if (timings == (id)[NSNull null]) {
                    
                    self.lblMonToSun.hidden = YES;
                    self.lblSat.hidden = YES;
                    self.lblMonToSunTiming.hidden = YES;
                    self.lblSatTiming.hidden = YES;
                    self.lbl3Timings.hidden = YES;
                    self.lbl3Value.hidden = YES;
                    
                    self.openHoursHeight.constant = 0;
                }
                else{
                    
                    self.openHoursHeight.constant = 65;
                    
                    
                    if ([timings count] >2) {
                        
                        
                        NSArray *data = [[timings objectAtIndex:0] componentsSeparatedByString:@"="];
                        NSLog(@"%@",data);
                        self.lblMonToSun.text = [data objectAtIndex:0];
                        self.lblMonToSunTiming.text = [data objectAtIndex:1];
                        
                        
                        NSArray *data1 = [[timings objectAtIndex:1] componentsSeparatedByString:@"="];
                        NSLog(@"%@",data1);
                        self.lblSat.text = [data1 objectAtIndex:0];
                        self.lblSatTiming.text = [data1 objectAtIndex:1];
                        
                        NSArray *data2 = [[timings objectAtIndex:2] componentsSeparatedByString:@"="];
                        NSLog(@"%@",data2);
                        
                        self.lbl3Timings.text = [data2 objectAtIndex:0];
                        self.lbl3Value.text = [data2 objectAtIndex:1];
                        
                        
                    }
                    else if ([timings count] == 2){
                        
                        NSArray *data = [[timings objectAtIndex:0] componentsSeparatedByString:@"="];
                        NSLog(@"%@",data);
                        self.lblMonToSun.text = [data objectAtIndex:0];
                        self.lblMonToSunTiming.text = [data objectAtIndex:1];
                        
                        
                        NSArray *data1 = [[timings objectAtIndex:1] componentsSeparatedByString:@"="];
                        NSLog(@"%@",data1);
                        self.lblSat.text = [data1 objectAtIndex:0];
                        self.lblSatTiming.text = [data1 objectAtIndex:1];
                        
                        self.lbl3Timings.hidden = YES;
                        self.lbl3Value.hidden = YES;
                        
                    }
                    else if([timings count] != 0){
                        
                        NSArray *data1 = [[timings objectAtIndex:0] componentsSeparatedByString:@"="];
                        NSLog(@"%@",data1);
                        self.lblMonToSun.text = [data1 objectAtIndex:0];
                        self.lblMonToSunTiming.text = [data1 objectAtIndex:1];
                        
                        self.lblSat.hidden = YES;
                        self.lblSatTiming.hidden = YES;
                        self.lbl3Timings.hidden = YES;
                        self.lbl3Value.hidden = YES;
                        
                    }
                }
                
                if (self.PlaceImageUrl == (id)[NSNull null] || self.PlaceImageUrl.length == 0) {
                    
                    NSLog(@"there is no image");
                }
                else{
                    self.imgPlace.imageURL =   [NSURL URLWithString:self.PlaceImageUrl];
                }
                
                if (self.isFavorite) {
                    [self.btnPlusClicked setImage:[UIImage imageNamed:@"Checkbox Selected"] forState:UIControlStateNormal];
                }

//                
//                NSString *distance;
//                
//                if ([resultData valueForKey:@"DistanceinMiles"] == (id)[NSNull null]) {
//                    
//                    distance = @"00.00mi";
//                }
//                else{
//                    float distanceMiles = [[resultData valueForKey:@"DistanceinMiles"]floatValue];
//                    
//                    distance = [NSString stringWithFormat:@"%.2f mi",distanceMiles];
//                }
                
                self.lblPlaceDistance.text = self.placesModel.DistanceinMiles;
                [self setmarker];
                
                UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
                tapGesture.numberOfTapsRequired = 2;
                [self.btnPlusClicked addGestureRecognizer:tapGesture];
                
                UILongPressGestureRecognizer *longPressGesture= [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(handleLongPress:)];
                longPressGesture.delegate = self;
                longPressGesture.delaysTouchesBegan = YES;
                [self.btnPlusClicked addGestureRecognizer:longPressGesture];
                
                [self.btnPlusClicked addTarget:self action:@selector(btnPlusClicked:) forControlEvents:UIControlEventTouchUpInside];
            }
        }
    }];
}

#pragma mark - gesture recognizer methods

-(void)handleTapGesture:(UITapGestureRecognizer *)tap{
    
    MAAddPlactToListVC *contributeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MAAddPlactToListVC"];
    CGRect fullScreenRect = [[UIScreen mainScreen] bounds];

    contributeViewController.RestaurantName = self.lblPlaceName.text;
    contributeViewController.placeAddress = self.lblPlaceAddress.text;
    contributeViewController.Latitude = self.PlaceLatitude;
    contributeViewController.Longitude = self.PlaceLongitude;
    contributeViewController.from = @"Place Detail";
    contributeViewController.delegate = self;
    
    BIZPopupViewController *popupViewController = [[BIZPopupViewController alloc] initWithContentViewController:contributeViewController contentSize:CGSizeMake(fullScreenRect.size.width , fullScreenRect.size.height)];
    popupViewController.showDismissButton = NO;
    
    [self presentViewController:popupViewController animated:NO completion:nil];
}
-(void)handleLongPress:(UILongPressGestureRecognizer *)longTap{
    
    MAAddPlactToListVC *contributeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MAAddPlactToListVC"];
    CGRect fullScreenRect = [[UIScreen mainScreen] bounds];
    
    contributeViewController.RestaurantName = self.lblPlaceName.text;
    contributeViewController.placeAddress = self.lblPlaceAddress.text;
    contributeViewController.Latitude = self.PlaceLatitude;
    contributeViewController.Longitude = self.PlaceLongitude;
    contributeViewController.PlaceID = self.PlaceID;
    contributeViewController.delegate = self;
    contributeViewController.from = @"Place Detail";
    BIZPopupViewController *popupViewController = [[BIZPopupViewController alloc] initWithContentViewController:contributeViewController contentSize:CGSizeMake(fullScreenRect.size.width , fullScreenRect.size.height)];
    popupViewController.showDismissButton = NO;
    
    [self presentViewController:popupViewController animated:NO completion:nil];
}

-(void)btnPlusClicked:(UIButton *)sender{
    
    NSMutableDictionary *param = [[NSMutableDictionary alloc]init];
    
    //MASearchPlacesModel *placesModel;// = [placesSearchArray objectAtIndex:sender.tag];
    
    NSString *userId = [[AppData sharedManager]getCurrentUserID];
    
    [param setValue:self.lblPlaceName.text forKey:@"RestaurantName"];
    [param setValue:self.PlaceLatitude forKey:@"Latitude"];
    [param setValue:self.PlaceLongitude forKey:@"Longitude"];
    [param setValue:self.PlaceID forKey:@"PlaceId"];
    [param setValue:userId forKey:@"UserID"];
    [param setValue:@"0" forKey:@"ListId"];
    
    [KVNProgress show];
    
    [[APIClient sharedManager]AddPlaceToUserList:param onCompletion:^(NSDictionary *resultData, bool success) {
        
        [KVNProgress dismiss];
        if (success) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
                                                          message:@"Place added to list"
                                                          delegate:self
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                [alert show];
            NSLog(@"%@",resultData);
            
             [self.btnPlusClicked setImage:[UIImage imageNamed:@"Checkbox Selected"] forState:UIControlStateNormal];
        }
    }];
}

#pragma mark - Set marker on Map

-(void)setmarker{
    
    double latitude,longitude;
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    
    latitude = [self.PlaceLatitude doubleValue];
    longitude = [self.PlaceLongitude doubleValue];
    
    marker.position=CLLocationCoordinate2DMake(latitude,longitude);
    
    marker.appearAnimation = YES;
    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0,0,200,60)];
    
    UIImageView *pinImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Placeblock.png"]];
    
    UILabel *lblPlaceName = [[UILabel alloc]initWithFrame:CGRectMake(5, 5, 150, 20)];
    UILabel *lblPlaceDetails = [[UILabel alloc]initWithFrame:CGRectMake(5, 20,180 ,35)];
    
    lblPlaceName.numberOfLines = 2.0;
    lblPlaceDetails.numberOfLines = 2.0;
    
    [lblPlaceName setFont:[UIFont systemFontOfSize:12.0]];
    [lblPlaceDetails setFont:[UIFont systemFontOfSize:12.0]];
    
    lblPlaceName.textColor = [UIColor whiteColor];
    lblPlaceDetails.textColor = [UIColor whiteColor];
    
    lblPlaceName.text = self.lblPlaceName.text;
    lblPlaceDetails.text = self.lblPlaceAddress.text;
    
    [lblPlaceName sizeToFit];
    
    [view addSubview:pinImageView];
    [view addSubview:lblPlaceName];
    [view addSubview:lblPlaceDetails];
    
    UIImage *markerIcon = [[AppData sharedManager]imageFromView:view];
    
    marker.icon = markerIcon;
    marker.map=self.googleMapView;
    
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    
    CLLocationCoordinate2D coordinate = marker.position;
    
    bounds = [bounds includingCoordinate:coordinate];
    self.googleMapView.myLocationEnabled = YES;
    
    [self.googleMapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:50.0f]];
    
    CLLocationCoordinate2D target =
    CLLocationCoordinate2DMake(coordinate.latitude,coordinate.longitude);
    self.googleMapView.camera = [GMSCameraPosition cameraWithTarget:target zoom:10];
    
}

#pragma mark - addList button clicked delagate

-(void)addLists {
    
    [self dismissViewControllerAnimated:true completion:nil];
    MACreateNewListVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MACreateNewListVC"];
    vc.delegate = self;
    vc.listType = @"Create List";
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)addListsWithId:(NSString *)placeId {
    
    [self dismissViewControllerAnimated:true completion:nil];
    MACreateNewListVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MACreateNewListVC"];
    vc.delegate = self;
    vc.placeIdForAddPlaceToList = placeId;
    vc.listType = @"Create List with Place";
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)selected:(NSString *)placeId {
    
    [self.btnPlusClicked setImage:[UIImage imageNamed:@"Checkbox Selected"] forState:UIControlStateNormal];
}

- (IBAction)btnBackClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)btnShareClicked:(id)sender {
    NSLog(@"%@",self.PlaceID);
    NSString *textToShare = [NSString stringWithFormat:@"%@ %@ %@",[[NSUserDefaults standardUserDefaults] valueForKey:MAUserName],@" has share you a Place ", [NSString stringWithFormat:@"rex.life://?PlaceID=%@&UserID=%@",self.PlaceID,[AppData sharedManager].userID]];
    NSLog(@"%@",textToShare);
    NSLog(@"%@",[NSString stringWithFormat:@"%@ %@ %@",[[NSUserDefaults standardUserDefaults] valueForKey:MAUserName],@" has share you a Place ", [NSString stringWithFormat:@"rex.life://?PlaceID=%@&UserID=%@",self.PlaceID,[AppData sharedManager].userID]]);
    NSArray *itemsToShare = @[textToShare];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:nil];
    activityVC.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll];
    [self presentViewController:activityVC animated:YES completion:nil];
}
@end
