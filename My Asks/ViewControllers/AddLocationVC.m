//
//  AddLocationVC.m
//  My Asks
//
//  Created by artis on 31/03/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.

#import "AddLocationVC.h"
#import "MAAddLocationTableCell.h"
#import "MVPlaceSearchTextField.h"
#import "AppData.h"

@interface AddLocationVC ()

@end

@implementation AddLocationVC

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
        locationDetails = [[NSMutableArray alloc]init];
    
    //self.mainMarkerView = [[[NSBundle mainBundle] loadNibNamed:@"InfoWindow" owner:self options:nil] firstObject];
    
    self.googleMapView.myLocationEnabled = YES;
    self.googleMapView.delegate = self;
    
    self.placeSearchTextField.placeSearchDelegate                 = self;
    self.placeSearchTextField.strApiKey                           = @"AIzaSyCyL6A9-IRxfUqept-WCEOC3Za2v_-27zI";
    self.placeSearchTextField.superViewOfList                     = self.autoComplete;  // View, on which Autocompletion list should be appeared.
    self.placeSearchTextField.autoCompleteShouldHideOnSelection   = YES;
    self.placeSearchTextField.maximumNumberOfAutoCompleteRows     = 10;
    
    CLLocationCoordinate2D target =
    CLLocationCoordinate2DMake(28.66,88.64);
    self.googleMapView.camera = [GMSCameraPosition cameraWithTarget:target zoom:2.0];
    
    [self.googleMapView animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:self.googleMapView.myLocation.coordinate.latitude
                                                                longitude:self.googleMapView.myLocation.coordinate.longitude
                                                                    zoom:2]];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TextFiled

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

#pragma mark - Button Actions

- (IBAction)btnBackClicked:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)btnSelectDeselectClicked:(UIButton*)sender{
    
    if (![[locationDetails[sender.tag]valueForKeyPath:@"isSelected"]integerValue] == 0) {
        
        [locationDetails[sender.tag] setValue:[NSNumber numberWithInt:0] forKey:@"isSelected"];
        [sender setImage:[UIImage imageNamed:@"Checkbox normal.png"] forState:UIControlStateNormal];
        
    }
    else{
        [locationDetails[sender.tag] setValue:[NSNumber numberWithInt:1] forKey:@"isSelected"];
        [sender setImage:[UIImage imageNamed:@"Checkbox Selected.png"] forState:UIControlStateNormal];
    }
}


#pragma mark - TableView Delegate / DataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [locationDetails count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MAAddLocationTableCell * addLocationCell = [tableView dequeueReusableCellWithIdentifier:@"MAAddLocationTableCell" forIndexPath: indexPath];
    addLocationCell.lblLocationName.text = [locationDetails[indexPath.row]placeName];
    addLocationCell.lblLocationDetail.text = [locationDetails[indexPath.row]placeAddress];
    
     [ addLocationCell.btnSelectDeselect addTarget:self action:@selector(btnSelectDeselectClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    addLocationCell.btnSelectDeselect.tag = indexPath.row;
    
    if (![[locationDetails[indexPath.row]valueForKeyPath:@"isSelected"]integerValue] == 0) {
        
        [addLocationCell.btnSelectDeselect setImage:[UIImage imageNamed:@"Checkbox Selected"] forState:UIControlStateNormal];
    }
    else{
        [addLocationCell.btnSelectDeselect setImage:[UIImage imageNamed:@"Checkbox normal"] forState:UIControlStateNormal];
    }
    return  addLocationCell;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
}

#pragma mark - Place search Textfield Delegates

-(void)placeSearchResponseForSelectedPlace:(NSMutableDictionary*)responseDict{
    
     addLocationVC = [[MALocationModel alloc]init];
    
    [self.view endEditing:YES];
    
    NSLog(@"%@",responseDict);
    
    NSDictionary *aDictLocation=[[[[responseDict objectForKey:@"result"] objectForKey:@"geometry"] objectForKey:@"location"]valueForKey:@"lat"];
    
    NSLog(@"SELECTED ADDRESS :%@",aDictLocation);
    NSLog(@"%@",[[responseDict objectForKey:@"result"] objectForKey:@"place_id"]);
    
    addLocationVC.placeName = [[responseDict objectForKey:@"result"] objectForKey:@"name"];
    addLocationVC.placeAddress = [[responseDict objectForKey:@"result"] objectForKey:@"formatted_address"];
    addLocationVC.latitude = [[[[responseDict objectForKey:@"result"] objectForKey:@"geometry"] objectForKey:@"location"]valueForKey:@"lat"];
    addLocationVC.longitude = [[[[responseDict objectForKey:@"result"] objectForKey:@"geometry"] objectForKey:@"location"]valueForKey:@"lng"];
    addLocationVC.placeID = [[responseDict objectForKey:@"result"] objectForKey:@"place_id"];
    addLocationVC.isSelected = YES;
    
    NSLog(@"%@",addLocationVC.placeAddress);
    
    [locationDetails addObject:addLocationVC];
    
    [_autoComplete setHidden:YES];
    
    [self.locationTableVIew setHidden:NO];
    
    [self.locationTableVIew reloadData];
    
    [self setmarker];
    
}

-(void)placeSearchWillShowResult{
  
    [self.locationTableVIew setHidden:YES];
     [_autoComplete setHidden:NO];
}

-(void)placeSearchWillHideResult{
    [_autoComplete setHidden:YES];
}

-(void)placeSearchResultCell:(UITableViewCell *)cell withPlaceObject:(PlaceObject *)placeObject atIndex:(NSInteger)index{
    
    if(index%2==0){
        cell.contentView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
    }else{
        cell.contentView.backgroundColor = [UIColor whiteColor];
    }
    
}


#pragma mark - Set marker on Map

-(void)setmarker{
    
    double latitude,longitude;
    
    GMSMarker *marker = [[GMSMarker alloc] init];
    
    latitude = [addLocationVC.latitude doubleValue];
    longitude = [addLocationVC.longitude doubleValue];
    
    marker.position=CLLocationCoordinate2DMake(latitude,longitude);

    marker.appearAnimation = YES;

    
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0,0,200,60)];
    
    UIImageView *pinImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Placeblock.png"]];
    
    UILabel *lblPlaceName = [[UILabel alloc]initWithFrame:CGRectMake(5, 5, 150, 20)];
    UILabel *lblPlaceDetails = [[UILabel alloc]initWithFrame:CGRectMake(5, 20,180 ,35)];
    

    lblPlaceDetails.numberOfLines = 2.0;
    
    [lblPlaceDetails setFont:[UIFont systemFontOfSize:15.0]];
    
    lblPlaceName.textColor = [UIColor whiteColor];
    lblPlaceDetails.textColor = [UIColor whiteColor];
    
    lblPlaceName.text = addLocationVC.placeName;
    lblPlaceDetails.text = addLocationVC.placeAddress;

    [lblPlaceName sizeToFit];
    
    [view addSubview:pinImageView];
    [view addSubview:lblPlaceName];
    [view addSubview:lblPlaceDetails];
    
    UIImage *markerIcon = [[AppData sharedManager]imageFromView:view];
    
    marker.icon = markerIcon;
    marker.map=self.googleMapView;
    
    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
    
    CLLocationCoordinate2D coordinate = marker.position;
    
    bounds = [bounds includingCoordinate:coordinate];
    self.googleMapView.myLocationEnabled = YES;
    
    [self.googleMapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:50.0f]];
    
    CLLocationCoordinate2D target =
    CLLocationCoordinate2DMake(coordinate.latitude,coordinate.longitude);
    self.googleMapView.camera = [GMSCameraPosition cameraWithTarget:target zoom:10];
}


#pragma mark - Set marker on Map

- (IBAction)btnAddLocationCllicked:(id)sender {
    
    NSMutableArray *locationArray =[[NSMutableArray alloc]init];
    
    for (int i = 0; i <[locationDetails count]; i++) {
        
        if (![[locationDetails[i]valueForKeyPath:@"isSelected"]integerValue] == 0) {
            
            [locationArray addObject:locationDetails[i]];
        }
    }
   
    NSLog(@"%@",locationArray);
    
    [self.delegate addLocations:locationArray];
    [self.navigationController popViewControllerAnimated:YES];    
    
}
@end
