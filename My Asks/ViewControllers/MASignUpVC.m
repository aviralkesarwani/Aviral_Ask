//
//  MASignUpVC.m
//  My Asks
//
//  Created by artis on 20/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "MASignUpVC.h"
#import <QuickBlox/Quickblox.h>
#import <QMServices/QMServices.h>

@interface MASignUpVC ()

@end

@implementation MASignUpVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.btnFemaleIcon addTarget:self action:@selector(btnFemaleClicked) forControlEvents:UIControlEventTouchUpInside];
    [self.btnMaleIcon addTarget:self action:@selector(btnMaleClicked) forControlEvents:UIControlEventTouchUpInside];
    countryDataRows = [[NSMutableArray alloc]init];
    
    self.isMaleSelected = YES;
    self.txtMobileNumber.delegate = self;
    
//    self.locationManager = [[CLLocationManager alloc] init];
//    self.locationManager.delegate = self;
//    self.locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters;
//    [self.locationManager requestWhenInUseAuthorization];
//    [self.locationManager startUpdatingLocation];
    
    [self parseJSON];
    
    for (NSMutableDictionary *dic in countriesList) {
        
        NSString *country = [dic valueForKey:@"name"];
        NSString *Code = [dic valueForKey:@"dial_code"];
        
        NSString *data = [NSString stringWithFormat:@"%@ (%@)",country,Code];
        [countryDataRows addObject:data];
    }

    self.btnDone.userInteractionEnabled = false;
    self.btnDone.titleLabel.textColor = [UIColor grayColor];
 
// For geting country code information without using location services
    
    CTTelephonyNetworkInfo *networkInfo = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    // Get carrier name
    NSString *carrierName = [carrier carrierName];
    if (carrierName != nil)
        NSLog(@"Carrier: %@", carrierName);
    
    // Get mobile country code
    NSString *mcc = [carrier mobileCountryCode];
    if (mcc != nil)
        NSLog(@"Mobile Country Code (MCC): %@", mcc);
    
    // Get mobile network code
    NSString *mnc = [carrier isoCountryCode];
    
    if (mnc == nil || mnc.length == 0){
        
        NSLog(@"Mobile Network Code (MNC): %@", mnc);
    
        self.txtCountryCode.text = @"United States";
        self.lblCountryCode.text = @"+1";
        dialingCode = @"1";

    }
    else{

        for (NSMutableDictionary *dic in countriesList) {
            
            NSString *country = [dic valueForKey:@"name"];
            NSString *Code = [dic valueForKey:@"dial_code"];
            
            NSString *countryCode = [[NSString stringWithFormat:@"%@",mnc]uppercaseString];
            
            NSLog(@"%@",[dic valueForKey:kCountryCode]);
            if ([countryCode isEqualToString:[dic valueForKey:kCountryCode]]) {
                
                self.txtCountryCode.text = [NSString stringWithFormat:@"%@",[dic valueForKey:kCountryName]];
                self.lblCountryCode.text = [NSString stringWithFormat:@"%@",[dic valueForKey:kCountryCallingCode]];
                
                dialingCode = [[[dic valueForKey:kCountryCallingCode] componentsSeparatedByCharactersInSet:
                                [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                               componentsJoinedByString:@""];
            }
            
            
            NSString *data = [NSString stringWithFormat:@"%@ (%@)",country,Code];
            [countryDataRows addObject:data];
        }
        
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{
    
    [self.txtMobileNumber becomeFirstResponder];
}

#pragma mark - Actions

- (IBAction)btnCoverImageClicked:(id)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
    
}

- (IBAction)btnCountryCodeClicked:(id)sender {
    
    [self.view endEditing:true];
    
//    CZPickerView *picker = [[CZPickerView alloc] initWithHeaderTitle:@"Select Number" cancelButtonTitle:@"Cancel" confirmButtonTitle:@"Add"];
//    picker.delegate = self;
//    picker.dataSource = self;
//    picker.needFooterView = YES;
//    [picker show];
    
    [Dropobj fadeOut];
    
    [self showPopUpWithTitle:@"Select Country" withOption:countryDataRows xy:CGPointMake(self.fillDataView.frame.origin.x,self.fillDataView.frame.origin.y) size:CGSizeMake(self.fillDataView.frame.size.width,self.fillDataView.frame.size.height) isMultiple:NO];
    
}


- (IBAction)btnBackClicked:(id)sender {
}
- (IBAction)btnCreateAccountClicked:(id)sender {
    
   
//    if (self.txtName.text.length == 0) {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
//                                                        message:@"Please enter name"
//                                                       delegate:self
//                                              cancelButtonTitle:@"OK"
//                                              otherButtonTitles:nil];
//        [alert show];
//        
//    }
//    else if (![[AppData sharedManager]validateEmail:self.txtEmail.text]) {
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
//                                                        message:@"Enter valid email adderess"
//                                                       delegate:self
//                                              cancelButtonTitle:@"OK"
//                                              otherButtonTitles:nil];
//        [alert show];
//    }
    if(self.txtCountryCode.text.length == 0){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
                                                        message:@"Enter valid country code"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    else if(self.txtMobileNumber.text.length == 0 || self.txtMobileNumber.text.length >10){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
                                                        message:@"Enter valid mobile number"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
//    else if([self.imgCoverImage.image isEqual:[UIImage imageNamed:@"Plus"]]){
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
//                                                        message:@"Please add profile picture"
//                                                       delegate:self
//                                              cancelButtonTitle:@"OK"
//                                              otherButtonTitles:nil];
//        [alert show];
//    }
    else{
//        [self varifyUser];
        
        
       // if ([self.imgCoverImage.image isEqual:[UIImage imageNamed:@"Plus"]]) {
            [self varifyUserWithCoverImageName:@""];
       // }
        //else {
//            [self saveProfileImage];
            
       // }
    }
}

- (IBAction)btnDoneClicked:(id)sender {
    
    if(self.txtCountryCode.text.length == 0){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
                                                        message:@"Enter valid country code"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    else if(self.txtMobileNumber.text.length == 0 || self.txtMobileNumber.text.length >10){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
                                                        message:@"Enter valid mobile number"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    
    else{
        
        [self varifyUserWithCoverImageName:@""];
    }
}

-(void)btnMaleClicked{
    
    if (self.isMaleSelected) {
        //[self.btnMaleIcon setImage:[UIImage imageNamed:@"Male icon unselect"] forState:UIControlStateNormal];
    }
    else{
        [self.btnMaleIcon setImage:[UIImage imageNamed:@"Male icon"] forState:UIControlStateNormal];
        [self.btnFemaleIcon setImage:[UIImage imageNamed:@"Female icon unselect"] forState:UIControlStateNormal];
        
        self.isMaleSelected = YES;
        self.isFemaleSelected = NO;
    }
}
-(void)btnFemaleClicked{
    
    if (self.isFemaleSelected) {
        //[self.btnFemaleIcon setImage:[UIImage imageNamed:@"Female icon unselect"] forState:UIControlStateNormal];
        
        //self.isFemaleSelected = NO;
        //self.isMaleSelected = YES;

    }
    else{
       [self.btnFemaleIcon setImage:[UIImage imageNamed:@"Female icon"] forState:UIControlStateNormal];
        [self.btnMaleIcon setImage:[UIImage imageNamed:@"Male icon unselect"] forState:UIControlStateNormal];
        
        self.isFemaleSelected = YES;
        self.isMaleSelected = NO;
    }
}

#pragma mark - Varify user API call

-(void)varifyUserWithCoverImageName : (NSString *) coverImageName {
   
    NSDictionary *param = [[NSMutableDictionary alloc]init];
    NSString *mobileNumber = [NSString stringWithFormat:@"%@%@",dialingCode,self.txtMobileNumber.text];
    [param setValue:mobileNumber forKey:@"MobileNumber"];
    [param setValue:[AppData sharedManager].deviceToken forKey:@"DeviceToken"];
    
    [KVNProgress show];
    
    [[APIClient sharedManager] CreateUser:param onCompletion:^(NSDictionary *resultData, bool success) {
        
        [KVNProgress dismiss];

        if ([[resultData valueForKey:@"Success"] boolValue]) {
            
            NSString *userID = [resultData valueForKey:@"Request_ID"];
            [AppData sharedManager].userID = userID;
            [AppData sharedManager].userMobileNumber = self.txtMobileNumber.text;
            [AppData sharedManager].userCountryCode = dialingCode;
            
            MAVarifyUserVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"MAVarifyUserVC"];
            [self.navigationController pushViewController:VC animated:YES];
        }
        else
        {
            NSString *errorMSG = [resultData valueForKey:@"ErrorMessage"];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
                                                            message:errorMSG
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    }];
}

- (void) saveProfileImage {
    
//    [KVNProgress show];
//    UIImage *compressedImage  = [[AppData sharedManager]compressImage:self.imgCoverImage.image];
//    [[AppData sharedManager] saveUserProfileImage:compressedImage];
//    [[APIClient sharedManager] saveASKBackgoundImage:compressedImage onCompletion:^(NSDictionary *resultData, bool success) {
//        [KVNProgress dismiss];
//            if (success) {
//                NSString *imageName = [resultData valueForKey:@"Request_ID"];
//                [self varifyUserWithCoverImageName:imageName];
//            }
//        }];
}

#pragma mark - image picker Delagate/Datasource

//- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
//    
//    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
//    
////    self.imgCoverImage.clipsToBounds = YES;
//    self.imgCoverImage.layer.cornerRadius = 44;
//     self.imgCoverImage.layer.masksToBounds = YES;
//    self.imgCoverImage.image = chosenImage;
//   
//    [picker dismissViewControllerAnimated:YES completion:NULL];
//}
//
//
//- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
//    
//    [picker dismissViewControllerAnimated:YES completion:NULL];
//}

#pragma mark - location delegate

// this delegate is called when the app successfully finds your current location
//- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
//{
//    // this creates a MKReverseGeocoder to find a placemark using the found coordinates
//
//    CLGeocoder *ceo = [[CLGeocoder alloc]init];
//    CLLocation *loc = [[CLLocation alloc]initWithLatitude:newLocation.coordinate.latitude longitude:newLocation.coordinate.longitude]; //insert your coordinates
//    
//    [ceo reverseGeocodeLocation:loc
//              completionHandler:^(NSArray *placemarks, NSError *error) {
//                  
//                  CLPlacemark *placemark = [placemarks objectAtIndex:0];
//                  
//                  NSLog(@"%@",placemark);
//                  NSLog(@"%@",placemark.ISOcountryCode);
//                  [self.locationManager stopUpdatingLocation];
//                  
//                  NSString *countryCode = placemark.ISOcountryCode;
//                  
//                  for (NSDictionary *code in countriesList) {
//                      
//                      NSString *codeToMatch = [code valueForKey:kCountryCode];
//                      
//                      if ([countryCode isEqualToString:codeToMatch]) {
//                          
//                          self.txtCountryCode.text = [NSString stringWithFormat:@"%@",[code valueForKey:kCountryName]];
//                          self.lblCountryCode.text = [NSString stringWithFormat:@"%@",[code valueForKey:kCountryCallingCode]];
//                          
//                          dialingCode = [[[code valueForKey:kCountryCallingCode] componentsSeparatedByCharactersInSet:
//                                          [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
//                                         componentsJoinedByString:@""];
//                      }
//                  }
//              }
//     ];
//}

- (void)parseJSON {
    
    NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"countries" ofType:@"json"]];
    NSError *localError = nil;
    NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
    
    if (localError != nil) {
        NSLog(@"%@", [localError userInfo]);
    }
    NSArray *objectArray = (NSArray *)parsedObject;
    
    NSSortDescriptor *sortDescriptor =
    [NSSortDescriptor sortDescriptorWithKey:@"name"
                                  ascending:YES
                                   selector:@selector(caseInsensitiveCompare:)];
    countriesList = [objectArray sortedArrayUsingDescriptors:@[sortDescriptor]];

}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [countriesList count];
}

#pragma mark - CZPickerView Delegate and Datasource

//- (NSAttributedString *)czpickerView:(CZPickerView *)pickerView
//               attributedTitleForRow:(NSInteger)row{
//    
//     NSString *countryName = [[countriesList objectAtIndex:row]valueForKey:kCountryName];
//     NSString *dialingCode = [[countriesList objectAtIndex:row]valueForKey:kCountryCallingCode];
//    
//    NSAttributedString *att = [[NSAttributedString alloc]
//                               initWithString:[NSString stringWithFormat:@"%@  %@",countryName,dialingCode]
//                               attributes:@{
//                                            NSFontAttributeName:[UIFont fontWithName:@"Avenir-Light" size:15.0]
//                                            }];
//    return att;
//}

//- (NSString *)czpickerView:(CZPickerView *)pickerView
//               titleForRow:(NSInteger)row{
//    
//    return [[countriesList objectAtIndex:row] valueForKey:kCountryName];
//}

//- (NSInteger)numberOfRowsInPickerView:(CZPickerView *)pickerView{
//    return [countriesList count];
//}

//- (void)czpickerViewDidClickCancelButton:(CZPickerView *)pickerView{
//    
//}

//- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemAtRow:(NSInteger)row{
//    
//    NSLog(@"%@ is chosen!",[[countriesList objectAtIndex:row]valueForKey:kCountryCallingCode]);
//
//    dialingCode = [[[[countriesList objectAtIndex:row]valueForKey:kCountryCallingCode] componentsSeparatedByCharactersInSet:
//                            [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
//                           componentsJoinedByString:@""];
//    
//    NSLog(@"%@ is chosen!",dialingCode);
//    
//     countryName = [[countriesList objectAtIndex:row]valueForKey:kCountryName];
//    
////    self.txtCountryCode.text = [NSString stringWithFormat:@"%@  (+%@)",countryName,dialingCode];
//    self.txtCountryCode.text = [NSString stringWithFormat:@"%@",countryName];
//    self.lblCountryCode.text = [NSString stringWithFormat:@"%@",dialingCode];
//
//}

#pragma  mark - textfield delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {

    if (textField == self.txtMobileNumber) {
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        
        if (newLength == 10) {
            
            self.btnDone.userInteractionEnabled = true;
            self.btnDone.titleLabel.textColor = [UIColor whiteColor];
        }
        else
        {
            self.btnDone.userInteractionEnabled = false;
            self.btnDone.titleLabel.textColor = [UIColor grayColor];
        }
        //return newLength <= 10;
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if (textField == self.txtMobileNumber) {
        
        [self btnDoneClicked:nil];
    }
    return YES;
}

#pragma  mark - methods for dropDown

-(void)showPopUpWithTitle:(NSString*)popupTitle withOption:(NSArray*)arrOptions xy:(CGPoint)point size:(CGSize)size isMultiple:(BOOL)isMultiple{
    
    
    Dropobj = [[DropDownListView alloc] initWithTitle:popupTitle options:arrOptions xy:point size:size isMultiple:isMultiple];
    Dropobj.delegate = self;
    [Dropobj showInView:self.view animated:YES];
    
    /*----------------Set DropDown backGroundColor-----------------*/

    Dropobj.backgroundColor = [UIColor colorWithRed:64.0/255.0 green:88.0/255.0 blue:122.0/255.0 alpha:0.9];
    
}
- (void)DropDownListView:(DropDownListView *)dropdownListView didSelectedIndex:(NSInteger)anIndex{
    /*----------------Get Selected Value[Single selection]-----------------*/
    
    dialingCode = [[[[countriesList objectAtIndex:anIndex]valueForKey:kCountryCallingCode] componentsSeparatedByCharactersInSet:
                    [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                   componentsJoinedByString:@""];
    
    NSLog(@"%@ is chosen!",dialingCode);
    
    countryName = [[countriesList objectAtIndex:anIndex]valueForKey:kCountryName];
    
    //self.txtCountryCode.text = [NSString stringWithFormat:@"%@  (+%@)",countryName,dialingCode];
    self.txtCountryCode.text = [NSString stringWithFormat:@"%@",countryName];
    self.lblCountryCode.text = [NSString stringWithFormat:@"+%@",dialingCode];
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [Dropobj fadeOut];
}
@end
