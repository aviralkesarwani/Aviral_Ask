//
//  CreateNewAskVC.h
//  My Asks
//
//  Created by artis on 31/03/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <KVNProgress.h>
#import "MAChatScreenVC.h"
#import "BIZPopupViewController.h"
#import "AddLocationVC.h"
#import "MAInviteFriendsVC.h"
#import "AddLocationVC.h"
#import "APIClient.h"


@interface CreateNewAskVC : UIViewController <UITableViewDelegate,UITableViewDataSource,UIImagePickerControllerDelegate,UINavigationControllerDelegate,MAInviteFriendsDelegate,MAAddLocationsDelegate>

@property (strong, nonatomic) IBOutlet UIView *TopTabBarView;

@property (weak, nonatomic) IBOutlet UIButton *btnPlaces;
@property (weak, nonatomic) IBOutlet UIButton *btnChat;
@property (weak, nonatomic) IBOutlet UIButton *btnAddCoverPhoto;
@property (weak, nonatomic) IBOutlet UIButton *btnAddLocations;
@property (weak, nonatomic) IBOutlet UIButton *btnInviteFriends;
@property (weak, nonatomic) IBOutlet UIButton *btnCreateAsk;
@property (weak, nonatomic) IBOutlet UIButton *btnInviteMoreFriends;
@property (weak, nonatomic) IBOutlet UIButton *btnAddMoreLocation;

@property (weak, nonatomic) IBOutlet UIImageView *imgTabBackground;
@property (weak, nonatomic) IBOutlet UIImageView *imgCoverPhoto;

@property (weak, nonatomic) IBOutlet UITableView *tblAddContacts;
@property (weak, nonatomic) IBOutlet UITableView *tblAddLocations;

@property (weak, nonatomic) IBOutlet UIView *addContactsView;
@property (weak, nonatomic) IBOutlet UIView *addLocationsView;
@property (weak, nonatomic) IBOutlet UIView *topTabBarContainView;
@property (weak, nonatomic) IBOutlet UITextField *txtQuestion;

@property (strong, nonatomic) NSMutableArray *contactsArray;
@property (strong, nonatomic) NSMutableArray *locationsArray;



- (IBAction)btnPlacesClicked:(id)sender;
- (IBAction)btnChatClicked:(id)sender;
- (IBAction)btnBackClicked:(id)sender;

- (IBAction)btnAddLocationClicked:(id)sender;
- (IBAction)btnInviteFriendsClicked:(id)sender;
- (IBAction)btnAddCoverPhotoClicked:(id)sender;
- (IBAction)btnInviteMoreFriendsClicked:(id)sender;
- (IBAction)btnCreateAskClicked:(id)sender;


@end
