//
//  SelectingFriendsVC.h
//  My Asks
//
//  Created by MAC on 08/07/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Contacts/Contacts.h>
#import "MAInviteFriendsModel.h"
#import "BIZPopupViewController.h"
#import "UIImageView+Letters.h"
#import "AppData.h"
#import <CZPicker/CZPicker.h>
#import "AskingFriendsTableViewCell.h"
#import "MAInviteFriendsTableCell.h"
#import "MAAddContactVC.h"


@protocol SelectingFriendsDelegate <NSObject>

@required
- (void)inviteFriends:(NSMutableArray *)contactsArray;
@end


@interface SelectingFriendsVC : UIViewController<UITableViewDataSource,UITableViewDelegate,UITextFieldDelegate,UIScrollViewDelegate,MAAddContactVCDelegate>
{
    BOOL isInviteAll;
    
    CNContactStore *contactsStrore;
    NSMutableArray *rexNonRexContactList;
    NSMutableArray *rexContactList;
    NSMutableArray *nonRexContactList;

}
@property (weak, nonatomic) IBOutlet UIButton *allowContactsBtn;
@property (weak, nonatomic) IBOutlet UILabel *contactsMessageLbl;
@property (nonatomic, assign) BOOL contactsAccessed;
@property (strong, nonatomic) NSMutableArray *searchContactArray;
@property (assign, nonatomic) BOOL isEdit;

@property (assign, nonatomic) BOOL isSearching;

@property (nonatomic, assign) id<SelectingFriendsDelegate> delegate;

@property (weak, nonatomic) IBOutlet UIView *noContactsView;
@property (weak, nonatomic) IBOutlet UITextField *txtSearchContacts;
@property (weak, nonatomic) IBOutlet UIImageView *imgTabBackground;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tabBarHeight;
@property (strong, nonatomic) IBOutlet UITableView *rexTableView;
@property (strong, nonatomic) IBOutlet UITableView *nonRexTableView;
@property (weak, nonatomic) IBOutlet UIPageControl *askPageController;
@property (weak, nonatomic) IBOutlet UIScrollView *askScrollView;

@property (strong, nonatomic) NSMutableArray *selectedContactsArray;
@property (strong, nonatomic) UIRefreshControl *refreshControlForRex;
@property (strong, nonatomic) UIRefreshControl *refreshControlForNonRex;

- (IBAction)btnCancelClicked:(id)sender;
- (IBAction)btnDoneClicked:(id)sender;

@end
