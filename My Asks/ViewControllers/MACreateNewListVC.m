//
//  MACreateNewListVC.m
//  My Asks
//
//  Created by MY PC on 4/19/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "MACreateNewListVC.h"

@interface MACreateNewListVC ()


@end

@implementation MACreateNewListVC
{
    int sec;
    int def;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.txtSearchPlaces addTarget:self
                             action:@selector(textFieldDidChange:)
                   forControlEvents:UIControlEventEditingChanged];
    
    [self.switchSecret addTarget:self action:@selector(secretSwitchToggled:) forControlEvents: UIControlEventTouchUpInside];
    [self.switchDefault addTarget:self action:@selector(defaultSwitchToggled:) forControlEvents: UIControlEventTouchUpInside];
    
    self.userPlacesArray = [[NSMutableArray alloc] init];
    self.searchPlacesArray = [[NSMutableArray alloc] init];
    
    // Do any additional setup after loading the view.
    self.tblPlaceList.delegate = self;
    self.tblPlaceList.dataSource = self;
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    
    self.isSearching = NO;
    if([self.listType isEqualToString: @"Edit List"]) {
        
        self.txtListName.text = [NSString stringWithFormat:@"%@",self.userlistData.Name];
        self.lblListType.text = @"Edit List";
        if(self.userlistData.IsPublic) {
            
            [self.switchSecret setOn:YES animated:YES];
            sec = 1;
        }
        else {
            
            [self.switchSecret setOn:NO animated:YES];
            sec = 0;
        }
        if(self.userlistData.IsDefault) {
            
            [self.switchDefault setOn:YES animated:YES];
            def = 1;
        }
        else {
            
            [self.switchDefault setOn:NO animated:YES];
            def = 0;
        }
    }
    else {
        
        sec = 0;
        def = 0;
    }
    
    [self getPlacesList];
}

- (void) secretSwitchToggled:(id)sender {
    
    if ([self.switchSecret isOn]) {
        
        sec = 1;
        NSLog(@"%d",sec);
    }
    else {
        
        sec = 0;
        NSLog(@"%d",sec);
    }
}

- (void) defaultSwitchToggled:(id)sender {
    
    if ([self.switchDefault isOn]) {
        
        def = 1;
        NSLog(@"%d",def);
    }
    else {
        
        def = 0;
        NSLog(@"%d",def);
    }
}

-(void)getPlacesList{
    
    [self.txtSearchPlaces resignFirstResponder];
    if(self.isSearching == YES) {
        
        [self.searchPlacesArray removeAllObjects];
    }
    NSMutableDictionary *param =[[NSMutableDictionary alloc]init];
    [param setValue:[AppData sharedManager].latitude forKey:@"Latitude"];
    [param setValue:[AppData sharedManager].longitude forKey:@"Longitude"];
    [param setValue:@"2000" forKey:@"Radius"];
    [param setValue:[AppData sharedManager].userID forKey:@"UserID"];
    [param setValue:self.txtSearchPlaces.text forKey:@"SearchText"];
    
//    [KVNProgress show];
    [AppData startNetworkIndicator];
    
    [[APIClient sharedManager]getSearchPlaces:param onCompletion:^(NSArray *resultData, bool success) {
        
//        [KVNProgress dismiss];
        [AppData stopNetworkIndicator];

        
        if (success) {
            
            if ([resultData count] == 0) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
                                                                message:@"No places to display"
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                
            }
            else{
                
                NSMutableArray *userPlacesArrayTemp = [[NSMutableArray alloc] init];
                
                for (NSDictionary *places in resultData) {
                    
                    MAListOrPlaceSearchModel *placesModel = [[MAListOrPlaceSearchModel alloc]init];
                    
                    placesModel.UserID = [places valueForKey:@"UserID"];
                    placesModel.PlaceLatitude = [places valueForKey:@"PlaceLatitude"];
                    placesModel.PlaceLongitude = [places valueForKey:@"PlaceLongitude"];
                    placesModel.PlaceTypes = [places valueForKey:@"PlaceTypes"];
                    placesModel.PlaceName = [places valueForKey:@"PlaceName"];
                    placesModel.PlaceId = [places valueForKey:@"PlaceId"];
                    placesModel.DistanceinMiles = [places valueForKey:@"DistanceinMiles"];
                    placesModel.DistanceinKMs = [places valueForKey:@"DistanceinKMs"];
                    placesModel.isFavorite = [[places valueForKey:@"isFavorite"]boolValue];
                    placesModel.LikesCount = [places valueForKey:@"LikesCount"];
                    placesModel.CommentsCount = [places valueForKey:@"CommentsCount"];
                    placesModel.PlaceAddress = [places valueForKey:@"PlaceAddress"];
                    placesModel.PlaceImage = [places valueForKey:@"PlaceImage"];
                    placesModel.PlaceImageURL = [places valueForKey:@"PlaceImageURL"];
                    placesModel.InternationalPhoneNumber = [places valueForKey:@"InternationalPhoneNumber"];
                    placesModel.OpeningHours = [places valueForKey:@"OpeningHours"];
                    
                    NSLog(@"%@",placesModel);
                    
                    if([self.listType isEqualToString: @"Edit List"]) {
                        
                        for (MAUserListsPlacesModel *userListsPlacesModel in self.userlistData.Places) {
                        
                            if ([userListsPlacesModel.PlaceID isEqualToString: [places valueForKey:@"PlaceId"]]) {
                            
                                placesModel.isSelected = YES;
                            }
                        }
                    }
                    else if([self.listType isEqualToString:@"Create List with Place"]) {
                        
                        if ([self.placeIdForAddPlaceToList isEqualToString: [places valueForKey:@"PlaceId"]]) {
                                
                                placesModel.isSelected = YES;
                        }
                    }
                    else {
                        
                        placesModel.isSelected = NO;
                    }
                    
                    if(self.isSearching == YES) {
                        
                        for (MAListOrPlaceSearchModel *userListsPlacesModel in self.userPlacesArray) {
                            
                            if ([userListsPlacesModel.PlaceId isEqualToString: placesModel.PlaceId]) {
                                
                                placesModel.isSelected = YES;
                            }
                        }
                        if (placesModel.isSelected) {
                            
                        }
                        else {
                     
                            [self.searchPlacesArray addObject:placesModel];
                        }
                    }
                    else {
                        
                        if(placesModel.isSelected) {
                            
                            [self.userPlacesArray addObject:placesModel];
                        }
                        else {
                            
                            [userPlacesArrayTemp addObject:placesModel];
                        }
                    }
                }
                if(self.isSearching != YES) {
                    
                    [self.userPlacesArray removeAllObjects];
                    [self.userPlacesArray addObjectsFromArray:userPlacesArrayTemp];
                }
                [self.tblPlaceList reloadData];
            }
        }
    }];
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSPredicate *resultPredicate = [NSPredicate
                                    predicateWithFormat:@"PlaceName contains[cd] %@",
                                    searchText];
    NSArray *filterArray;
    
    filterArray = [self.userPlacesArray filteredArrayUsingPredicate:resultPredicate];
    
    self.searchPlacesArray = [NSMutableArray arrayWithArray:filterArray];
}


- (void)textFieldDidChange:(UITextField *)textField
{
    
    if (textField.text.length > 0) {
        
        self.isSearching = YES;
        [self getPlacesList];
    }
    else {
        self.isSearching = NO;
        
        [textField performSelector:@selector(resignFirstResponder) withObject:nil afterDelay:0];
        
    }
    [self.tblPlaceList reloadData];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return true;
}

-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    textField.text = @"";
    [textField resignFirstResponder];
    self.isSearching = NO;
    [self.tblPlaceList reloadData];
    return NO;
}

#pragma mark - TableView Delegate method

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(self.isSearching == YES) {
        
        return [self.searchPlacesArray count];
    }
    else {
    
        return [self.userPlacesArray count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MACreateNewListCell * listsCell = [tableView dequeueReusableCellWithIdentifier:@"MACreateNewListCell" forIndexPath: indexPath];
    
    MAListOrPlaceSearchModel *listOrPlaceSearchModel;
    if(self.isSearching == YES) {
        
        listOrPlaceSearchModel = [self.searchPlacesArray objectAtIndex:indexPath.row];
    }
    else {
        
        listOrPlaceSearchModel = [self.userPlacesArray objectAtIndex:indexPath.row];
    }
    
    [listsCell.btnAddSelect addTarget:self action:@selector(btnAddSelectClicked:) forControlEvents:UIControlEventTouchUpInside];
    listsCell.btnAddSelect.tag = indexPath.row;
    
//    if (listOrPlaceSearchModel.isFavorite) {
//        [listsCell.btnAddSelect setBackgroundImage:[UIImage imageNamed:@"Checkbox Selected.png"] forState:UIControlStateNormal];
//    }
//    else{
//         [listsCell.btnAddSelect setBackgroundImage:[UIImage imageNamed:@"Add to list plus.png"] forState:UIControlStateNormal];
//    }
    
    if (listOrPlaceSearchModel.isSelected) {
        
        [listsCell.btnAddSelect setImage:[UIImage imageNamed:@"Checkbox Selected"] forState:UIControlStateNormal];
    }
    else {
        
        [listsCell.btnAddSelect setImage:[UIImage imageNamed:@"Plus top bar"] forState:UIControlStateNormal];
    }

    if(listOrPlaceSearchModel.PlaceName == (id)[NSNull null]) {
        
        listsCell.lblPlaceName.text = @"";
    }
    else {
        
        listsCell.lblPlaceName.text = [NSString stringWithFormat:@"%@", listOrPlaceSearchModel.PlaceName];
    }
    if(listOrPlaceSearchModel.PlaceAddress == (id)[NSNull null]) {
        
        listsCell.lblAddress.text = @"";
    }
    else {
        
        listsCell.lblAddress.text = [NSString stringWithFormat:@"%@", listOrPlaceSearchModel.PlaceAddress];
    }
    if(listOrPlaceSearchModel.DistanceinMiles == (id)[NSNull null]) {
        
        listsCell.lblMiles.text = @"";
    }
    else {
        
        float distanceMiles = [listOrPlaceSearchModel.DistanceinMiles floatValue];
        NSString *str = [NSString stringWithFormat:@"%.2f mi",distanceMiles];
        listsCell.lblMiles.text = [NSString stringWithFormat:@"%@", str];
    }
    if (listOrPlaceSearchModel.PlaceImageURL == (id)[NSNull null]) {
        
        listsCell.imgPlacePic.image = [UIImage imageNamed:@""];
    }
    else{
        
        listsCell.imgPlacePic.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@",listOrPlaceSearchModel.PlaceImageURL]];
    }
    return listsCell;   
}

#pragma mark - Button action


- (void)btnAddSelectClicked:(UIButton*)sender{
    
    NSInteger index = sender.tag;
    
    MAListOrPlaceSearchModel *listOrPlaceSearchModel;
    if(self.isSearching == YES) {
        
        listOrPlaceSearchModel = [self.searchPlacesArray objectAtIndex:index];
    }
    else {
        
        listOrPlaceSearchModel = [self.userPlacesArray objectAtIndex:index];
    }
    
    if (listOrPlaceSearchModel.isSelected) {
        
        [sender setImage:[UIImage imageNamed:@"Plus top bar"] forState:UIControlStateNormal];
        listOrPlaceSearchModel.isSelected = NO;
        for (MAListOrPlaceSearchModel *places in self.userPlacesArray) {
            
            if ([listOrPlaceSearchModel.PlaceId isEqualToString: places.PlaceId]) {
                
                places.isSelected = NO;
            }
        }
    }
    else {
        
        if(self.isSearching == YES) {
            
            [self.userPlacesArray insertObject:listOrPlaceSearchModel atIndex:0];
//            [self.userPlacesArray addObject:listOrPlaceSearchModel];
        }
        [sender setImage:[UIImage imageNamed:@"Checkbox Selected"] forState:UIControlStateNormal];
        listOrPlaceSearchModel.isSelected = YES;
        for (MAListOrPlaceSearchModel *places in self.userPlacesArray) {
            
            if ([listOrPlaceSearchModel.PlaceId isEqualToString: places.PlaceId]) {
                
                places.isSelected = YES;
            }
        }
    }
}

- (IBAction)btnBackClicked:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnDoneClicked:(id)sender {

    NSString *value = [self.txtListName.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    if(value.length > 0) {
        [KVNProgress show];

        NSMutableArray *selectedPlacesArray = [[NSMutableArray alloc] init];
        
        for (MAListOrPlaceSearchModel *places in self.userPlacesArray) {
            
            if(places.isSelected) {

                NSMutableDictionary *paramPlace =[[NSMutableDictionary alloc]init];
                [paramPlace setValue:places.PlaceName forKey:@"Name"];
                [paramPlace setValue:places.PlaceLatitude forKey:@"Latitude"];
                [paramPlace setValue:places.PlaceLongitude forKey:@"Longitude"];
                NSDictionary *askDict = @{@"Name":places.PlaceName,@"Latitude":places.PlaceLatitude,@"Longitude":places.PlaceLongitude};
                [selectedPlacesArray addObject:askDict];
            }
        }
        NSDictionary *params;
        if([self.listType isEqualToString: @"Edit List"]) {
            
            params = @{@"UserID":[AppData sharedManager].userID,@"UserListID":self.userlistData.UserListID,@"Name":self.txtListName.text,@"IsPublic":[NSNumber numberWithInt:sec],@"IsDefault":[NSNumber numberWithInt:def],@"Places":selectedPlacesArray};
        }
        else {
            
            params = @{@"UserID":[AppData sharedManager].userID,@"Name":self.txtListName.text,@"IsPublic":[NSNumber numberWithInt:sec],@"IsDefault":[NSNumber numberWithInt:def],@"Places":selectedPlacesArray};
        }
        
        [[APIClient sharedManager]insertUpdateUserList:params onCompletion:^(NSDictionary *resultData, bool success) {
            [KVNProgress dismiss];
            NSLog(@"%@",resultData);
            if (success) {
                NSLog(@"%@",resultData);
                
                
                if([self.listType isEqualToString:@"Create List with Place"]) {
                    
                    [self.delegate doneAdding:self.placeIdForAddPlaceToList];
                }
                [self.navigationController popViewControllerAnimated:YES];
            }
            else {
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error"
                                                                message:@"Failed to create list"
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
            
        }];
    }
    else {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Warning"
                                                        message:@"Please add List Name"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}
@end
