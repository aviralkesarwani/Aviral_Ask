//
//  CreateNewAskVC.m
//  My Asks
//
//  Created by artis on 31/03/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//


#import "CreateNewAskVC.h"
#import "MACreateAskInviteContactsCell.h"
#import "MACreateAskAddLocationsCell.h"
#import "MAInviteFriendsVC.h"
#import "AppData.h"

@interface CreateNewAskVC () 

@end

@implementation CreateNewAskVC

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.TopTabBarView = [[[NSBundle mainBundle] loadNibNamed:@"NewAsk_Chat_Places" owner:self options:nil] firstObject];
    [self.topTabBarContainView addSubview:self.TopTabBarView];
    
    [self.btnInviteFriends setHidden:NO];
    [self.btnAddLocations setHidden:YES];
    [self.btnInviteMoreFriends setHidden:NO];
   
    self.contactsArray = [[NSMutableArray alloc]init];
    self.locationsArray = [[NSMutableArray alloc]init];
    
    
    if ([self.contactsArray count] == 0) {
        
        [self.btnInviteFriends setHidden:NO];
        [self.btnAddLocations setHidden:YES];
        [self.btnInviteMoreFriends setHidden:YES];
        [self.addLocationsView setHidden:YES];
        [self.addContactsView setHidden:YES];
        [self.btnAddMoreLocation setHidden:YES];
    }
    else{
        
        [self.addContactsView setHidden:NO];
        [self.btnInviteMoreFriends setHidden:NO];
        [self.btnAddLocations setHidden:YES];
        [self.addLocationsView setHidden:YES];
        [self.btnAddMoreLocation setHidden:YES];
        
        [self.tblAddContacts reloadData];
    }
    
}

- (void)didReceiveMemoryWarning {
    
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Button Actions

- (IBAction)btnChatClicked:(id)sender {
    
    self.imgTabBackground.image = [UIImage imageNamed:@"Tab 1"];
    
    if ([self.contactsArray count] == 0) {
        
        [self.btnInviteFriends setHidden:NO];
        [self.btnAddLocations setHidden:YES];
        [self.btnInviteMoreFriends setHidden:YES];
        [self.addLocationsView setHidden:YES];
        [self.addContactsView setHidden:YES];
        [self.btnAddMoreLocation setHidden:YES];
    }
    else{
        [self.btnInviteMoreFriends setHidden:NO];
        [self.btnAddLocations setHidden:YES];
        [self.addLocationsView setHidden:YES];
        [self.addContactsView setHidden:NO];
        [self.btnAddMoreLocation setHidden:YES];
        
        [self.tblAddContacts reloadData];
    }
    
}

- (IBAction)btnPlacesClicked:(id)sender {
    
     self.imgTabBackground.image = [UIImage imageNamed:@"Tab 2"];
    
    
    if ([self.locationsArray count]==0) {
        
        [self.btnInviteFriends setHidden:YES];
        [self.btnAddLocations setHidden:NO];
        [self.btnInviteMoreFriends setHidden:YES];
        [self.addContactsView setHidden:YES];
        [self.addLocationsView setHidden:YES];
        [self.btnAddMoreLocation setHidden:YES];

    }
    else{
        
        [self.btnInviteMoreFriends setHidden:YES];
        [self.btnAddLocations setHidden:YES];
        [self.addContactsView setHidden:YES];
        [self.addContactsView setHidden:YES];
        [self.addLocationsView setHidden:NO];
        [self.btnAddMoreLocation setHidden:NO];
        
        [self.tblAddLocations reloadData];
    
    }
    
}

- (IBAction)btnAddCoverPhotoClicked:(id)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
}

- (IBAction)btnBackClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnAddLocationClicked:(id)sender {
    
    AddLocationVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"AddLocationVC"];
    [self.navigationController pushViewController:vc animated:YES];
    
    vc.delegate = self;
    
}

- (IBAction)btnInviteFriendsClicked:(id)sender {
    
    MAInviteFriendsVC *contributeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MAInviteFriendsVC"];
    CGRect fullScreenRect = [[UIScreen mainScreen] bounds];
    
    contributeViewController.delegate = self;
    
//    BIZPopupViewController *popupViewController = [[BIZPopupViewController alloc] initWithContentViewController:contributeViewController contentSize:CGSizeMake((fullScreenRect.size.width - 40), (fullScreenRect.size.height - 60))];
    
    BIZPopupViewController *popupViewController = [[BIZPopupViewController alloc] initWithContentViewController:contributeViewController contentSize:CGSizeMake(fullScreenRect.size.width , fullScreenRect.size.height)];
    popupViewController.showDismissButton = NO;

    [self presentViewController:popupViewController animated:NO completion:nil];
}

- (IBAction)btnInviteMoreFriendsClicked:(id)sender {
    
    MAInviteFriendsVC *contributeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MAInviteFriendsVC"];
    CGRect fullScreenRect = [[UIScreen mainScreen] bounds];
    
    contributeViewController.delegate = self;
    
    
    if ([self.contactsArray count] == 0) {
        contributeViewController.isEdit = false;
        [[AppData sharedManager].invitedFriendsGlobalArray removeAllObjects];
    }
    else {
        contributeViewController.isEdit = true;
    }
    
    
    
    BIZPopupViewController *popupViewController = [[BIZPopupViewController alloc] initWithContentViewController:contributeViewController contentSize:CGSizeMake(fullScreenRect.size.width , fullScreenRect.size.height)];
    popupViewController.showDismissButton = NO;
    
    [self presentViewController:popupViewController animated:NO completion:nil];
}

- (IBAction)btnCreateAskClicked:(id)sender {
    [self saveBackgroundImage];
}

#pragma mark - image picker Delagate/Datasource

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.imgCoverPhoto.image = chosenImage;
    [self.btnAddCoverPhoto setTitle:@"Change cover photo" forState:UIControlStateNormal];
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark - Tableview Delegate/Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    
    if (tableView == self.tblAddContacts) {

        return [self.contactsArray count];
    }
    else{
        
        return [self.locationsArray count];
    }

    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    if (tableView == self.tblAddContacts) {
        
        MACreateAskInviteContactsCell *inviteContactsCell = [tableView dequeueReusableCellWithIdentifier:@"MACreateAskInviteContactsCell" forIndexPath: indexPath];
        
         tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
        
        inviteContactsCell.lblContactName.text = [self.contactsArray[indexPath.row]valueForKeyPath:@"contactFullName"];
        inviteContactsCell.imgContactImage.image = [self.contactsArray[indexPath.row]valueForKeyPath:@"contactImage"];
        
        inviteContactsCell.imgContactImage.clipsToBounds = YES;
        inviteContactsCell.imgContactImage.layer.cornerRadius =   inviteContactsCell.imgContactImage.frame.size.width/2.0;
        
        inviteContactsCell.btnRemoveContact.tag = indexPath.row;
        
        [inviteContactsCell.btnRemoveContact addTarget:self action:@selector(btnRemoveContact:) forControlEvents:UIControlEventTouchUpInside];
        
        return  inviteContactsCell;
    }
    else{
        
        MACreateAskAddLocationsCell *addLocationsCell = [tableView dequeueReusableCellWithIdentifier:@"MACreateAskAddLocationsCell" forIndexPath: indexPath];
        tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
        addLocationsCell.lblPlaceName.text = [self.locationsArray[indexPath.row]valueForKeyPath:@"placeName"];
        addLocationsCell.lblPlaceDetails.text = [self.locationsArray[indexPath.row]valueForKeyPath:@"placeAddress"];
    
        return  addLocationsCell;
    }
    
}

#pragma mark - InviteFriends button clicked delagate

-(void)inviteFriends:(NSMutableArray *)contactsArray{
    
    [self.contactsArray removeAllObjects];
    for (MAInviteFriendsModel *model in contactsArray) {
        
//        if ([self.contactsArray containsObject:model]) {
//            NSLog(@"%@",contactsArray);
//        }
//        else{
        
        [self.contactsArray addObject:model];
//        }
    }
    
    
    NSLog(@"%@",self.contactsArray);

    if ([self.contactsArray count] == 0 ) {
        
        [self.btnInviteFriends setHidden:NO];
        [self.btnAddLocations setHidden:YES];
        [self.btnInviteMoreFriends setHidden:YES];
        [self.addLocationsView setHidden:YES];
        [self.addContactsView setHidden:YES];
        [self.btnAddMoreLocation setHidden:YES];
    }
    else{
        
        [self.btnInviteFriends setHidden:YES];
        [self.btnInviteMoreFriends setHidden:NO];
        [self.btnAddLocations setHidden:YES];
        [self.addLocationsView setHidden:YES];
        [self.addContactsView setHidden:NO];
        [self.btnAddMoreLocation setHidden:YES];
        
        [self.tblAddContacts reloadData];
        
       }
}

#pragma mark - AddLocations button clicked delagate

-(void)addLocations:locationsArray{
    
    NSLog(@"%@",self.locationsArray);
    
     NSLog(@"%@",locationsArray);
    
    for (MALocationModel *model in locationsArray ) {
        
        [self.locationsArray addObject:model];
    }
    
    NSLog(@"%@",self.locationsArray);
    
    if ([self.locationsArray count]==0) {
        
        [self.btnInviteFriends setHidden:YES];
        [self.btnAddLocations setHidden:NO];
        [self.btnInviteMoreFriends setHidden:YES];
        [self.addContactsView setHidden:YES];
        [self.addLocationsView setHidden:YES];
        [self.btnAddMoreLocation setHidden:YES];
        
    }
    else{
        
        [self.btnInviteMoreFriends setHidden:YES];
        [self.btnAddLocations setHidden:YES];
        [self.addContactsView setHidden:YES];
        [self.addLocationsView setHidden:NO];
        [self.btnAddMoreLocation setHidden:NO];
        
        [self.tblAddLocations reloadData];
        
    }
    
}
-(void)btnRemoveContact:(UIButton *)sender{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
    
    [self.contactsArray removeObjectAtIndex:sender.tag];
    
    [self.tblAddContacts deleteRowsAtIndexPaths: [NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    
    [self.tblAddContacts reloadData];
}

#pragma mark - Custom Methods

- (void) saveBackgroundImage {
   
    
//    if ( self.imgCoverPhoto.image == nil) {
//        
//        [KVNProgress dismiss];
//        
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Sorry"
//                                                        message:@"Please add cover photo"
//                                                       delegate:self
//                                              cancelButtonTitle:@"OK"
//                                              otherButtonTitles:nil];
//        [alert show];
//    }
     if (self.txtQuestion.text.length == 0){
        
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"MyAsks"
                                                        message:@"What are you asking for ?"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];

    }
    else{
        if (self.imgCoverPhoto.image == nil) {
            
            self.imgCoverPhoto.image = [UIImage imageNamed:@"background-8"];
        }
        
        UIImage *img = self.imgCoverPhoto.image;
        
        img = [[AppData sharedManager]compressImage:img];
        [KVNProgress show];

        [[APIClient sharedManager] saveASKBackgoundImage:img onCompletion:^(NSDictionary *resultData, bool success) {
            [KVNProgress dismiss];

            if (success) {
                NSString *imageName = [resultData valueForKey:@"Request_ID"];
                
                [self createAskWithImageName:imageName];
            }
        }];
    }
}

-(void)createAskWithImageName :(NSString * ) imageName {
    
    NSArray *invitedContacts = [[NSArray alloc]initWithArray:[self filteredContacts]];
    
    NSMutableArray *askDetails = [[NSMutableArray alloc] init];
    
    for (MALocationModel *locationModel in self.locationsArray){
        
        NSString *latitude = locationModel.latitude;
        NSString *longitude = locationModel.longitude;
        NSString *placeId = locationModel.placeID;
        NSString *placeName = locationModel.placeName;
        NSString *placeAddress = locationModel.placeAddress;
        
        NSDictionary *askDict = @{@"PlaceId":placeId,@"PlaceName":placeName,@"PlaceAddress":placeAddress,@"Latitude":latitude,@"Longitude":longitude, @"AskId":@""};
        
        NSLog(@"%@",askDict);
        
        [askDetails addObject:askDict];
    }
    
    NSDictionary *param = @{@"Question":self.txtQuestion.text,@"UserId":[AppData sharedManager].userID,@"BackgroundImage":imageName,@"InvitedContacts":invitedContacts,@"AskDetails":askDetails};
    
    [AppData startNetworkIndicator];

    [[APIClient sharedManager] createAsk:param onCompletion:^(NSDictionary *resultData, bool success) {
        
        NSLog(@"%@",resultData);
//        [KVNProgress dismiss];
        
        [AppData stopNetworkIndicator];

        if (success) {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"MyAsks"
//                                                            message:@"Ask successfully created"
//                                                           delegate:self
//                                                  cancelButtonTitle:@"OK"
//                                                  otherButtonTitles:nil];
//            [alert show];
            
            
            [[AppData sharedManager].invitedFriendsGlobalArray removeAllObjects];
//            NSArray *requestId =    [[resultData valueForKey:@"Request_ID"]componentsSeparatedByString:@"-"];
//            NSString *askId = [requestId firstObject];
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"" message:@"Ask created successfully" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
               
                self.txtQuestion.text = @"";
                self.imgCoverPhoto.image = [UIImage imageNamed:@""];
                self.contactsArray = [[NSMutableArray alloc]init];
                self.locationsArray = [[NSMutableArray alloc]init];
                [self.tblAddContacts reloadData];
                [self.tblAddLocations reloadData];
                
//                MAChatScreenVC *chatScreenVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MAChatScreenVC"];
//                chatScreenVC.askId = askId;
                //    chatScreenVC.userAskData = askDetailModel;
                //[self.navigationController pushViewController:chatScreenVC animated:true];
                
                [self.navigationController popViewControllerAnimated:true];
            }];
            
            [alert addAction:okAction];
            [self presentViewController:alert animated:true completion:nil];
        }
    }];
}

-(NSMutableArray *)filteredContacts{
    
//    NSArray *phoneNumberArray = [[AppData sharedManager].ContactsArray valueForKey:@"arrPhoneNumbers"];
    
    NSMutableArray *filteredArray = [[NSMutableArray alloc]init];
//
//    for (NSArray *phoneNumber in phoneNumberArray) {
//        
//        if (phoneNumber.count > 0) {
//            NSString *number = [phoneNumber firstObject];
//            NSLog(@"%@",number);
//            
//            [filteredArray addObject:number];
//        }
//    }

    NSMutableArray *contactArray = self.contactsArray;
    
    for (MAInviteFriendsModel *inviteFriend in contactArray) {
        
        NSString *phoneNumber = inviteFriend.selectedPhoneNumber;
//        if (phoneNumber.length == 10) {
//            
//            NSString *countryCode = [[AppData sharedManager]getCurrentCountryCode];
//            phoneNumber = [NSString stringWithFormat:@"%@%@",countryCode,phoneNumber];
//        }
        
        [filteredArray addObject:phoneNumber];
    }
    
    NSLog(@"%@",filteredArray);
    
    return filteredArray;
}
@end
