//
//  MAChatScreenVC.h
//  My Asks
//
//  Created by Mac on 31/03/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuickBlox/Quickblox.h>
#import <KVNProgress.h>

#import "AppDelegate.h"
#import "NewAskScreenVC.h"

@interface MAChatScreenVC : UIViewController 

@property (strong, nonatomic) QBChatDialog *selectedChatDialod;
@property (strong, nonatomic) NSMutableArray *chatPlaceArray;

-(void)fetchDialogDetailFromRexServer:(void(^)())completionBlock;

@end
