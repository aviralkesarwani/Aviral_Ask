//
//  MAMapVC.h
//  My Asks
//
//  Created by artis on 17/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <PSLocation/PSlocation.h>
#import "AppData.h"
#import "MAMapVC.h"
#import "MVPlaceSearchTextField.h"

@protocol MAMapViewDelegate <NSObject>

@required
- (void)getLocationFromMap : (NSDictionary *) location;
@end

@interface MAMapVC : UIViewController <GMSMapViewDelegate,MKMapViewDelegate,CLLocationManagerDelegate,UIGestureRecognizerDelegate,PlaceSearchTextFieldDelegate>

//@property (retain ,nonatomic) CLLocationManager *locationManager;
@property (strong,nonatomic) PSLocationManager *pslocationManager;

@property (weak, nonatomic) IBOutlet UIView *autoCompleteView;
@property (weak, nonatomic) IBOutlet GMSMapView *googleMapView;
@property (strong, nonatomic) id<MAMapViewDelegate> delegate;
@property (weak, nonatomic) IBOutlet MVPlaceSearchTextField *placeSearchTextField;

- (IBAction)btnBackClicked:(id)sender;

@end
