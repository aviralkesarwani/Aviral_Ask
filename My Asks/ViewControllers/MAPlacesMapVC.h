//
//  MAPlacesMapVC.h
//  My Asks
//
//  Created by artis on 30/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <GoogleMaps/GoogleMaps.h>
#import <KVNProgress.h>
#import <PSLocation/PSlocation.h>
#import "AppData.h"
#import "APIClient.h"
#import "MASearchPlacesModel.h"

@interface MAPlacesMapVC : UIViewController<GMSMapViewDelegate,CLLocationManagerDelegate,MKMapViewDelegate>
{
    NSMutableArray *placeSearchArray;
    NSDictionary *locationDict;
    CLLocation *currentLocation;
}

// Property for getting curent lattude and longitude and pass into the API
@property (strong, nonatomic)NSString *latitude;
@property (strong, nonatomic)NSString *longitude;

@property(strong, nonatomic) NSMutableArray *placesSearchArray;;
@property (retain ,nonatomic) CLLocationManager *locationManager;

@property(strong, nonatomic) IBOutlet GMSMapView *googleMapView;
@property (retain ,nonatomic) PSLocationManager *psLocationManager;

- (IBAction)btnBackClicked:(id)sender;
@end
