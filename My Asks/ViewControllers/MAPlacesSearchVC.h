//
//  MAPlacesSearchVC.h
//  My Asks
//
//  Created by Mac on 20/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MAPlacesSearchTableCell.h"
#import "MAPlacesDetailVC.h"
#import "MAPlacesFilterVC.h"


@protocol MAPlacesSearchCategoryDelegate <NSObject>

@required
- (void)selectedSearchCategory:(NSString *)categoryName;
- (void)selectedPlaceLatLong:(NSString *)latitude :(NSString *)longitude;
- (void) clearSelectedSearchCategoryAndPlace;

@end


@interface MAPlacesSearchVC : UIViewController <UITableViewDataSource, UITableViewDelegate, PlaceSearchTextFieldDelegate>
{
    NSArray *searchArray;
    NSArray *categoryArray;
}
@property (weak, nonatomic) IBOutlet UITableView *tblPlacesSearch;
- (IBAction)btnBackClicked:(id)sender;

@property (nonatomic, assign) id<MAPlacesSearchCategoryDelegate> delegate;
@property (strong, nonatomic) IBOutlet MVPlaceSearchTextField *placeSearchTextField;
@property (weak, nonatomic) IBOutlet UIView *autoComplete;

@property (strong, nonatomic)NSString *latitude;
@property (strong, nonatomic)NSString *longitude;
//@property (strong, nonatomic)NSString *subLocality;
@property (strong, nonatomic)NSString *restaurantName;
//@property (strong, nonatomic)NSString *restaurantType;
//@property (strong, nonatomic)NSString *nextPageToken;

- (IBAction)btnClearSeachClicked:(id)sender;

@end
