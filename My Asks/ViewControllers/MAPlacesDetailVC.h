//
//  MAPlacesDetailVC.h
//  My Asks
//
//  Created by Mac on 21/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.

#import <UIKit/UIKit.h>
#import <AsyncImageView.h>
#import <GoogleMaps/GoogleMaps.h>
#import <MapKit/MapKit.h>
#import "MASearchPlacesModel.h"
#import "APIClient.h"
#import "AppData.h"
#import "MAAddPlactToListVC.h"
#import "MACreateNewListVC.h"
#import "BIZPopupViewController.h"

@interface MAPlacesDetailVC : UIViewController<GMSMapViewDelegate,UIGestureRecognizerDelegate,BIZPopupViewControllerDelegate>

@property (strong, nonatomic)NSString *PlaceName;
@property (strong, nonatomic)NSString *PlaceAddress;
@property (strong, nonatomic)NSString *PlaceImageUrl;
@property (strong, nonatomic)NSString *PlaceDistanceInMiles;
@property (strong, nonatomic)NSString *PlaceLatitude;
@property (strong, nonatomic)NSString *PlaceLongitude;
@property (strong, nonatomic)NSString *PlaceSubLocality;
@property (strong, nonatomic)NSString *PlaceID;
@property (assign, nonatomic) bool isFavorite;

@property (strong, nonatomic) IBOutlet AsyncImageView *imgPlace;
@property (strong, nonatomic) IBOutlet UILabel *lblPlaceName;
@property (strong, nonatomic) IBOutlet UILabel *lblPlaceAddress;
@property (strong, nonatomic) IBOutlet UILabel *lblPlaceCityState;
@property (strong, nonatomic) IBOutlet UILabel *lblPlaceDistance;
@property (strong, nonatomic) IBOutlet UIButton *btnPlusClicked;
@property (weak, nonatomic) IBOutlet UILabel *lblMonToSun;
@property (weak, nonatomic) IBOutlet UILabel *lblSat;
@property (weak, nonatomic) IBOutlet UILabel *lblMonToSunTiming;
@property (weak, nonatomic) IBOutlet UILabel *lblSatTiming;
@property (strong, nonatomic) IBOutlet UILabel *lbl3Timings;
@property (strong, nonatomic) IBOutlet UILabel *lbl3Value;


@property (strong, nonatomic) MASearchPlacesModel *placesModel;

- (IBAction)btnBackClicked:(id)sender;
- (IBAction)btnShareClicked:(id)sender;

@property (strong, nonatomic) IBOutlet GMSMapView *googleMapView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *openHoursHeight;

@end
