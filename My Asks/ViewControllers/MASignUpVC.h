//
//  MASignUpVC.h
//  My Asks
//
//  Created by artis on 20/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <KVNProgress.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <CZPickerView.h>
#import "AppData.h"
#import "APIClient.h"
#import "MAVarifyUserVC.h"
#import <DropDownListView.h>
#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>

#define kCountriesFileName  @"countries.json"
#define kCountryName        @"name"
#define kCountryCallingCode @"dial_code"
#define kCountryCode        @"code"

@interface MASignUpVC : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate,MKMapViewDelegate,CLLocationManagerDelegate,kDropDownListViewDelegate,UITextFieldDelegate>

#pragma mark - Outlets
{
     NSArray *countriesList;
     NSMutableArray *countryDataRows;
     NSString *dialingCode;
     NSString *countryName;
     DropDownListView * Dropobj;
}
@property (weak, nonatomic) IBOutlet UIImageView *imgCoverImage;
@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;
@property (weak, nonatomic) IBOutlet UITextField *txtCountryCode;
@property (weak, nonatomic) IBOutlet UITextField *txtMobileNumber;
@property (weak, nonatomic) IBOutlet UIButton *btnMaleIcon;
@property (weak, nonatomic) IBOutlet UIButton *btnFemaleIcon;
@property (strong, nonatomic) IBOutlet UIView *fillDataView;
@property (strong, nonatomic) IBOutlet UILabel *lblCountryCode;
@property (strong, nonatomic) IBOutlet UIButton *btnDone;


@property (nonatomic, assign) BOOL isMaleSelected;
@property (nonatomic, assign) BOOL isFemaleSelected;

@property (retain ,nonatomic) CLLocationManager *locationManager;

#pragma mark - Actions

- (IBAction)btnBackClicked:(id)sender;
- (IBAction)btnCreateAccountClicked:(id)sender;
- (IBAction)btnCoverImageClicked:(id)sender;
- (IBAction)btnCountryCodeClicked:(id)sender;
- (IBAction)btnDoneClicked:(id)sender;

@end
