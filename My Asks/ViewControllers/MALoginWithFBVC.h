//
//  MALoginWithFBVC.h
//  My Asks
//
//  Created by artis on 27/05/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import <AsyncImageView.h>
#import "FAGoToScreenVC.h"

@interface MALoginWithFBVC : UIViewController<UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property(strong,nonatomic)   NSDictionary *param;
@property(strong,nonatomic)   NSString *email;
@property(strong,nonatomic)   NSString *gender;
@property(strong,nonatomic)   NSString *name;
@property(strong,nonatomic)   NSString *profileImage;

- (IBAction)btnFBLoginClicked:(id)sender;
- (IBAction)btnDoneClicked:(id)sender;
- (IBAction)btnProfilePicClicked:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *btnDone;
@property (strong, nonatomic) IBOutlet AsyncImageView *imgProfilePic;
@property (strong, nonatomic) IBOutlet UITextField *txtUserName;

@property (weak, nonatomic) IBOutlet UIButton *btnFBLogin;

@end
