//
//  MAPlacesFilterVC.m
//  My Asks
//
//  Created by Mac on 22/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "MAPlacesFilterVC.h"
#import "CustomInfiniteIndicator.h"


@interface MAPlacesFilterVC () <MAPlacesSearchCategoryDelegate,PSLocationManagerDelegate>
@end

NSTimer *timer;


@implementation MAPlacesFilterVC

#pragma mark - Life Cycle

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.tblPlacesContent.delegate = self;
    self.tblPlacesContent.dataSource = self;
    
    self.subLocality = @"";
    
    placesSearchArray = [[NSMutableArray alloc]init];
    placesPhotoArray = [[NSMutableArray alloc]init];
    
    //[self initFooterView];
    
//    NSString* name = [nameFromDeviceName(UIDevice.currentDevice.name) componentsJoinedByString:@" "];
    
    NSString* name = [[self nameFromDeviceName:(UIDevice.currentDevice.name)] componentsJoinedByString:@" "];
    NSLog(@"%@",name);
    
    self.loadFromGooglePlace = true;
    
    self.automaticallyAdjustsScrollViewInsets = false;
    
    self.topTabBarView = [[[NSBundle mainBundle] loadNibNamed:@"PlacesFilterTopTabBar" owner:self options:nil] firstObject];
    self.topTabBarView.frame = CGRectMake(0, 0, [UIScreen mainScreen].bounds.size.width, 10);
    
    self.placeSearchTextField.placeSearchDelegate                 = self;
    self.placeSearchTextField.strApiKey                           = @"AIzaSyCyL6A9-IRxfUqept-WCEOC3Za2v_-27zI";
    self.placeSearchTextField.superViewOfList                     = self.autoComplete;  // View, on which Autocompletion list should be appeared.
    self.placeSearchTextField.autoCompleteShouldHideOnSelection   = YES;
    self.placeSearchTextField.maximumNumberOfAutoCompleteRows     = 10;
    
    self.placeSearchTextField.autoCompleteTableFrame = CGRectMake(10, 0, self.autoComplete.frame.size.width - 20, self.autoComplete.frame.size.height);
    
    self.placeSearchTextField.delegate = self;
//    self.placeSearchTextField.autoCompleteTableView.frame = CGRectMake(0, 0, self.autoComplete.frame.size.width, self.autoComplete.frame.size.height);
    
    self.restaurantName = @"";
    self.restaurantType = @"";
    
//    self.locationManager = [[CLLocationManager alloc] init];
//    self.locationManager.delegate = self;
//    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
//    
//    [self.locationManager requestWhenInUseAuthorization];
    
    _pslocationManager = [PSLocationManager new];
    [_pslocationManager setDelegate:self];
    [_pslocationManager setMaximumLatency:20];
    [_pslocationManager setPausesLocationUpdatesAutomatically:NO];
    [_pslocationManager startUpdatingLocation];
    [_pslocationManager requestWhenInUseAuthorization];
    
//    if ([CMMotionActivityManager isActivityAvailable]) {
//        [_pslocationManager setDesiredAccuracy:kCLLocationAccuracyBest];
//    }
   // else {
        [_pslocationManager setDesiredAccuracy:kPSLocationAccuracyPathSenseNavigation];
    //}
    
    if ([_pslocationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) { // iOS 8 or later
        [_pslocationManager requestAlwaysAuthorization];
    }
    
    self.refreshControl = [[UIRefreshControl alloc]init];
    self.refreshControl.backgroundColor = [UIColor clearColor];
    self.refreshControl.tintColor = [UIColor whiteColor];
    
    [self.tblPlacesContent addSubview:self.refreshControl];
    //[self.refreshControl addTarget:self action:@selector(getPlacesList) forControlEvents:UIControlEventValueChanged];
    
     [self.refreshControl addTarget:self action:@selector(updateMethod) forControlEvents:UIControlEventValueChanged];
    
    CGRect indicatorRect;
    indicatorRect = CGRectMake(0, 0, 24, 24);
    
     CustomInfiniteIndicator *indicator = [[CustomInfiniteIndicator alloc] initWithFrame:indicatorRect];
     indicator.backgroundColor = [UIColor clearColor];
     self.tblPlacesContent.infiniteScrollIndicatorView = indicator;
     self.tblPlacesContent.infiniteScrollIndicatorMargin = 40;
    
    NSLog(@"%@",[[AppData sharedManager]getCurrentCountryCode]);
    
    [self.tblPlacesContent addInfiniteScrollWithHandler:^(UITableView *tableView) {
                   // Finish infinite scroll animations
        
//        [self getPlacesListWithNextPageToken];
        
        [self fetchData:^{
            // Finish infinite scroll animations
            [tableView finishInfiniteScroll];
            [tableView reloadData];
        }];
        
//        [self.tblPlacesContent reloadData];

//        [tableView finishInfiniteScroll];
    }];
    
    [self.pslocationManager startUpdatingLocation];
    // Load initial data
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{
    
    
//    [self.locationManager startUpdatingLocation];
//    
//    timer = [NSTimer scheduledTimerWithTimeInterval:30
//                                             target:self
//                                           selector:@selector(updateMethod)
//                                           userInfo:nil
//                                            repeats:YES];

}

- (void) viewDidDisappear:(BOOL)animated {
    [timer invalidate];
}

-(void)updateMethod{
    [self.pslocationManager startUpdatingLocation];
}

#pragma mark - UITableView Delegate/DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [placesSearchArray count];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if ([placesSearchArray count] > 0) {
        return 10;
    }
    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return self.topTabBarView;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    MAPlacesFilterTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MAPlacesFilterTableCell"];
    
    cell.btnShare.tag = indexPath.row;
    [cell.btnShare addTarget:self action:@selector(btnShareClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    MASearchPlacesModel *placesModel = [placesSearchArray objectAtIndex:indexPath.row];

    if(placesModel.PlaceName == (id)[NSNull null]) {
        
       cell.lblPlaceName.text = @"";
    }
    else{
         cell.lblPlaceName.text = placesModel.PlaceName;
    }
   
    if(placesModel.PlaceAddress == (id)[NSNull null]) {
        
        cell.lblPlaceDetails.text = @"";
    }
    else{
        cell.lblPlaceDetails.text =  [NSString stringWithFormat:@"%@",placesModel.PlaceAddress];
    }

    if(placesModel.DistanceinMiles == (id)[NSNull null]) {
        
        cell.lblDistance.text = @"";
    }
    else{
        cell.lblDistance.text = placesModel.DistanceinMiles;
    }
    
    cell.btnIsFavorite.tag = indexPath.row;

    if (placesModel.isFavorite) {
        
        [cell.btnIsFavorite setImage:[UIImage imageNamed:@"Checkbox Selected"] forState:UIControlStateNormal];
    }
    else{
        
        [cell.btnIsFavorite setImage:[UIImage imageNamed:@"plus_black"] forState:UIControlStateNormal];
        
        UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(handleTapGesture:)];
        tapGesture.numberOfTapsRequired = 2;
        [cell.btnIsFavorite addGestureRecognizer:tapGesture];
        
        UILongPressGestureRecognizer *longPressGesture= [[UILongPressGestureRecognizer alloc]initWithTarget:self action:@selector(handleLongPress:)];
        longPressGesture.delegate = self;
        longPressGesture.delaysTouchesBegan = YES;
        [cell.btnIsFavorite addGestureRecognizer:longPressGesture];
        
        [cell.btnIsFavorite addTarget:self action:@selector(btnIsFavoriteClicked:) forControlEvents:UIControlEventTouchUpInside];
        
    }
    
    NSLog(@"%@",placesModel.PlaceImageURL);
    if (placesModel.PlaceImageURL == (id)[NSNull null] || placesModel.PlaceImageURL.length == 0) {
       
        cell.imgPlaces.image = [UIImage imageNamed:@""];
       // NSLog(@"there is no image");
    }
    else{
         cell.imgPlaces.imageURL =   [NSURL URLWithString:placesModel.PlaceImageURL];
    }
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MAPlacesDetailVC *detailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MAPlacesDetailVC"];
    MASearchPlacesModel *placesModel = [placesSearchArray objectAtIndex:indexPath.row];
    
    detailVC.PlaceSubLocality = self.subLocality;
    detailVC.PlaceID = placesModel.PlaceId;
    detailVC.placesModel = placesModel;
    
    [self.navigationController pushViewController:detailVC animated:true];
}

//-(void)initFooterView
//{
//    footerView = [[UIView alloc] initWithFrame:CGRectMake(0.0, 0.0, self.tblPlacesContent.frame.size.width, 40.0)];
//    
//    UIActivityIndicatorView * actInd = [[UIActivityIndicatorView alloc] initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleGray];
//    actInd.tintColor = [UIColor whiteColor];
//    actInd.color = [UIColor whiteColor];
//    actInd.activityIndicatorViewStyle = UIActivityIndicatorViewStyleWhite;
//    actInd.tag = 10;
//    
//    actInd.frame = CGRectMake(self.view.frame.size.width/2.0, 5.0, 20.0, 20.0);
//    
//    actInd.hidesWhenStopped = YES;
//    
//    [footerView addSubview:actInd];
//    
//    actInd = nil;
//}

//-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView
//{
//    BOOL endOfTable = (scrollView.contentOffset.y >= ((self->placesSearchArray.count * 120) - scrollView.frame.size.height)); // Here 120 is row height
//    
//    if (endOfTable  && !scrollView.dragging && !scrollView.decelerating)
//    {
//        self.tblPlacesContent.tableFooterView = footerView;
//        
//        [(UIActivityIndicatorView *)[footerView viewWithTag:10] startAnimating];
//    
//        if (self.nextPageToken == (id)[NSNull null] ||self.nextPageToken.length == 0) {
//            
//             [(UIActivityIndicatorView *)[footerView viewWithTag:10] stopAnimating];
//        }
//        else{
//            [self getPlacesListWithNextPageToken];
//        }
//    }    
//}

#pragma mark - Button Actions

- (IBAction)btnBackClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)btnPlaceLocationClicked:(id)sender {
    
    MAPlacesSearchVC *searchVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MAPlacesSearchVC"];
    searchVC.delegate = self;
    
    [self.navigationController pushViewController:searchVC animated:true];
}

- (IBAction)btnMapClicked:(id)sender {
    
    MAPlacesMapVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"MAPlacesMapVC"];
    VC.placesSearchArray = placesSearchArray;
    [self.navigationController pushViewController:VC animated:YES];
}

- (void)btnShareClicked:(UIButton*)sender{
    
    MASearchPlacesModel *placesModel = [placesSearchArray objectAtIndex:sender.tag];
    NSLog(@"%@",placesModel.PlaceId);
    NSString *textToShare = [NSString stringWithFormat:@"%@ %@ %@",[[NSUserDefaults standardUserDefaults] valueForKey:MAUserName],@" has share you a Place ", [NSString stringWithFormat:@"rex.life://?PlaceID=%@&UserID=%@",placesModel.PlaceId,[AppData sharedManager].userID]];
    NSLog(@"%@",textToShare);
    NSLog(@"%@",[NSString stringWithFormat:@"%@ %@ %@",[[NSUserDefaults standardUserDefaults] valueForKey:MAUserName],@" has share you a Place ", [NSString stringWithFormat:@"rex.life://?PlaceID=%@&UserID=%@",placesModel.PlaceId,[AppData sharedManager].userID]]);
    NSArray *itemsToShare = @[textToShare];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:nil];
    activityVC.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll];
    [self presentViewController:activityVC animated:YES completion:nil];
}

#pragma mark - TextFiled

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    [textField resignFirstResponder];
    return YES;
}

- (BOOL)textFieldShouldClear:(UITextField *)textField {

    self.latitude = self.currentLatitude;
    self.longitude = self.currentLongitude;
    
    self.placeSearchTextField.text = @"";
    
    self.restaurantName = @"";
    self.restaurantType = @"";
    
//    [self getPlacesList];
    [self getCurrentPlace];
    return NO;
}

#pragma mark - Place search Textfield Delegates

-(void)placeSearchResponseForSelectedPlace:(NSMutableDictionary*)responseDict{
    
    [self.view endEditing:YES];
    
    self.latitude = [[[[responseDict objectForKey:@"result"] objectForKey:@"geometry"] objectForKey:@"location"]valueForKey:@"lat"];
    self.longitude = [[[[responseDict objectForKey:@"result"] objectForKey:@"geometry"] objectForKey:@"location"]valueForKey:@"lng"];
    
    self.restaurantName = [[responseDict objectForKey:@"result"] objectForKey:@"formatted_address"];
    
    [_autoComplete setHidden:YES];
    
    self.loadFromGooglePlace = false;
    
    [self getPlacesList];
}

-(void)placeSearchWillShowResult{
    
    [_autoComplete setHidden:NO];
}

-(void)placeSearchWillHideResult{
    
}

-(void)placeSearchResultCell:(UITableViewCell *)cell withPlaceObject:(PlaceObject *)placeObject atIndex:(NSInteger)index{
    
    if(index%2==0){
        cell.contentView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
    }else{
        cell.contentView.backgroundColor = [UIColor whiteColor];
    }
}

#pragma mark - location delegate

// this delegate is called when the app successfully finds your current location
//- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
//{
//    // this creates a MKReverseGeocoder to find a placemark using the found coordinates
//    
////    [placesSearchArray removeAllObjects];
//    
//    CLGeocoder *ceo = [[CLGeocoder alloc]init];
//    CLLocation *loc = [[CLLocation alloc]initWithLatitude:newLocation.coordinate.latitude longitude:newLocation.coordinate.longitude]; //insert your coordinates
//    
//    locationDict = @{@"lat":[NSString stringWithFormat:@"%f",newLocation.coordinate.latitude],@"long":[NSString stringWithFormat:@"%f",newLocation.coordinate.longitude]};
//    
//    [ceo reverseGeocodeLocation:loc
//              completionHandler:^(NSArray *placemarks, NSError *error) {
//                  
//                  CLPlacemark *placemark = [placemarks objectAtIndex:0];
//                  self.latitude =  [[NSNumber numberWithDouble:newLocation.coordinate.latitude]stringValue];
//                  self.longitude =  [[NSNumber numberWithDouble:newLocation.coordinate.longitude]stringValue];
//                  
//                  self.currentLatitude = self.latitude;
//                  self.currentLongitude = self.longitude;
//                
//                 // NSLog(@"placemark.subLocality %@",placemark.subLocality);
//                
//                  self.subLocality =  [NSString stringWithFormat:@"%@,%@",placemark.subLocality,placemark.locality];
//                  
//                  [self.btnCurrentLocationTop setTitle:placemark.locality forState:UIControlStateNormal];
//                  
//                  if (oldLocation.coordinate.latitude != newLocation.coordinate.latitude && oldLocation.coordinate.longitude != newLocation.coordinate.longitude) {
//                      
//                      [self getPlacesList];
//                      
//                      [self.locationManager stopUpdatingLocation];
//                      
//                      GMSPlacesClient *placesClient = [[GMSPlacesClient alloc]init];
//                      [placesClient currentPlaceWithCallback:^(GMSPlaceLikelihoodList *likelihoodList, NSError *error) {
//                          if (error != nil) {
//                              NSLog(@"Current Place error %@", [error localizedDescription]);
//                              return;
//                          }
//                          
//                          NSLog(@"%@",likelihoodList.description);
//                          for (GMSPlaceLikelihood *likelihood in likelihoodList.likelihoods) {
//                              GMSPlace* place = likelihood.place;
//                              NSLog(@"Current Place name %@ at likelihood %g", place.name, likelihood.likelihood);
//                              NSLog(@"Current Place address %@", place.formattedAddress);
//                              NSLog(@"Current Place attributions %@", place.attributions);
//                              NSLog(@"Current PlaceID %@", place.placeID);
//                          }
//                          
//                      }];
//                      
//                  }
//              }
//     ];
//}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    [self.pslocationManager stopUpdatingLocation];
    
    CLLocation *location = [locations lastObject];
    
    // this creates a MKReverseGeocoder to find a placemark using the found coordinates
    
    CLGeocoder *ceo = [[CLGeocoder alloc]init];
    CLLocation *loc = [[CLLocation alloc]initWithLatitude:location.coordinate.latitude longitude:location.coordinate.longitude]; //insert your coordinates
    
    [ceo reverseGeocodeLocation:loc
              completionHandler:^(NSArray *placemarks, NSError *error) {
                  
                  CLPlacemark *placemark = [placemarks objectAtIndex:0];
                  
                  self.latitude =  [[NSNumber numberWithDouble:location.coordinate.latitude]stringValue];
                  self.longitude =  [[NSNumber numberWithDouble:location.coordinate.longitude]stringValue];
                  
                  [AppData sharedManager].latitude = [[NSNumber numberWithDouble:location.coordinate.latitude]stringValue];
                  [AppData sharedManager].longitude = [[NSNumber numberWithDouble:location.coordinate.longitude]stringValue];
                  
                  self.currentLatitude = self.latitude;
                  self.currentLongitude = self.longitude;
                  
                  // NSLog(@"placemark.subLocality %@",placemark.subLocality);
                  
                  self.subLocality =  [NSString stringWithFormat:@"%@,%@",placemark.subLocality,placemark.locality];
                  
                  [self.btnCurrentLocationTop setTitle:placemark.locality forState:UIControlStateNormal];
                  [self getCurrentPlace];
              }
     ];
}

- (void) getCurrentPlace {
    
    self.loadFromGooglePlace = true;
    
    GMSPlacesClient *placesClient = [[GMSPlacesClient alloc]init];
    [placesClient currentPlaceWithCallback:^(GMSPlaceLikelihoodList *likelihoodList, NSError *error) {
        if (error != nil) {
            NSLog(@"Current Place error %@", [error localizedDescription]);
            return;
        }
        
        NSLog(@"%@",likelihoodList.description);
        for (GMSPlaceLikelihood *likelihood in likelihoodList.likelihoods) {
            GMSPlace* place = likelihood.place;
            NSLog(@"Current Place name %@ at likelihood %g", place.name, likelihood.likelihood);
            NSLog(@"Current Place address %@", place.formattedAddress);
            NSLog(@"Current Place attributions %@", place.attributions);
            NSLog(@"Current PlaceID %@", place.placeID);
        }
        [self getPSLocation:likelihoodList];
    }];

}

- (void) getPSLocation:(GMSPlaceLikelihoodList *)likelihoodList{
    
    [placesSearchArray removeAllObjects];
    
    for (GMSPlaceLikelihood *likelihood in likelihoodList.likelihoods) {
        
        GMSPlace* place = likelihood.place;
        NSLog(@"Current Place name %@ at likelihood %g", place.name, likelihood.likelihood);
        NSLog(@"Current Place address %@", place.formattedAddress);
        NSLog(@"Current Place attributions %@", place.attributions);
        NSLog(@"Current PlaceID %@", place.placeID);
        NSLog(@"Current PlaceID %@", place.description);
        
        MASearchPlacesModel *placesModel = [[MASearchPlacesModel alloc]init];
        
        placesModel.PlaceName = place.name;
        placesModel.PlaceId = place.placeID;
        placesModel.PlaceAddress = place.formattedAddress;
        placesModel.PlaceImageURL = @"";
        
        [placesSearchArray addObject:placesModel];
        
    }
    
    //[self.tblPlacesContent reloadData];
    [self getPlaceDetails];
    
}

-(void)getPlaceDetails{
    
    [AppData startNetworkIndicator];
    
    CLLocation *LocationCurrent = [[CLLocation alloc] initWithLatitude:[self.latitude floatValue] longitude:[self.longitude floatValue]];
    
    for (NSInteger i = 0; i < placesSearchArray.count; i++) {
        MASearchPlacesModel *placesModel = [placesSearchArray objectAtIndex:i];
        
        NSString *param = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/details/json?placeid=%@&key=AIzaSyCyL6A9-IRxfUqept-WCEOC3Za2v_-27zI",placesModel.PlaceId];
        
        [[APIClient sharedManager]GetPlacesFilter:param indexOf:i onCompletion:^(NSDictionary *resultData, bool success, NSInteger index) {
            if (success) {
                if ([resultData count] == 0) {
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
                                                                    message:@"Place details not available"
                                                                   delegate:self
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    [alert show];
                }
                else{
                    
                    if ([resultData objectForKey:@"result"] != nil || [resultData objectForKey:@"result"] != (id)[NSNull null]) {
                        
                        NSLog(@"%@",[resultData valueForKeyPath:@"result"]);
                        NSDictionary *result = [resultData valueForKey:@"result"];
                        
                        if ([result valueForKey:@"photos"] != nil || [result valueForKey:@"photos"] != (id)[NSNull null]) {
                            
                            NSMutableArray *photoArray = [result valueForKey:@"photos"];
                            NSMutableDictionary *dic = [photoArray firstObject];
                            
                            NSLog(@"%@",[dic valueForKey:@"photo_reference"]);
                            NSString *photo_reference = [dic valueForKey:@"photo_reference"];
                            NSLog(@"%@",photo_reference);
                            
                            NSString *placeImageUrl =  [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/photo?photoreference=%@&sensor=false&maxheight=60&maxwidth=60&key=AIzaSyCyL6A9-IRxfUqept-WCEOC3Za2v_-27zI",photo_reference] ;
                            
                            MASearchPlacesModel *updatePlacesModel = [placesSearchArray objectAtIndex:index];
                            updatePlacesModel.PlaceImageURL = placeImageUrl;
                            
                            NSMutableArray *locationData = [result valueForKey:@"geometry"];
                            
                            if (locationData != nil || locationData != (id)[NSNull null]) {
                            
                                CLLocation *LocationActual = [[CLLocation alloc] initWithLatitude:[[locationData valueForKeyPath:@"location.lat"]floatValue] longitude:[[locationData valueForKeyPath:@"location.lng"]floatValue]];
                                CLLocationDistance distanceInMiles = [LocationCurrent distanceFromLocation:LocationActual];
                                
                                double totalDistanceBetween = (distanceInMiles * 0.000621371192);
                                updatePlacesModel.DistanceinMiles = [NSString stringWithFormat:@"%.2f mi",totalDistanceBetween];
                                updatePlacesModel.PlaceLatitude =  [locationData valueForKeyPath:@"location.lat"];
                                updatePlacesModel.PlaceLongitude = [locationData valueForKeyPath:@"location.lng"];
                             }
                            else{
                                updatePlacesModel.DistanceinMiles = @"00.00mi";
                            }
                            
                        }
                    }
                }
            }
        }];
    }
    
    [self checkForFavoritePlace];
    [AppData stopNetworkIndicator];
    
    //[self.refreshControl endRefreshing];
    //[self.tblPlacesContent reloadData];
}

- (NSArray *) nameFromDeviceName : (NSString * ) deviceName
{
    NSError * error;
//    static NSString * expression = (@"^(?:iPhone|phone|iPad|iPod)\\s+(?:de\\s+)?(?:[1-9]?S?\\s+)?|(\\S+?)(?:['']?s)?(?:\\s+(?:iPhone|phone|iPad|iPod)\\s+(?:[1-9]?S?\\s+)?)?$|(\\S+?)(?:['']?的)?(?:\\s*(?:iPhone|phone|iPad|iPod))?$|(\\S+)\\s+");
    
    static NSString * expression = (@"^(?:iPhone|phone|iPad|iPod)\\s+(?:de\\s+)?|"
                                    "(\\S+?)(?:['\"]?s)?(?:\\s+(?:iPhone|phone|iPad|iPod))?$|"
                                    "(\\S+?)(?:['’]?s)?(?:\\s+(?:iPhone|phone|iPad|iPod))?$|"
                                    "(\\S+?)(?:['’]?的)?(?:\\s*(?:iPhone|phone|iPad|iPod))?$|"
                                    "(\\S+)\\s+");
    
    static NSRange RangeNotFound = (NSRange){.location=NSNotFound, .length=0};
    NSRegularExpression * regex = [NSRegularExpression regularExpressionWithPattern:expression
                                                                            options:(NSRegularExpressionCaseInsensitive)
                                                                              error:&error];
    NSMutableArray * name = [NSMutableArray new];
    for (NSTextCheckingResult * result in [regex matchesInString:deviceName
                                                         options:0
                                                           range:NSMakeRange(0, deviceName.length)]) {
        for (int i = 1; i < result.numberOfRanges; i++) {
            if (! NSEqualRanges([result rangeAtIndex:i], RangeNotFound)) {
                [name addObject:[deviceName substringWithRange:[result rangeAtIndex:i]].capitalizedString];
            }
        }
    }
    return name;
}


-(void)checkForFavoritePlace{

    [AppData startNetworkIndicator];
    
     NSString *userID = [[AppData sharedManager]getCurrentUserID];
    
    for (NSInteger i = 0; i < placesSearchArray.count; i++) {
        
        MASearchPlacesModel *placesModel = [placesSearchArray objectAtIndex:i];
        NSMutableDictionary *param =[[NSMutableDictionary alloc]init];
        
        [param setValue:placesModel.PlaceId forKey:@"placeId"];
        [param setValue:userID forKey:@"UserID"];
       
        [[APIClient sharedManager]PlaceFavorite:param onCompletion:^(NSDictionary *resultData, bool success) {
            
            placesModel.isFavorite = [[resultData valueForKey:@"Success"]boolValue];
        }];
    }
    
    [AppData stopNetworkIndicator];
    
    [self.refreshControl endRefreshing];
    [self.tblPlacesContent reloadData];
    
}

-(void)getPlacesList{
    
    NSMutableDictionary *param =[[NSMutableDictionary alloc]init];
    NSString *userID = [[AppData sharedManager]getCurrentUserID];
    
    [param setValue:self.latitude forKey:@"Latitude"];
    [param setValue:self.longitude forKey:@"Longitude"];
    [param setValue:userID forKey:@"UserID"];
    [param setValue:self.restaurantName forKey:@"RestaurantName"];
    [param setValue:self.restaurantType forKey:@"RestaurantType"];
    [param setValue:@"2000" forKey:@"Radius"];
    
//    [param setValue:@"37.4357930" forKey:@"Latitude"];
//    [param setValue:@"-121.9205470" forKey:@"Longitude"];
//    [param setValue:@"2" forKey:@"UserID"];
//    [param setValue:self.restaurantName forKey:@"RestaurantName"];
//    [param setValue:self.restaurantType forKey:@"RestaurantType"];
//    [param setValue:@"2000" forKey:@"Radius"];
    
//    [KVNProgress show];
    [AppData startNetworkIndicator];
    
    [[APIClient sharedManager]SearchPlaces:param onCompletion:^(NSDictionary *resultData, bool success) {
        
        [placesSearchArray removeAllObjects];
        
//        [KVNProgress dismiss];
        [AppData stopNetworkIndicator];
        NSLog(@"%@",resultData);
        
        if ([resultData count] == 0) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
                                                            message:@"No places to display"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            
        }
        else{
            
            NSArray *placesArray = [resultData valueForKey:@"Places"];
            self.nextPageToken = [resultData valueForKey:@"NextPageToken"];
            
            for (NSDictionary *places in placesArray) {
                
//                NSLog(@"%@",places);
//                
//                NSLog(@"%@",[places valueForKey:@"UserID"]);
//                NSLog(@"%@",[places valueForKey:@"PlaceLatitude"]);
//                NSLog(@"%@",[places valueForKey:@"PlaceLongitude"]);
//                NSLog(@"%@",[places valueForKey:@"PlaceTypes"]);
//                NSLog(@"%@",[places valueForKey:@"PlaceName"]);
//                NSLog(@"%@",[places valueForKey:@"PlaceId"]);
//                NSLog(@"%@",[places valueForKey:@"DistanceinMiles"]);
//                NSLog(@"%@",[places valueForKey:@"DistanceinKMs"]);
//                NSLog(@"%@",[places valueForKey:@"isFavorite"]);
//                NSLog(@"%@",[places valueForKey:@"LikesCount"]);
//                NSLog(@"%@",[places valueForKey:@"CommentsCount"]);
//                NSLog(@"%@",[places valueForKey:@"PlaceAddress"]);
//                NSLog(@"%@",[places valueForKey:@"PlaceImage"]);
//                NSLog(@"%@",[places valueForKey:@"PlaceImageURL"]);
//                NSLog(@"%@",[places valueForKey:@"InternationalPhoneNumber"]);

                  NSLog(@"%@",[places valueForKey:@"PlaceName"]);
                
                MASearchPlacesModel *placesModel = [[MASearchPlacesModel alloc]init];
                
                placesModel.UserID = [places valueForKey:@"UserID"];
                placesModel.PlaceLatitude = [places valueForKey:@"PlaceLatitude"];
                placesModel.PlaceLongitude = [places valueForKey:@"PlaceLongitude"];
                placesModel.PlaceTypes = [places valueForKey:@"PlaceTypes"];
                placesModel.PlaceName = [places valueForKey:@"PlaceName"];
                placesModel.PlaceId = [places valueForKey:@"PlaceId"];
                
                NSString *distance;
                
                if ([places valueForKey:@"DistanceinMiles"] == (id)[NSNull null]) {
                    
                    distance = @"00.00mi";
                }
                else{
                     float distanceMiles = [[places valueForKey:@"DistanceinMiles"]floatValue];
            
                    distance = [NSString stringWithFormat:@"%.2f mi",distanceMiles];
                }
    
                placesModel.DistanceinMiles = distance;
                placesModel.DistanceinKMs = [places valueForKey:@"DistanceinKMs"];
                placesModel.isFavorite = [[places valueForKey:@"isFavorite"]boolValue];
                placesModel.LikesCount = [places valueForKey:@"LikesCount"];
                placesModel.CommentsCount = [places valueForKey:@"CommentsCount"];
                placesModel.PlaceAddress = [places valueForKey:@"PlaceAddress"];
                placesModel.PlaceImage = [places valueForKey:@"PlaceImage"];
                placesModel.PlaceImageURL = [places valueForKey:@"PlaceImageURL"];
                placesModel.InternationalPhoneNumber = [places valueForKey:@"InternationalPhoneNumber"];
                //placesModel.OpeningHours = [places valueForKey:@"OpeningHours"];
                
                NSLog(@"%@",placesModel);
                [placesSearchArray addObject:placesModel];
            }
            [self.refreshControl endRefreshing];
            
            [self.tblPlacesContent reloadData];
        }
        
    }];
}

-(void)getPlacesListWithNextPageToken{
    
    NSMutableDictionary *param =[[NSMutableDictionary alloc]init];
    NSString *userID = [[AppData sharedManager]getCurrentUserID];
    NSLog(@"%@",self.nextPageToken);
    
    if(self.nextPageToken == (id)[NSNull null] || self.nextPageToken.length == 0) {
        
       NSLog(@"%@",self.nextPageToken);
    }
    else
    {
        [param setValue:self.latitude forKey:@"Latitude"];
        [param setValue:self.longitude forKey:@"Longitude"];
        [param setValue:userID forKey:@"UserID"];
        [param setValue:self.nextPageToken forKey:@"NextPageToken"];
        
        [[APIClient sharedManager]SearchPlaces:param onCompletion:^(NSDictionary *resultData, bool success) {
            
            
            NSLog(@"%@",resultData);
            
            if ([resultData count] == 0) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
                                                                message:@"No places to display"
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                
            }
            else{
                
                NSArray *placesArray = [resultData valueForKey:@"Places"];
                self.nextPageToken = [resultData valueForKey:@"NextPageToken"];
                
                for (NSDictionary *places in placesArray) {
                    
                    MASearchPlacesModel *placesModel = [[MASearchPlacesModel alloc]init];
                    
                    placesModel.UserID = [places valueForKey:@"UserID"];
                    placesModel.PlaceLatitude = [places valueForKey:@"PlaceLatitude"];
                    placesModel.PlaceLongitude = [places valueForKey:@"PlaceLongitude"];
                    placesModel.PlaceTypes = [places valueForKey:@"PlaceTypes"];
                    placesModel.PlaceName = [places valueForKey:@"PlaceName"];
                    placesModel.PlaceId = [places valueForKey:@"PlaceId"];
                    
                    NSString *distance;
                    
                    if ([places valueForKey:@"DistanceinMiles"] == (id)[NSNull null]) {
                        
                        distance = @"00.00mi";
                    }
                    else{
                        float distanceMiles = [[places valueForKey:@"DistanceinMiles"]floatValue];
                        
                        distance = [NSString stringWithFormat:@"%.2f mi",distanceMiles];
                    }
                    
                    placesModel.DistanceinMiles = distance;
                    placesModel.DistanceinKMs = [places valueForKey:@"DistanceinKMs"];
                    placesModel.isFavorite = [[places valueForKey:@"isFavorite"]boolValue];
                    placesModel.LikesCount = [places valueForKey:@"LikesCount"];
                    placesModel.CommentsCount = [places valueForKey:@"CommentsCount"];
                    placesModel.PlaceAddress = [places valueForKey:@"PlaceAddress"];
                    placesModel.PlaceImage = [places valueForKey:@"PlaceImage"];
                    placesModel.PlaceImageURL = [places valueForKey:@"PlaceImageURL"];
                    placesModel.InternationalPhoneNumber = [places valueForKey:@"InternationalPhoneNumber"];
                    //placesModel.OpeningHours = [places valueForKey:@"OpeningHours"];
                    
                    [placesSearchArray addObject:placesModel];
                }
                [self.refreshControl endRefreshing];
                
                [self.tblPlacesContent reloadData];
            }
            
        }];
    }
    
}

- (void)fetchData:(void(^)(void))completion {
    
    NSMutableDictionary *param =[[NSMutableDictionary alloc]init];
    NSString *userID = [[AppData sharedManager]getCurrentUserID];
    NSLog(@"%@",self.nextPageToken);
    
    if(self.nextPageToken == (id)[NSNull null]) {
        NSLog(@"%@",self.nextPageToken);
        if(completion) {
            completion();
        }
    }
    else if(self.loadFromGooglePlace == true) {
        if(completion) {
            completion();
        }
    }
    else
    {
        [param setValue:self.latitude forKey:@"Latitude"];
        [param setValue:self.longitude forKey:@"Longitude"];
        [param setValue:userID forKey:@"UserID"];
        [param setValue:self.nextPageToken forKey:@"NextPageToken"];
        
        [[APIClient sharedManager]SearchPlaces:param onCompletion:^(NSDictionary *resultData, bool success) {
            
            
            NSLog(@"%@",resultData);
            
            if ([resultData count] == 0) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
                                                                message:@"No places to display"
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                
            }
            else{
                
                NSArray *placesArray = [resultData valueForKey:@"Places"];
                self.nextPageToken = [resultData valueForKey:@"NextPageToken"];
                
                for (NSDictionary *places in placesArray) {
                    
                    MASearchPlacesModel *placesModel = [[MASearchPlacesModel alloc]init];
                    
                    placesModel.UserID = [places valueForKey:@"UserID"];
                    placesModel.PlaceLatitude = [places valueForKey:@"PlaceLatitude"];
                    placesModel.PlaceLongitude = [places valueForKey:@"PlaceLongitude"];
                    placesModel.PlaceTypes = [places valueForKey:@"PlaceTypes"];
                    placesModel.PlaceName = [places valueForKey:@"PlaceName"];
                    placesModel.PlaceId = [places valueForKey:@"PlaceId"];
                    
                    float distanceMiles = [[places valueForKey:@"DistanceinMiles"]floatValue];
                    
                    NSString *str = [NSString stringWithFormat:@"%.2f mi",distanceMiles];
                    
                    placesModel.DistanceinMiles = str;
                    placesModel.DistanceinKMs = [places valueForKey:@"DistanceinKMs"];
                    placesModel.isFavorite = [[places valueForKey:@"isFavorite"]boolValue];
                    placesModel.LikesCount = [places valueForKey:@"LikesCount"];
                    placesModel.CommentsCount = [places valueForKey:@"CommentsCount"];
                    placesModel.PlaceAddress = [places valueForKey:@"PlaceAddress"];
                    placesModel.PlaceImage = [places valueForKey:@"PlaceImage"];
                    placesModel.PlaceImageURL = [places valueForKey:@"PlaceImageURL"];
                    placesModel.InternationalPhoneNumber = [places valueForKey:@"InternationalPhoneNumber"];
                    //placesModel.OpeningHours = [places valueForKey:@"OpeningHours"];
                    
                    [placesSearchArray addObject:placesModel];
                }
//                [self.refreshControl endRefreshing];
                
                if(completion) {
                    completion();
                }
            }
        }];
    }
}

#pragma mark - MAPlacesSearchCategoryDelegate

- (void)selectedSearchCategory:(NSString *)categoryName{
    NSLog(@"%@",categoryName);
    
    self.loadFromGooglePlace = false;
    self.restaurantType = categoryName;
    [self getPlacesList];
}

-(void)selectedPlaceLatLong:(NSString *)latitude :(NSString *)longitude{
    self.loadFromGooglePlace = false;
    self.latitude = latitude;
    self.longitude = longitude;
    [self getPlacesList];
}

- (void) clearSelectedSearchCategoryAndPlace {
    self.latitude = self.currentLatitude;
    self.longitude = self.currentLongitude;
    
    self.placeSearchTextField.text = @"";
    
    self.restaurantName = @"";
    self.restaurantType = @"";
    
//    [self getPlacesList];
    [self getCurrentPlace];
}

#pragma mark - gesture recognizer methods

-(void)handleTapGesture:(UITapGestureRecognizer *)tap{
    
    MAAddPlactToListVC *contributeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MAAddPlactToListVC"];
    CGRect fullScreenRect = [[UIScreen mainScreen] bounds];
    
    MASearchPlacesModel *placesModel = [placesSearchArray objectAtIndex:tap.view.tag];
    
    contributeViewController.RestaurantName = placesModel.PlaceName;
    contributeViewController.placeAddress = placesModel.PlaceAddress;
    contributeViewController.Latitude = placesModel.PlaceLatitude;
    contributeViewController.Longitude = placesModel.PlaceLongitude;
    contributeViewController.from = @"Place Filter";
    contributeViewController.delegate = self;
    
    BIZPopupViewController *popupViewController = [[BIZPopupViewController alloc] initWithContentViewController:contributeViewController contentSize:CGSizeMake(fullScreenRect.size.width , fullScreenRect.size.height)];
    popupViewController.showDismissButton = NO;
    
    
    [self presentViewController:popupViewController animated:NO completion:nil];
}
-(void)handleLongPress:(UILongPressGestureRecognizer *)longTap{
    
    MAAddPlactToListVC *contributeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MAAddPlactToListVC"];
    CGRect fullScreenRect = [[UIScreen mainScreen] bounds];
    
    MASearchPlacesModel *placesModel = [placesSearchArray objectAtIndex:longTap.view.tag];
    
    contributeViewController.RestaurantName = placesModel.PlaceName;
    contributeViewController.placeAddress = placesModel.PlaceAddress;
    contributeViewController.Latitude = placesModel.PlaceLatitude;
    contributeViewController.Longitude = placesModel.PlaceLongitude;
    contributeViewController.PlaceID = placesModel.PlaceId;
    contributeViewController.delegate = self;
    contributeViewController.from = @"Place Filter";
    BIZPopupViewController *popupViewController = [[BIZPopupViewController alloc] initWithContentViewController:contributeViewController contentSize:CGSizeMake(fullScreenRect.size.width , fullScreenRect.size.height)];
    popupViewController.showDismissButton = NO;
    
    [self presentViewController:popupViewController animated:NO completion:nil];
}

-(void)btnIsFavoriteClicked:(UIButton *)sender{
    
    NSMutableDictionary *param = [[NSMutableDictionary alloc]init];
    
    MASearchPlacesModel *placesModel = [placesSearchArray objectAtIndex:sender.tag];
    
    NSString *userId = [[AppData sharedManager]getCurrentUserID];
    
    [param setValue:placesModel.PlaceName forKey:@"RestaurantName"];
    [param setValue:placesModel.PlaceId forKey:@"PlaceId"];
    [param setValue:placesModel.PlaceLatitude forKey:@"Latitude"];
    [param setValue:placesModel.PlaceLongitude forKey:@"Longitude"];
    [param setValue:userId forKey:@"UserID"];
    [param setValue:@"0" forKey:@"ListId"];
    
    [KVNProgress show];
    
    [[APIClient sharedManager]AddPlaceToUserList:param onCompletion:^(NSDictionary *resultData, bool success) {
      
        [KVNProgress dismiss];
        if (success) {
//            if (resultData == (id)[NSNull null]) {
//                
//                NSLog(@"success");
//                
//                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
//                                                                message:@"Place added to list"
//                                                               delegate:self
//                                                      cancelButtonTitle:@"OK"
//                                                      otherButtonTitles:nil];
//                [alert show];
//                [self getPlacesList];
//                
//            }
//            [self getPlacesList];
            placesModel.isFavorite = true;
//            [sender setImage:[UIImage imageNamed:@"Checkbox Selected"] forState:UIControlStateNormal];
            
            [self.tblPlacesContent reloadData];
            NSLog(@"%@",resultData);
        }
    }];
}

#pragma mark - addList button clicked delagate

-(void)doneAdding:(NSString *)placeId {
    
    for (MASearchPlacesModel *places in placesSearchArray) {
        
        if ([places.PlaceId isEqualToString:placeId]) {
            
            places.isFavorite = true;
        }
    }
    [self.tblPlacesContent reloadData];
}


-(void)addListsWithId:(NSString *)placeId {
    
    [self dismissViewControllerAnimated:true completion:nil];
    MACreateNewListVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MACreateNewListVC"];
    vc.delegate = self;
    vc.placeIdForAddPlaceToList = placeId;
    vc.listType = @"Create List with Place";
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)selected:(NSString *)placeID {
    
    NSLog(@"%@",placeID);
    
    for (MASearchPlacesModel *places in placesSearchArray) {
        
        if ([places.PlaceId isEqualToString:placeID]) {
            
            places.isFavorite = true;
        }
    }
    [self.tblPlacesContent reloadData];
}

#pragma mark - Private methods

- (void)showRetryAlertWithError:(NSError *)error {
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:NSLocalizedString(@"Error fetching data", @"") message:error.localizedDescription preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Dismiss", @"") style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
        
    }]];
    
    [alertController addAction:[UIAlertAction actionWithTitle:NSLocalizedString(@"Retry", @"") style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [self getPlacesListWithNextPageToken];
    }]];
    
    [self presentViewController:alertController animated:YES completion:nil];
}

@end
