//
//  MAMapVC.m
//  My Asks
//
//  Created by artis on 17/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "MAMapVC.h"


@interface MAMapVC ()<PSLocationManagerDelegate>
{
    NSDictionary *locationDict;
}
@end

@implementation MAMapVC

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.googleMapView.myLocationEnabled = YES;
    self.googleMapView.delegate = self;
    
//    self.locationManager = [[CLLocationManager alloc] init];
//    self.locationManager.delegate = self;
//
//    [self.locationManager requestWhenInUseAuthorization];
   
    
    self.pslocationManager = [PSLocationManager new];
    [self.pslocationManager setDelegate:self];
    [self.pslocationManager setMaximumLatency:20];
    [self.pslocationManager requestWhenInUseAuthorization];
    
    //    if ([CMMotionActivityManager isActivityAvailable]) {
    //        [_pslocationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    //    }
    // else {
    [_pslocationManager setDesiredAccuracy:kPSLocationAccuracyPathSenseNavigation];
    //}
    
     [self showCurrentLocation];
    
    self.placeSearchTextField.placeSearchDelegate                 = self;
    self.placeSearchTextField.strApiKey                           = @"AIzaSyCyL6A9-IRxfUqept-WCEOC3Za2v_-27zI";
    self.placeSearchTextField.superViewOfList                     = self.autoCompleteView;  // View, on which Autocompletion list should be appeared.
    self.placeSearchTextField.autoCompleteShouldHideOnSelection   = YES;
    self.placeSearchTextField.maximumNumberOfAutoCompleteRows     = 10;

    
//    CLLocationCoordinate2D target =
//    CLLocationCoordinate2DMake(28.66,88.64);
    //self.googleMapView.camera = [GMSCameraPosition cameraWithTarget:target zoom:2.0];
    
//    [self.googleMapView animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:self.googleMapView.myLocation.coordinate.latitude
//                                                                            longitude:self.googleMapView.myLocation.coordinate.longitude
//                                                                                 zoom:2]];
    
//    [self.googleMapView animateToCameraPosition:[GMSCameraPosition cameraWithLatitude:self.googleMapView.myLocation.coordinate.latitude
//                                                                  longitude:self.googleMapView.myLocation.coordinate.longitude
//                                                                       zoom:8]];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Google Maps Delegate

//- (void)mapView:(GMSMapView *)mapView didLongPressAtCoordinate:(CLLocationCoordinate2D)coordinate
//{
//    [self.googleMapView clear];
//    
//    GMSMarker *marker = [[GMSMarker alloc] init];
//    
//    // TO get information of the place from latitude and longitude
//    
//    CLGeocoder *ceo = [[CLGeocoder alloc]init];
//    CLLocation *loc = [[CLLocation alloc]initWithLatitude:coordinate.latitude longitude:coordinate.longitude]; //insert your coordinates
//    
//    locationDict = @{@"lat":[NSString stringWithFormat:@"%f",coordinate.latitude],@"long":[NSString stringWithFormat:@"%f",coordinate.longitude]};
//    
//    [ceo reverseGeocodeLocation:loc
//              completionHandler:^(NSArray *placemarks, NSError *error) {
//                  
//                  CLPlacemark *placemark = [placemarks objectAtIndex:0];
//                 
//                  NSString *locatedAt = [[placemark.addressDictionary valueForKey:@"FormattedAddressLines"] componentsJoinedByString:@", "];
//                 
//                  // setting marker in mapview
//                  
//                  UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0,0,200,60)];
//                  
//                  UIImageView *pinImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Placeblock.png"]];
//                  
//                  UILabel *lblPlaceName = [[UILabel alloc]initWithFrame:CGRectMake(5, 5, 150, 20)];
//                  UILabel *lblPlaceDetails = [[UILabel alloc]initWithFrame:CGRectMake(5, 20,180 ,35)];
//                
//                  lblPlaceDetails.numberOfLines = 2.0;
//                  [lblPlaceDetails setFont:[UIFont systemFontOfSize:15.0]];
//                  
//                  lblPlaceName.textColor = [UIColor whiteColor];
//                  lblPlaceDetails.textColor = [UIColor whiteColor];
//                  
//                  if (placemark.locality.length == 0) {
//                      
//                      lblPlaceName.text = [NSString stringWithFormat:@"%@",placemark.country];
//                  }
//                  else{
//                      
//                      lblPlaceName.text = [NSString stringWithFormat:@"%@,%@",placemark.locality,placemark.country];
//                  }
//                  
//                  lblPlaceDetails.text = locatedAt;
//                  
//                  [view addSubview:pinImageView];
//                  [view addSubview:lblPlaceName];
//                  [view addSubview:lblPlaceDetails];
//                  
//                  UIImage *markerIcon = [[AppData sharedManager]imageFromView:view];
//                  
//                  marker.position = coordinate;
//                  marker.icon = markerIcon;
//                  marker.map=self.googleMapView;
//                  
//                  GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
//                  
//                  CLLocationCoordinate2D coordinate = marker.position;
//                  
//                  bounds = [bounds includingCoordinate:coordinate];
//                  self.googleMapView.myLocationEnabled = YES;
//                  
//                  [self.googleMapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:50.0f]];
//                  
//                  CLLocationCoordinate2D target =
//                  CLLocationCoordinate2DMake(coordinate.latitude,coordinate.longitude);
//                  self.googleMapView.camera = [GMSCameraPosition cameraWithTarget:target zoom:15.0];
//              }
//     ];
//
//}

- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker
{
    [self sendCoordinatesToChatScreen];
    return true;
}

//- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
//    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:newLocation.coordinate.latitude
//                                                            longitude:newLocation.coordinate.longitude
//                                                                 zoom:15.0];
//    [self.googleMapView animateToCameraPosition:camera];
//    [self.pslocationManager stopUpdatingLocation];
//}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray<CLLocation *> *)locations{
    
    [self.pslocationManager stopUpdatingLocation];
    
    CLLocation *location = [locations lastObject];
    
    GMSCameraPosition *camera = [GMSCameraPosition cameraWithLatitude:location.coordinate.latitude
                                                            longitude:location.coordinate.longitude
                                                                 zoom:15.0];
     [self.googleMapView animateToCameraPosition:camera];
}
#pragma mark - Place search Textfield Delegates

-(void)placeSearchResponseForSelectedPlace:(NSMutableDictionary*)responseDict{
    
    
    [self.view endEditing:YES];
    
    NSDictionary *aDictLocation=[[[[responseDict objectForKey:@"result"] objectForKey:@"geometry"] objectForKey:@"location"]valueForKey:@"lat"];
    
    NSLog(@"SELECTED ADDRESS :%@",aDictLocation);
    NSLog(@"%@",[[responseDict objectForKey:@"result"] objectForKey:@"place_id"]);
    
    NSDictionary *placeResult = [responseDict objectForKey:@"result"];
    
//    addLocationVC.placeName = [[responseDict objectForKey:@"result"] objectForKey:@"name"];
//    addLocationVC.placeAddress = [[responseDict objectForKey:@"result"] objectForKey:@"formatted_address"];
//    addLocationVC.latitude = [[[[responseDict objectForKey:@"result"] objectForKey:@"geometry"] objectForKey:@"location"]valueForKey:@"lat"];
//    addLocationVC.longitude = [[[[responseDict objectForKey:@"result"] objectForKey:@"geometry"] objectForKey:@"location"]valueForKey:@"lng"];
//    addLocationVC.placeID = [[responseDict objectForKey:@"result"] objectForKey:@"place_id"];
//    addLocationVC.isSelected = YES;
    [self setMarkerOnGooglePlaceWithData:placeResult];
    
}

-(void)placeSearchWillShowResult{
    
    [self.autoCompleteView setHidden:NO];
}

-(void)placeSearchWillHideResult{
    [self.autoCompleteView setHidden:YES];
}

-(void)placeSearchResultCell:(UITableViewCell *)cell withPlaceObject:(PlaceObject *)placeObject atIndex:(NSInteger)index{
    
    if(index%2==0){
        cell.contentView.backgroundColor = [UIColor colorWithWhite:0.9 alpha:1.0];
    }else{
        cell.contentView.backgroundColor = [UIColor whiteColor];
    }
    
}


#pragma mark - Button Actions

- (IBAction)btnBackClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

#pragma mark - Custom Methods

- (void)showCurrentLocation {
    self.googleMapView.myLocationEnabled = YES;
    [self.pslocationManager startUpdatingLocation];
}

- (void) sendCoordinatesToChatScreen {
    [self.delegate getLocationFromMap:locationDict];
    [self.navigationController popViewControllerAnimated:true];
}

-(void)setMarkerOnGooglePlaceWithData : (NSDictionary *) placeData {
    
    
    //    addLocationVC.placeName = [[responseDict objectForKey:@"result"] objectForKey:@"name"];
    //    addLocationVC.placeAddress = [[responseDict objectForKey:@"result"] objectForKey:@"formatted_address"];
    //    addLocationVC.latitude = [[[[responseDict objectForKey:@"result"] objectForKey:@"geometry"] objectForKey:@"location"]valueForKey:@"lat"];
    //    addLocationVC.longitude = [[[[responseDict objectForKey:@"result"] objectForKey:@"geometry"] objectForKey:@"location"]valueForKey:@"lng"];
    //    addLocationVC.placeID = [[responseDict objectForKey:@"result"] objectForKey:@"place_id"];
    //    addLocationVC.isSelected = YES;

    
    NSDictionary *geometry = [placeData valueForKey:@"geometry"];
    NSDictionary *location = [geometry valueForKey:@"location"];
    
    
    double latitude,longitude;
    
    latitude = [[location valueForKey:@"lat"] doubleValue];
    longitude = [[location valueForKey:@"lng"] doubleValue];
    
    NSString *place_id = [placeData valueForKey:@"place_id"];
    
    locationDict = @{@"lat":[NSString stringWithFormat:@"%f",latitude],@"long":[NSString stringWithFormat:@"%f",longitude],@"place_id":place_id};
    
    [self sendCoordinatesToChatScreen];
    
//    marker.position=CLLocationCoordinate2DMake(latitude,longitude);
//    
//    marker.appearAnimation = YES;
//    
//    
//    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0,0,200,60)];
//    
//    UIImageView *pinImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Placeblock.png"]];
//    
//    UILabel *lblPlaceName = [[UILabel alloc]initWithFrame:CGRectMake(5, 5, 150, 20)];
//    UILabel *lblPlaceDetails = [[UILabel alloc]initWithFrame:CGRectMake(5, 20,180 ,35)];
//    
//    
//    lblPlaceDetails.numberOfLines = 2.0;
//    
//    [lblPlaceDetails setFont:[UIFont systemFontOfSize:15.0]];
//    
//    lblPlaceName.textColor = [UIColor whiteColor];
//    lblPlaceDetails.textColor = [UIColor whiteColor];
//    
//    lblPlaceName.text = [placeData valueForKey:@"name"];
//    lblPlaceDetails.text = [placeData valueForKey:@"formatted_address"];
//    
//    [lblPlaceName sizeToFit];
//    
//    [view addSubview:pinImageView];
//    [view addSubview:lblPlaceName];
//    [view addSubview:lblPlaceDetails];
//    
//    UIImage *markerIcon = [[AppData sharedManager]imageFromView:view];
//    
//    marker.icon = markerIcon;
//    marker.map=self.googleMapView;
//    
//    GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
//    
//    CLLocationCoordinate2D coordinate = marker.position;
//    
//    bounds = [bounds includingCoordinate:coordinate];
//    self.googleMapView.myLocationEnabled = YES;
//    
//    [self.googleMapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:50.0f]];
//    
//    CLLocationCoordinate2D target =
//    CLLocationCoordinate2DMake(coordinate.latitude,coordinate.longitude);
//    self.googleMapView.camera = [GMSCameraPosition cameraWithTarget:target zoom:10];
}


@end
