//
//  ValidateCodeVC.h
//  My Asks
//
//  Created by MAC on 08/06/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppData.h"
#import "APIClient.h"
#import "ChatClient.h"

@protocol ValidateCodeDelegate <NSObject>

@required

- (void) codeVarifiedSuccessfully;
- (void) codeVarificationFailed;

@end

@interface ValidateCodeVC : UIViewController

@property (nonatomic, assign) id<ValidateCodeDelegate> delegate;

@property (nonatomic, strong) NSString *validateCode;
@end
