//
//  MAChatScreenVC.m
//  My Asks
//
//  Created by Mac on 31/03/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "MAChatScreenVC.h"
#import "DiscussionDetailView.h"
#import "MAChatPlacesCell.h"
#import "MAChatDetailPeopleCell.h"
#import "MAChattingVC.h"
#import "MAFriendListModel.h"

@interface MAChatScreenVC () <UITextViewDelegate,PNObjectEventListener, UITableViewDataSource, UITableViewDelegate,UIGestureRecognizerDelegate,MACreateNewListDelegate,MAAddPlactToListDelegate>
{
    AppDelegate *appDelegate;
    UIImage *userProfileImage;
    NSIndexPath *expandedIndexPath;
}

@property (weak, nonatomic) IBOutlet UIView *headeView;

@property (weak, nonatomic) IBOutlet UIPageControl *chatPageController;

@property (weak, nonatomic) IBOutlet UIView *viewPeople;

@property (weak, nonatomic) IBOutlet UIView *viewPlaces;

@property (weak, nonatomic) IBOutlet UIView *viewChat;

@property (weak, nonatomic) IBOutlet UIView *TopTabBarView;

@property (weak, nonatomic) IBOutlet UITableView *tblPlaces;

@property (weak, nonatomic) IBOutlet UITableView *tblPeople;

@property (weak, nonatomic) IBOutlet UIButton *btnOpenLeftSideMenu;

@property (weak, nonatomic) IBOutlet UIButton *btn3DotEdit;

@property (weak, nonatomic) IBOutlet UILabel *lblAsk;

@property (weak, nonatomic) IBOutlet UILabel *lblNoOfRecomendAsk;

@property (weak, nonatomic) IBOutlet UILabel *nameLbl;

@property (weak, nonatomic) IBOutlet UILabel *dateLbl;

@property (weak, nonatomic) IBOutlet UIButton *btnPeople;

@property (weak, nonatomic) IBOutlet UIButton *btnPlaces;

@property (weak, nonatomic) IBOutlet UIButton *btnChat;

@property (strong, nonatomic) NSMutableArray *selectedLocationsArray;

@property (strong, nonatomic) NSMutableArray *chatPeopleArray;

@property (strong, nonatomic) NSMutableArray *chatInvitedContactsList;

@end



@implementation MAChatScreenVC

#pragma mark - View lifecycle

- (void)viewDidLoad {
    
    [super viewDidLoad];    
    
    userProfileImage = [[AppData sharedManager]getUserProfileImage];
    
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDelegate slideMenuOpenWithPan:NO];
    
    self.chatPeopleArray = [[NSMutableArray alloc] init];
    self.chatPlaceArray = [[NSMutableArray alloc] init];
    
    
    self.tblPlaces.delegate = self;
    self.tblPlaces.dataSource = self;
    
    self.tblPeople.delegate = self;
    self.tblPeople.dataSource = self;

    self.tblPlaces.separatorStyle = UITableViewCellSeparatorStyleNone;

    UISwipeGestureRecognizer * swipeleft=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swipeleft:)];
    swipeleft.direction=UISwipeGestureRecognizerDirectionLeft;
    [self.view addGestureRecognizer:swipeleft];
    
    UISwipeGestureRecognizer * swiperight=[[UISwipeGestureRecognizer alloc]initWithTarget:self action:@selector(swiperight:)];
    swiperight.direction=UISwipeGestureRecognizerDirectionRight;
    [self.view addGestureRecognizer:swiperight];
    
    if (self.selectedChatDialod.userID == [self quickBloxUserID]) {
        self.btn3DotEdit.hidden = NO;
    }
    else{
        self.btn3DotEdit.hidden = YES;
    }
    
    MAChattingVC *chattingVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MAChattingVC"];
    
    chattingVC.parentVC = self;
    chattingVC.selectedChatDialod = self.selectedChatDialod;
    [self addChildViewController:chattingVC];
    [chattingVC.view setFrame:CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height - 185 - 49 )];
    [self.viewChat addSubview:chattingVC.view];
    [chattingVC didMoveToParentViewController:self];
    
    [self fetchDialogDetailFromRexServer:nil];
}

#pragma mark -

- (NSUInteger)quickBloxUserID {
    return [QBSession currentSession].currentUser.ID;
}

#pragma mark - 

-(void)fetchDialogDetailFromRexServer:(void(^)())completionBlock; {
    
    [[APIClient sharedManager] getAskByDialog:@{ @"dialog" : self.selectedChatDialod.ID, @"UserId" : [AppData sharedManager].userID} onCompletion:^(NSDictionary *resultData, bool success) {
        
        if (success) {
            
            NSInteger invitedContactsCount = self.selectedChatDialod.occupantIDs.count-1;// remove self's count

            NSString *recomendString;
            NSString *nameText;

            if (self.selectedChatDialod.userID == [self quickBloxUserID]) {

                nameText = @"You";
                
                if(invitedContactsCount > 1) {
                    recomendString = [NSString stringWithFormat:@"You asked %ld friends",(long)invitedContactsCount];
                }
                else {
                    recomendString = [NSString stringWithFormat:@"You asked %ld friend",(long)invitedContactsCount];
                }
            }
            else{
                
                nameText = resultData[@"OwnerName"];

                if(invitedContactsCount > 1) {
                    recomendString = [NSString stringWithFormat:@"%@ asked %ld friends",resultData[@"OwnerName"], (long)invitedContactsCount];
                }
                else {
                    recomendString = [NSString stringWithFormat:@"%@ asked %ld friend",resultData[@"OwnerName"], (long)invitedContactsCount];
                }
            }

            self.lblNoOfRecomendAsk.text = recomendString;
            self.lblAsk.text = resultData[@"Question"];
            self.nameLbl.text = nameText;
            self.dateLbl.text = [[AppData sharedManager] getYesterdayOrDateStringFromDate:[[AppData sharedManager] getLocalDateFrmGMT:resultData[@"CreatedDate"]]];
            
            
            self.chatPeopleArray = resultData[@"InvitedContacts"];
            self.chatPlaceArray = resultData[@"AskDetails"];
            self.chatInvitedContactsList =resultData[@"InvitedContactsList"];

            [self.tblPeople reloadData];
            [self.tblPlaces reloadData];
        }
        
        if (completionBlock != nil) {
            completionBlock();
        }
    }];
}

#pragma mark - Swipe Gesture methods

- (void)swiperight:(UISwipeGestureRecognizer*)gestureRecognizer
{
    NSInteger page = self.chatPageController.currentPage;
    
    if (page == 2) {
        [self btnChatClicked:self];
    }
    else if (page == 1) {
        [self btnPeopleClicked:self];
    }
    else {
        [self btnPeopleClicked:self];
    }
}

- (void)swipeleft:(UISwipeGestureRecognizer*)gestureRecognizer
{
    NSInteger page = self.chatPageController.currentPage;
    if (page == 2) {
        [self btnPlacesClicked:self];
    }
    else if (page == 1) {
        [self btnPlacesClicked:self];
    }
    else {
        [self btnChatClicked:self];
    }
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.view layoutIfNeeded];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated
{
    [super viewWillDisappear:animated];

}

- (void)viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
}

#pragma mark - UITableView Delegate/DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if(tableView == self.tblPlaces) {
        return [self.chatPlaceArray count];
    }
    else {
        return [self.chatPeopleArray count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == self.tblPlaces) {
        
        MAChatPlacesCell *addLocationsCell = [tableView dequeueReusableCellWithIdentifier:@"MAChatPlacesCell" forIndexPath: indexPath];
        
        NSDictionary *dict = [self.chatPlaceArray objectAtIndex:indexPath.row];
        addLocationsCell.lblPlaceName.text = NilCheckOfNSString( dict[@"Name"]);
        addLocationsCell.lblPlaceDetail.text = NilCheckOfNSString( dict[@"Address"]);
        addLocationsCell.lblDistance.text = NilCheckOfNSString( dict[@"DistanceFromUserLocation"]);
        
        return  addLocationsCell;
    }
    else {
        MAChatDetailPeopleCell *peopleCell = [tableView dequeueReusableCellWithIdentifier:@"MAChatDetailPeopleCell" forIndexPath: indexPath];
        
        NSDictionary *dict = [self.chatPeopleArray objectAtIndex:indexPath.row];

        peopleCell.lblPeopleName.text = dict[@"QuickbloxUserNickName"];
        
        return peopleCell;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (tableView == self.tblPlaces) {
        
        NSDictionary *dict = [self.chatPlaceArray objectAtIndex:indexPath.row];

        CGFloat width = [UIScreen mainScreen].bounds.size.width;
        CGRect frame1 = [NilCheckOfNSString( dict[@"Name"]) boundingRectWithSize:CGSizeMake(width-145, CGFLOAT_MAX)
                                                           options:NSStringDrawingUsesLineFragmentOrigin
                                                        attributes:@{NSFontAttributeName:[UIFont boldSystemFontOfSize:16]}
                                                           context:nil];
        
        CGRect frame2 = [NilCheckOfNSString( dict[@"Address"]) boundingRectWithSize:CGSizeMake(width-145, CGFLOAT_MAX)
                                          options:NSStringDrawingUsesLineFragmentOrigin
                                       attributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16]}
                                          context:nil];
        
        return 110 - 40 + frame1.size.height +  frame2.size.height;
    }
    else {
        
        return 60;
    }
}

#pragma mark - SlideNavigationController Methods -

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return NO;
}

- (BOOL)slideNavigationControllerShouldDisplayRightMenu
{
    return YES;
}

#pragma mark - Button Actions

- (IBAction)btnBackClicked:(id)sender {
    
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)btnOpenLeftSideMenuClicked:(id)sender {
    [appDelegate openRightSlideDrawer];
}

- (IBAction)btnChatClicked:(id)sender {

    [self.view endEditing:YES];
    
    self.viewChat.hidden = false;
    self.viewPeople.hidden = true;
    self.viewPlaces.hidden = true;
    
    //self.btnChat.backgroundColor = [UIColor darkGrayColor];
    [self.btnChat setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.btnChat setImage:[UIImage imageNamed:@"comments_white.png"] forState:UIControlStateNormal];
    
    //self.btnPlaces.backgroundColor = [UIColor lightGrayColor];
    [self.btnPlaces setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [self.btnPlaces setImage:[UIImage imageNamed:@"ask_places_gray.png"] forState:UIControlStateNormal];
    
    //self.btnPeople.backgroundColor = [UIColor lightGrayColor];
    [self.btnPeople setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [self.btnPeople setImage:[UIImage imageNamed:@"ask_people_gray.png"] forState:UIControlStateNormal];
    
    self.chatPageController.currentPage = 1;
}

- (IBAction)btnPlacesClicked:(id)sender {
    [self.view endEditing:YES];
    
    if(self.viewPlaces.hidden){
    CGRect placesDefaultFrame = self.viewPlaces.frame;
    placesDefaultFrame.origin.x = 0;
    
    CGRect placesAnimationFrame = self.viewPlaces.frame;
    placesAnimationFrame.origin.x = -placesAnimationFrame.size.width;
    
    self.viewPlaces.frame = placesAnimationFrame;
    
    [UIView animateWithDuration:0.2
                          delay:0.1
                        options: (UIViewAnimationOptions)UIViewAnimationCurveEaseOut
                     animations:^{
                         self.viewPlaces.frame = placesDefaultFrame;
                     }
                     completion:^(BOOL finished){
                         NSLog(@"Done!");
                     }];
    }
    
    //self.btnPlaces.backgroundColor = [UIColor darkGrayColor];
    [self.btnPlaces setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.btnPlaces setImage:[UIImage imageNamed:@"ask_places_white.png"] forState:UIControlStateNormal];
    
    //self.btnPeople.backgroundColor = [UIColor lightGrayColor];
    [self.btnPeople setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [self.btnPeople setImage:[UIImage imageNamed:@"ask_people_gray.png"] forState:UIControlStateNormal];
    
    //self.btnChat.backgroundColor = [UIColor lightGrayColor];
    [self.btnChat setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [self.btnChat setImage:[UIImage imageNamed:@"comments-gray.png"] forState:UIControlStateNormal];
    
    self.viewChat.hidden = true;
    self.viewPeople.hidden = true;
    self.viewPlaces.hidden = false;
    
    self.chatPageController.currentPage = 2;
}

- (IBAction)btnPeopleClicked:(id)sender {
    
    [self.view endEditing:YES];
    
    if(self.viewPeople.hidden){
    CGRect peopleDefaultFrame = self.viewPeople.frame;
    peopleDefaultFrame.origin.x = 0;
    
    CGRect peopleAnimationFrame = self.viewPeople.frame;
    peopleAnimationFrame.origin.x = -peopleAnimationFrame.size.width;
    
    self.viewPeople.frame = peopleAnimationFrame;
    
    [UIView animateWithDuration:0.2
                          delay:0.1
                        options: (UIViewAnimationOptions)UIViewAnimationCurveEaseOut
                     animations:^{
                         self.viewPeople.frame = peopleDefaultFrame;
                     }
                     completion:^(BOOL finished){
                         NSLog(@"Done!");
                     }];
    }
    
    //self.btnPeople.backgroundColor = [UIColor darkGrayColor];
    [self.btnPeople setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.btnPeople setImage:[UIImage imageNamed:@"ask_people_white.png"] forState:UIControlStateNormal];
    
    //self.btnPlaces.backgroundColor = [UIColor lightGrayColor];
    [self.btnPlaces setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [self.btnPlaces setImage:[UIImage imageNamed:@"ask_places_gray.png"] forState:UIControlStateNormal];
    
    //self.btnChat.backgroundColor = [UIColor lightGrayColor];
    [self.btnChat setTitleColor:[UIColor darkGrayColor] forState:UIControlStateNormal];
    [self.btnChat setImage:[UIImage imageNamed:@"comments-gray.png"] forState:UIControlStateNormal];
    
    self.viewChat.hidden = true;
    self.viewPlaces.hidden = true;
    self.viewPeople.hidden = false;
    
    self.chatPageController.currentPage = 0;
}

- (IBAction)chatPageControllerChanged:(id)sender {
    NSInteger page = self.chatPageController.currentPage;
    
    if(page == 0) {
        [self btnPeopleClicked: self];
    }
    else if(page == 1) {
        [self btnChatClicked: self];
    }
    else {
        [self btnPlacesClicked: self];
    }
}

- (IBAction)editGroupBtnAction:(UIButton *)sender {
    
    NSMutableArray* arr = [NSMutableArray new];
    
    for (NSDictionary *list in self.chatInvitedContactsList) {
        
        MAFriendListModel *friendListModel = [MAFriendListModel new];
        
        friendListModel.Id = [list valueForKey:@"Id"];
        friendListModel.ContactNumber = [list valueForKey:@"ContactNumber"];
        friendListModel.ContactName = [list valueForKey:@"ContactName"];
        friendListModel.ListCount = [list valueForKey:@"ListCount"];
        friendListModel.PictureCount = [list valueForKey:@"PictureCount"];
        friendListModel.PlacesCount = [list valueForKey:@"PlacesCount"];
        friendListModel.IsRexUser = [[list valueForKey:@"IsRexUser"] boolValue];
        friendListModel.ProfilePicture = [list valueForKey:@"ProfilePicture"];
    
        [arr addObject:friendListModel];
    }


    NewAskScreenVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"NewAskScreenVC"];
    vc.isForEdit = YES;
    vc.contactsArray = arr;
    vc.selectedChatDialod = self.selectedChatDialod;
    [self.navigationController pushViewController:vc animated:YES];

}

@end

