//
//  MAListDetailVC.h
//  My Asks
//
//  Created by MY PC on 4/19/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MACreateNewListCell.h"
#import <KVNProgress.h>
#import "BIZPopupViewController.h"
#import "AppDelegate.h"
#import "AppData.h"
#import "MAUserListsModel.h"
#import "APIClient.h"
#import "MACreateNewListVC.h"
#import "MAPlacesDetailVC.h"


@interface MAListDetailVC : UIViewController<UITableViewDataSource, UITableViewDelegate, UITextFieldDelegate>

{
     MAUserListsPlacesModel *userListsPlacesModel;
    MAListOrPlaceSearchModel *listOrPlaceSearchModel;
}
@property (weak, nonatomic) IBOutlet UITableView *tblListDetail;

@property (strong, nonatomic) MAUserListsModel *userlistData;

@property (strong, nonatomic) NSMutableArray *userPlacesArray;
@property (strong, nonatomic) NSMutableArray *searchPlacesArray;

@property (strong, nonatomic) NSString *userId;
@property (strong, nonatomic) NSString *userListId;
@property (assign, nonatomic) BOOL isSearching;

@property (strong, nonatomic) IBOutlet UILabel *lblTitle;

@property (strong, nonatomic) IBOutlet UITextField *txtSearchPlaces;
@property (weak, nonatomic) IBOutlet UIButton *btnLock;
@property (weak, nonatomic) IBOutlet UIButton *btnEdit;
@property (strong, nonatomic) IBOutlet UIImageView *imgLock;

- (IBAction)btnBackClicked:(id)sender;
- (IBAction)btnEditClicked:(id)sender;

@end
