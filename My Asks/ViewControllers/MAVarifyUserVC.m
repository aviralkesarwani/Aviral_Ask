//
//  MAVarifyUserVC.m
//  My Asks
//
//  Created by artis on 21/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "MAVarifyUserVC.h"

@interface MAVarifyUserVC ()

@end

@implementation MAVarifyUserVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    
    self.txt1.tag = 1;
    self.txt2.tag = 2;
    self.txt3.tag = 3;
    self.txt4.tag = 4;

    self.txt1.delegate = self;
    self.txt2.delegate = self;
    self.txt3.delegate = self;
    self.txt4.delegate = self;
    [self.txt1 becomeFirstResponder];

    self.lblMobileNumber.text = [NSString stringWithFormat:@"+%@ %@",[AppData sharedManager].userCountryCode,[AppData sharedManager].userMobileNumber];
    
    self.lblValidationFail.hidden = true;
    
    ContactList = [[NSMutableArray alloc]init];
    contactsStrore = [[CNContactStore alloc] init];
    contactDetails = [[NSMutableArray alloc]init];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - TextField Delegate

-(BOOL)textFieldShouldReturn:(UITextField*)textField;
{
    if (textField == self.txt1)
    {
        [self.txt2 becomeFirstResponder];
    }
    else if (textField == self.txt2)
    {
        [self.txt3 becomeFirstResponder];
    }
    else if (textField == self.txt3)
    {
        [self.txt4 becomeFirstResponder];
    }
    
    return NO;
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string
{
    if ((textField.text.length < 1) && (string.length > 0))
    {
        NSInteger nextTag = textField.tag + 1;
        UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
        if (!nextResponder)
            nextResponder = [textField.superview viewWithTag:1];
        textField.text = string;
        [nextResponder becomeFirstResponder];
        if (textField.tag == 4) {
            [self performSelector:@selector(varifyCode) withObject:self.txt4 afterDelay:0.01];
        }
        return NO;
    }
    else if((textField.text.length >= 1) && (string.length == 0)) {
        NSInteger previousTag = textField.tag - 1;
        UIResponder* previousResponder = [textField.superview viewWithTag:previousTag];
        if (!previousResponder)
            previousResponder = [textField.superview viewWithTag:1];
        textField.text = @"";
        [previousResponder becomeFirstResponder];
        return NO;
    }
    else {
        NSInteger nextTag = textField.tag + 1;
        UIResponder* nextResponder = [textField.superview viewWithTag:nextTag];
        if (!nextResponder)
            nextResponder = [textField.superview viewWithTag:1];
        textField.text = string;
        [nextResponder becomeFirstResponder];
        if (textField.tag == 4) {
            [self performSelector:@selector(varifyCode) withObject:self.txt4 afterDelay:0.01];
        }
        return NO;
    }
    return YES;
}

- (void)setNextResponder:(UITextField *)nextResponder
{
    [nextResponder becomeFirstResponder];
}

#pragma mark - Custom Methods

-(void)varifyCode{
    [self.view endEditing:true];
    
    NSString *code = [NSString stringWithFormat:@"%@%@%@%@",self.txt1.text,self.txt2.text,self.txt3.text,self.txt4.text];
    if(code.length == 4) {
        ValidateCodeVC *validateCode = [self.storyboard instantiateViewControllerWithIdentifier:@"ValidateCodeVC"];
        validateCode.validateCode = code;
        validateCode.delegate = self;
        [self presentViewController:validateCode animated:true completion:nil];
    }
}

- (void) setTabBar {
    
    AppDelegate *appdelegte =(AppDelegate*)[[UIApplication sharedApplication]delegate];
    appdelegte.window.rootViewController = appdelegte.container;
    [appdelegte.window makeKeyAndVisible];
}

#pragma mark - Button Action

- (IBAction)btnBackClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

#pragma mark - UIalertview Delegate

-(void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 1) {
        [self checkContactsAccess];
    }
}

#pragma mark - Check for contact access

-(void)checkContactsAccess{
    
    MALoginWithFBVC *vc =[self.storyboard instantiateViewControllerWithIdentifier:@"MALoginWithFBVC"];
    [self.navigationController pushViewController:vc animated:YES];
}

-(void)requestContactsAccessWithHandler:(void (^)(BOOL grandted))handler{
    
    switch ([CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts]) {
        case CNAuthorizationStatusAuthorized:
            handler(YES);
            break;
        case CNAuthorizationStatusDenied:
        case CNAuthorizationStatusNotDetermined:{
            [contactsStrore requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
                
                handler(granted);
            }];
            break;
        }
        case CNAuthorizationStatusRestricted:
            handler(NO);
            break;
    }
}

#pragma Validate Code Delegate

- (void)codeVarificationFailed {
    self.lblValidationFail.hidden = false;
    self.lblVarificationText1.hidden = true;
    self.lblVarificationText2.hidden = true;
    
    self.txt1.text = @"";
    self.txt2.text = @"";
    self.txt3.text = @"";
    self.txt4.text = @"";
    
    [self.txt1 becomeFirstResponder];

}

- (void)codeVarifiedSuccessfully {
    self.lblValidationFail.hidden = true;
    self.lblVarificationText1.hidden = false;
    self.lblVarificationText2.hidden = false;


    [self checkContactsAccess];

 }

@end
