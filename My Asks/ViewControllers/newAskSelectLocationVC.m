//
//  newAskSelectLocationVC.m
//  My Asks
//
//  Created by artis on 08/07/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "newAskSelectLocationVC.h"

@interface newAskSelectLocationVC ()

@end

@implementation newAskSelectLocationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.searchContactArray = [[NSMutableArray alloc] init];
    
    
   UITextField *textField = [self.searchBar valueForKey:@"_searchField"];
    textField.backgroundColor = [UIColor whiteColor];
    
    
//    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setBackgroundColor:[UIColor whiteColor]];
//    [[UITextField appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]] setBackgroundColor:[UIColor greenColor]];
//    [[UILabel appearanceWhenContainedInInstancesOfClasses:@[[UISearchBar class]]]setBackgroundColor:[UIColor greenColor]];
    
    placesSearchArray = [[NSMutableArray alloc]init];
    
    _pslocationManager = [PSLocationManager new];
    [_pslocationManager setDelegate:self];
    [_pslocationManager setMaximumLatency:20];
    [_pslocationManager setPausesLocationUpdatesAutomatically:NO];
    [_pslocationManager startUpdatingLocation];
    [_pslocationManager requestWhenInUseAuthorization];
    [_pslocationManager setDesiredAccuracy:kPSLocationAccuracyPathSenseNavigation];
    if ([_pslocationManager respondsToSelector:@selector(requestAlwaysAuthorization)]) { // iOS 8 or later
        [_pslocationManager requestAlwaysAuthorization];
    }
    
    [self.pslocationManager startUpdatingLocation];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    
    self.goBtn.layer.cornerRadius = 5;
    self.goBtn.layer.masksToBounds = YES;
}

#pragma mark - Searchbar 

-(void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar{
    //self.searchBar.showsCancelButton = YES;
}
-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
    self.isSearching = YES;
    [self filterListForSearchText:searchText];
    [self.tblLocation reloadData];
}
- (void)filterListForSearchText:(NSString *)searchText{
    
    @try {
        NSPredicate *resultPredicate = [NSPredicate
                                        predicateWithFormat:@"PlaceName contains[cd] %@",
                                        searchText];
        NSArray *searchArray = [placesSearchArray filteredArrayUsingPredicate:resultPredicate];
        self.searchContactArray =[NSMutableArray arrayWithArray:searchArray];
        
    }
    @catch (NSException *exception) {
        
    }
    @finally {
        
    }
    
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar{
    NSLog(@"done");
}

-(void)searchBarCancelButtonClicked:(UISearchBar *)searchBar{
    self.searchBar.text=@"";
    self.searchBar.showsCancelButton=NO;
    self.isSearching=NO;
    [self.view endEditing:YES];
    
    [self.tblLocation reloadData];
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar{
    self.searchBar.showsCancelButton=NO;
    [self.view endEditing:YES];
}

#pragma mark - Tableview Delegate/Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.isSearching && self.searchBar.text.length != 0) {
        return [self.searchContactArray count];
    }
    else {
        return placesSearchArray.count;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
     MAInviteFriendsTableCell *inviteFriendsTableCell = [tableView dequeueReusableCellWithIdentifier:@"MAInviteFriendsTableCell" forIndexPath: indexPath];
    
    MASearchPlacesModel *placesModel = [placesSearchArray objectAtIndex:indexPath.row];
    
    if(placesModel.PlaceName == (id)[NSNull null]) {
        inviteFriendsTableCell.placeNameLbl.text = @"";
    }
    else{
        inviteFriendsTableCell.placeNameLbl.text = placesModel.PlaceName;
    }
    if(placesModel.PlaceAddress == (id)[NSNull null]) {
        
        inviteFriendsTableCell.placeAddresslbl.text = @"";
    }
    else{
        inviteFriendsTableCell.placeAddresslbl.text =  [NSString stringWithFormat:@"%@",placesModel.PlaceAddress];
    }

    return  inviteFriendsTableCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    if (self.isSearching && self.searchBar.text.length != 0){
        MASearchPlacesModel *placesModel = [self.searchContactArray objectAtIndex:indexPath.row];
        [self.delegate selectedLocationDetails:placesModel];
    }
    else{
        MASearchPlacesModel *placesModel = [placesSearchArray objectAtIndex:indexPath.row];
        [self.delegate selectedLocationDetails:placesModel];
    }
    
    [self.navigationController popViewControllerAnimated:YES];

}

- (IBAction)btnCancelClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnGoClicked:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations {
    
    [self.pslocationManager stopUpdatingLocation];
    
    CLLocation *location = [locations lastObject];
    
    // this creates a MKReverseGeocoder to find a placemark using the found coordinates
    
    CLGeocoder *ceo = [[CLGeocoder alloc]init];
    CLLocation *loc = [[CLLocation alloc]initWithLatitude:location.coordinate.latitude longitude:location.coordinate.longitude]; //insert your coordinates
    
    [ceo reverseGeocodeLocation:loc
              completionHandler:^(NSArray *placemarks, NSError *error) {
                  
                  CLPlacemark *placemark = [placemarks objectAtIndex:0];
                  
                  self.latitude =  [[NSNumber numberWithDouble:location.coordinate.latitude]stringValue];
                  self.longitude =  [[NSNumber numberWithDouble:location.coordinate.longitude]stringValue];
                  
                  [AppData sharedManager].latitude = [[NSNumber numberWithDouble:location.coordinate.latitude]stringValue];
                  [AppData sharedManager].longitude = [[NSNumber numberWithDouble:location.coordinate.longitude]stringValue];
                  
                  self.currentLatitude = self.latitude;
                  self.currentLongitude = self.longitude;
                  
                  // NSLog(@"placemark.subLocality %@",placemark.subLocality);
                  
                  self.subLocality =  [NSString stringWithFormat:@"%@,%@",placemark.subLocality,placemark.locality];
                [self getCurrentPlace];
              }
     ];
}

- (void) getCurrentPlace {
    
    self.loadFromGooglePlace = true;
    
    GMSPlacesClient *placesClient = [[GMSPlacesClient alloc]init];
    [placesClient currentPlaceWithCallback:^(GMSPlaceLikelihoodList *likelihoodList, NSError *error) {
        if (error != nil) {
            NSLog(@"Current Place error %@", [error localizedDescription]);
            return;
        }
        
        NSLog(@"%@",likelihoodList.description);
        for (GMSPlaceLikelihood *likelihood in likelihoodList.likelihoods) {
            GMSPlace* place = likelihood.place;
            NSLog(@"Current Place name %@ at likelihood %g", place.name, likelihood.likelihood);
            NSLog(@"Current Place address %@", place.formattedAddress);
            NSLog(@"Current Place attributions %@", place.attributions);
            NSLog(@"Current PlaceID %@", place.placeID);
        }
        [self getPSLocation:likelihoodList];
    }];
    
}

- (void) getPSLocation:(GMSPlaceLikelihoodList *)likelihoodList{
    
    [placesSearchArray removeAllObjects];
    
    for (GMSPlaceLikelihood *likelihood in likelihoodList.likelihoods) {
        
        GMSPlace* place = likelihood.place;
        NSLog(@"Current Place name %@ at likelihood %g", place.name, likelihood.likelihood);
        NSLog(@"Current Place address %@", place.formattedAddress);
        NSLog(@"Current Place attributions %@", place.attributions);
        NSLog(@"Current PlaceID %@", place.placeID);
        NSLog(@"Current PlaceID %@", place.description);
        
        MASearchPlacesModel *placesModel = [[MASearchPlacesModel alloc]init];
        
        placesModel.PlaceName = place.name;
        placesModel.PlaceId = place.placeID;
        placesModel.PlaceAddress = place.formattedAddress;
        placesModel.PlaceImageURL = @"";
        
        [placesSearchArray addObject:placesModel];
        
    }
    
    //[self.tblPlacesContent reloadData];
    [self getPlaceDetails];
    
}

-(void)getPlaceDetails{
    
    [AppData startNetworkIndicator];
    
    CLLocation *LocationCurrent = [[CLLocation alloc] initWithLatitude:[self.latitude floatValue] longitude:[self.longitude floatValue]];
    
    for (NSInteger i = 0; i < placesSearchArray.count; i++) {
        MASearchPlacesModel *placesModel = [placesSearchArray objectAtIndex:i];
        
        NSString *param = [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/details/json?placeid=%@&key=AIzaSyCyL6A9-IRxfUqept-WCEOC3Za2v_-27zI",placesModel.PlaceId];
        
        [[APIClient sharedManager]GetPlacesFilter:param indexOf:i onCompletion:^(NSDictionary *resultData, bool success, NSInteger index) {
            if (success) {
                if ([resultData count] == 0) {
                    
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
                                                                    message:@"Place details not available"
                                                                   delegate:self
                                                          cancelButtonTitle:@"OK"
                                                          otherButtonTitles:nil];
                    [alert show];
                }
                else{
                    
                    if ([resultData objectForKey:@"result"] != nil || [resultData objectForKey:@"result"] != (id)[NSNull null]) {
                        
                        NSLog(@"%@",[resultData valueForKeyPath:@"result"]);
                        NSDictionary *result = [resultData valueForKey:@"result"];
                        
                        if ([result valueForKey:@"photos"] != nil || [result valueForKey:@"photos"] != (id)[NSNull null]) {
                            
                            NSMutableArray *photoArray = [result valueForKey:@"photos"];
                            NSMutableDictionary *dic = [photoArray firstObject];
                            
                            NSLog(@"%@",[dic valueForKey:@"photo_reference"]);
                            NSString *photo_reference = [dic valueForKey:@"photo_reference"];
                            NSLog(@"%@",photo_reference);
                            
                            NSString *placeImageUrl =  [NSString stringWithFormat:@"https://maps.googleapis.com/maps/api/place/photo?photoreference=%@&sensor=false&maxheight=60&maxwidth=60&key=AIzaSyCyL6A9-IRxfUqept-WCEOC3Za2v_-27zI",photo_reference] ;
                            
                            MASearchPlacesModel *updatePlacesModel = [placesSearchArray objectAtIndex:index];
                            updatePlacesModel.PlaceImageURL = placeImageUrl;
                            
                            NSMutableArray *locationData = [result valueForKey:@"geometry"];
                            
                            if (locationData != nil || locationData != (id)[NSNull null]) {
                                
                                CLLocation *LocationActual = [[CLLocation alloc] initWithLatitude:[[locationData valueForKeyPath:@"location.lat"]floatValue] longitude:[[locationData valueForKeyPath:@"location.lng"]floatValue]];
                                CLLocationDistance distanceInMiles = [LocationCurrent distanceFromLocation:LocationActual];
                                
                                double totalDistanceBetween = (distanceInMiles * 0.000621371192);
                                updatePlacesModel.DistanceinMiles = [NSString stringWithFormat:@"%.2f mi",totalDistanceBetween];
                                updatePlacesModel.PlaceLatitude =  [locationData valueForKeyPath:@"location.lat"];
                                updatePlacesModel.PlaceLongitude = [locationData valueForKeyPath:@"location.lng"];
                            }
                            else{
                                updatePlacesModel.DistanceinMiles = @"00.00mi";
                            }
                            
                        }
                    }
                }
            }
        }];
    }
    
    [self checkForFavoritePlace];
    [AppData stopNetworkIndicator];
    
    //[self.refreshControl endRefreshing];
    //[self.tblPlacesContent reloadData];
}
-(void)checkForFavoritePlace{
    
    [AppData startNetworkIndicator];
    
    NSString *userID = [[AppData sharedManager]getCurrentUserID];
    
    for (NSInteger i = 0; i < placesSearchArray.count; i++) {
        
        MASearchPlacesModel *placesModel = [placesSearchArray objectAtIndex:i];
        NSMutableDictionary *param =[[NSMutableDictionary alloc]init];
        
        [param setValue:placesModel.PlaceId forKey:@"placeId"];
        [param setValue:userID forKey:@"UserID"];
        
        [[APIClient sharedManager]PlaceFavorite:param onCompletion:^(NSDictionary *resultData, bool success) {
            
            placesModel.isFavorite = [[resultData valueForKey:@"Success"]boolValue];
        }];
    }
    
    [AppData stopNetworkIndicator];
    
    [_tblLocation reloadData];
    
}

@end
