//
//  MACreateNewListVC.h
//  My Asks
//
//  Created by MY PC on 4/19/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MACreateNewListCell.h"
#import <KVNProgress.h>
#import "BIZPopupViewController.h"
#import "AppDelegate.h"
#import "AppData.h"
#import "MAUserListsModel.h"
#import "MAUserListsPlacesModel.h"
#import "APIClient.h"
#import "MAListOrPlaceSearchModel.h"
#import "MAInsertUpdateUserListsModel.h"

@protocol MACreateNewListDelegate <NSObject>

@required

- (void)doneAdding:(NSString *)placeId;

@end

@interface MACreateNewListVC : UIViewController<UITableViewDelegate, UITableViewDataSource, UITextFieldDelegate>


@property (nonatomic, assign) id<MACreateNewListDelegate> delegate;

@property (strong, nonatomic) MAUserListsModel *userlistData;

@property (strong, nonatomic) MAListOrPlaceSearchModel *listOrPlaceSearchModel;

@property (strong, nonatomic) MAInsertUpdateUserListsModel *insertUpdateUserListsModel;

@property (strong, nonatomic) NSMutableArray *userPlacesArray;
@property (strong, nonatomic) NSMutableArray *userListsArray;

@property (strong, nonatomic) NSMutableArray *searchPlacesArray;
@property (strong, nonatomic) NSMutableArray *selectedPlacesArray;
@property (assign, nonatomic) BOOL isSearching;


@property (strong, nonatomic) NSString *listType;
@property (strong, nonatomic) NSString *placeIdForAddPlaceToList;

@property (weak, nonatomic) IBOutlet UITextField *txtListName;
@property (weak, nonatomic) IBOutlet UISwitch *switchSecret;
@property (weak, nonatomic) IBOutlet UITableView *tblPlaceList;
@property (weak, nonatomic) IBOutlet UISwitch *switchDefault;
@property (weak, nonatomic) IBOutlet UILabel *lblListType;
@property (weak, nonatomic) IBOutlet UITextField *txtSearchPlaces;

- (IBAction)btnBackClicked:(id)sender;
- (IBAction)btnDoneClicked:(id)sender;

@end
