//
//  MAPlacesMapVC.m
//  My Asks
//
//  Created by artis on 30/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "MAPlacesMapVC.h"

@interface MAPlacesMapVC ()<PSLocationManagerDelegate>

@end

@implementation MAPlacesMapVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    placeSearchArray = [[NSMutableArray alloc]init];
    locationDict = [[NSMutableDictionary alloc]init];
    
//    self.locationManager = [[CLLocationManager alloc] init];
//    self.locationManager.delegate = self;
//    self.locationManager.desiredAccuracy = kCLLocationAccuracyKilometer;
//    [self.locationManager requestWhenInUseAuthorization];
    
    self.psLocationManager = [PSLocationManager new];
    [self.psLocationManager setDelegate:self];
    [self.psLocationManager setMaximumLatency:20];
    
    //    if ([CMMotionActivityManager isActivityAvailable]) {
    //        [_pslocationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    //    }
    // else {
    [self.psLocationManager setDesiredAccuracy:kPSLocationAccuracyPathSenseNavigation];
    //}
    
    [self.psLocationManager requestWhenInUseAuthorization];
    
    self.googleMapView.delegate = self;
    [self setmarker];
    //[self.locationManager startUpdatingLocation];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//-(void)getPlacesList{
//    
//    NSMutableDictionary *param =[[NSMutableDictionary alloc]init];
//    NSString *userID = [[AppData sharedManager]getCurrentUserID];
//    
//    [param setValue:self.latitude forKey:@"Latitude"];
//    [param setValue:self.longitude forKey:@"Longitude"];
//    [param setValue:@"" forKey:@"RestaurantName"];
//    [param setValue:@"" forKey:@"RestaurantType"];
//    [param setValue:userID forKey:@"UserID"];
//    [param setValue:@"2000" forKey:@"Radius"];
//    
//    [KVNProgress show];
//    
//    [[APIClient sharedManager]SearchPlaces:param onCompletion:^(NSDictionary *resultData, bool success) {
//        
//        [placeSearchArray removeAllObjects];
//        
//        [KVNProgress dismiss];
//        NSLog(@"%@",resultData);
//        
//        if ([resultData count] == 0) {
//            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
//                                                            message:@"No places to display"
//                                                           delegate:self
//                                                  cancelButtonTitle:@"OK"
//                                                  otherButtonTitles:nil];
//            [alert show];
//            
//        }
//        else{
//            
//            NSMutableArray *placesArray = [resultData valueForKey:@"Places"];
//        
//            for (NSDictionary *places in placesArray) {
//                
//                //                NSLog(@"%@",places);
//                //
//                //                NSLog(@"%@",[places valueForKey:@"UserID"]);
//                //                NSLog(@"%@",[places valueForKey:@"PlaceLatitude"]);
//                //                NSLog(@"%@",[places valueForKey:@"PlaceLongitude"]);
//                //                NSLog(@"%@",[places valueForKey:@"PlaceTypes"]);
//                //                NSLog(@"%@",[places valueForKey:@"PlaceName"]);
//                //                NSLog(@"%@",[places valueForKey:@"PlaceId"]);
//                //                NSLog(@"%@",[places valueForKey:@"DistanceinMiles"]);
//                //                NSLog(@"%@",[places valueForKey:@"DistanceinKMs"]);
//                //                NSLog(@"%@",[places valueForKey:@"isFavorite"]);
//                //                NSLog(@"%@",[places valueForKey:@"LikesCount"]);
//                //                NSLog(@"%@",[places valueForKey:@"CommentsCount"]);
//                //                NSLog(@"%@",[places valueForKey:@"PlaceAddress"]);
//                //                NSLog(@"%@",[places valueForKey:@"PlaceImage"]);
//                //                NSLog(@"%@",[places valueForKey:@"PlaceImageURL"]);
//                //                NSLog(@"%@",[places valueForKey:@"InternationalPhoneNumber"]);
//                
//                NSLog(@"%@",[places valueForKey:@"PlaceName"]);
//                
//                MASearchPlacesModel *placesModel = [[MASearchPlacesModel alloc]init];
//                
//                placesModel.UserID = [places valueForKey:@"UserID"];
//                placesModel.PlaceLatitude = [places valueForKey:@"PlaceLatitude"];
//                placesModel.PlaceLongitude = [places valueForKey:@"PlaceLongitude"];
//                placesModel.PlaceTypes = [places valueForKey:@"PlaceTypes"];
//                placesModel.PlaceName = [places valueForKey:@"PlaceName"];
//                placesModel.PlaceId = [places valueForKey:@"PlaceId"];
//                
//                
//                NSString *distance;
//                
//                if ([places valueForKey:@"DistanceinMiles"] == (id)[NSNull null]) {
//                    
//                    distance = @"00.00mi";
//                }
//                else{
//                    float distanceMiles = [[places valueForKey:@"DistanceinMiles"]floatValue];
//                    
//                    distance = [NSString stringWithFormat:@"%.2f mi",distanceMiles];
//                }
//                
//                placesModel.DistanceinMiles = distance;
//                placesModel.DistanceinKMs = [places valueForKey:@"DistanceinKMs"];
//                placesModel.isFavorite = [[places valueForKey:@"isFavorite"]boolValue];
//                placesModel.LikesCount = [places valueForKey:@"LikesCount"];
//                placesModel.CommentsCount = [places valueForKey:@"CommentsCount"];
//                placesModel.PlaceAddress = [places valueForKey:@"PlaceAddress"];
//                placesModel.PlaceImage = [places valueForKey:@"PlaceImage"];
//                placesModel.PlaceImageURL = [places valueForKey:@"PlaceImageURL"];
//                placesModel.InternationalPhoneNumber = [places valueForKey:@"InternationalPhoneNumber"];
//                //placesModel.OpeningHours = [places valueForKey:@"OpeningHours"];
//                
//                NSLog(@"%@",placesModel);
//                
//                [placeSearchArray addObject:placesModel];
//                
//                NSLog(@"%@",placesModel.PlaceLatitude);
//                NSLog(@"%@",placesModel.PlaceLongitude);
//            }
//            [self setmarker];
//            
//        }
//        
//    }];
//}


//- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation
//{
//    // this creates a MKReverseGeocoder to find a placemark using the found coordinates
//    
//        self.latitude =  [[NSNumber numberWithDouble:newLocation.coordinate.latitude]stringValue];
//        self.longitude =  [[NSNumber numberWithDouble:newLocation.coordinate.longitude]stringValue];
//    
//    currentLocation = newLocation;
//    
//    //if (  oldLocation.coordinate.latitude != newLocation.coordinate.latitude &&  oldLocation.coordinate.longitude != newLocation.coordinate.longitude){
//        [self getPlacesList];
//   // }
//   
//        // NSLog(@"placemark.subLocality %@",placemark.subLocality);
//                  
//    
//      [self.locationManager stopUpdatingLocation];
//
//}

-(void)setmarker{
    
    for ( MASearchPlacesModel *placesModel in self.placesSearchArray) {
        
        double latitude,longitude;
        
        GMSMarker *marker = [[GMSMarker alloc] init];
        
        latitude = [placesModel.PlaceLatitude doubleValue];
        longitude = [placesModel.PlaceLongitude doubleValue];
        
        NSLog(@"%f",latitude);
        NSLog(@"%f",longitude);
        
        marker.position=CLLocationCoordinate2DMake(latitude,longitude);
        
        marker.appearAnimation = YES;
        
        UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0,0,200,60)];
        
        UIImageView *pinImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"Placeblock.png"]];
        
        UILabel *lblPlaceName = [[UILabel alloc]initWithFrame:CGRectMake(5, 5, 150, 20)];
        UILabel *lblPlaceDetails = [[UILabel alloc]initWithFrame:CGRectMake(5, 20,180 ,35)];
        
        lblPlaceName.numberOfLines = 2.0;
        lblPlaceDetails.numberOfLines = 2.0;
        
        [lblPlaceName setFont:[UIFont systemFontOfSize:12.0]];
        [lblPlaceDetails setFont:[UIFont systemFontOfSize:12.0]];
        
        lblPlaceName.textColor = [UIColor whiteColor];
        lblPlaceDetails.textColor = [UIColor whiteColor];
        
        lblPlaceName.text = placesModel.PlaceName;
        lblPlaceDetails.text = placesModel.PlaceAddress;
        
        [lblPlaceName sizeToFit];
        [lblPlaceDetails sizeToFit];
        
        [view addSubview:pinImageView];
        [view addSubview:lblPlaceName];
        [view addSubview:lblPlaceDetails];
        
       // UIImage *markerIcon = [[AppData sharedManager]imageFromView:view];
        
        //marker.icon = markerIcon;
        marker.title = placesModel.PlaceName;
        marker.snippet = placesModel.PlaceAddress;
        marker.map=self.googleMapView;
        
        GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] init];
        
        CLLocationCoordinate2D coordinate = marker.position;
        
        bounds = [bounds includingCoordinate:coordinate];
        self.googleMapView.myLocationEnabled = YES;
        
        [self.googleMapView animateWithCameraUpdate:[GMSCameraUpdate fitBounds:bounds withPadding:50.0f]];
        
        CLLocationCoordinate2D target =
        CLLocationCoordinate2DMake(coordinate.latitude,coordinate.longitude);
        self.googleMapView.camera = [GMSCameraPosition cameraWithTarget:target zoom:12];
    }
}


- (IBAction)btnBackClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
