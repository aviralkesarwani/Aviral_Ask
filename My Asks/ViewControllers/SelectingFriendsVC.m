//
//  SelectingFriendsVC.m
//  My Asks
//
//  Created by MAC on 08/07/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "SelectingFriendsVC.h"
#import "MAFriendListModel.h"
#import <KVNProgress.h>

@interface SelectingFriendsVC ()

@property (strong, nonatomic) IBOutlet UIView *tblHeaderForRexTbl;
@property (strong, nonatomic) IBOutlet UIView *tblHeaderForNonRexTbl;


@end

@implementation SelectingFriendsVC


- (void)viewDidLoad {
    [super viewDidLoad];
    
    contactsStrore = [[CNContactStore alloc] init];
    rexNonRexContactList = [NSMutableArray new];
    self.searchContactArray = [[NSMutableArray alloc] init];
    
    rexContactList = [NSMutableArray new];
    nonRexContactList = [NSMutableArray new];
    
    [self.txtSearchContacts addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
    self.rexTableView.dataSource = self;
    self.rexTableView.delegate = self;
    self.rexTableView.tableFooterView = [UIView new];
    self.rexTableView.tableHeaderView = self.tblHeaderForRexTbl;
    
    self.refreshControlForRex = [[UIRefreshControl alloc]init];
    self.refreshControlForRex.backgroundColor = [UIColor clearColor];
    self.refreshControlForRex.tintColor = [UIColor blackColor];
    
    [self.rexTableView addSubview:self.refreshControlForRex];
    [self.refreshControlForRex addTarget:self action:@selector(sendContacts) forControlEvents:UIControlEventValueChanged];
    

    self.nonRexTableView.dataSource = self;
    self.nonRexTableView.delegate = self;
    self.nonRexTableView.tableFooterView = [UIView new];
    self.nonRexTableView.tableHeaderView = self.tblHeaderForNonRexTbl;

    self.refreshControlForNonRex = [[UIRefreshControl alloc]init];
    self.refreshControlForNonRex.backgroundColor = [UIColor clearColor];
    self.refreshControlForNonRex.tintColor = [UIColor blackColor];
    
    [self.nonRexTableView addSubview:self.refreshControlForNonRex];
    [self.refreshControlForNonRex addTarget:self action:@selector(sendContacts) forControlEvents:UIControlEventValueChanged];
    
    
    [self requestContactsAccessWithHandler:^(BOOL grandted) {
        
        if (grandted) {
            self.askScrollView.hidden=NO;
            self.noContactsView.hidden=YES;
            self.tabBarHeight.constant = 30;
            
            if ([[NSUserDefaults standardUserDefaults] valueForKey:@"OneTimeContactUploaded"] == nil) {
                [self checkContactsAccess];
            }else{
                [self getFriendsList];
            }
        }else{
            
            if ([[NSUserDefaults standardUserDefaults] valueForKey:@"OneTimeContactUploaded"] == nil) {
                self.askScrollView.hidden=YES;
                self.noContactsView.hidden=NO;
                self.tabBarHeight.constant = 0;
                _contactsMessageLbl.text = @"Your list of contacts would appear here. If you’d like to change your mind and allow Rex to use the list of contacts already in your phone and make it easy to ask friends by simply tapping on their name instead of entering phone numbers or email addresses. Go to Settings -> Privacy -> Contacts -> Rex -> ON";
                _allowContactsBtn.hidden=YES;
            }else{
                self.askScrollView.hidden=NO;
                self.noContactsView.hidden=YES;
                self.tabBarHeight.constant = 30;
                [self getFriendsList];
            }
        }
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (IBAction)accessContactsTapped:(id)sender {
}

#pragma mark - Send Contacts

-(void)sendContacts{
    [self checkContactsAccess];
    
}

#pragma mark - Contact

-(void)checkContactsAccess{
    
    [self requestContactsAccessWithHandler:^(BOOL grandted) {
        
        if (grandted) {
            
            [[NSUserDefaults standardUserDefaults] setObject:@"YES" forKey:@"OneTimeContactUploaded"];
            [[NSUserDefaults standardUserDefaults] synchronize];
            
            [KVNProgress show];
            //keys with fetching properties
            NSArray *keys = @[CNContactFamilyNameKey, CNContactGivenNameKey, CNContactNamePrefixKey, CNContactMiddleNameKey, CNContactPhoneNumbersKey,CNContactImageDataKey,CNContactImageDataAvailableKey,CNContactThumbnailImageDataKey];
            NSString *containerId = contactsStrore.defaultContainerIdentifier;
            NSPredicate *predicate = [CNContact predicateForContactsInContainerWithIdentifier:containerId];
            NSError *error;
            NSArray *cnContacts = [contactsStrore unifiedContactsMatchingPredicate:predicate keysToFetch:keys error:&error];
            
            if (error) {
                NSLog(@"error fetching contacts %@", error);
            }
            else {
                
                NSMutableArray *localContactsList = [NSMutableArray new];
                NSMutableArray *localContactsParamList = [NSMutableArray new];
                for (CNContact *contact in cnContacts) {
                    
                    
                    // copy data to my custom Contacts class.
                    MAInviteFriendsModel *inviteFriends = [[MAInviteFriendsModel alloc]init];
                    
                    inviteFriends.contactFullName = [NSString stringWithFormat:@"%@ %@",contact.givenName,contact.familyName];
                    inviteFriends.arrPhoneNumbers = [[contact.phoneNumbers valueForKey:@"value"] valueForKey:@"digits"];
                    
                    [localContactsList addObject:inviteFriends];
                }
                
                for (MAInviteFriendsModel *inviteFriend in localContactsList) {
                    
                    NSMutableDictionary *contactInfo = [[NSMutableDictionary alloc]init];
                    
                    if ([inviteFriend.arrPhoneNumbers count] == 1) {
                        
                        NSString *phoneNumber = [inviteFriend.arrPhoneNumbers firstObject];
                        [contactInfo setValue:inviteFriend.contactFullName forKey:@"ContactName"];
                        [contactInfo setValue:phoneNumber forKey:@"ContactNumber"];
                        
                        
                        [localContactsParamList addObject:contactInfo];
                    }
                    else{
                        
                        for (int i =0; i < [inviteFriend.arrPhoneNumbers count]; i++) {
                            
                            NSMutableDictionary *contactInfo = [[NSMutableDictionary alloc]init];
                            NSString *phoneNumber = inviteFriend.arrPhoneNumbers[i];
                            [contactInfo setValue:inviteFriend.contactFullName forKey:@"ContactName"];
                            [contactInfo setValue:phoneNumber forKey:@"ContactNumber"];
                            [localContactsParamList addObject:contactInfo];
                        }
                    }
                }
                
                NSMutableDictionary *contacts = [[NSMutableDictionary alloc]init];
                NSString *userId = [[AppData sharedManager]getCurrentUserID];
                [contacts setValue:localContactsParamList forKey:@"ContactList"];
                [contacts setValue:userId forKey:@"UserId"];
                
                [KVNProgress show];
                [[APIClient sharedManager] InsertUpdateUserContacts:contacts onCompletion:^(NSDictionary *resultData, bool success) {
                    [KVNProgress dismiss];
                    
                    if (success) {
                        
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
                                                                        message:@"Contacts Updated"
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                        [alert show];
                    }
                    else{
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
                                                                        message:@"Unable to update contacts"
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                        [alert show];
                    }
                    
                    [self.refreshControlForRex endRefreshing];
                    [self.refreshControlForNonRex endRefreshing];

                    [self getFriendsList];
                }];
            }
        }
        else{
            [self.refreshControlForRex endRefreshing];
            [self.refreshControlForNonRex endRefreshing];
        }
    }];
}

-(void)requestContactsAccessWithHandler:(void (^)(BOOL grandted))handler{
    
    switch ([CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts]) {
        case CNAuthorizationStatusAuthorized:
            handler(YES);
            break;
        case CNAuthorizationStatusDenied:
        case CNAuthorizationStatusNotDetermined:{
            [contactsStrore requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
                
                handler(granted);
            }];
            break;
        }
        case CNAuthorizationStatusRestricted:
            handler(NO);
            break;
    }
}

#pragma mark - Get Friends List

-(void)getFriendsList{
    
    NSString *userID = [[AppData sharedManager]getCurrentUserID];
    
    [AppData startNetworkIndicator];
    
    [[APIClient sharedManager]FriendsList:userID onCompletion:^(NSDictionary *resultData, bool success) {
        
        [AppData stopNetworkIndicator];
        
        NSMutableArray *arr = [resultData valueForKey:@"ContactList"];
        
        if ([resultData count]==0) {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
                                                            message:@"No friends available"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        else{
            [rexNonRexContactList removeAllObjects];
            for (NSArray *list in arr) {
                
                MAFriendListModel *friendListModel = [MAFriendListModel new];
                
                friendListModel.Id = [list valueForKey:@"Id"];
                friendListModel.ContactNumber = [list valueForKey:@"ContactNumber"];
                friendListModel.ContactName = [list valueForKey:@"ContactName"];
                friendListModel.ListCount = [list valueForKey:@"ListCount"];
                friendListModel.PictureCount = [list valueForKey:@"PictureCount"];
                friendListModel.PlacesCount = [list valueForKey:@"PlacesCount"];
                friendListModel.IsRexUser = [[list valueForKey:@"IsRexUser"] boolValue];
                friendListModel.ProfilePicture = [list valueForKey:@"ProfilePicture"];
                
                [rexNonRexContactList addObject:friendListModel];
                
                if (self.selectedContactsArray.count > 0) {
                    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"ContactNumber == %@",friendListModel.ContactNumber];
                    NSArray *filterArray = [self.selectedContactsArray filteredArrayUsingPredicate:resultPredicate];

                    if (filterArray.count > 0) {
                        friendListModel.isSelected = YES;
                    }
                }
            }
        }
        
        [self filterRexNonRexUsers];
    }];
}

#pragma mark - UIScrollViewDelegate

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    if(scrollView == self.askScrollView){
        CGFloat pageWidth = CGRectGetWidth(scrollView.frame);
        CGFloat currentPage = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1;
        self.askPageController.currentPage = currentPage;
        
        self.txtSearchContacts.text = @"";
        self.isSearching = false;

        if (self.askPageController.currentPage == 0) {
            [self.rexTableView reloadData];
        }else if (self.askPageController.currentPage == 1) {
            [self.nonRexTableView reloadData];
        }
    }
}

#pragma mark - Tableview Delegate/Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (self.isSearching) {
        return  [self.searchContactArray count];
    }else {
        if (tableView == self.rexTableView) {
            return [rexContactList count];
        }else if (tableView == self.nonRexTableView) {
            return [nonRexContactList count];
        }
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MAFriendListModel *friendListModel;
    
    if (self.isSearching) {
        friendListModel = [self.searchContactArray objectAtIndex:indexPath.row];
        
    }else{
        if (tableView == self.rexTableView) {
            friendListModel = [rexContactList objectAtIndex:indexPath.row];
        }else if (tableView == self.nonRexTableView) {
            friendListModel = [nonRexContactList objectAtIndex:indexPath.row];
        }
    }
    
    MAInviteFriendsTableCell *inviteFriendsTableCell = [tableView dequeueReusableCellWithIdentifier:@"MAInviteFriendsTableCell" forIndexPath: indexPath];
    
    
    inviteFriendsTableCell.lblContactName.text = friendListModel.ContactName;
    
    if (friendListModel.IsRexUser) {
        inviteFriendsTableCell.placeNameLbl.text = [NSString stringWithFormat:@"I’m a member and have %@ places on Rex",friendListModel.PlacesCount];
    }else{
        inviteFriendsTableCell.placeNameLbl.text = @"";
    }
    
    [inviteFriendsTableCell.btnSelectDeselect addTarget:self action:@selector(btnSelectDeselectClicked:) forControlEvents:UIControlEventTouchUpInside];
    inviteFriendsTableCell.btnSelectDeselect.tag = indexPath.row;
    
    if (friendListModel.isSelected == true) {
        [inviteFriendsTableCell.btnSelectDeselect setImage:[UIImage imageNamed:@"Checkbox Selected"] forState:UIControlStateNormal];
    }
    else {
        [inviteFriendsTableCell.btnSelectDeselect setImage:[UIImage imageNamed:@"Checkbox normal"] forState:UIControlStateNormal];
    }
    
    return  inviteFriendsTableCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MAFriendListModel *friendListModel;
    
    if (self.isSearching) {
        friendListModel = [self.searchContactArray objectAtIndex:indexPath.row];
    }else {
        if (tableView == self.rexTableView) {
            friendListModel = [rexContactList objectAtIndex:indexPath.row];
        }else if (tableView == self.nonRexTableView) {
            friendListModel = [nonRexContactList objectAtIndex:indexPath.row];
        }
    }
    
    if (friendListModel.IsRexUser) {
        return 60;
    }else{
        return 44;
    }
}
#pragma mark - MAAddContactVCDelegate 

- (void)addedContact:(MAFriendListModel*)friendListModel{
    
    [nonRexContactList addObject:friendListModel];
    [rexNonRexContactList addObject:friendListModel];

    
    [self.nonRexTableView reloadData];
}

#pragma mark - Button Actions

- (IBAction)askPageControllerChanged:(id)sender {
    NSInteger page = self.askPageController.currentPage;
    CGRect frame = self.askScrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [self.askScrollView scrollRectToVisible:frame animated:YES];
    
    self.txtSearchContacts.text = @"";
    self.isSearching = false;
    
    if (self.askPageController.currentPage == 0) {
        [self.rexTableView reloadData];
    }else if (self.askPageController.currentPage == 1) {
        [self.nonRexTableView reloadData];
    }
}

- (void)btnSelectDeselectClicked:(UIButton*)sender{
    
    NSInteger index = sender.tag;
    
    MAFriendListModel *friendListModel;
    
    if (self.askPageController.currentPage == 0) {
        if (self.isSearching) {
            friendListModel = [self.searchContactArray objectAtIndex:index];
        }else{
            friendListModel = [rexContactList objectAtIndex:index];
        }
        friendListModel.isSelected = !friendListModel.isSelected;
        [self.rexTableView reloadData];
        
    }else if (self.askPageController.currentPage == 1) {
        if (self.isSearching) {
            friendListModel = [self.searchContactArray objectAtIndex:index];
        }else{
            friendListModel = [nonRexContactList objectAtIndex:index];
        }
        friendListModel.isSelected = !friendListModel.isSelected;
        [self.nonRexTableView reloadData];
    }
}

- (IBAction)btnCancelClicked:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)btnDoneClicked:(id)sender {
    
    NSMutableArray *contactsArray =[[NSMutableArray alloc]init];
    
    for (MAFriendListModel *frndModal in rexNonRexContactList) {
        if (frndModal.isSelected) {
            [contactsArray addObject:frndModal];
        }
    }
    
    [AppData sharedManager].invitedFriendsGlobalArray = rexNonRexContactList;
    [self dismissViewControllerAnimated:true completion:nil];
    [self.delegate inviteFriends:contactsArray];
}

- (IBAction)filterRexNonRexUsers {
    
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"IsRexUser == %@",@YES];
    
    NSArray *filterArray = [rexNonRexContactList filteredArrayUsingPredicate:resultPredicate];
    rexContactList = [NSMutableArray arrayWithArray:filterArray];
    [self.rexTableView reloadData];

    filterArray = nil;
    resultPredicate = [NSPredicate predicateWithFormat:@"IsRexUser == %@",@NO];
    
    filterArray = [rexNonRexContactList filteredArrayUsingPredicate:resultPredicate];
    nonRexContactList = [NSMutableArray arrayWithArray:filterArray];
    [self.nonRexTableView reloadData];
}

- (IBAction)addContactsBtnAction:(UIButton*)sender {

    MAAddContactVC *addContactVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MAAddContactVC"];
    addContactVC.delegate = self;
    [self.navigationController pushViewController:addContactVC animated:YES];
}

#pragma mark UITextFieldDelegate methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    [self.view endEditing:YES];
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSPredicate *resultPredicate = [NSPredicate
                                    predicateWithFormat:@"ContactName contains[cd] %@",
                                    searchText];
    
    if (self.askPageController.currentPage == 0) {
        NSArray *filterArray = [rexContactList filteredArrayUsingPredicate:resultPredicate];
        self.searchContactArray = [NSMutableArray arrayWithArray:filterArray];
    }else if (self.askPageController.currentPage == 1) {
        NSArray *filterArray = [nonRexContactList filteredArrayUsingPredicate:resultPredicate];
        self.searchContactArray = [NSMutableArray arrayWithArray:filterArray];
    }
}

-(void)textFieldDidChange:(UITextField*)textField
{
    NSString *searchString = [self.txtSearchContacts.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([searchString length] > 0) {
        self.isSearching = true;
        [self filterContentForSearchText:searchString scope:@""];
    }
    else {
        self.isSearching = false;
    }
    
    if (self.askPageController.currentPage == 0) {
        [self.rexTableView reloadData];
    }else if (self.askPageController.currentPage == 1) {
        [self.nonRexTableView reloadData];
    }
}

@end
