//
//  MAPeopleVC.m
//  My Asks
//
//  Created by artis on 18/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "MAPeopleVC.h"

@interface MAPeopleVC ()

@end

@implementation MAPeopleVC

#pragma Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.friendList = [[NSMutableArray alloc]init];
    self.searchContactArray = [[NSMutableArray alloc] init];
    
    ContactList = [[NSMutableArray alloc]init];
    contactsStrore = [[CNContactStore alloc] init];
    contactDetails = [[NSMutableArray alloc]init];
    
    self.friendsSearchBar.delegate = self;
    
    self.friendsSearchBar.text = @"";
    
    self.refreshControl = [[UIRefreshControl alloc]init];
    self.refreshControl.backgroundColor = [UIColor clearColor];
    self.refreshControl.tintColor = [UIColor blackColor];
    
    [self.tblFriendsList addSubview:self.refreshControl];
    [self.refreshControl addTarget:self action:@selector(sendContacts) forControlEvents:UIControlEventValueChanged];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillAppear:(BOOL)animated{
    
  [self getFriendsList];
}
#pragma mark - TableView Delegate / DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (self.isSearching) {
        return [self.searchContactArray count];
    }
    else{
         return [self.friendList count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MAPeopletableCell * peopleCell = [tableView dequeueReusableCellWithIdentifier:@"MAPeopletableCell" forIndexPath: indexPath];
    
    if (self.isSearching) {
        friendModel = [self.searchContactArray objectAtIndex:indexPath.row];
    }
    else{
        friendModel = [self.friendList objectAtIndex:indexPath.row];
    }
    
    peopleCell.lblName.text = friendModel.ContactName;

    if(friendModel.ProfilePicture == (id)[NSNull null]) {
        peopleCell.imgPeople.image = [UIImage imageNamed:@"Photo"];
    }
    else{
        peopleCell.imgPeople.imageURL =  [NSURL URLWithString:friendModel.ProfilePicture];
    }
    
    [peopleCell.btnInvite addTarget:self action:@selector(btnCellInviteClicked:) forControlEvents:UIControlEventTouchUpInside];
    peopleCell.btnInvite.tag = indexPath.row;
    
    if (friendModel.IsRexUser) {
        
        peopleCell.btnInvite.hidden = YES;
        peopleCell.btnForward.hidden = NO;
        peopleCell.lblListPlaces.text = [NSString stringWithFormat:@"%@ lists %@ places",friendModel.ListCount,friendModel.PlacesCount];
    }
    else {
        
        peopleCell.btnInvite.hidden = NO;
        peopleCell.btnForward.hidden = YES;
        peopleCell.lblListPlaces.text =  friendModel.ContactNumber;
    }
    
    peopleCell.imgPeople.clipsToBounds = YES;
    peopleCell.imgPeople.layer.cornerRadius =  peopleCell.imgPeople.frame.size.width/2.0;
    
    return peopleCell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MAFriendListModel *friendModelLocal;
    
    if (self.isSearching) {
        
         friendModelLocal = [self.searchContactArray objectAtIndex:indexPath.row];
    }
    else{
        friendModelLocal = [self.friendList objectAtIndex:indexPath.row];
    }

    if (friendModelLocal.IsRexUser) {
        
        MAPeopleDetailVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MAPeopleDetailVC"];
        
        vc.friendID = friendModelLocal.Id;
        
        vc.hidesBottomBarWhenPushed=NO;
        [self.navigationController pushViewController:vc animated:YES];
    }
    else{
        
        NSLog(@"%ld",(long)indexPath.row);
        NSLog(@"%@",friendModelLocal.ContactNumber);
        
        MAPeopleInviteFriendsVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MAPeopleInviteFriendsVC"];
        vc.ContactsArray=self.friendList;
        vc.selectedNumber =  friendModelLocal.ContactNumber;
        vc.hidesBottomBarWhenPushed=YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}

#pragma mark - Events

- (IBAction)btnInviteFriendsClicked:(id)sender {
    
    MAPeopleInviteFriendsVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MAPeopleInviteFriendsVC"];
    vc.ContactsArray=self.friendList;
    vc.hidesBottomBarWhenPushed=YES;
    [self.navigationController pushViewController:vc animated:YES];
}

-(void) btnCellInviteClicked:(UIButton *)sender {
    
    MAFriendListModel *friendModelLocal;
    
    if (self.isSearching) {
        
        friendModelLocal = [self.searchContactArray objectAtIndex:sender.tag];
    }
    else{
        friendModelLocal = [self.friendList objectAtIndex:sender.tag];
    }

    if (friendModelLocal.IsRexUser) {
        
        MAPeopleDetailVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MAPeopleDetailVC"];
        vc.friendID = friendModelLocal.Id;
        
        vc.hidesBottomBarWhenPushed=NO;
        [self.navigationController pushViewController:vc animated:YES];
    }
    else{
        MAPeopleInviteFriendsVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MAPeopleInviteFriendsVC"];
        vc.ContactsArray=self.friendList;
        
        NSLog(@"%@", friendModelLocal.ContactNumber);
        vc.selectedNumber =  friendModelLocal.ContactNumber;
        
        vc.hidesBottomBarWhenPushed=YES;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (IBAction)btnBackClicked:(id)sender {
    
}

#pragma mark - Get Friends List

-(void)getFriendsList{
    
    NSString *userID = [[AppData sharedManager]getCurrentUserID];
    
//    [KVNProgress show];
    [AppData startNetworkIndicator];

    [[APIClient sharedManager]FriendsList:userID onCompletion:^(NSDictionary *resultData, bool success) {
      
//        [KVNProgress dismiss];
        [AppData stopNetworkIndicator];

        NSLog(@"%@",resultData);
        NSMutableArray *arr = [resultData valueForKey:@"ContactList"];
        
        if ([resultData count]==0) {
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
                                                            message:@"No friends available"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
        else{
            [self.friendList removeAllObjects];
            for (NSArray *list in arr) {
                
                NSLog(@"%@",list);
                MAFriendListModel *friendListModel = [MAFriendListModel new];
                
                friendListModel.Id = [list valueForKey:@"Id"];
                friendListModel.ContactNumber = [list valueForKey:@"ContactNumber"];
                friendListModel.ContactName = [list valueForKey:@"ContactName"];
                friendListModel.ListCount = [list valueForKey:@"ListCount"];
                friendListModel.PictureCount = [list valueForKey:@"PictureCount"];
                friendListModel.PlacesCount = [list valueForKey:@"PlacesCount"];
                friendListModel.IsRexUser = [[list valueForKey:@"IsRexUser"] boolValue];
                friendListModel.ProfilePicture = [list valueForKey:@"ProfilePicture"];
                
                [self.friendList addObject:friendListModel];
                NSLog(@"%@",friendListModel);
            }
            NSLog(@"%@",self.friendList);
            [self.tblFriendsList reloadData];
        }
        
    }];
}
#pragma mark - Send Contacts

-(void)sendContacts{
    [self checkContactsAccess];
 
}

#pragma mark - SearchBar Delegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)text {
    if (text.length > 0) {
        self.isSearching = YES;
        [self filterContentForSearchText:text scope:@""];
    }
    else {
        self.isSearching = NO;
        [searchBar performSelector:@selector(resignFirstResponder) withObject:nil afterDelay:0];
    }
    [self.tblFriendsList reloadData];
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    if(searchBar.text.length == 0){
    }
}


- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    self.isSearching = NO;
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSPredicate *resultPredicate = [NSPredicate
                                    predicateWithFormat:@"ContactName contains[cd] %@",
                                    searchText];
    
    NSArray *filterArray;
    
    filterArray = [self.friendList filteredArrayUsingPredicate:resultPredicate];
    
    self.searchContactArray = [NSMutableArray arrayWithArray:filterArray];
}

#pragma mark - Check for contact access

-(void)checkContactsAccess{
    
    [self requestContactsAccessWithHandler:^(BOOL grandted) {
        
        if (grandted) {
            [KVNProgress show];
            //keys with fetching properties
            NSArray *keys = @[CNContactFamilyNameKey, CNContactGivenNameKey, CNContactNamePrefixKey, CNContactMiddleNameKey, CNContactPhoneNumbersKey,CNContactImageDataKey,CNContactImageDataAvailableKey,CNContactThumbnailImageDataKey];
            NSString *containerId = contactsStrore.defaultContainerIdentifier;
            NSPredicate *predicate = [CNContact predicateForContactsInContainerWithIdentifier:containerId];
            NSError *error;
            NSArray *cnContacts = [contactsStrore unifiedContactsMatchingPredicate:predicate keysToFetch:keys error:&error];
           
            if (error) {
                NSLog(@"error fetching contacts %@", error);
            }
            else {
                for (CNContact *contact in cnContacts) {
                    
                    
                    // copy data to my custom Contacts class.
                    MAInviteFriendsModel *inviteFriends = [[MAInviteFriendsModel alloc]init];
                    
                    inviteFriends.contactFullName = [NSString stringWithFormat:@"%@ %@",contact.givenName,contact.familyName];
                    inviteFriends.arrPhoneNumbers = [[contact.phoneNumbers valueForKey:@"value"] valueForKey:@"digits"];
                    
                    [contactDetails addObject:inviteFriends];
                }
                
                for (MAInviteFriendsModel *inviteFriend in contactDetails) {
                    
                    NSMutableDictionary *contactInfo = [[NSMutableDictionary alloc]init];
                    
                    if ([inviteFriend.arrPhoneNumbers count] == 1) {
                        
                        NSString *phoneNumber = [inviteFriend.arrPhoneNumbers firstObject];
                        [contactInfo setValue:inviteFriend.contactFullName forKey:@"ContactName"];
                        [contactInfo setValue:phoneNumber forKey:@"ContactNumber"];
                       
                        
                        [ContactList addObject:contactInfo];
                    }
                    else{
                        
                        for (int i =0; i < [inviteFriend.arrPhoneNumbers count]; i++) {
                            
                            NSMutableDictionary *contactInfo = [[NSMutableDictionary alloc]init];
                            NSString *phoneNumber = inviteFriend.arrPhoneNumbers[i];
                            [contactInfo setValue:inviteFriend.contactFullName forKey:@"ContactName"];
                            [contactInfo setValue:phoneNumber forKey:@"ContactNumber"];
                            [ContactList addObject:contactInfo];
                        }
                    }
                }
                
                NSLog(@"%@",ContactList);
                
                NSMutableDictionary *contacts = [[NSMutableDictionary alloc]init];
                NSString *userId = [[AppData sharedManager]getCurrentUserID];
                [contacts setValue:ContactList forKey:@"ContactList"];
                [contacts setValue:userId forKey:@"UserId"];
                
                NSLog(@"%@",contacts);
                
                [KVNProgress show];
                [[APIClient sharedManager]InsertUpdateUserContacts:contacts onCompletion:^(NSDictionary *resultData, bool success) {
                    [KVNProgress dismiss];
                    NSLog(@"%@",resultData);
                    
                    if (success) {
                        
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
                                                                        message:@"Contacts Updated"
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                        [alert show];
                    }
                    else{
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
                                                                        message:@"Unable to update contacts"
                                                                       delegate:self
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                        [alert show];
                    }
                    [contacts removeAllObjects];
                    [ContactList removeAllObjects];
                    
                    [self.refreshControl endRefreshing];
                    
                }];
                
            }
        }
        else{
            [self.refreshControl endRefreshing];
        }
    }];
}

-(void)requestContactsAccessWithHandler:(void (^)(BOOL grandted))handler{
    
    switch ([CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts]) {
        case CNAuthorizationStatusAuthorized:
            handler(YES);
            break;
        case CNAuthorizationStatusDenied:
        case CNAuthorizationStatusNotDetermined:{
            [contactsStrore requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
                
                handler(granted);
            }];
            break;
        }
        case CNAuthorizationStatusRestricted:
            handler(NO);
            break;
    }
}

@end
