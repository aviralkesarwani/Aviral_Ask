//
//  NewAskScreenVC.h
//  My Asks
//
//  Created by MAC on 08/07/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <Contacts/Contacts.h>
#import "SelectingFriendsVC.h"
#import "MACreateAskInviteContactsCell.h"
#import "MACreateAskAddLocationsCell.h"
#import "MAInviteFriendsVC.h"
#import "AppData.h"
#import <KVNProgress.h>
#import "MAChatScreenVC.h"
#import "BIZPopupViewController.h"
#import "AddLocationVC.h"
#import "MAInviteFriendsVC.h"
#import "AddLocationVC.h"
#import "APIClient.h"
#import "SelectingFriendsVC.h"
#import "AskingFriendsTableViewCell.h"
#import "ContactAccess.h"

@interface NewAskScreenVC : UIViewController <UITableViewDelegate, UITableViewDataSource, UITextViewDelegate, SelectingFriendsDelegate, ContactAccessDelegate>{
    CNContactStore *contactsStrore;
}

@property (nonatomic, assign) BOOL isForEdit;
@property (strong, nonatomic) NSMutableArray *contactsArray;
@property (strong, nonatomic) QBChatDialog *selectedChatDialod;

@end
