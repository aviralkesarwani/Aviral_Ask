//
//  MAPlacesFilterVC.h
//  My Asks
//
//  Created by Mac on 22/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import <KVNProgress.h>
#import "MVPlaceSearchTextField.h"
#import "AppData.h"
#import "APIClient.h"
#import "BIZPopupViewController.h"
#import "MAPlacesFilterTableCell.h"
#import "MAPlacesSearchVC.h"
#import "MAPlacesDetailVC.h"
#import "MAsearchPlacesModel.h"
#import "MAAddPlactToListVC.h"
#import "MAPlacesFilterVC.h"
#import "MAPlacesMapVC.h"
#import <UIScrollView+InfiniteScroll.h>
#import <PSLocation/PSlocation.h>

@interface MAPlacesFilterVC : UIViewController <UITableViewDataSource, UITableViewDelegate,PlaceSearchTextFieldDelegate,PlaceSearchTextFieldDelegate,MKMapViewDelegate,CLLocationManagerDelegate,UIGestureRecognizerDelegate,BIZPopupViewControllerDelegate,MAAddPlactToListDelegate, UITextFieldDelegate,MACreateNewListDelegate>
{
      NSDictionary *locationDict;
      NSMutableArray *placesSearchArray;
      NSMutableArray *placesPhotoArray;
      UIView * footerView;
}

@property (strong, nonatomic) IBOutlet UIView *topTabBarView;
@property (weak, nonatomic) IBOutlet UITableView *tblPlacesContent;
@property (weak, nonatomic) IBOutlet UIView *placesTableHeaderView;
@property (weak, nonatomic) IBOutlet MVPlaceSearchTextField *placeSearchTextField;

@property (weak, nonatomic) IBOutlet UIView *autoComplete;
@property (weak, nonatomic) IBOutlet UIButton *btnCurrentLocationTop;

// Property for getting curent lattude and longitude and pass into the API
@property (strong, nonatomic)NSString *latitude;
@property (strong, nonatomic)NSString *longitude;

@property (strong, nonatomic)NSString *currentLatitude;
@property (strong, nonatomic)NSString *currentLongitude;

@property (strong, nonatomic)NSString *subLocality;
@property (strong, nonatomic)NSString *restaurantName;
@property (strong, nonatomic)NSString *restaurantType;
@property (strong, nonatomic)NSString *nextPageToken;

@property (strong, nonatomic) UIRefreshControl *refreshControl;

@property (retain ,nonatomic) CLLocationManager *locationManager;

@property (assign ,nonatomic) BOOL loadFromGooglePlace;

@property (strong, nonatomic) PSLocationManager *pslocationManager;

- (IBAction)btnBackClicked:(id)sender;
- (IBAction)btnPlaceLocationClicked:(id)sender;
- (IBAction)btnMapClicked:(id)sender;

@end
