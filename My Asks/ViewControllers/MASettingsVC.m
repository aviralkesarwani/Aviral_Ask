//
//  MASettingsVC.m
//  My Asks
//
//  Created by artis on 18/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "MASettingsVC.h"

@interface MASettingsVC ()

@end

@implementation MASettingsVC

bool isButtonProfilePictureClicked;
bool isButtonCoverPictureClicked;


#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
   
    self.tblHistory.delegate = self;
    self.tblHistory.dataSource = self;
    
    self.txtEmail.delegate = self;
    self.txtName.delegate = self;
    
    self.topTabBarView = [[[NSBundle mainBundle] loadNibNamed:@"SettingsTopTabBar" owner:self options:nil] firstObject];
    [self.topTabBarViewContainer addSubview:self.topTabBarView];
    [self getMyProfile:[AppData sharedManager].userID];

    self.imgProfilePic.layer.cornerRadius = self.imgProfilePic.frame.size.height/2;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


#pragma mark - UITextField Delegate

- (void)textFieldDidEndEditing:(UITextField *)textField {
    NSLog(@"End");
    
    if (textField == self.txtName) {
        [self updateName];
    }
    else if (textField == self.txtEmail) {
        [self updateEmail];
    }
}

#pragma mark - UITableView DataSource/Delegate

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MAHistoryCell * historyCell = [tableView dequeueReusableCellWithIdentifier:@"MAHistoryCell" forIndexPath: indexPath];
    
    if(indexPath.row == 1) {
        
        historyCell.imgHistoryType.image = [UIImage imageNamed:@"Like big"];
        historyCell.lblHistoryText.text = @"photo in <<Venice Beach>> list";
    }
    if(indexPath.row == 2) {
        
        historyCell.imgHistoryType.image = [UIImage imageNamed:@"Comment big"];
        historyCell.lblHistoryText.text = @"comment in <<Can i drive?>> ask";
    }
    if(indexPath.row == 3) {
        
        historyCell.imgHistoryType.image = [UIImage imageNamed:@"plus_black"];
        historyCell.lblHistoryText.text = @"add <<Jaganath Vegs>>to your ask";
    }
    return historyCell;
}

#pragma mark - Actions

- (IBAction)btnInformationClicked:(id)sender {
    self.imgTabBackground.image = [UIImage imageNamed:@"Tab 1"];
    self.informationView.hidden = false;
//    self.historyView.hidden = true;
    self.statisticView.hidden = true;
}

//- (IBAction)btnHistoryClicked:(id)sender {
//    self.imgTabBackground.image = [UIImage imageNamed:@"3tabs-right"];
//    self.informationView.hidden = true;
//    self.historyView.hidden = false;
//    self.statisticView.hidden = true;
//}

- (IBAction)btnStatisticClicked:(id)sender {
    self.imgTabBackground.image = [UIImage imageNamed:@"Tab 2"];
    self.informationView.hidden = true;
//    self.historyView.hidden = true;
    self.statisticView.hidden = false;
}

- (IBAction)btnListClicked:(id)sender {
    MAListDetailVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MAListDetailVC"];
    MAUserListsModel *userListsModel = [self.settingsModel.Lists firstObject];
//    vc.userlistData = userListsModel;
//    [self.navigationController pushViewController:vc animated:YES];
    
    
    vc.userListId = userListsModel.UserListID;
    vc.userId = userListsModel.UserId;
    [self.navigationController pushViewController:vc animated:YES];


}

- (IBAction)btnAskClicked:(id)sender {
    MAUserAsksModel *userAsksModel = [self.settingsModel.Asks firstObject];
    MAChatScreenVC *chatScreenVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MAChatScreenVC"];
//    chatScreenVC.userAskData = userAsksModel;
//    chatScreenVC.askId = userAsksModel.UserAsksID;
    [self.navigationController pushViewController:chatScreenVC animated:true];
}

- (IBAction)btnProfileImageClicked:(id)sender {
    isButtonProfilePictureClicked = true;
    isButtonCoverPictureClicked = false;
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];

}

- (IBAction)btnCoverPhotoClicked:(id)sender {
    isButtonProfilePictureClicked = false;
    isButtonCoverPictureClicked = true;
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];
}


#pragma mark - image picker Delagate/Datasource

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    
    //    self.imgCoverImage.clipsToBounds = YES;
//    self.imgCoverImage.layer.cornerRadius = 44;
//    self.imgCoverImage.layer.masksToBounds = YES;
//    self.imgCoverImage.image = chosenImage;
    if (isButtonProfilePictureClicked) {
        isButtonProfilePictureClicked = false;
        self.imgProfilePic.image = chosenImage;
        [self updateProfilePicture];
    }
    else if (isButtonCoverPictureClicked) {
        isButtonCoverPictureClicked = false;
        self.imgCoverPhoto.image = chosenImage;
        [self updateCoverPicture];
    }
    
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    isButtonProfilePictureClicked = false;
    isButtonCoverPictureClicked = false;

    [picker dismissViewControllerAnimated:YES completion:NULL];
}


#pragma mark - Custom Methods

- (void) getMyProfile : (NSString *) userId {
//    [KVNProgress show];
    [AppData startNetworkIndicator];

    [[APIClient sharedManager] getMyProfile:userId onCompletion:^(NSDictionary *resultData, bool success) {
//        [KVNProgress dismiss];
        [AppData stopNetworkIndicator];

        if (success) {
            NSLog(@"%@",resultData);
            
            NSDictionary *resultDict = resultData;
            
            self.settingsModel = [[MASettingModelClass alloc] init];
            self.settingsModel.AsksCount = [[resultDict valueForKey:@"AsksCount"] stringValue];
            self.settingsModel.CommentsCount = [[resultDict valueForKey:@"CommentsCount"] stringValue];
            self.settingsModel.LikesCount = [[resultDict valueForKey:@"LikesCount"] stringValue];
            self.settingsModel.ListsCount = [[resultDict valueForKey:@"ListsCount"] stringValue];
            self.settingsModel.PlacesCount = [[resultDict valueForKey:@"PlacesCount"] stringValue];
            self.settingsModel.SharesCount = [[resultDict valueForKey:@"SharesCount"] stringValue];
            
            NSArray *asksArray = [resultDict valueForKey:@"Asks"];
            
            NSMutableArray *userAsksArray = [[NSMutableArray alloc] init];
            
            for (NSDictionary *userAsks in asksArray) {
                MAUserAsksModel *userAsksModel = [MAUserAsksModel new];
                
                userAsksModel.UserAsksID = [userAsks valueForKey:@"Id"];
                userAsksModel.UserId = [userAsks valueForKey:@"UserId"];
                userAsksModel.Question = [userAsks valueForKey:@"Question"];
                userAsksModel.TotalMessages = [userAsks valueForKey:@"TotalMessages"];
                userAsksModel.IsOwner = [userAsks valueForKey:@"IsOwner"];
                
                userAsksModel.BackgroundImage = [userAsks valueForKey:@"BackgroundImageUrl"];
                
                userAsksModel.ChannelName = [userAsks valueForKey:@"ChannelName"];
                
                NSString *createdDateString = [userAsks valueForKey:@"CreatedDate"];
                
                userAsksModel.CreatedDate = [self getLocalDateStringFrmGMT:createdDateString];
                
                userAsksModel.InvitedContactsArray = [userAsks valueForKey:@"InvitedContacts"];
                userAsksModel.InvitedContactsCount =  [NSString stringWithFormat:@"you have asked %lu friends for recommendation",[[userAsks valueForKey:@"InvitedContacts"]count]];
                
                NSArray *askDetailsArray = [userAsks valueForKey:@"AskDetails"];
                
                NSMutableArray *askDetailsMutableArray = [[NSMutableArray alloc] init];
                
                for (NSDictionary *askDetail in askDetailsArray) {
                    
                    MAAskDetailsModel *askDetailModel = [MAAskDetailsModel new];
                    askDetailModel.AskDetailID = [askDetail valueForKey:@"Id"];
                    askDetailModel.AskId = [askDetail valueForKey:@"AskId"];
                    askDetailModel.Number = [askDetail valueForKey:@"Number"];
                    askDetailModel.latitude = [askDetail valueForKey:@"Latitude"];
                    askDetailModel.longitude = [askDetail valueForKey:@"Longitude"];
                    askDetailModel.placeAddress = [askDetail valueForKey:@"PlaceAddress"];
                    askDetailModel.placeID = [askDetail valueForKey:@"PlaceId"];
                    askDetailModel.placeName = [askDetail valueForKey:@"PlaceName"];
                    askDetailModel.Message = [askDetail valueForKey:@"Message"];
                    askDetailModel.Type = [askDetail valueForKey:@"Type"];
                    askDetailModel.MessageTime = [askDetail valueForKey:@"MessageTime"];
                    [askDetailsMutableArray addObject:askDetailModel];
                }
                userAsksModel.AskDetailsArray = askDetailsMutableArray;
                
                [userAsksArray addObject:userAsksModel];
                
                NSLog(@"%@",userAsksArray);
                NSLog(@"%@",userAsksModel);
            }
            
            self.settingsModel.Asks = userAsksArray;
            
            NSArray *listsArray = [resultDict valueForKey:@"Lists"];
            NSMutableArray *userListsArray = [[NSMutableArray alloc] init];
            
            for (NSDictionary *userLists in listsArray) {
                
                MAUserListsModel *userListsModel = [MAUserListsModel new];
                
                userListsModel.UserListID = [userLists valueForKey:@"UserListID"];
                userListsModel.UserId = [userLists valueForKey:@"UserId"];
                userListsModel.Name = [userLists valueForKey:@"Name"];
                userListsModel.Description = [userLists valueForKey:@"Description"];
                userListsModel.LikesCount = [userLists valueForKey:@"LikesCount"];
                userListsModel.PicturesCount = [userLists valueForKey:@"PicturesCount"];
                userListsModel.CommmentsCount = [userLists valueForKey:@"CommmentsCount"];
                NSString *modifiedDateString = [userLists valueForKey:@"LastmodifiedDate"];
                userListsModel.LastmodifiedDate = [self getLocalDateStringFrmGMT:modifiedDateString];
                
                userListsModel.IsPublic = [userLists valueForKey:@"IsPublic"];
                
                userListsModel.Pictures = [userLists valueForKey:@"Pictures"];
                NSArray *listsPlacesArray = [userLists valueForKey:@"Places"];
                NSMutableArray *listsPlacesMutableArray = [[NSMutableArray alloc] init];
                
                for (NSDictionary *places in listsPlacesArray) {
                    MAUserListsPlacesModel *userListsPlacesModel = [MAUserListsPlacesModel new];
                    userListsPlacesModel.ID = [places valueForKey:@"ID"];
                    userListsPlacesModel.Name = [places valueForKey:@"Name"];
                    userListsPlacesModel.Address = [places valueForKey:@"Address"];
                    userListsPlacesModel.City = [places valueForKey:@"City"];
                    userListsPlacesModel.State = [places valueForKey:@"State"];
                    userListsPlacesModel.Latitude = [places valueForKey:@"Latitude"];
                    userListsPlacesModel.Longitude = [places valueForKey:@"Longitude"];
                    userListsPlacesModel.IsUserFavorite = [places valueForKey:@"IsUserFavorite"];
                    userListsPlacesModel.DistanceFromUserLocation = [places valueForKey:@"DistanceFromUserLocation"];
                    userListsPlacesModel.PlaceID = [places valueForKey:@"PlaceID"];
                    userListsPlacesModel.PhoneNumber = [places valueForKey:@"PhoneNumber"];
                    userListsPlacesModel.OpenHours = [places valueForKey:@"OpenHours"];
                    userListsPlacesModel.UserId = [places valueForKey:@"UserId"];
                    userListsPlacesModel.ListId = [places valueForKey:@"ListId"];
                    userListsPlacesModel.PlaceImage = [places valueForKey:@"PlaceImage"];
                    [listsPlacesMutableArray addObject:userListsPlacesModel];
                }
                userListsModel.Places = listsPlacesMutableArray;
                [userListsArray addObject:userListsModel];
            }
            self.settingsModel.Lists = userListsArray;
            
            NSDictionary *profileDict = [resultDict valueForKey:@"Profile"];
            
            MAMyProfileModelClass *profileModel = [MAMyProfileModelClass new];
            profileModel.BackgroundImage = [profileDict valueForKey:@"BackgroundImage"];
            profileModel.CountryCode = [profileDict valueForKey:@"CountryCode"];
            profileModel.CreatedDate = [profileDict valueForKey:@"CreatedDate"];
            profileModel.DeviceToken = [profileDict valueForKey:@"DeviceToken"];
            profileModel.Email = [profileDict valueForKey:@"Email"];
            profileModel.Gender = [profileDict valueForKey:@"Gender"];
            profileModel.Id = [[profileDict valueForKey:@"Id"] stringValue];
            profileModel.IsActive = [[profileDict valueForKey:@"IsActive"] boolValue];
            profileModel.MobileNumber = [[profileDict valueForKey:@"MobileNumber"] integerValue];
            profileModel.Name = [profileDict valueForKey:@"Name"];
            profileModel.ProfilePicture = [profileDict valueForKey:@"ProfilePicture"];
            
//            NSData *imageData = [self dataFromBase64EncodedString:profileImageString];
//            profileModel.ProfilePicture = [UIImage imageWithData:imageData];
            
            self.settingsModel.Profile = profileModel;
            
            [self setSettingData];
        }
    }];
}

- (NSString *) getLocalDateStringFrmGMT : (NSString *) createdDateString {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    NSDate *sourceDate = [formatter dateFromString:createdDateString];
    NSLog(@"%@",sourceDate);
    
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
    dateFormat.timeZone = [NSTimeZone systemTimeZone];
    [dateFormat setDateFormat:@"yyyy/MM/dd hh:mm a"];
    NSString* localTime = [dateFormat stringFromDate:sourceDate];
    NSLog(@"localTime:%@", localTime);
    return localTime;
}

- (void) setSettingData {
    
    if (self.settingsModel != nil) {
        if (self.settingsModel.Profile.ProfilePicture == (id)[NSNull null] || self.settingsModel.Profile.ProfilePicture.length == 0 ) {
            //        askCell.imgBackground.image = [UIImage imageNamed:@"background-8"];'
            self.imgProfilePic.image = [UIImage imageNamed:@""];
        }
        else{
            self.imgProfilePic.imageURL = [NSURL URLWithString:self.settingsModel.Profile.ProfilePicture];
        }
        
        if (self.settingsModel.Profile.BackgroundImage == (id)[NSNull null] || self.settingsModel.Profile.BackgroundImage.length == 0 ) {
            //        askCell.imgBackground.image = [UIImage imageNamed:@"background-8"];'
            self.imgCoverPhoto.image = [UIImage imageNamed:@""];
        }
        else{
            self.imgCoverPhoto.imageURL = [NSURL URLWithString:self.settingsModel.Profile.BackgroundImage];
        }

        
        self.txtName.text = self.settingsModel.Profile.Name;
        self.txtNumber.text = [NSString stringWithFormat:@"%ld",self.settingsModel.Profile.MobileNumber];
        self.txtEmail.text = self.settingsModel.Profile.Email;
        
        self.lblAskCount.text = [NSString stringWithFormat:@"%@",self.settingsModel.AsksCount];
        self.lblCommentsCount.text = [NSString stringWithFormat:@"%@",self.settingsModel.CommentsCount];
        self.lblLikesCount.text = [NSString stringWithFormat:@"%@",self.settingsModel.LikesCount];
        self.lblListCount.text = [NSString stringWithFormat:@"%@",self.settingsModel.ListsCount];
        self.lblPlacesCount.text = [NSString stringWithFormat:@"%@",self.settingsModel.PlacesCount];
        self.lblShareCount.text = [NSString stringWithFormat:@"%@",self.settingsModel.SharesCount];
        
        [self setAskDetails];
        [self setListDetails];
    }
}

- (void) setAskDetails {
    
    if ([self.settingsModel.Asks count] > 0) {
        self.mostAnsweredAskView.hidden = false;
        
        MAUserAsksModel *userAsksModel = [self.settingsModel.Asks firstObject];
        
        self.lblAskName.text = userAsksModel.Question;
        self.lblAskDescription.text = userAsksModel.InvitedContactsCount;
        
        NSString *message = userAsksModel.TotalMessages;
        
        NSString *totalMessages = [NSString stringWithFormat:@"%@ Messages",message];

        [self.btnMessageCount setTitle:totalMessages forState:UIControlStateNormal];
        
        if (userAsksModel.BackgroundImage == (id)[NSNull null] || userAsksModel.BackgroundImage.length == 0 ) {
            //        askCell.imgBackground.image = [UIImage imageNamed:@"background-8"];'
            self.imgAskBackgroundImage.image = [UIImage imageNamed:@""];
        }
        else{
            self.imgAskBackgroundImage.imageURL = [NSURL URLWithString:userAsksModel.BackgroundImage];
        }

        
        NSDateFormatter* dateFormatter = [[NSDateFormatter alloc] init];
        dateFormatter.dateFormat = @"yyyy/MM/dd hh:mm a";
        
        NSDate *createdDate = [dateFormatter dateFromString:userAsksModel.CreatedDate];
        if ([self checkTodayDate:createdDate]) {
            self.lblDateTime.text = [self getTimeStringFromDate:createdDate];
        }
        else {
            
            self.lblDateTime.text = [self getDateStringFromDate:createdDate];
        }
    }
    else {
        self.mostAnsweredAskView.hidden = true;
    }
}

- (void) setListDetails {
    
    if ([self.settingsModel.Lists count] > 0) {
        self.mostLikedListView.hidden = false;
        
        MAUserListsModel *userListsModel = [self.settingsModel.Lists firstObject];
        
        if(userListsModel.Name == (id)[NSNull null]) {
            
            self.lblListName.text = [NSString stringWithFormat:@""];
        }
        else {
            
            self.lblListName.text = [NSString stringWithFormat:@"%@",userListsModel.Name];
        }
        if(userListsModel.PicturesCount == (id)[NSNull null]) {
            
            self.lblListPictureCount.text = [NSString stringWithFormat:@"0"];
        }
        else {
            
            self.lblListPictureCount.text = [NSString stringWithFormat:@"%@",userListsModel.PicturesCount];
        }
        if(userListsModel.LikesCount == (id)[NSNull null]) {
            
            self.lblListLikeCount.text = @"0";
        }
        else {
            
            self.lblListLikeCount.text = [NSString stringWithFormat:@"%@",userListsModel.LikesCount];
        }
        if(userListsModel.CommmentsCount == (id)[NSNull null]) {
            
            self.lblCommentsCount.text = @"0";
        }
        else {
            self.lblListCommentCount.text = [NSString stringWithFormat:@"%@",userListsModel.CommmentsCount];
        }
        if ( userListsModel.Pictures.count == 0 ) {
            
            self.imgListBackgroundImage.image = [UIImage imageNamed:@""];
        }
        else{
            self.imgListBackgroundImage.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@", userListsModel.Pictures.firstObject]];
        }
    }
    else {
        self.mostLikedListView.hidden = true;
    }
}

- (void) updateProfilePicture {
    
    [KVNProgress show];
    UIImage *compressedImage  = [[AppData sharedManager]compressImage:self.imgProfilePic.image];
    [[AppData sharedManager] saveUserProfileImage:compressedImage];
    [[APIClient sharedManager] saveASKBackgoundImage:compressedImage onCompletion:^(NSDictionary *resultData, bool success) {
        [KVNProgress dismiss];
        if (success) {
            NSString *imageName = [resultData valueForKey:@"Request_ID"];
//            [self varifyUserWithCoverImageName:imageName];
            NSDictionary *param = @{@"ProfilePicture":imageName,@"Id": [[AppData sharedManager] getCurrentUserID]};
            [self updateUserData:param];
        }
    }];
}

- (void) updateCoverPicture {
    [KVNProgress show];
    UIImage *compressedImage  = [[AppData sharedManager]compressImage:self.imgCoverPhoto.image];
    [[AppData sharedManager] saveUserProfileImage:compressedImage];
    [[APIClient sharedManager] saveASKBackgoundImage:compressedImage onCompletion:^(NSDictionary *resultData, bool success) {
        [KVNProgress dismiss];
        if (success) {
            NSString *imageName = [resultData valueForKey:@"Request_ID"];
            //     [self varifyUserWithCoverImageName:imageName];
            NSDictionary *param = @{@"BackgroundImage":imageName,@"Id": [[AppData sharedManager] getCurrentUserID]};
            [self updateUserData:param];
        }
    }];
}

- (void) updateName {
    NSDictionary *param = @{@"Name":self.txtName.text,@"Id": [[AppData sharedManager] getCurrentUserID]};
    [self updateUserData:param];
}

- (void) updateEmail {
    NSDictionary *param = @{@"Email":self.txtEmail.text,@"Id": [[AppData sharedManager] getCurrentUserID]};
    [self updateUserData:param];
}

- (void) updateUserData : (NSDictionary *) param {
//    [KVNProgress show];
    [AppData startNetworkIndicator];

    [[APIClient sharedManager] updateUserProfile:param onCompletion:^(NSDictionary *resultData, bool success) {
//        [KVNProgress dismiss];
        [AppData stopNetworkIndicator];
    }];
}

-(NSData *)dataFromBase64EncodedString:(NSString *)string{
    if (string.length > 0) {
        NSString *data64URLString = [NSString stringWithFormat:@"data:;base64,%@", string];
        NSData *data = [NSData dataWithContentsOfURL:[NSURL URLWithString:data64URLString]];
        return data;
    }
    return nil;
}

- (BOOL) checkTodayDate : (NSDate *) createdDateWithTime {
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *createdDateComp = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:createdDateWithTime];
    
    NSDateComponents *curentDateComp = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:[NSDate date]];
    
    NSDate *todayDate = [calendar dateFromComponents:curentDateComp];
    
    NSDate *createdDate = [calendar dateFromComponents:createdDateComp];
    
    if ([todayDate compare:createdDate] == NSOrderedDescending) {
        return false;
    }
    else if ([todayDate compare:createdDate] == NSOrderedAscending) {
        return false;
    }
    else {
        return true;
    }
    return false;
}


- (NSString *) getDateStringFromDate : (NSDate *) createdDateWithTime {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *createdDateComp = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:createdDateWithTime];
    NSDate *createdDate = [calendar dateFromComponents:createdDateComp];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy/MM/dd";
    return [dateFormatter stringFromDate:createdDate];
}

- (NSString *) getTimeStringFromDate : (NSDate *) createdDateWithTime {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *createdDateComp = [calendar components:NSCalendarUnitHour | NSCalendarUnitMinute fromDate:createdDateWithTime];
    NSDate *createdDate = [calendar dateFromComponents:createdDateComp];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"hh:mm a";
    return [dateFormatter stringFromDate:createdDate];
}


@end
