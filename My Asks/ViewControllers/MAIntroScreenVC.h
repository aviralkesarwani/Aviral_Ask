//
//  MAIntroScreenVC.h
//  My Asks
//
//  Created by artis on 18/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MASignUpVC.h"
#import "MAInviteFriendsModel.h"
#import "APIClient.h"

@interface MAIntroScreenVC : UIViewController<UIScrollViewDelegate,UIAlertViewDelegate>
{
    NSMutableArray * imagesArray;
    
}
@property (weak, nonatomic) IBOutlet UIScrollView *scrollImages;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (strong, nonatomic) IBOutlet UIButton *btnLetsStart;



- (IBAction)changePageSlider:(id)sender;
- (IBAction)btnLetsStartClicked:(id)sender;

@end
