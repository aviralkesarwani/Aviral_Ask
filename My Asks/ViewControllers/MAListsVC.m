//
//  MAListsVC.m
//  My Asks
//
//  Created by MY PC on 4/17/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "MAListsVC.h"

@interface MAListsVC ()


@end


@implementation MAListsVC



- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.isSearching = NO;
    
    self.tblLists.delegate = self;
    self.tblLists.dataSource = self;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillAppear:(BOOL)animated {
    
    self.userListsArray = [[NSMutableArray alloc] init];
    self.searchListsArray = [[NSMutableArray alloc] init];

    [self.tblLists reloadData];
    
    [self getLists];
}

#pragma mark - Custom Methods

- (void) getLists {
    
//    [KVNProgress show];
    [AppData startNetworkIndicator];

    
    [self.userListsArray removeAllObjects];
    [self.searchListsArray removeAllObjects];
    
    [[APIClient sharedManager] getUserLists:[AppData sharedManager].userID onCompletion:^(NSArray *resultData, bool success) {
        
        if (success == true) {
            
//            [KVNProgress dismiss];
            [AppData stopNetworkIndicator];

            for (NSDictionary *userLists in resultData) {
                
                MAUserListsModel *userListsModel = [MAUserListsModel new];
                
                userListsModel.UserListID = [userLists valueForKey:@"UserListID"];
                userListsModel.UserId = [userLists valueForKey:@"UserId"];
                userListsModel.Name = [userLists valueForKey:@"Name"];
                userListsModel.Description = [userLists valueForKey:@"Description"];
                userListsModel.LikesCount = [userLists valueForKey:@"LikesCount"];
                userListsModel.PicturesCount = [userLists valueForKey:@"PicturesCount"];
                userListsModel.CommmentsCount = [userLists valueForKey:@"CommmentsCount"];
                NSString *modifiedDateString = [userLists valueForKey:@"LastmodifiedDate"];
                userListsModel.LastmodifiedDate = [self getLocalDateStringFrmGMT:modifiedDateString];
                
                userListsModel.IsPublic = [[userLists valueForKey:@"IsPublic"] integerValue];
                userListsModel.IsDefault = [[userLists valueForKey:@"IsDefault"] integerValue];
                userListsModel.Pictures = [userLists valueForKey:@"Pictures"];
                NSArray *listsPlacesArray = [userLists valueForKey:@"Places"];
                NSMutableArray *listsPlacesMutableArray = [[NSMutableArray alloc] init];
                
                for (NSDictionary *places in listsPlacesArray) {
                    MAUserListsPlacesModel *userListsPlacesModel = [MAUserListsPlacesModel new];
                    userListsPlacesModel.ID = [places valueForKey:@"ID"];
                    userListsPlacesModel.Name = [places valueForKey:@"Name"];
                    userListsPlacesModel.Address = [places valueForKey:@"Address"];
                    userListsPlacesModel.City = [places valueForKey:@"City"];
                    userListsPlacesModel.State = [places valueForKey:@"State"];
                    userListsPlacesModel.Latitude = [places valueForKey:@"Latitude"];
                    userListsPlacesModel.Longitude = [places valueForKey:@"Longitude"];
                    userListsPlacesModel.IsUserFavorite = [places valueForKey:@"IsUserFavorite"];
                    userListsPlacesModel.DistanceFromUserLocation = [places valueForKey:@"DistanceFromUserLocation"];
                    userListsPlacesModel.PlaceID = [places valueForKey:@"PlaceID"];
                    userListsPlacesModel.PhoneNumber = [places valueForKey:@"PhoneNumber"];
                    userListsPlacesModel.OpenHours = [places valueForKey:@"OpenHours"];
                    userListsPlacesModel.UserId = [places valueForKey:@"UserId"];
                    userListsPlacesModel.ListId = [places valueForKey:@"ListId"];
                    userListsPlacesModel.PlaceImage = [places valueForKey:@"PlaceImage"];
                    [listsPlacesMutableArray addObject:userListsPlacesModel];
                }
                userListsModel.Places = listsPlacesMutableArray;
                [self.userListsArray addObject:userListsModel];
            }
            [self.tblLists reloadData];
        }
        else {
            
            [KVNProgress showError];
            NSLog(@"Error");
        }
    }];
}

- (NSString *) getLocalDateStringFrmGMT : (NSString *) createdDateString {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    NSDate *sourceDate = [formatter dateFromString:createdDateString];
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
    dateFormat.timeZone = [NSTimeZone systemTimeZone];
    [dateFormat setDateFormat:@"yyyy/MM/dd hh:mm a"];
    NSString* localTime = [dateFormat stringFromDate:sourceDate];
    
    return localTime;
}

- (NSString *) getDateStringFromDate : (NSDate *) createdDateWithTime {
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *createdDateComp = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:createdDateWithTime];
    NSDate *createdDate = [calendar dateFromComponents:createdDateComp];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy/MM/dd";
    return [dateFormatter stringFromDate:createdDate];
}

- (NSString *) getTimeStringFromDate : (NSDate *) createdDateWithTime {
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *createdDateComp = [calendar components:NSCalendarUnitHour | NSCalendarUnitMinute fromDate:createdDateWithTime];
    NSDate *createdDate = [calendar dateFromComponents:createdDateComp];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"hh:mm a";
    return [dateFormatter stringFromDate:createdDate];
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSPredicate *resultPredicate = [NSPredicate
                                    predicateWithFormat:@"Name contains[cd] %@",
                                    searchText];
    NSArray *filterArray;

    filterArray = [self.userListsArray filteredArrayUsingPredicate:resultPredicate];
    
    self.searchListsArray = [NSMutableArray arrayWithArray:filterArray];
}


#pragma mark - SearchBar Delegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)text {
    
    if (text.length > 0) {
        
        self.isSearching = YES;
        [self filterContentForSearchText:text scope:@""];
    }
    else {
        self.isSearching = NO;
        [searchBar performSelector:@selector(resignFirstResponder) withObject:nil afterDelay:0];
    }
    [self.tblLists reloadData];
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    if(searchBar.text.length == 0){
        
    }
}

-(void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    
    [searchBar resignFirstResponder];
}

#pragma mark - TableView Delegate method

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(self.isSearching == YES) {
    
        return [self.searchListsArray count];
    }
    else {
    
        return [self.userListsArray count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MAListsTableCell * listsCell = [tableView dequeueReusableCellWithIdentifier:@"MAListsTableCell" forIndexPath: indexPath];
    MAUserListsModel *userListsModel;
    
    if(self.isSearching == YES) {
        
        userListsModel = [self.searchListsArray objectAtIndex:indexPath.row];
    }
    else {
        
        userListsModel = [self.userListsArray objectAtIndex:indexPath.row];
    }

    if(userListsModel.Name == (id)[NSNull null]) {
        
        listsCell.lblPlace.text = [NSString stringWithFormat:@""];
    }
    else {
        
        listsCell.lblPlace.text = [NSString stringWithFormat:@"%@",userListsModel.Name];
    }
    if(userListsModel.PicturesCount == (id)[NSNull null]) {
        
        listsCell.lblPlaceCount.text = [NSString stringWithFormat:@""];
    }
    else {
        
        listsCell.lblPlaceCount.text = [NSString stringWithFormat:@"%@",userListsModel.PicturesCount];
    }
    if(userListsModel.LikesCount == (id)[NSNull null]) {
        
        listsCell.lblLikeCount.text = @"";
    }
    else {
        
        listsCell.lblLikeCount.text = [NSString stringWithFormat:@"%@",userListsModel.LikesCount];
    }
    if(userListsModel.CommmentsCount == (id)[NSNull null]) {
        
        listsCell.lblCommentCount.text = @"";
    }
    else {
        
        listsCell.lblCommentCount.text = [NSString stringWithFormat:@"%@",userListsModel.CommmentsCount];
    }
    if ( userListsModel.Pictures.count == 0 ) {
        
        listsCell.imgBackground.image = [UIImage imageNamed:@""];
    }
    else{
        
        listsCell.imgBackground.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@", userListsModel.Pictures.firstObject]];
    }
    
    return listsCell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MAListDetailVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MAListDetailVC"];
    
    MAUserListsModel *userListsModel;
    
    if(self.isSearching == YES) {
        
        userListsModel = [self.searchListsArray objectAtIndex:indexPath.row];
    }
    else {
        
        userListsModel = [self.userListsArray objectAtIndex:indexPath.row];
    }

//    vc.userlistData = userListsModel;
    
    vc.userListId = [userListsModel valueForKey:@"UserListID"];
    vc.userId = [userListsModel valueForKey:@"UserId"];
    NSLog(@"%@",vc.userListId);
    NSLog(@"%@",vc.userId);
    [self.navigationController pushViewController:vc animated:YES];
    
    // for add place to list pop up.
    
    //    MAAddPlactToListVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MAAddPlactToListVC"];
    //    CGRect fullScreenRect = [[UIScreen mainScreen] bounds];
//        vc.userListsArray = self.userListsArray;
//        vc.delegate = self;
    //    BIZPopupViewController *popupViewController = [[BIZPopupViewController alloc] initWithContentViewController:vc contentSize:CGSizeMake(fullScreenRect.size.width , fullScreenRect.size.height)];
    //    popupViewController.showDismissButton = NO;
    //    [self presentViewController:popupViewController animated:NO completion:nil];
}

#pragma mark - Button action

- (IBAction)btnCreateNewListClicked:(id)sender {
    
    MACreateNewListVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MACreateNewListVC"];
    vc.listType = @"Create List";
    [self.navigationController pushViewController:vc animated:YES];
}

@end
