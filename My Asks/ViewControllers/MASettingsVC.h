//
//  MASettingsVC.h
//  My Asks
//
//  Created by artis on 18/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MAHistoryCell.h"
#import "AppData.h"
#import "APIClient.h"
#import "MAUserListsModel.h"
#import "MAUserAsksModel.h"
#import "MAMyProfileModelClass.h"
#import "MASettingModelClass.h"
#import "MAAskDetailsModel.h"
#import <KVNProgress.h>
#import "MAUserListsPlacesModel.h"
#import "MAMyProfileModelClass.h"
#import "AsyncImageView.h"
#import "MAChatScreenVC.h"
#import "MAListDetailVC.h"

@interface MASettingsVC : UIViewController<UITextFieldDelegate,UITableViewDataSource, UITableViewDelegate,UIImagePickerControllerDelegate,UINavigationControllerDelegate>

@property (weak, nonatomic) IBOutlet UIView *topTabBarViewContainer;
@property (strong, nonatomic) IBOutlet UIView *topTabBarView;

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;

@property (weak, nonatomic) IBOutlet UITextField *txtName;
@property (weak, nonatomic) IBOutlet UITextField *txtNumber;
@property (weak, nonatomic) IBOutlet UITextField *txtEmail;

@property (weak, nonatomic) IBOutlet UITableView *tblHistory;

@property (weak, nonatomic) IBOutlet UIView *statisticView;
@property (weak, nonatomic) IBOutlet UIView *historyView;
@property (weak, nonatomic) IBOutlet UIView *informationView;

@property (weak, nonatomic) IBOutlet UIImageView *imgTabBackground;

@property (strong, nonatomic) MASettingModelClass *settingsModel;

- (IBAction)btnInformationClicked:(id)sender;
//- (IBAction)btnHistoryClicked:(id)sender;
- (IBAction)btnStatisticClicked:(id)sender;

- (IBAction)btnListClicked:(id)sender;
- (IBAction)btnAskClicked:(id)sender;

@property (weak, nonatomic) IBOutlet AsyncImageView *imgProfilePic;
@property (weak, nonatomic) IBOutlet AsyncImageView *imgCoverPhoto;

@property (weak, nonatomic) IBOutlet UILabel *lblAskCount;
@property (weak, nonatomic) IBOutlet UILabel *lblListCount;
@property (weak, nonatomic) IBOutlet UILabel *lblPlacesCount;

@property (weak, nonatomic) IBOutlet UILabel *lblLikesCount;
@property (weak, nonatomic) IBOutlet UILabel *lblCommentsCount;
@property (weak, nonatomic) IBOutlet UILabel *lblShareCount;

@property (weak, nonatomic) IBOutlet AsyncImageView *imgListBackgroundImage;

@property (weak, nonatomic) IBOutlet UILabel *lblListName;
@property (weak, nonatomic) IBOutlet UILabel *lblListPictureCount;
@property (weak, nonatomic) IBOutlet UILabel *lblListLikeCount;
@property (weak, nonatomic) IBOutlet UILabel *lblListCommentCount;
@property (weak, nonatomic) IBOutlet UIView *mostLikedListView;

@property (weak, nonatomic) IBOutlet AsyncImageView *imgAskBackgroundImage;

@property (weak, nonatomic) IBOutlet UILabel *lblAskName;
@property (weak, nonatomic) IBOutlet UILabel *lblAskDescription;
@property (weak, nonatomic) IBOutlet UIButton *btnMessageCount;
@property (weak, nonatomic) IBOutlet UILabel *lblDateTime;
@property (weak, nonatomic) IBOutlet UIView *mostAnsweredAskView;

- (IBAction)btnProfileImageClicked:(id)sender;

- (IBAction)btnCoverPhotoClicked:(id)sender;


@end
