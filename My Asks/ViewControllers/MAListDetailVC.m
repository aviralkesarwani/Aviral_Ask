//
//  MAListDetailVC.m
//  My Asks
//
//  Created by MY PC on 4/19/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "MAListDetailVC.h"

@interface MAListDetailVC ()

@end

@implementation MAListDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.txtSearchPlaces addTarget:self
                  action:@selector(textFieldDidChange:)
        forControlEvents:UIControlEventEditingChanged];
    
    
    self.userPlacesArray = [[NSMutableArray alloc] init];
    self.searchPlacesArray = [[NSMutableArray alloc] init];
    
    
    // Do any additional setup after loading the view.
    self.tblListDetail.dataSource = self;
    self.tblListDetail.delegate = self;
    self.userlistData = [[MAUserListsModel alloc] init];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) viewWillAppear:(BOOL)animated {

    [self getPlaceList];
}

#pragma mark - Custom Methods

- (void) getPlaceList {
    
//    [KVNProgress show];
    [AppData startNetworkIndicator];

    

    NSMutableDictionary *params =[[NSMutableDictionary alloc]init];
    
    [params setValue:[AppData sharedManager].latitude forKey:@"Latitude"];
    [params setValue:[AppData sharedManager].latitude forKey:@"Longitude"];
    [params setValue:[AppData sharedManager].userID forKey:@"UserID"];
    [params setValue:self.userListId forKey:@"ListId"];
    
    [[APIClient sharedManager]getListDetailsById:params onCompletion:^(NSDictionary *resultData, bool success) {
        [AppData stopNetworkIndicator];
        [self.userPlacesArray removeAllObjects];
        [self.searchPlacesArray removeAllObjects];
        //            [KVNProgress dismiss];

        if (success == true) {
            
            self.userlistData.UserListID = [resultData valueForKey:@"UserListID"];
            self.userlistData.UserId = [resultData valueForKey:@"UserId"];
            self.userlistData.Name = [resultData valueForKey:@"Name"];
            self.userlistData.Description = [resultData valueForKey:@"Description"];
            self.userlistData.LikesCount = [resultData valueForKey:@"LikesCount"];
            self.userlistData.PicturesCount = [resultData valueForKey:@"PicturesCount"];
            self.userlistData.CommmentsCount = [resultData valueForKey:@"CommmentsCount"];
            NSString *modifiedDateString = [resultData valueForKey:@"LastmodifiedDate"];
            self.userlistData.LastmodifiedDate = [self getLocalDateStringFrmGMT:modifiedDateString];
            
            self.userlistData.IsPublic = [[resultData valueForKey:@"IsPublic"] integerValue];
            self.userlistData.IsDefault = [[resultData valueForKey:@"IsDefault"] integerValue];
            self.userlistData.Pictures = [resultData valueForKey:@"Pictures"];
            
            
            NSArray *listsPlacesArray = [resultData valueForKey:@"Places"];
            for (NSDictionary *places in listsPlacesArray) {
                
                MAUserListsPlacesModel *userListsPlacesModelTemp = [MAUserListsPlacesModel new];
                userListsPlacesModelTemp.ID = [places valueForKey:@"ID"];
                userListsPlacesModelTemp.Name = [places valueForKey:@"Name"];
                userListsPlacesModelTemp.Address = [places valueForKey:@"Address"];
                userListsPlacesModelTemp.City = [places valueForKey:@"City"];
                userListsPlacesModelTemp.State = [places valueForKey:@"State"];
                userListsPlacesModelTemp.Latitude = [places valueForKey:@"Latitude"];
                userListsPlacesModelTemp.Longitude = [places valueForKey:@"Longitude"];
                userListsPlacesModelTemp.IsUserFavorite = [places valueForKey:@"IsUserFavorite"];
                userListsPlacesModelTemp.PlaceID = [places valueForKey:@"PlaceID"];
                userListsPlacesModelTemp.PhoneNumber = [places valueForKey:@"PhoneNumber"];
                userListsPlacesModelTemp.OpenHours = [places valueForKey:@"OpenHours"];
                userListsPlacesModelTemp.UserId = [places valueForKey:@"UserId"];
                userListsPlacesModelTemp.ListId = [places valueForKey:@"ListId"];
                userListsPlacesModelTemp.PlaceImage = [places valueForKey:@"PlaceImage"];
                userListsPlacesModelTemp.Category = [places valueForKey:@"Category"];
                
                NSString *distance;
                if ([places valueForKey:@"DistanceFromUserLocation"] == (id)[NSNull null]) {
                    
                    distance = @"00.00mi";
                }
                else{
                    float distanceMiles = [[places valueForKey:@"DistanceFromUserLocation"]floatValue];
                    
                    distance = [NSString stringWithFormat:@"%.2f mi",distanceMiles];
                }

                userListsPlacesModelTemp.DistanceFromUserLocation = distance;
                
                [self.userPlacesArray addObject:userListsPlacesModelTemp];
                [self.userlistData.Places addObject:userListsPlacesModelTemp];
            }
            if(self.isSearching == NO) {
                [self.tblListDetail reloadData];
            }
            
            [self.tblListDetail reloadData];
            
            if(self.userlistData.IsPublic) {
                
                self.imgLock.hidden = false;
            }
            else {
                
                self.imgLock.hidden = true;
            }
            
            NSLog(@"%@",self.userlistData.UserId);
            NSLog(@"%@",[AppData sharedManager].userID);
            
            NSString *listUserID = [NSString stringWithFormat:@"%@",self.userlistData.UserId];
            
            if ([listUserID isEqualToString:[AppData sharedManager].userID]) {
                
                self.btnEdit.hidden = false;
            }
            else{
                
                self.btnEdit.hidden = true;
            }
            
            self.lblTitle.text = [NSString stringWithFormat:@"%@",self.userlistData.Name];
            
        }
        else {
            
            [KVNProgress showError];
            NSLog(@"Error");
        }
    }];
}

- (NSString *) getLocalDateStringFrmGMT : (NSString *) createdDateString {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    NSDate *sourceDate = [formatter dateFromString:createdDateString];
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
    dateFormat.timeZone = [NSTimeZone systemTimeZone];
    [dateFormat setDateFormat:@"yyyy/MM/dd hh:mm a"];
    NSString* localTime = [dateFormat stringFromDate:sourceDate];
    
    return localTime;
}

- (NSString *) getDateStringFromDate : (NSDate *) createdDateWithTime {
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *createdDateComp = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:createdDateWithTime];
    NSDate *createdDate = [calendar dateFromComponents:createdDateComp];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy/MM/dd";
    return [dateFormatter stringFromDate:createdDate];
}

- (NSString *) getTimeStringFromDate : (NSDate *) createdDateWithTime {
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *createdDateComp = [calendar components:NSCalendarUnitHour | NSCalendarUnitMinute fromDate:createdDateWithTime];
    NSDate *createdDate = [calendar dateFromComponents:createdDateComp];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"hh:mm a";
    return [dateFormatter stringFromDate:createdDate];
}



- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    [self.txtSearchPlaces resignFirstResponder];
    [self getPlacesList];
}

-(void)getPlacesList{
    
    NSMutableDictionary *param =[[NSMutableDictionary alloc]init];
    [param setValue:[AppData sharedManager].latitude forKey:@"Latitude"];
    [param setValue:[AppData sharedManager].longitude forKey:@"Longitude"];
    [param setValue:@"2000" forKey:@"Radius"];
    [param setValue:[AppData sharedManager].userID forKey:@"UserID"];
    [param setValue:self.txtSearchPlaces.text forKey:@"SearchText"];
    
//    [KVNProgress show];
    [AppData startNetworkIndicator];

    
    [[APIClient sharedManager]getSearchPlaces:param onCompletion:^(NSArray *resultData, bool success) {
        
//        [KVNProgress dismiss];
        [AppData stopNetworkIndicator];
        [self.searchPlacesArray removeAllObjects];

        if (success) {
            
            if ([resultData count] == 0) {
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
                                                                message:@"No places to display"
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                
                [alert show];
                [self.tblListDetail reloadData];
            }
            else{
                
                for (NSDictionary *places in resultData) {
                    
                    MAListOrPlaceSearchModel *placesModel = [[MAListOrPlaceSearchModel alloc]init];
                    
                    placesModel.UserID = [places valueForKey:@"UserID"];
                    placesModel.PlaceLatitude = [places valueForKey:@"PlaceLatitude"];
                    placesModel.PlaceLongitude = [places valueForKey:@"PlaceLongitude"];
                    placesModel.PlaceTypes = [places valueForKey:@"PlaceTypes"];
                    placesModel.PlaceName = [places valueForKey:@"PlaceName"];
                    placesModel.PlaceId = [places valueForKey:@"PlaceId"];
                    placesModel.DistanceinMiles = [places valueForKey:@"DistanceinMiles"];
                    NSString *distance;
                    if ([places valueForKey:@"DistanceinMiles"] == (id)[NSNull null]) {
                        
                        distance = @"00.00mi";
                    }
                    else{
                        float distanceMiles = [[places valueForKey:@"DistanceinMiles"]floatValue];
                        
                        distance = [NSString stringWithFormat:@"%.2f mi",distanceMiles];
                    }
                    
                    placesModel.DistanceinMiles = distance;
                    placesModel.DistanceinKMs = [places valueForKey:@"DistanceinKMs"];
                    placesModel.isFavorite = [places valueForKey:@"isFavorite"];
                    placesModel.LikesCount = [places valueForKey:@"LikesCount"];
                    placesModel.CommentsCount = [places valueForKey:@"CommentsCount"];
                    placesModel.PlaceAddress = [places valueForKey:@"PlaceAddress"];
                    placesModel.PlaceImage = [places valueForKey:@"PlaceImage"];
                    placesModel.PlaceImageURL = [places valueForKey:@"PlaceImageURL"];
                    placesModel.InternationalPhoneNumber = [places valueForKey:@"InternationalPhoneNumber"];
                    placesModel.OpeningHours = [places valueForKey:@"OpeningHours"];
                    
                    [self.searchPlacesArray addObject:placesModel];
                }
                [self.tblListDetail reloadData];
            }
        }
    }];
}


- (void)textFieldDidChange:(UITextField *)textField
{
    
    if (textField.text.length > 0) {
        
        self.isSearching = YES;
        [self filterContentForSearchText:textField.text scope:@""];
    }
    else {
        self.isSearching = NO;
        [textField performSelector:@selector(resignFirstResponder) withObject:nil afterDelay:0];
    }
    [self.tblListDetail reloadData];
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField {
    
    [textField resignFirstResponder];
    return true;
}

-(BOOL)textFieldShouldClear:(UITextField *)textField
{
    textField.text = @"";
    [textField resignFirstResponder];
    self.isSearching = NO;
    [self.tblListDetail reloadData];
    return NO;
}


#pragma mark - TableView Delegate method

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(self.isSearching == YES) {
        
        return [self.searchPlacesArray count];
    }
    else {
        
        NSLog(@"%lu",(unsigned long)self.userPlacesArray.count);
        return [self.userPlacesArray count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSLog(@"%lu",(unsigned long)self.userPlacesArray.count);
    MACreateNewListCell * listsCell = [tableView dequeueReusableCellWithIdentifier:@"MACreateNewListCell" forIndexPath: indexPath];
    listsCell.btnShare.tag = indexPath.row;
    [listsCell.btnShare addTarget:self action:@selector(btnShareClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    if(self.isSearching == YES) {
        
        listOrPlaceSearchModel = [self.searchPlacesArray objectAtIndex:indexPath.row];
        
        if(listOrPlaceSearchModel.PlaceName == (id)[NSNull null]) {
            
            listsCell.lblPlaceName.text = @"";
        }
        else {
            
            listsCell.lblPlaceName.text = [NSString stringWithFormat:@"%@", listOrPlaceSearchModel.PlaceName];
        }
        if(listOrPlaceSearchModel.PlaceAddress == (id)[NSNull null]) {
            
            listsCell.lblAddress.text = @"";
        }
        else {
            
            listsCell.lblAddress.text = [NSString stringWithFormat:@"%@", listOrPlaceSearchModel.PlaceAddress];
        }
        if(listOrPlaceSearchModel.DistanceinMiles == (id)[NSNull null]) {
            
            listsCell.lblMiles.text = @"";
        }
        else {
            
            listsCell.lblMiles.text = [NSString stringWithFormat:@"%@",listOrPlaceSearchModel.DistanceinMiles];
        }
        NSString *picUrl = listOrPlaceSearchModel.PlaceImage;
        if (listOrPlaceSearchModel.PlaceImage == (id)[NSNull null] || picUrl.length == 0) {
            
            listsCell.imgPlacePic.image = [UIImage imageNamed:@""];
        }
        else{
            
            listsCell.imgPlacePic.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@",listOrPlaceSearchModel.PlaceImageURL]];
        }
        
        
    }
    else {
        
        userListsPlacesModel = [self.userPlacesArray objectAtIndex:indexPath.row];
        if(userListsPlacesModel.Name == (id)[NSNull null]) {
            
            listsCell.lblPlaceName.text = @"";
        }
        else {
            
            listsCell.lblPlaceName.text = [NSString stringWithFormat:@"%@", userListsPlacesModel.Name];
        }
        if(userListsPlacesModel.Address == (id)[NSNull null]) {
            
            listsCell.lblAddress.text = @"";
        }
        else {
            
            listsCell.lblAddress.text = [NSString stringWithFormat:@"%@", userListsPlacesModel.Address];
        }
        if(userListsPlacesModel.DistanceFromUserLocation == (id)[NSNull null]) {
            
            listsCell.lblMiles.text = @"";
        }
        else {
            
            listsCell.lblMiles.text = userListsPlacesModel.DistanceFromUserLocation;
        }
        NSString *picUrl = userListsPlacesModel.PlaceImage;
        if (userListsPlacesModel.PlaceImage == (id)[NSNull null] || picUrl.length == 0) {
            
            listsCell.imgPlacePic.image = [UIImage imageNamed:@""];
        }
        else{
            
            listsCell.imgPlacePic.imageURL = [NSURL URLWithString:userListsPlacesModel.PlaceImage];
        }
    }
    
    

    return listsCell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
 
    MAPlacesDetailVC *detailVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MAPlacesDetailVC"];
    
    if(self.isSearching == YES) {
        
        listOrPlaceSearchModel = [self.searchPlacesArray objectAtIndex:indexPath.row];
        NSString *distance;
        NSLog(@"%@",listOrPlaceSearchModel.DistanceinMiles);
        
        if (listOrPlaceSearchModel.DistanceinMiles == (id)[NSNull null]) {
            
            distance = @"00.00mi";
        }
        else{
            
            float distanceMiles = [listOrPlaceSearchModel.DistanceinMiles floatValue];
            distance = [NSString stringWithFormat:@"%.2f mi",distanceMiles];
        }
        
//        detailVC.PlaceName = listOrPlaceSearchModel.PlaceName;
//        detailVC.PlaceAddress = listOrPlaceSearchModel.PlaceAddress;
//        detailVC.PlaceImageUrl = listOrPlaceSearchModel.PlaceImageURL;
//        detailVC.PlaceDistanceInMiles = distance;
//        detailVC.PlaceLatitude = listOrPlaceSearchModel.PlaceLatitude;
//        detailVC.PlaceLongitude = listOrPlaceSearchModel.PlaceLongitude;
        detailVC.PlaceID = listOrPlaceSearchModel.PlaceId;
        //detailVC.isFavorite = listOrPlaceSearchModel.isFavorite;
        
        NSLog(@"%@",detailVC.PlaceLatitude);
        NSLog(@"%@",detailVC.PlaceLongitude);
        NSLog(@"%@",detailVC.PlaceID);
    }
    else {
        
        userListsPlacesModel = [self.userPlacesArray objectAtIndex:indexPath.row];
        NSString *distance;
        NSLog(@"%@",userListsPlacesModel.DistanceFromUserLocation);
        
        if (userListsPlacesModel.DistanceFromUserLocation == (id)[NSNull null]) {
            
            distance = @"00.00mi";
        }
        else{
            float distanceMiles = [userListsPlacesModel.DistanceFromUserLocation floatValue];
            
            distance = [NSString stringWithFormat:@"%.2f mi",distanceMiles];
        }
        
//        detailVC.PlaceName = userListsPlacesModel.Name;
//        detailVC.PlaceAddress = userListsPlacesModel.Address;
//        detailVC.PlaceImageUrl = userListsPlacesModel.PlaceImage;
//        detailVC.PlaceDistanceInMiles = distance;
//        detailVC.PlaceLatitude = userListsPlacesModel.Latitude;
//        detailVC.PlaceLongitude = userListsPlacesModel.Longitude;
        detailVC.PlaceID = userListsPlacesModel.PlaceID;
        //detailVC.isFavorite = userListsPlacesModel.IsUserFavorite;
        
        NSLog(@"%@",detailVC.PlaceLatitude);
        NSLog(@"%@",detailVC.PlaceLongitude);
        NSLog(@"%@",detailVC.PlaceID);
    }
    [self.navigationController pushViewController:detailVC animated:YES];
}

#pragma mark - Button action

- (IBAction)btnBackClicked:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnEditClicked:(id)sender {
    
    MACreateNewListVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MACreateNewListVC"];
    vc.userlistData = self.userlistData;
    vc.listType = @"Edit List";
    [self.navigationController pushViewController:vc animated:YES];
}

- (void)btnShareClicked:(UIButton*)sender{
    
    MAListOrPlaceSearchModel *listOrPlaceSearchModelTemp = [[MAListOrPlaceSearchModel alloc]init];

    NSLog(@"%@",[[self.userPlacesArray objectAtIndex:sender.tag] description]);
    if(self.isSearching == YES) {
        
        listOrPlaceSearchModelTemp = [self.searchPlacesArray objectAtIndex:sender.tag];
       }
    else {
        
        listOrPlaceSearchModelTemp = [self.userPlacesArray objectAtIndex:sender.tag];
    }
    
    NSLog(@"%@",[listOrPlaceSearchModelTemp valueForKey:@"PlaceID"]);
    
    
    NSString *textToShare = [NSString stringWithFormat:@"%@ %@ %@",[[NSUserDefaults standardUserDefaults] valueForKey:MAUserName],@" has share you a Place ", [NSString stringWithFormat:@"rex.life://?PlaceID=%@&UserID=%@",[listOrPlaceSearchModelTemp valueForKey:@"PlaceID"],[AppData sharedManager].userID]];
    NSLog(@"%@",textToShare);
    NSLog(@"%@",[NSString stringWithFormat:@"%@ %@ %@",[[NSUserDefaults standardUserDefaults] valueForKey:MAUserName],@" has share you a Place ", [NSString stringWithFormat:@"rex.life://?PlaceID=%@&UserID=%@",[listOrPlaceSearchModelTemp valueForKey:@"PlaceID"],[AppData sharedManager].userID]]);
    NSArray *itemsToShare = @[textToShare];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:nil];
    activityVC.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll];
    [self presentViewController:activityVC animated:YES completion:nil];
}

- (IBAction)btnListShareClicked:(id)sender {
    
    NSString *textToShare = [NSString stringWithFormat:@"%@ %@ %@",[[NSUserDefaults standardUserDefaults] valueForKey:MAUserName],@" has share you a List ", [NSString stringWithFormat:@"rex.life://?ListID=%@&UserID=%@",self.userlistData.UserListID,[AppData sharedManager].userID]];
    NSLog(@"%@",textToShare);
    NSLog(@"%@",[NSString stringWithFormat:@"%@ %@ %@",[[NSUserDefaults standardUserDefaults] valueForKey:MAUserName],@" has share you a List ", [NSString stringWithFormat:@"rex.life://?ListID=%@&UserID=%@",self.userlistData.UserListID,[AppData sharedManager].userID]]);
    NSArray *itemsToShare = @[textToShare];
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:itemsToShare applicationActivities:nil];
    activityVC.excludedActivityTypes = @[UIActivityTypePrint, UIActivityTypeCopyToPasteboard, UIActivityTypeAssignToContact, UIActivityTypeSaveToCameraRoll];
    [self presentViewController:activityVC animated:YES completion:nil];
}

@end
