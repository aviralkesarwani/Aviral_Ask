//
//  MAPeopleDetailVC.m
//  My Asks
//
//  Created by MY PC on 4/22/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.


#import "MAPeopleDetailVC.h"

@interface MAPeopleDetailVC ()

@end

@implementation MAPeopleDetailVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.imgProfile.clipsToBounds = YES;
    self.imgProfile.layer.cornerRadius = self.imgProfile.frame.size.width/2.0;
    
    userAsksArray = [[NSMutableArray alloc]init];
    userListsArray = [[NSMutableArray alloc]init];
    // Do any additional setup after loading the view.
    
    [self getFriendProfile:self.friendID];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - UITableView Delegate/DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (tableView == self.tblAsks) {
        
        return [userAsksArray count];
    }
    else{
        return [userListsArray count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(tableView == self.tblLists) {
        
        MAListsTableCell *listsCell = [tableView dequeueReusableCellWithIdentifier:@"MAListsTableCell" forIndexPath: indexPath];
       
        MAUserListsModel *listModel = [userListsArray objectAtIndex:indexPath.row];
        
         NSLog(@"%@",listModel.Name);
         NSLog(@"%@",listModel.LikesCount);
         NSLog(@"%@",listModel.CommmentsCount);
         NSLog(@"%@",listModel.PicturesCount);
    
        if ([listModel.Pictures count] > 0) {
            
            if ([listModel.Pictures firstObject] != (id)[NSNull null]) {
                listsCell.imgBackground.imageURL = [NSURL URLWithString:[listModel.Pictures firstObject]];
            }
        }
        
        listsCell.lblPlace.text = listModel.Name;
        listsCell.lblLikeCount.text = [NSString stringWithFormat:@"%@",listModel.LikesCount] ;
        listsCell.lblCommentCount.text = [NSString stringWithFormat:@"%@",listModel.CommmentsCount] ;
        listsCell.lblPlaceCount.text =  [NSString stringWithFormat:@"%@",listModel.PicturesCount];
        
        return listsCell;
    }
    else {
        
        MAAsksTableCell *asksCell = [tableView dequeueReusableCellWithIdentifier:@"MAAsksTableCell" forIndexPath: indexPath];
        MAUserAsksModel *askDetailModel;
        askDetailModel = [userAsksArray objectAtIndex:indexPath.row];
        
        if (askDetailModel.BackgroundImage != (id)[NSNull null]) {
            asksCell.imgBackground.imageURL = [NSURL URLWithString:askDetailModel.BackgroundImage];
        }
        
        asksCell.lblAsk.text = askDetailModel.Question;
        asksCell.lblNoOfRecomendAsk.text = askDetailModel.InvitedContactsCount;
        asksCell.lblDateTimeOfAsk.text = askDetailModel.CreatedDate;
        
        return asksCell;
    }
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if(tableView == self.tblLists) {
        MAUserListsModel *listModel = [userListsArray objectAtIndex:indexPath.row];
        MAListDetailVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"MAListDetailVC"];
        MAUserListsModel *userListsModel = listModel;
        vc.userlistData = userListsModel;
        [self.navigationController pushViewController:vc animated:YES];
    }
    else {
        MAUserAsksModel *askDetailModel = [userAsksArray objectAtIndex:indexPath.row];
        MAChatScreenVC *chatScreenVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MAChatScreenVC"];
//        chatScreenVC.userAskData = askDetailModel;
//        chatScreenVC.askId = askDetailModel.UserAsksID;
        [self.navigationController pushViewController:chatScreenVC animated:true];
    }
}

#pragma mark - Button Action

- (IBAction)btnBackClicked:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnListsClicked:(id)sender {
    
    self.imgTabBackground.image = [UIImage imageNamed:@"Tab 1"];
    self.listsView.hidden = false;
    self.asksView.hidden = true;
    [self.tblLists reloadData];
}

- (IBAction)btnAsksClicked:(id)sender {
    
    self.imgTabBackground.image = [UIImage imageNamed:@"Tab 2"];
    self.asksView.hidden = false;
    self.listsView.hidden = true;
    [self.tblAsks reloadData];
}

- (void) getFriendProfile : (NSString *) friendID {
    
//    [KVNProgress show];
    [AppData startNetworkIndicator];

    [[APIClient sharedManager] GetFriendProfile:friendID onCompletion:^(NSDictionary *resultData, bool success) {
//        [KVNProgress dismiss];
        [AppData stopNetworkIndicator];

        if (success) {
            
            NSLog(@"%@",resultData);
            
            NSDictionary *resultDict = resultData;
            
            self.settingsModel = [[MASettingModelClass alloc] init];
            self.settingsModel.AsksCount = [[resultDict valueForKey:@"AsksCount"] stringValue];
            self.settingsModel.CommentsCount = [[resultDict valueForKey:@"CommentsCount"] stringValue];
            self.settingsModel.LikesCount = [[resultDict valueForKey:@"LikesCount"] stringValue];
            self.settingsModel.ListsCount = [[resultDict valueForKey:@"ListsCount"] stringValue];
            self.settingsModel.PlacesCount = [[resultDict valueForKey:@"PlacesCount"] stringValue];
            self.settingsModel.SharesCount = [[resultDict valueForKey:@"SharesCount"] stringValue];
            self.settingsModel.FriendsCount = [[resultDict valueForKey:@"FriendsCount"] stringValue];
            
            NSArray *asksArray = [resultDict valueForKey:@"Asks"];
            

            for (NSDictionary *userAsks in asksArray) {
                MAUserAsksModel *userAsksModel = [MAUserAsksModel new];
                
                userAsksModel.UserAsksID = [userAsks valueForKey:@"Id"];
                userAsksModel.UserId = [userAsks valueForKey:@"UserId"];
                userAsksModel.Question = [userAsks valueForKey:@"Question"];
                userAsksModel.TotalMessages = [userAsks valueForKey:@"TotalMessages"];
                userAsksModel.IsOwner = [userAsks valueForKey:@"IsOwner"];
                
                userAsksModel.BackgroundImage = [userAsks valueForKey:@"BackgroundImageUrl"];
                
                userAsksModel.ChannelName = [userAsks valueForKey:@"ChannelName"];
                
                NSString *createdDateString = [userAsks valueForKey:@"CreatedDate"];
                
                userAsksModel.CreatedDate = [self getLocalDateStringFrmGMT:createdDateString];
                
                userAsksModel.InvitedContactsArray = [userAsks valueForKey:@"InvitedContacts"];
                userAsksModel.InvitedContactsCount =  [NSString stringWithFormat:@"you have asked %lu friends for recommendation",[[userAsks valueForKey:@"InvitedContacts"]count]];
                
                NSArray *askDetailsArray = [userAsks valueForKey:@"AskDetails"];
                
                NSMutableArray *askDetailsMutableArray = [[NSMutableArray alloc] init];
                
                for (NSDictionary *askDetail in askDetailsArray) {
                    
                    MAAskDetailsModel *askDetailModel = [MAAskDetailsModel new];
                    askDetailModel.AskDetailID = [askDetail valueForKey:@"Id"];
                    askDetailModel.AskId = [askDetail valueForKey:@"AskId"];
                    askDetailModel.Number = [askDetail valueForKey:@"Number"];
                    askDetailModel.latitude = [askDetail valueForKey:@"Latitude"];
                    askDetailModel.longitude = [askDetail valueForKey:@"Longitude"];
                    askDetailModel.placeAddress = [askDetail valueForKey:@"PlaceAddress"];
                    askDetailModel.placeID = [askDetail valueForKey:@"PlaceId"];
                    askDetailModel.placeName = [askDetail valueForKey:@"PlaceName"];
                    askDetailModel.Message = [askDetail valueForKey:@"Message"];
                    askDetailModel.Type = [askDetail valueForKey:@"Type"];
                    askDetailModel.MessageTime = [askDetail valueForKey:@"MessageTime"];
                    [askDetailsMutableArray addObject:askDetailModel];
                }
                userAsksModel.AskDetailsArray = askDetailsMutableArray;
                
                [userAsksArray addObject:userAsksModel];
                
                NSLog(@"%@",userAsksArray);
                NSLog(@"%@",userAsksModel);
                
                NSLog(@"%@",userAsksModel.Question);
                NSLog(@"%@",userAsksModel.InvitedContactsCount);
                NSLog(@"%@",userAsksModel.CreatedDate);
                
                
            }
            

            NSArray *listsArray = [resultDict valueForKey:@"Lists"];
           
            for (NSDictionary *userLists in listsArray) {
                
                MAUserListsModel *userListsModel = [MAUserListsModel new];
                
                userListsModel.UserListID = [userLists valueForKey:@"UserListID"];
                userListsModel.UserId = [userLists valueForKey:@"UserId"];
                userListsModel.Name = [userLists valueForKey:@"Name"];
                userListsModel.Description = [userLists valueForKey:@"Description"];
                userListsModel.LikesCount = [userLists valueForKey:@"LikesCount"];
                userListsModel.PicturesCount = [userLists valueForKey:@"PicturesCount"];
                userListsModel.CommmentsCount = [userLists valueForKey:@"CommmentsCount"];
                NSString *modifiedDateString = [userLists valueForKey:@"LastmodifiedDate"];
                userListsModel.LastmodifiedDate = [self getLocalDateStringFrmGMT:modifiedDateString];
                
                userListsModel.IsPublic = [userLists valueForKey:@"IsPublic"];
                
                userListsModel.Pictures = [userLists valueForKey:@"Pictures"];
                NSArray *listsPlacesArray = [userLists valueForKey:@"Places"];
                NSMutableArray *listsPlacesMutableArray = [[NSMutableArray alloc] init];
                
                for (NSDictionary *places in listsPlacesArray) {
                    MAUserListsPlacesModel *userListsPlacesModel = [MAUserListsPlacesModel new];
                    userListsPlacesModel.ID = [places valueForKey:@"ID"];
                    userListsPlacesModel.Name = [places valueForKey:@"Name"];
                    userListsPlacesModel.Address = [places valueForKey:@"Address"];
                    userListsPlacesModel.City = [places valueForKey:@"City"];
                    userListsPlacesModel.State = [places valueForKey:@"State"];
                    userListsPlacesModel.Latitude = [places valueForKey:@"Latitude"];
                    userListsPlacesModel.Longitude = [places valueForKey:@"Longitude"];
                    userListsPlacesModel.IsUserFavorite = [places valueForKey:@"IsUserFavorite"];
                    userListsPlacesModel.DistanceFromUserLocation = [places valueForKey:@"DistanceFromUserLocation"];
                    userListsPlacesModel.PlaceID = [places valueForKey:@"PlaceID"];
                    userListsPlacesModel.PhoneNumber = [places valueForKey:@"PhoneNumber"];
                    userListsPlacesModel.OpenHours = [places valueForKey:@"OpenHours"];
                    userListsPlacesModel.UserId = [places valueForKey:@"UserId"];
                    userListsPlacesModel.ListId = [places valueForKey:@"ListId"];
                    userListsPlacesModel.PlaceImage = [places valueForKey:@"PlaceImage"];
                    [listsPlacesMutableArray addObject:userListsPlacesModel];
                }
                userListsModel.Places = listsPlacesMutableArray;
                [userListsArray addObject:userListsModel];
                
                
                NSLog(@"%@",userListsModel.Name);
                NSLog(@"%@",userListsModel.LikesCount);
                NSLog(@"%@",userListsModel.CommmentsCount);
                NSLog(@"%@",userListsModel.PicturesCount);
            }
            self.settingsModel.Lists = userListsArray;
            
            NSDictionary *profileDict = [resultDict valueForKey:@"Profile"];
            
            MAMyProfileModelClass *profileModel = [MAMyProfileModelClass new];
            profileModel.BackgroundImage = [profileDict valueForKey:@"BackgroundImage"];
            profileModel.CountryCode = [profileDict valueForKey:@"CountryCode"];
            profileModel.CreatedDate = [profileDict valueForKey:@"CreatedDate"];
            profileModel.DeviceToken = [profileDict valueForKey:@"DeviceToken"];
            profileModel.Email = [profileDict valueForKey:@"Email"];
            profileModel.Gender = [profileDict valueForKey:@"Gender"];
            profileModel.Id = [[profileDict valueForKey:@"Id"] stringValue];
            profileModel.IsActive = [[profileDict valueForKey:@"IsActive"] boolValue];
            profileModel.MobileNumber = [[profileDict valueForKey:@"MobileNumber"] integerValue];
            profileModel.Name = [profileDict valueForKey:@"Name"];
            profileModel.ProfilePicture = [profileDict valueForKey:@"ProfilePicture"];
            
            //            NSData *imageData = [self dataFromBase64EncodedString:profileImageString];
            //            profileModel.ProfilePicture = [UIImage imageWithData:imageData];
            
            self.settingsModel.Profile = profileModel;
            
            [self setSettingData];
            [self.tblLists reloadData];
        }
    }];
}

- (void) setSettingData {
    
    if (self.settingsModel != nil) {
        if (self.settingsModel.Profile.ProfilePicture == (id)[NSNull null] || self.settingsModel.Profile.ProfilePicture.length == 0 ) {
           
            self.imgProfile.image = [UIImage imageNamed:@"Photo"];
        }
        else{
            self.imgProfile.imageURL = [NSURL URLWithString:self.settingsModel.Profile.ProfilePicture];
        }
        
        if (self.settingsModel.Profile.BackgroundImage == (id)[NSNull null] || self.settingsModel.Profile.BackgroundImage.length == 0 ) {
            
            self.imgBackground.image = [UIImage imageNamed:@""];
        }
        else{
            self.imgBackground.imageURL = [NSURL URLWithString:self.settingsModel.Profile.BackgroundImage];
        }
        
        self.lblPeopleName.text = self.settingsModel.Profile.Name;
        self.lblAskCount.text = [NSString stringWithFormat:@"%@",self.settingsModel.AsksCount];
        self.lblListCount.text = [NSString stringWithFormat:@"%@",self.settingsModel.ListsCount];
        self.lblFriendCount.text = [NSString stringWithFormat:@"%@",self.settingsModel.ListsCount];
    }
}

- (NSString *) getLocalDateStringFrmGMT : (NSString *) createdDateString {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    NSDate *sourceDate = [formatter dateFromString:createdDateString];
    NSLog(@"%@",sourceDate);
    
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
    dateFormat.timeZone = [NSTimeZone systemTimeZone];
    [dateFormat setDateFormat:@"yyyy/MM/dd hh:mm a"];
    NSString* localTime = [dateFormat stringFromDate:sourceDate];
    NSLog(@"localTime:%@", localTime);
    return localTime;
}

@end
