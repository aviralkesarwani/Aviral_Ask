//
//  MAListsVC.h
//  My Asks
//
//  Created by MY PC on 4/17/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreFoundation/CoreFoundation.h>
#import <KVNProgress.h>
#import "MAListsTableCell.h"
#import "MACreateNewListVC.h"
#import "MAListDetailVC.h"
#import "MAAddPlactToListVC.h"
#import "BIZPopupViewController.h"
#import "AppDelegate.h"
#import "AppData.h"
#import "MAUserListsModel.h"
#import "MAUserListsPlacesModel.h"
#import "APIClient.h"

@interface MAListsVC : UIViewController<UITableViewDataSource, UITableViewDelegate, MAAddPlactToListDelegate, UISearchBarDelegate>

#pragma mark - Variables
@property (strong, nonatomic) NSMutableArray *userListsArray;
@property (strong, nonatomic) NSMutableArray *searchListsArray;

@property (assign, nonatomic) BOOL isSearching;

#pragma mark - Outlets
@property (strong, nonatomic) IBOutlet UISearchBar *listsSearchBar;
@property (weak, nonatomic) IBOutlet UITableView *tblLists;

- (IBAction)btnCreateNewListClicked:(id)sender;

@end
