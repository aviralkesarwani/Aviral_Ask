//
//  MAInviteFriendsVC.m
//  My Asks
//
//  Created by artis on 01/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "MAInviteFriendsVC.h"
#import "MAInviteFriendsTableCell.h"
#import "MAInviteFriendsModel.h"
#import "AppData.h"

@interface MAInviteFriendsVC ()

@end

@implementation MAInviteFriendsVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.inviteFriendsView.layer.cornerRadius = 10.0;
    
    //contactsStrore = [[CNContactStore alloc] init];
    contactDetails = [[NSMutableArray alloc]init];
    
    autocompleteContacts = [[NSMutableArray alloc]init];
    autocompleteContactsName = [[NSMutableArray alloc]init];
    
    self.searchContactArray = [[NSMutableArray alloc] init];
    
    [self.txtSearchContacts addTarget:self action:@selector(textFieldDidChange:) forControlEvents:UIControlEventEditingChanged];
    
//    _tblContactSearchResultTable = [[UITableView alloc] initWithFrame:CGRectMake(8, 95, 320, 120) style:UITableViewStylePlain];
//    _tblContactSearchResultTable.delegate = self;
//    _tblContactSearchResultTable.dataSource = self;
//    _tblContactSearchResultTable.scrollEnabled = YES;
//    _tblContactSearchResultTable.hidden = YES;
//    
//    [self.inviteFriendsView addSubview:_tblContactSearchResultTable];
    
    if (self.isEdit) {
        if ([[AppData sharedManager].invitedFriendsGlobalArray count] > 0) {
            contactDetails = [NSMutableArray arrayWithArray:[AppData sharedManager].invitedFriendsGlobalArray] ;
            [self.inviteFriendsTableView reloadData];
        }
        else {
            [self checkContactsAccess];
        }
    }
    else {
        [self checkContactsAccess];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

-(void)viewWillDisappear:(BOOL)animated{
}

#pragma mark - Contact

-(void)checkContactsAccess{
    
    /*[self requestContactsAccessWithHandler:^(BOOL grandted) {
        
        if (grandted) {
            
//            CNContactFetchRequest *request = [[CNContactFetchRequest alloc] initWithKeysToFetch:@[CNContactFamilyNameKey, CNContactGivenNameKey, CNContactNamePrefixKey, CNContactMiddleNameKey, CNContactPhoneNumbersKey,CNContactImageDataKey,CNContactImageDataAvailableKey,CNContactThumbnailImageDataKey]];
//            [contactsStrore enumerateContactsWithFetchRequest:request error:nil usingBlock:^(CNContact * _Nonnull contact, BOOL * _Nonnull stop) {
//                
//                inviteFriends= [[MAInviteFriendsModel alloc]init];
//                
////                NSLog(@"%@", contact.familyName);
////                NSLog(@"%@", contact.givenName);
////                NSLog(@"%@", contact.namePrefix);
////                NSLog(@"%@", contact.middleName);
////                NSLog(@"%@", contact.phoneNumbers);
////                NSLog(@"%@",[[contact.phoneNumbers valueForKey:@"value"] valueForKey:@"digits"]);
//                
//                inviteFriends.contactFullName = [NSString stringWithFormat:@"%@ %@",contact.givenName,contact.familyName];
//                inviteFriends.arrPhoneNumbers = [[contact.phoneNumbers valueForKey:@"value"] valueForKey:@"digits"];
//                inviteFriends.isSelected = NO;
//                
//                if (contact.imageDataAvailable) {
//                    
//                    inviteFriends.contactImage = [UIImage imageWithData:contact.imageData];
//                }
//                else {
//                    inviteFriends.contactImage = [UIImage imageNamed:@"Photo.png"];
//                    
//                }
//                [autocompleteContactsName addObject:inviteFriends.contactFullName];
//                
//                [contactDetails addObject:inviteFriends];
//                
//                NSLog(@"%@",contactDetails );
//            }];
            
            //keys with fetching properties
            NSArray *keys = @[CNContactFamilyNameKey, CNContactGivenNameKey, CNContactNamePrefixKey, CNContactMiddleNameKey, CNContactPhoneNumbersKey,CNContactImageDataKey,CNContactImageDataAvailableKey,CNContactThumbnailImageDataKey];
            NSString *containerId = contactsStrore.defaultContainerIdentifier;
            NSPredicate *predicate = [CNContact predicateForContactsInContainerWithIdentifier:containerId];
            NSError *error;
            NSArray *cnContacts = [contactsStrore unifiedContactsMatchingPredicate:predicate keysToFetch:keys error:&error];
            if (error) {
                NSLog(@"error fetching contacts %@", error);
            } else {
                for (CNContact *contact in cnContacts) {
                    // copy data to my custom Contacts class.
                    MAInviteFriendsModel *inviteFriends = [[MAInviteFriendsModel alloc]init];

                    inviteFriends.contactFullName = [NSString stringWithFormat:@"%@ %@",contact.givenName,contact.familyName];
                    inviteFriends.arrPhoneNumbers = [[contact.phoneNumbers valueForKey:@"value"] valueForKey:@"digits"];
                    inviteFriends.isSelected = NO;
                    
                    if (contact.imageDataAvailable) {
                        
                        inviteFriends.contactImage = [UIImage imageWithData:contact.imageData];
                    }
                    else {
                        inviteFriends.contactImage = [UIImage imageNamed:@"Photo.png"];
                    }
                    [contactDetails addObject:inviteFriends];
                }
            }
            
            NSLog(@"%ld",[contactDetails count]);
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [self.inviteFriendsTableView reloadData];
            });
        }
    }];*/
}

/*

-(void)requestContactsAccessWithHandler:(void (^)(BOOL grandted))handler{
    
    switch ([CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts]) {
        case CNAuthorizationStatusAuthorized:
            handler(YES);
            break;
        case CNAuthorizationStatusDenied:
        case CNAuthorizationStatusNotDetermined:{
            [contactsStrore requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
                
                handler(granted);
            }];
            break;
        }
        case CNAuthorizationStatusRestricted:
            handler(NO);
            break;
    }
}
 */


#pragma mark - Tableview Delegate/Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (self.isSearching) {
        return [self.searchContactArray count];
    }
    else {
        return [contactDetails count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    
    MAInviteFriendsModel *inviteFriendModel;

    if (self.isSearching) {
        inviteFriendModel = [self.searchContactArray objectAtIndex:indexPath.row];
    }
    else {
        inviteFriendModel = [contactDetails objectAtIndex:indexPath.row];
    }
    
    MAInviteFriendsTableCell *inviteFriendsTableCell = [tableView dequeueReusableCellWithIdentifier:@"MAInviteFriendsTableCell" forIndexPath: indexPath];
    
    
    NSLog(@"%@",inviteFriendModel.contactFullName);
    
    inviteFriendsTableCell.lblContactName.text = inviteFriendModel.contactFullName;
    inviteFriendsTableCell.imgContact.image = [contactDetails[indexPath.row]valueForKeyPath:@"contactImage"];
    
    inviteFriendsTableCell.imgContact.clipsToBounds = YES;
    inviteFriendsTableCell.imgContact.layer.cornerRadius =   inviteFriendsTableCell.imgContact.frame.size.width/2.0;
    
    [inviteFriendsTableCell.btnSelectDeselect addTarget:self action:@selector(btnSelectDeselectClicked:) forControlEvents:UIControlEventTouchUpInside];
    inviteFriendsTableCell.btnSelectDeselect.tag = indexPath.row;
    
//    if (![[contactDetails[indexPath.row]valueForKeyPath:@"isSelected"]integerValue] == 0) {
//        
//        [InviteFriendsTableCell.btnSelectDeselect setImage:[UIImage imageNamed:@"Checkbox Selected.png"] forState:UIControlStateNormal];
//    }
//    else{
//        
//        [InviteFriendsTableCell.btnSelectDeselect setImage:[UIImage imageNamed:@"Checkbox normal.png"] forState:UIControlStateNormal];
//    }
    
    if (inviteFriendModel.isSelected == true) {
        [inviteFriendsTableCell.btnSelectDeselect setImage:[UIImage imageNamed:@"Checkbox Selected"] forState:UIControlStateNormal];
    }
    else {
        [inviteFriendsTableCell.btnSelectDeselect setImage:[UIImage imageNamed:@"Checkbox normal"] forState:UIControlStateNormal];
    }
    
    return  inviteFriendsTableCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  
    if (tableView == _tblContactSearchResultTable) {
   
    }
}

#pragma mark - Button Actions

- (void)btnSelectDeselectClicked:(UIButton*)sender{
   
//    if (![[contactDetails[sender.tag]valueForKeyPath:@"isSelected"]integerValue] == 0) {
//        
//        [contactDetails[sender.tag] setValue:[NSNumber numberWithInt:0] forKey:@"isSelected"];
//         
//        [sender setImage:[UIImage imageNamed:@"Checkbox normal.png"] forState:UIControlStateNormal];
//    }
//    else{
//        [contactDetails[sender.tag] setValue:[NSNumber numberWithInt:1] forKey:@"isSelected"];
//        [sender setImage:[UIImage imageNamed:@"Checkbox Selected.png"] forState:UIControlStateNormal];
//    }
    
    NSInteger index = sender.tag;
    
    if (self.isSearching) {
        
        inviteFriendsModel = [self.searchContactArray objectAtIndex:index];
        NSLog(@"%@",inviteFriendsModel.arrPhoneNumbers);
        
    }
    else {
        inviteFriendsModel = [contactDetails objectAtIndex:index];
        
        NSLog(@"%@",inviteFriendsModel.arrPhoneNumbers);
        
    }

    if (inviteFriendsModel.isSelected == false) {
        
        if ([inviteFriendsModel.arrPhoneNumbers count] > 0) {
            
            NSLog(@"%lu",(unsigned long)[inviteFriendsModel.arrPhoneNumbers count]);
            
            if ([inviteFriendsModel.arrPhoneNumbers count] > 1) {
                CZPickerView *picker = [[CZPickerView alloc] initWithHeaderTitle:@"Select Number" cancelButtonTitle:@"Cancel" confirmButtonTitle:@"Add"];
                picker.delegate = self;
                picker.dataSource = self;
                picker.needFooterView = YES;
                [picker show];
            }
            else {
                inviteFriendsModel.isSelected = true;
                inviteFriendsModel.selectedPhoneNumber  = [inviteFriendsModel.arrPhoneNumbers firstObject];
            }
        }
    }
    
    else {
        inviteFriendsModel.isSelected = false;
    }
    
    [self.inviteFriendsTableView reloadData];
}


- (IBAction)btnDismissClicked:(id)sender {
    [self dismissViewControllerAnimated:true completion:nil];
}

- (IBAction)btnSelectDeselectAllClicked:(id)sender {
    
    if (!isInviteAll) {
        
        [self.btnSelectDeselectAll setImage:[UIImage imageNamed:@"Checkbox Selected"] forState:UIControlStateNormal];
        isInviteAll = true;
        for ( MAInviteFriendsModel *inviteFriend in contactDetails) {
            inviteFriend.isSelected = true;
        }
    }
    else {
        [self.btnSelectDeselectAll setImage:[UIImage imageNamed:@"Checkbox normal"] forState:UIControlStateNormal];
        isInviteAll = false;
        for ( MAInviteFriendsModel *inviteFriend in contactDetails) {
            inviteFriend.isSelected = false;
        }
    }

    [self.inviteFriendsTableView reloadData];

}

- (IBAction)btnInviteFriendsClicked:(id)sender {
    
    NSMutableArray *contactsArray =[[NSMutableArray alloc]init];
    
    NSMutableArray *contactsGlobalArray = [[NSMutableArray alloc] init];
    
    
    for (MAInviteFriendsModel *inviteFriend in contactDetails) {
        if (inviteFriend.isSelected) {
            
            if ([inviteFriend.selectedPhoneNumber length] > 0 || inviteFriend.selectedPhoneNumber != nil) {
                [contactsArray addObject:inviteFriend];
            }
            else if ([inviteFriend.arrPhoneNumbers count] > 0) {
                inviteFriend.selectedPhoneNumber = [inviteFriend.arrPhoneNumbers firstObject];
                [contactsArray addObject:inviteFriend];
            }
        }
        
        
        [contactsGlobalArray addObject:inviteFriend];
    }
    
    [AppData sharedManager].invitedFriendsGlobalArray = [NSMutableArray arrayWithArray:contactsGlobalArray];
    
    NSLog(@"%ld",[[AppData sharedManager].invitedFriendsGlobalArray count]);
    
    [self dismissViewControllerAnimated:true completion:nil];
    
    [self.delegate inviteFriends:contactsArray];
}

- (void)searchAutocompleteEntriesWithSubstring:(NSString *)substring {
    
    // Put anything that starts with this substring into the autocompleteContacts array
    // The items in this array is what will show up in the table view
    
    [autocompleteContacts removeAllObjects];
    
    for(NSString *curString in autocompleteContactsName) {
        
        NSRange substringRange = [curString rangeOfString:substring];
        if (substringRange.location == 0) {
            [autocompleteContacts addObject:curString];
        }
    }
    [_tblContactSearchResultTable reloadData];
}

#pragma mark UITextFieldDelegate methods

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
//     NSLog(@"%lu",textField.text.length);
//    
//    if (textField.text.length > 0) {
//        self.isSearching = true;
//        NSString *substring = [NSString stringWithString:textField.text];
//        substring = [substring stringByReplacingCharactersInRange:range withString:string];
//        [self filterContentForSearchText:substring scope:@""];
//    }
//    else{
//        self.isSearching = false;
//    }
//    [self.tblContactSearchResultTable reloadData];
    return YES;
}

-(void)textFieldDidEndEditing:(UITextField *)textField{
    
    [self.view endEditing:YES];
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSPredicate *resultPredicate = [NSPredicate
                                    predicateWithFormat:@"contactFullName contains[cd] %@",
                                    searchText];
    
    NSArray *filterArray = [contactDetails filteredArrayUsingPredicate:resultPredicate];
    self.searchContactArray = [NSMutableArray arrayWithArray:filterArray];
    
}

-(void)textFieldDidChange:(UITextField*)textField
{
    NSString *searchString = [self.txtSearchContacts.text stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
    if ([searchString length] > 0) {
        self.isSearching = true;
        [self filterContentForSearchText:searchString scope:@""];
    }
    else {
        self.isSearching = false;
    }
    [self.inviteFriendsTableView reloadData];
}

#pragma mark - CZPickerView Delegate and Datasource

- (NSAttributedString *)czpickerView:(CZPickerView *)pickerView
               attributedTitleForRow:(NSInteger)row{
    
    NSAttributedString *att = [[NSAttributedString alloc]
                               initWithString:inviteFriendsModel.arrPhoneNumbers[row]
                               attributes:@{
                                            NSFontAttributeName:[UIFont fontWithName:@"Avenir-Light" size:18.0]
                                            }];
    return att;
}

- (NSString *)czpickerView:(CZPickerView *)pickerView
               titleForRow:(NSInteger)row{
    return inviteFriendsModel.arrPhoneNumbers[row];
}

- (NSInteger)numberOfRowsInPickerView:(CZPickerView *)pickerView{
    return [inviteFriendsModel.arrPhoneNumbers count];
}

- (void)czpickerViewDidClickCancelButton:(CZPickerView *)pickerView{
    inviteFriendsModel.isSelected = false;
    [self.inviteFriendsTableView reloadData];
}

- (void)czpickerView:(CZPickerView *)pickerView didConfirmWithItemAtRow:(NSInteger)row{
    
    NSLog(@"%ld is chosen!", (long)[inviteFriendsModel.arrPhoneNumbers[row] integerValue]);
    
    inviteFriendsModel.selectedPhoneNumber = inviteFriendsModel.arrPhoneNumbers[row];
    inviteFriendsModel.isSelected = true;
    [self.inviteFriendsTableView reloadData];
}

@end
