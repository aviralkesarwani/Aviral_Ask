//
//  AsksVC.m
//  My Asks
//
//  Created by artis on 30/03/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "AsksVC.h"
#import <QuickBlox/Quickblox.h>

@interface AsksVC (){
    AppDelegate *appDel;
    DCPathButton *dcPathButton;
}

@end

@implementation AsksVC

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    [[QBChat instance] addDelegate:self];
    CGFloat scrollViewWidth = [[UIScreen mainScreen ] bounds].size.width;
    CGFloat scrollViewHeight = [[UIScreen mainScreen ] bounds].size.height - 113;
    
    self.myAskTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 0,scrollViewWidth, scrollViewHeight)];
    self.allAskTableView = [[UITableView alloc] initWithFrame:CGRectMake(scrollViewWidth, 0,scrollViewWidth, scrollViewHeight)];
    self.friendsAskTableView = [[UITableView alloc] initWithFrame:CGRectMake(scrollViewWidth*2, 0,scrollViewWidth, scrollViewHeight)];
    
    [self.askScrollView addSubview:self.myAskTableView];
    [self.askScrollView addSubview:self.allAskTableView];
    [self.askScrollView addSubview:self.friendsAskTableView];
    
    self.askScrollView.contentSize = CGSizeMake(scrollViewWidth*3, scrollViewHeight);
    self.askScrollView.delegate = self;
    [self.askScrollView setPagingEnabled:YES];
    [self.askScrollView setScrollEnabled:YES];
    
    self.automaticallyAdjustsScrollViewInsets = NO;

    self.userAsksArray = [[NSMutableArray alloc] init];
    self.friendsAsksArray = [[NSMutableArray alloc] init];
    self.allAsksArray = [[NSMutableArray alloc] init];
    
    self.TopTabBarView = [[[NSBundle mainBundle] loadNibNamed:@"TabButtonView" owner:self options:nil] firstObject];
    [self.topTabBarContainView addSubview:self.TopTabBarView];
    
    [self.myAskTableView registerNib:[UINib nibWithNibName:@"MAAskTableViewCell" bundle:nil] forCellReuseIdentifier:@"MAAskTableViewCell"];
    [self.allAskTableView registerNib:[UINib nibWithNibName:@"MAAskTableViewCell" bundle:nil] forCellReuseIdentifier:@"MAAskTableViewCell"];
    [self.friendsAskTableView registerNib:[UINib nibWithNibName:@"MAAskTableViewCell" bundle:nil] forCellReuseIdentifier:@"MAAskTableViewCell"];

    self.myAskTableView.delegate = self;
    self.myAskTableView.dataSource = self;
    
    self.allAskTableView.delegate = self;
    self.allAskTableView.dataSource = self;

    self.friendsAskTableView.delegate = self;
    self.friendsAskTableView.dataSource = self;

    self.myAskTableView.rowHeight = UITableViewAutomaticDimension;
    self.myAskTableView.estimatedRowHeight = 81.0;

    self.allAskTableView.rowHeight = UITableViewAutomaticDimension;
    self.allAskTableView.estimatedRowHeight = 81.0;

    self.friendsAskTableView.rowHeight = UITableViewAutomaticDimension;
    self.friendsAskTableView.estimatedRowHeight = 81.0;
    
    self.myAskTableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    self.allAskTableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    self.friendsAskTableView.separatorStyle = UITableViewCellSeparatorStyleNone;

    self.searchAsksArray = [[NSMutableArray alloc] init];
    
    appDel = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    [appDel slideMenuOpenWithPan:NO];

}

- (void)configureDCPathButtonWithPoint : (CGPoint) point
{
    // Configure center button
    //
    dcPathButton = [[DCPathButton alloc]initWithCenterImage:[UIImage imageNamed:@""]
                                                         highlightedImage:[UIImage imageNamed:@""]];
    dcPathButton.delegate = self;
    
    // Configure item buttons
    //
    DCPathItemButton *itemButton_1 = [[DCPathItemButton alloc]initWithImage:[UIImage imageNamed:@"ic_edit"]
                                                           highlightedImage:[UIImage imageNamed:@"ic_edit"]
                                                            backgroundImage:[UIImage imageNamed:@"ic_edit"]
                                                 backgroundHighlightedImage:[UIImage imageNamed:@"ic_edit"]];
    
    DCPathItemButton *itemButton_2 = [[DCPathItemButton alloc]initWithImage:[UIImage imageNamed:@"ic_delete"]
                                                           highlightedImage:[UIImage imageNamed:@"ic_delete"]
                                                            backgroundImage:[UIImage imageNamed:@"ic_delete"]
                                                 backgroundHighlightedImage:[UIImage imageNamed:@"ic_delete"]];
    
    
    // Add the item button into the center button
    //
    [dcPathButton addPathItems:@[itemButton_1,
                                 itemButton_2
                                 ]];
    
    // Change the bloom radius, default is 105.0f
    //
    dcPathButton.bloomRadius = 40.0f;
    
    // Change the DCButton's center
    //
    dcPathButton.dcButtonCenter = CGPointMake(point.x, point.y);
    
    // Setting the DCButton appearance
    //
    dcPathButton.allowSounds = NO;
    dcPathButton.allowCenterButtonRotation = NO;
    
    dcPathButton.bottomViewColor = [UIColor grayColor];
    
    dcPathButton.bloomDirection = kDCPathButtonBloomDirectionRight;
    dcPathButton.dcButtonCenter = CGPointMake(point.x, point.y);
    dcPathButton.bottomViewColor = [UIColor whiteColor];
    
    [self.view addSubview:dcPathButton];
    
    [self.view bringSubviewToFront:dcPathButton];
    
    [dcPathButton centerButtonTapped];
    
}

- (void)viewDidAppear:(BOOL)animated {
    
    [self getChatDialogs];

    [appDel slideMenuOpenWithPan:NO];
    [self askPageControllerChanged:self];
}
/*
-(void)showAskDeatils:(NSArray *)resultData{
    
    for (NSDictionary *userAsks in resultData) {
        
        MAUserAsksModel *userAsksModel = [MAUserAsksModel new];
        
        userAsksModel.UserAsksID = [userAsks valueForKey:@"Id"];
        userAsksModel.UserId = [userAsks valueForKey:@"UserId"];
        userAsksModel.Question = [userAsks valueForKey:@"Question"];
        userAsksModel.TotalMessages =[NSString stringWithFormat:@"%ld",[[userAsks valueForKey:@"TotalMessages"] integerValue]] ;
        
        if ([[userAsks valueForKey:@"IsOwner"] intValue]==1)
            userAsksModel.IsOwner = YES;
        else
            userAsksModel.IsOwner = NO;
        
        userAsksModel.BackgroundImage = [userAsks valueForKey:@"BackgroundImageUrl"];
        
        userAsksModel.ChannelName = [userAsks valueForKey:@"ChannelName"];
        
        NSString *createdDateString = [userAsks valueForKey:@"CreatedDate"];
        
        userAsksModel.CreatedDate = [self getLocalDateStringFrmGMT:createdDateString];
        
        userAsksModel.InvitedContactsArray = [userAsks valueForKey:@"InvitedContacts"];
        userAsksModel.InvitedContactsCount =  [NSString stringWithFormat:@"you have asked %lu friends for recommendation",[[userAsks valueForKey:@"InvitedContacts"]count]];
        
        NSArray *askDetailsArray = [userAsks valueForKey:@"AskDetails"];
        
        NSMutableArray *askDetailsMutableArray = [[NSMutableArray alloc] init];
        
        for (NSDictionary *askDetail in askDetailsArray) {
            
            MAAskDetailsModel *askDetailModel = [MAAskDetailsModel new];
            askDetailModel.Name = [askDetail valueForKey:@"Name"];
            askDetailModel.AskDetailID = [askDetail valueForKey:@"Id"];
            askDetailModel.AskId = [askDetail valueForKey:@"AskId"];
            askDetailModel.Number = [askDetail valueForKey:@"Number"];
            askDetailModel.latitude = [askDetail valueForKey:@"Latitude"];
            askDetailModel.longitude = [askDetail valueForKey:@"Longitude"];
            askDetailModel.placeAddress = [askDetail valueForKey:@"Address"];
            askDetailModel.placeID = [askDetail valueForKey:@"PlaceId"];
            askDetailModel.placeName = [askDetail valueForKey:@"PlaceName"];
            askDetailModel.isFavorite =[[askDetail valueForKey:@"IsUserFavorite"]boolValue];
            //                  askDetailModel.Message = [askDetail valueForKey:@"Message"];
            //                  askDetailModel.Type = [askDetail valueForKey:@"Type"];
            //                  askDetailModel.MessageTime = [askDetail valueForKey:@"MessageTime"];
            [askDetailsMutableArray addObject:askDetailModel];
        }
        
        
        userAsksModel.AskDetailsArray = askDetailsMutableArray;
        NSLog(@"%@",[userAsks valueForKey:@"IsOwner"]);
        
        [self.allAsksArray addObject:userAsksModel];
        
        if ([[userAsks valueForKey:@"IsOwner"]boolValue]) {
            
            [self.userAsksArray addObject:userAsksModel];
        }
        else{
            [self.friendsAsksArray addObject:userAsksModel];
        }
    }
    
    [self.allAskTableView reloadData];
    [self.myAskTableView reloadData];
    [self.friendsAskTableView reloadData];
}
*/
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -

- (NSUInteger)quickBloxUserID {
    return [QBSession currentSession].currentUser.ID;
}

- (NSString *)quickBloxUserName {
    return [QBSession currentSession].currentUser.fullName;
}

-(void)getChatDialogs {
    
    QBResponsePage *page = [QBResponsePage responsePageWithLimit:100 skip:0];
    
    [QBRequest dialogsForPage:page extendedRequest:nil successBlock:^(QBResponse *response, NSArray *dialogObjects, NSSet *dialogsUsersIDs, QBResponsePage *page) {
        
        [self.allAsksArray removeAllObjects];
        [self.userAsksArray removeAllObjects];
        [self.friendsAsksArray removeAllObjects];
        
        self.allAsksArray = [dialogObjects mutableCopy];

        for (QBChatDialog *chatDialog in dialogObjects) {
            
            if (chatDialog.userID == [self quickBloxUserID]) {
                [self.userAsksArray addObject:chatDialog];
            }else{
                [self.friendsAsksArray addObject:chatDialog];
            }
        }
        
        [self.allAskTableView reloadData];
        [self.myAskTableView reloadData];
        [self.friendsAskTableView reloadData];
        
    } errorBlock:^(QBResponse *response) {
        
    }];
}

- (void)chatDidReceiveSystemMessage:(QBChatMessage *)message
{
    [self getChatDialogs];
}

#pragma mark - QBChatDelegate

- (void)chatRoomDidReceiveMessage:(QB_NONNULL QBChatMessage *)message fromDialogID:(QB_NONNULL NSString *)dialogID {
    
    [self getChatDialogs];
}

#pragma mark - UIScrollViewDelegate
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    if(scrollView == self.askScrollView){
        CGFloat pageWidth = CGRectGetWidth(scrollView.frame);
        CGFloat currentPage = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1;
        self.askPageController.currentPage = currentPage;
        if(currentPage == 0) {
            self.lblAskType.text = @"My Asks";
        }
        else if(currentPage == 1) {
            self.lblAskType.text = @"All Asks";
        }
        else {
            self.lblAskType.text = @"Asks from Friends";
        }
    }
    
}

#pragma mark - DCPathButton Delegate

- (void)willPresentDCPathButtonItems:(DCPathButton *)dcPathButton {
    
    NSLog(@"ItemButton will present");
    
}

- (void)didPresentDCPathButtonItems:(DCPathButton *)dcPathButton {
    
    NSLog(@"ItemButton did present");
    
}

- (void)pathButton:(DCPathButton *)pathButton clickItemButtonAtIndex:(NSUInteger)itemButtonIndex {
    NSLog(@"You tap %@ at index : %lu", pathButton, (unsigned long)itemButtonIndex);
    
    if(itemButtonIndex == 0) {
        
        NewAskScreenVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"NewAskScreenVC"];
        vc.isForEdit = true;
        [self.navigationController pushViewController:vc animated:YES];
    }
}

- (void)willDismissDCPathButtonItems:(DCPathButton *)dcPathButton {
    
    NSLog(@"ItemButton will dismiss");
}

- (void)didDismissDCPathButtonItems:(DCPathButton *)dcPathButton {
    
    NSLog(@"ItemButton did dismiss");
    
    [dcPathButton removeFromSuperview];
    
}


#pragma mark - SlideNavigationController Methods -

- (BOOL)slideNavigationControllerShouldDisplayLeftMenu
{
    return NO;
}

- (BOOL)slideNavigationControllerShouldDisplayRightMenu
{
    return YES;
}

#pragma mark - Button Actions

- (IBAction)btnMyAsksClicked:(id)sender {
    self.imgTabBackground.image = [UIImage imageNamed:@"Tab 1"];
    self.isFriendsAsk = NO;
    [self.tblAsk reloadData];
}

- (IBAction)btnFriendAsksClicked:(id)sender {
    self.imgTabBackground.image = [UIImage imageNamed:@"Tab 2"];
    self.isFriendsAsk = YES;
    [self.tblAsk reloadData];
}

- (IBAction)askPageControllerChanged:(id)sender {
    NSInteger page = self.askPageController.currentPage;
    CGRect frame = self.askScrollView.frame;
    frame.origin.x = frame.size.width * page;
    frame.origin.y = 0;
    [self.askScrollView scrollRectToVisible:frame animated:YES];
    
    if(page == 0) {
        self.lblAskType.text = @"My Asks";
    }
    else if(page == 1) {
        self.lblAskType.text = @"All Asks";
    }
    else {
        self.lblAskType.text = @"Asks from Friends";
    }

}

- (IBAction)btnCreateAskClicked:(id)sender {
    NewAskScreenVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"NewAskScreenVC"];
    [self.navigationController pushViewController:vc animated:YES];
}

#pragma mark - TableView Delegate / DataSource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if(tableView == self.allAskTableView){
        return self.allAsksArray.count;
    }
    else if(tableView == self.myAskTableView){
        return self.userAsksArray.count;
    }
    else if(tableView == self.friendsAskTableView){
        return self.friendsAsksArray.count;
    }
    
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    MAAskTableViewCell * askCell = [tableView dequeueReusableCellWithIdentifier:@"MAAskTableViewCell" forIndexPath: indexPath];

    
    QBChatDialog *chatDialog;
    
    if(tableView == self.allAskTableView){
        chatDialog = [self.allAsksArray objectAtIndex:indexPath.row];
    }
    else if(tableView == self.myAskTableView){
        chatDialog = [self.userAsksArray objectAtIndex:indexPath.row];
    }
    else if(tableView == self.friendsAskTableView){
        chatDialog = [self.friendsAsksArray objectAtIndex:indexPath.row];
    }
    
    askCell.lblAsk.text = chatDialog.name;
    
    NSInteger invitedContactsCount = chatDialog.occupantIDs.count-1;// remove self's count
    NSString *recomendString = @"";
    
    if (chatDialog.userID == [self quickBloxUserID]) {
        if(invitedContactsCount > 1) {
            
            recomendString = [NSString stringWithFormat:@"You asked %ld friends",(long)invitedContactsCount];
        }
        else {
            recomendString = [NSString stringWithFormat:@"You asked %ld friend",(long)invitedContactsCount];
        }
    }
    else{
        if(invitedContactsCount > 1) {
            
            recomendString = [NSString stringWithFormat:@"%@ asked %ld friends",chatDialog.data[@"dialogOwnerName"], (long)invitedContactsCount];
        }
        else {
            recomendString = [NSString stringWithFormat:@"%@ asked %ld friend",chatDialog.data[@"dialogOwnerName"], (long)invitedContactsCount];
        }
    }
    
    askCell.lblNoOfRecomendAsk.text = recomendString;
    
    
    
    if(chatDialog.unreadMessagesCount == 0) {
        askCell.btnTotalMessage.hidden = true;
        [askCell.btnTotalMessage setTitle:@"" forState:UIControlStateNormal];
    }
    else if(chatDialog.unreadMessagesCount == 1) {
        askCell.btnTotalMessage.hidden = false;

        [askCell.btnTotalMessage setTitle:@"New" forState:UIControlStateNormal];
    }
    else {
        askCell.btnTotalMessage.hidden = false;

        [askCell.btnTotalMessage setTitle:[NSString stringWithFormat:@"%ld New",chatDialog.unreadMessagesCount] forState:UIControlStateNormal];
    }
    
    askCell.lblDateTimeOfAsk.text = [[AppData sharedManager] getYesterdayOrDateStringFromDate:chatDialog.createdAt];

    
    UILongPressGestureRecognizer* longPress = [ [ UILongPressGestureRecognizer alloc ] initWithTarget:self action:@selector(longPressEvent:)];
    [longPress setDelegate:self];
    [askCell addGestureRecognizer:longPress];

    return  askCell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
//    MAUserAsksModel *askDetailModel;
//    if (self.isSearching == YES) {
//        askDetailModel = [self.searchAsksArray objectAtIndex:indexPath.row];
//    }
//    else {
//        if (self.isFriendsAsk) {
//            askDetailModel = [self.friendsAsksArray objectAtIndex:indexPath.row];
//        }
//        else{
//           askDetailModel = [self.userAsksArray objectAtIndex:indexPath.row];
//        }
//    }
    
    QBChatDialog *chatDialog;
    
    if(tableView == self.allAskTableView){
        chatDialog = [self.allAsksArray objectAtIndex:indexPath.row];
    }
    else if(tableView == self.myAskTableView){
        chatDialog = [self.userAsksArray objectAtIndex:indexPath.row];
    }
    else if(tableView == self.friendsAskTableView){
        chatDialog = [self.friendsAsksArray objectAtIndex:indexPath.row];
    }

    [chatDialog joinWithCompletionBlock:^(NSError * _Nullable error) {
        
        MAChatScreenVC *chatScreenVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MAChatScreenVC"];
        chatScreenVC.selectedChatDialod = chatDialog;
        [self.navigationController pushViewController:chatScreenVC animated:true];
        
    }];
    
}

//- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
//{
//    // Return NO if you do not want the specified item to be editable.
//    
//    MAUserAsksModel *userAsksModel;
//    
//    if(tableView == self.allAskTableView){
//        userAsksModel = [self.allAsksArray objectAtIndex:indexPath.row];
//    }
//    else if(tableView == self.myAskTableView){
//        userAsksModel = [self.userAsksArray objectAtIndex:indexPath.row];
//    }
//    else if(tableView == self.friendsAskTableView){
//        userAsksModel = [self.friendsAsksArray objectAtIndex:indexPath.row];
//    }
//    
//    if (userAsksModel.IsOwner)
//        return YES;
//    else
//    return NO;
//}
//
//- (nullable NSArray<UITableViewRowAction *> *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath{
//    
//    UITableViewRowAction *delete = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDestructive title:@"Delete" handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
//        NSLog(@"Delete Tapped");
//    }];
//    //delete.backgroundColor = [UIColor greenColor];
//    
//    UITableViewRowAction *edit = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleNormal title:@"Edit" handler:^(UITableViewRowAction * _Nonnull action, NSIndexPath * _Nonnull indexPath) {
//        NSLog(@"Edit Tapped");
//    }];
//    //edit.backgroundColor = [UIColor yellowColor];
//    return @[delete,edit];
//}


#pragma mark - SearchBar Delegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)text {
    if (text.length > 0) {
        self.isSearching = YES;
        [self filterContentForSearchText:text scope:@""];
    }
    else {
        self.isSearching = NO;
        [searchBar performSelector:@selector(resignFirstResponder) withObject:nil afterDelay:0];
    }
    [self.tblAsk reloadData];
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    if(searchBar.text.length == 0){
    }
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    self.isSearching = NO;
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"name contains[cd] %@",searchText];
    
    NSArray *filterArray;
    if (self.isFriendsAsk) {
        filterArray = [self.friendsAsksArray filteredArrayUsingPredicate:resultPredicate];
    }
    else {
        filterArray = [self.userAsksArray filteredArrayUsingPredicate:resultPredicate];
    }
    
    self.searchAsksArray = [NSMutableArray arrayWithArray:filterArray];
}


#pragma mark - Custom Methods

/*
- (void) getAskList {
    
//    [KVNProgress show];
    [AppData startNetworkIndicator];
    
    [[APIClient sharedManager] getUserAsks:[AppData sharedManager].userID onCompletion:^(NSArray *resultData, bool success) {
        [AppData stopNetworkIndicator];
        [self.userAsksArray removeAllObjects];
        [self.friendsAsksArray removeAllObjects];

        if (success == true) {
            NSLog(@"%@",resultData);
//            [KVNProgress dismiss];

            for (NSDictionary *userAsks in resultData) {
                
                MAUserAsksModel *userAsksModel = [MAUserAsksModel new];
                
                userAsksModel.UserAsksID = [userAsks valueForKey:@"Id"];
                userAsksModel.UserId = [userAsks valueForKey:@"UserId"];
                userAsksModel.Question = [userAsks valueForKey:@"Question"];
                userAsksModel.TotalMessages =[NSString stringWithFormat:@"%ld",[[userAsks valueForKey:@"TotalMessages"] integerValue]] ;
                
                if ([[userAsks valueForKey:@"IsOwner"] intValue]==1)
                    userAsksModel.IsOwner = YES;
                else
                    userAsksModel.IsOwner = NO;

                userAsksModel.BackgroundImage = [userAsks valueForKey:@"BackgroundImageUrl"];
                
                userAsksModel.ChannelName = [userAsks valueForKey:@"ChannelName"];
                
                NSString *createdDateString = [userAsks valueForKey:@"CreatedDate"];
                
                userAsksModel.CreatedDate = [self getLocalDateStringFrmGMT:createdDateString];
                
                userAsksModel.InvitedContactsArray = [userAsks valueForKey:@"InvitedContacts"];
                userAsksModel.InvitedContactsCount =  [NSString stringWithFormat:@"you have asked %lu friends for recommendation",[[userAsks valueForKey:@"InvitedContacts"]count]];
                
                NSArray *askDetailsArray = [userAsks valueForKey:@"AskDetails"];
                
                NSMutableArray *askDetailsMutableArray = [[NSMutableArray alloc] init];
                
                for (NSDictionary *askDetail in askDetailsArray) {
                    
                    MAAskDetailsModel *askDetailModel = [MAAskDetailsModel new];
                    askDetailModel.Name = [askDetail valueForKey:@"Name"];
                    askDetailModel.AskDetailID = [askDetail valueForKey:@"Id"];
                    askDetailModel.AskId = [askDetail valueForKey:@"AskId"];
                    askDetailModel.Number = [askDetail valueForKey:@"Number"];
                    askDetailModel.latitude = [askDetail valueForKey:@"Latitude"];
                    askDetailModel.longitude = [askDetail valueForKey:@"Longitude"];
                    askDetailModel.placeAddress = [askDetail valueForKey:@"PlaceAddress"];
                    askDetailModel.placeID = [askDetail valueForKey:@"PlaceId"];
                    askDetailModel.placeName = [askDetail valueForKey:@"PlaceName"];
                    askDetailModel.isFavorite =[askDetail valueForKey:@"IsUserFavorite"];
//                  askDetailModel.Message = [askDetail valueForKey:@"Message"];
//                  askDetailModel.Type = [askDetail valueForKey:@"Type"];
//                  askDetailModel.MessageTime = [askDetail valueForKey:@"MessageTime"];
                    [askDetailsMutableArray addObject:askDetailModel];
                }
                
            
                userAsksModel.AskDetailsArray = askDetailsMutableArray;
                NSLog(@"%@",[userAsks valueForKey:@"IsOwner"]);
                
                [self.allAsksArray addObject:userAsksModel];
                
                if ([[userAsks valueForKey:@"IsOwner"]boolValue]) {
                    
                    [self.userAsksArray addObject:userAsksModel];
                }
                else{
                    [self.friendsAsksArray addObject:userAsksModel];
                }
                
            }
            
            NSLog(@"%@",self.friendsAsksArray);
            
        }
        else {
            
            [KVNProgress showError];
            NSLog(@"Error");
        }
        
        [self.allAskTableView reloadData];
        [self.myAskTableView reloadData];
        [self.friendsAskTableView reloadData];

    }];
}
*/

- (BOOL) checkTodayDate : (NSDate *) createdDateWithTime {
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    
    NSDateComponents *createdDateComp = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:createdDateWithTime];
    
    NSDateComponents *curentDateComp = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:[NSDate date]];
    
    NSDate *todayDate = [calendar dateFromComponents:curentDateComp];
    
    NSDate *createdDate = [calendar dateFromComponents:createdDateComp];
    
    if ([todayDate compare:createdDate] == NSOrderedDescending) {
        return false;
    }
    else if ([todayDate compare:createdDate] == NSOrderedAscending) {
        return false;
    }
    else {
        return true;
    }
    return false;
}

- (NSString *) getLocalDateStringFrmGMT : (NSString *) createdDateString {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    NSDate *sourceDate = [formatter dateFromString:createdDateString];
    NSLog(@"%@",sourceDate);
    
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
    dateFormat.timeZone = [NSTimeZone systemTimeZone];
    [dateFormat setDateFormat:@"yyyy/MM/dd hh:mm a"];
    NSString* localTime = [dateFormat stringFromDate:sourceDate];
    NSLog(@"localTime:%@", localTime);
    return localTime;
}

- (NSString *) getDateStringFromDate : (NSDate *) createdDateWithTime {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *createdDateComp = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:createdDateWithTime];
    NSDate *createdDate = [calendar dateFromComponents:createdDateComp];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy/MM/dd";
    return [dateFormatter stringFromDate:createdDate];
}

- (NSString *) getTimeStringFromDate : (NSDate *) createdDateWithTime {
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *createdDateComp = [calendar components:NSCalendarUnitHour | NSCalendarUnitMinute fromDate:createdDateWithTime];
    NSDate *createdDate = [calendar dateFromComponents:createdDateComp];
    
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"hh:mm a";
    return [dateFormatter stringFromDate:createdDate];
}

#pragma mark - Gesture

- (BOOL)longPressEvent:(UILongPressGestureRecognizer *)gesture {

    if (gesture.state == UIGestureRecognizerStateEnded) {
        
        UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@""
                                                           delegate:self
                                                  cancelButtonTitle:@"Cancel"
                                             destructiveButtonTitle:nil
                                                  otherButtonTitles:@"Edit", @"Delete", nil];
        
        [sheet showInView:self.view];
        
        return YES;
    }
    return NO;
}


- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
     NewAskScreenVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"NewAskScreenVC"];
    
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        
        return;
    }
    
    switch (buttonIndex) {
        case 0:
        
            vc.isForEdit = true;
            [self.navigationController pushViewController:vc animated:YES];
            
            break;
            
        case 1:
        
            break;
    }
    
}


@end
