//
//  MAIntroScreenVC.m
//  My Asks
//
//  Created by artis on 18/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "MAIntroScreenVC.h"

@interface MAIntroScreenVC ()

@end

@implementation MAIntroScreenVC

#pragma Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
//    
//    imagesArray = [[NSMutableArray alloc]init];
//   
//    self.scrollImages.frame = CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height);
//    CGFloat scrollViewWidth = self.scrollImages.frame.size.width;
//    CGFloat scrollViewHeight = self.scrollImages.frame.size.height;
//    
//    UIImageView *img1 = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0,scrollViewWidth, scrollViewHeight)];
//    UIImageView *img2 = [[UIImageView alloc]initWithFrame:CGRectMake(scrollViewWidth, 0,scrollViewWidth, scrollViewHeight)];
//    UIImageView *img3 = [[UIImageView alloc]initWithFrame:CGRectMake(scrollViewWidth*2, 0,scrollViewWidth, scrollViewHeight)];
//    UIImageView *img4 = [[UIImageView alloc]initWithFrame:CGRectMake(scrollViewWidth*3, 0,scrollViewWidth, scrollViewHeight)];
//    
//    img1.image = [UIImage imageNamed:@"Slide 1"];
//    img2.image = [UIImage imageNamed:@"Slide 2"];
//    img3.image = [UIImage imageNamed:@"Slide 3"];
//    img4.image = [UIImage imageNamed:@"Slide 4"];
//    
//
//    [self.scrollImages addSubview:img1];
//    [self.scrollImages addSubview:img2];
//    [self.scrollImages addSubview:img3];
//    [self.scrollImages addSubview:img4];
//    
//    self.scrollImages.contentSize = CGSizeMake(self.scrollImages.frame.size.width*4, self.scrollImages.frame.size.height);
//    self.scrollImages.delegate = self;
//    [self.scrollImages setPagingEnabled:YES];
//    [self.scrollImages setScrollEnabled:YES];
//    
//    self.automaticallyAdjustsScrollViewInsets = NO;
   
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewWillAppear:(BOOL)animated{
    
    self.btnLetsStart.layer.cornerRadius = 5.0;
    self.btnLetsStart.clipsToBounds = YES;
    
}

#pragma mark - Methods
-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView{
    
    CGFloat pageWidth = CGRectGetWidth(scrollView.frame);
    CGFloat currentPage = floor((scrollView.contentOffset.x-pageWidth/2)/pageWidth)+1;
    self.pageControl.currentPage = currentPage;
    
}

#pragma mark - Actions

- (IBAction)changePageSlider:(id)sender {
//    int page = self.pageControl.currentPage;
//    CGRect frame = self.scrollImages.frame;
//    frame.origin.x = frame.size.width * page;
//    frame.origin.y = 0;
    //[self.scrollImages scrollRectToVisible:frame animated:YES];
}

- (IBAction)btnLetsStartClicked:(id)sender {
    
    MASignUpVC *VC = [self.storyboard instantiateViewControllerWithIdentifier:@"MASignUpVC"];
    [self.navigationController pushViewController:VC animated:YES];
}

@end
