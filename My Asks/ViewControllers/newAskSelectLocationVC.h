//
//  newAskSelectLocationVC.h
//  My Asks
//
//  Created by artis on 08/07/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MAInviteFriendsTableCell.h"
#import <PSLocation/PSlocation.h>
#import "AppData.h"
#import "MAPlacesMapVC.h"

@protocol SelectedLocationDelegate <NSObject>
-(void)selectedLocationDetails:(id)selectedlocationModal;
@end

@interface newAskSelectLocationVC : UIViewController<PSLocationManagerDelegate,UISearchBarDelegate>{
    NSMutableArray *placesSearchArray;
}

@property (strong, nonatomic) NSMutableArray *searchContactArray;

@property (assign, nonatomic) BOOL isSearching;

@property (strong, nonatomic) IBOutlet UITableView *tblLocation;
@property (weak, nonatomic) IBOutlet UISearchBar *searchBar;
@property (weak, nonatomic) IBOutlet UIButton *goBtn;

@property (strong, nonatomic)NSString *latitude;
@property (strong, nonatomic)NSString *longitude;

@property (strong, nonatomic)NSString *currentLatitude;
@property (strong, nonatomic)NSString *currentLongitude;

@property (strong, nonatomic)NSString *subLocality;
@property (assign ,nonatomic) BOOL loadFromGooglePlace;
@property (strong, nonatomic) PSLocationManager *pslocationManager;


@property(weak,nonatomic) id <SelectedLocationDelegate> delegate;
- (IBAction)btnCancelClicked:(id)sender;
- (IBAction)btnGoClicked:(id)sender;

@end
