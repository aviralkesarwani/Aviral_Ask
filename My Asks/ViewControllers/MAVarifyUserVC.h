//
//  MAVarifyUserVC.h
//  My Asks
//
//  Created by artis on 21/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <KVNProgress.h>
#import <IQKeyboardManager.h>
#import "APIClient.h"
#import "AppDelegate.h"
#import "MAInviteFriendsModel.h"
#import "MALoginWithFBVC.h"
#import "ValidateCodeVC.h"

@interface MAVarifyUserVC : UIViewController <UITextFieldDelegate, UITabBarControllerDelegate,UIAlertViewDelegate, ValidateCodeDelegate>
{
    CNContactStore *contactsStrore;
    NSMutableArray *contactDetails;
    MAInviteFriendsModel *inviteFriendsModel;
    
    NSMutableArray *ContactList;
}

@property (weak, nonatomic) IBOutlet UITextField *txt1;
@property (weak, nonatomic) IBOutlet UITextField *txt2;
@property (weak, nonatomic) IBOutlet UITextField *txt3;
@property (weak, nonatomic) IBOutlet UITextField *txt4;
@property (strong, nonatomic) IBOutlet UILabel *lblMobileNumber;

@property (weak, nonatomic) IBOutlet UILabel *lblVarificationText1;
@property (weak, nonatomic) IBOutlet UILabel *lblVarificationText2;
@property (weak, nonatomic) IBOutlet UILabel *lblValidationFail;


@property (nonatomic, retain)UITabBarController *tabBarController;
- (IBAction)btnBackClicked:(id)sender;

@end
