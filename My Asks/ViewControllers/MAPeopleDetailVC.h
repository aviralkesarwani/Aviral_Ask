//
//  MAPeopleDetailVC.h
//  My Asks
//
//  Created by MY PC on 4/22/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MAAsksTableCell.h"
#import "MAListsTableCell.h"
#import "MAListDetailVC.h"
#import "MASettingModelClass.h"
#import "MAAskDetailsModel.h"
#import <KVNProgress.h>
#import "MAChatScreenVC.h"

@interface MAPeopleDetailVC : UIViewController
{
    NSMutableArray *userAsksArray;
    NSMutableArray *userListsArray;
}
@property (weak, nonatomic) IBOutlet UIImageView *imgBackground;
@property (weak, nonatomic) IBOutlet UIImageView *imgProfile;
@property (weak, nonatomic) IBOutlet UILabel *lblPeopleName;
@property (weak, nonatomic) IBOutlet UILabel *lblAskCount;
@property (weak, nonatomic) IBOutlet UILabel *lblListCount;
@property (weak, nonatomic) IBOutlet UILabel *lblFriendCount;
@property (weak, nonatomic) IBOutlet UIView *topBarView;
@property (weak, nonatomic) IBOutlet UIImageView *imgTabBackground;
@property (weak, nonatomic) IBOutlet UIView *asksView;
@property (weak, nonatomic) IBOutlet UIView *listsView;
@property (weak, nonatomic) IBOutlet UITableView *tblAsks;
@property (weak, nonatomic) IBOutlet UITableView *tblLists;

@property (strong, nonatomic)NSString *friendID;
@property (strong, nonatomic) MASettingModelClass *settingsModel;

- (IBAction)btnBackClicked:(id)sender;
- (IBAction)btnListsClicked:(id)sender;
- (IBAction)btnAsksClicked:(id)sender;

@end
