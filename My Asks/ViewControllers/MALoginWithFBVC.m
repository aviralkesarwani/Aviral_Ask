//
//  MALoginWithFBVC.m
//  My Asks
//
//  Created by artis on 27/05/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "MALoginWithFBVC.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
@import CloudKit;

@interface MALoginWithFBVC ()

@end

@implementation MALoginWithFBVC

- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.param = [[NSDictionary alloc]init];
    self.automaticallyAdjustsScrollViewInsets = false;
    
    NSString* name = [NSString stringWithFormat:@"%@",UIDevice.currentDevice.name];
    self.txtUserName.text = name;
    self.name = name;
    self.email = @"";
    self.imgProfilePic.layer.cornerRadius = self.imgProfilePic.frame.size.width/2;;
    self.imgProfilePic.clipsToBounds = YES;
    
    CKContainer *container = [CKContainer defaultContainer];
    [container fetchUserRecordIDWithCompletionHandler:^(CKRecordID * _Nullable recordID, NSError * _Nullable error) {
        
        [container requestApplicationPermission:CKApplicationPermissionUserDiscoverability completionHandler:^(CKApplicationPermissionStatus applicationPermissionStatus, NSError * _Nullable error) {
            
            if (applicationPermissionStatus == CKApplicationPermissionStatusGranted) {
                
            [container discoverUserInfoWithUserRecordID:recordID completionHandler:^(CKDiscoveredUserInfo * _Nullable userInfo, NSError * _Nullable error) {
                
                if (error != Nil) {
                    
                    NSLog(@"%@ %@",userInfo.firstName,userInfo.lastName);
                    NSString* name = [NSString stringWithFormat:@"%@%@",userInfo.firstName,userInfo.lastName];
                    self.txtUserName.text = name;

                }
                else{
                    
                    NSLog(@"%@",recordID.recordName);
                    
                    NSLog(@"%@",UIDevice.currentDevice.name);
                    
                    // NSString* name = [nameFromDeviceName(UIDevice.currentDevice.name) componentsJoinedByString:@" "];
                    NSString* name = [NSString stringWithFormat:@"%@",UIDevice.currentDevice.name];
                    self.txtUserName.text = name;
                }
               
                }];
            }
        }];
        
    }];
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated{
    
    self.btnDone.layer.cornerRadius = 5.0;
    self.btnDone.clipsToBounds = YES;
}

// To get usename from device name

NSArray * nameFromDeviceName(NSString * deviceName)
{
    NSError * error;
//    static NSString * expression = (@"^(?:iPhone|phone|iPad|iPod)\\s+(?:de\\s+)?(?:[1-9]?S?\\s+)?|(\\S+?)(?:['']?s)?(?:\\s+(?:iPhone|phone|iPad|iPod)\\s+(?:[1-9]?S?\\s+)?)?$|(\\S+?)(?:['']?的)?(?:\\s*(?:iPhone|phone|iPad|iPod))?$|(\\S+)\\s+");
    
    static NSString * expression = (@"^(?:iPhone|phone|iPad|iPod)\\s+(?:de\\s+)?|"
                                    "(\\S+?)(?:['\"]?s)?(?:\\s+(?:iPhone|phone|iPad|iPod))?$|"
                                    "(\\S+?)(?:['’]?s)?(?:\\s+(?:iPhone|phone|iPad|iPod))?$|"
                                    "(\\S+?)(?:['’]?的)?(?:\\s*(?:iPhone|phone|iPad|iPod))?$|"
                                    "(\\S+)\\s+");

    
    static NSRange RangeNotFound = (NSRange){.location=NSNotFound, .length=0};
    NSRegularExpression * regex = [NSRegularExpression regularExpressionWithPattern:expression
                                                                            options:(NSRegularExpressionCaseInsensitive)
                                                                              error:&error];
    NSMutableArray * name = [NSMutableArray new];
    for (NSTextCheckingResult * result in [regex matchesInString:deviceName
                                                         options:0
                                                           range:NSMakeRange(0, deviceName.length)]) {
        for (int i = 1; i < result.numberOfRanges; i++) {
            if (! NSEqualRanges([result rangeAtIndex:i], RangeNotFound)) {
                [name addObject:[deviceName substringWithRange:[result rangeAtIndex:i]].capitalizedString];
            }
        }
    }
    return name;
}

- (IBAction)btnFBLoginClicked:(id)sender {
    
    FBSDKLoginManager *login = [[FBSDKLoginManager alloc] init];
    login.loginBehavior = FBSDKLoginBehaviorWeb;
    
    [login
     logInWithReadPermissions: @[@"public_profile",@"email"]
     fromViewController:self
     handler:^(FBSDKLoginManagerLoginResult *result, NSError *error) {
         if (error) {
             NSLog(@"Process error");
             
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
                                                             message:@"Failed to login with Facebook"
                                                            delegate:self
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
             [alert show];

         } else if (result.isCancelled) {
             NSLog(@"Cancelled");
             
             UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
                                                             message:@"Facebook login cancelled"
                                                            delegate:self
                                                   cancelButtonTitle:@"OK"
                                                   otherButtonTitles:nil];
             [alert show];
             
         } else {
             NSLog(@"Logged in");
             
             [KVNProgress show];
             
             FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                           initWithGraphPath:@"/me"
                                           parameters:@{ @"fields": @"id,name,email,picture{height,width,is_silhouette,url},gender",}
                                           HTTPMethod:@"GET"];
             [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
                 [KVNProgress dismiss];
                 NSLog(@"%@",error);
                 NSLog(@"%@",result);
                 
                 if (error == (id)[NSNull null]) {
                     
                     
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
                                                                     message:@"Facebook data not available"
                                                                    delegate:self
                                                           cancelButtonTitle:@"OK"
                                                           otherButtonTitles:nil];
                     [alert show];

                     
                 }
                 else{
                     self.btnFBLogin.hidden = true;
                     NSMutableDictionary *dic = [result mutableCopy];
                     [self updateProfilePicture:dic];
                 }
                 
             }];
         }
     }];
}

- (void) updateProfilePicture:(NSMutableDictionary *)fbData{
    
    self.email =[fbData valueForKey:@"email"];
    self.gender = [fbData valueForKey:@"gender"];
    self.name = [fbData valueForKey:@"name"];
    
    UIImage *img = [[UIImage alloc]init];
    
    if ( self.email.length == 0 ||  self.email == (id)[NSNull null]) {
        self. email = @"";
    }
    else if ( self.gender.length == 0 ||  self.gender == (id)[NSNull null]) {
         self.gender=@"";
    }
    else if ( self.name.length == 0 ||  self.name == (id)[NSNull null]) {
         self.name= @"";
    }
    
    self.txtUserName.text = self.name;
    
    if ([fbData valueForKeyPath:@"picture.data"] == (id)[NSNull null]) {
        
         self.profileImage = @"";
    }
    else{
          self.profileImage = [fbData valueForKeyPath:@"picture.data.url"];
    }
    
    if ([ self.profileImage  isEqualToString: @""] ||  self.profileImage.length == 0) {
        
        img = [UIImage imageNamed:@"Photo"];
    }
    else{
        
        NSURL *url = [NSURL URLWithString: self.profileImage];
        NSData *data = [NSData dataWithContentsOfURL:url];
        img = [[UIImage alloc] initWithData:data];
    }
    
    self.imgProfilePic.image  = [[AppData sharedManager]compressImage:img];
}

-(void)btnDoneClicked:(id)sender{
    [self updateUserData];
}
- (void) updateUserData {
    
    if (self.txtUserName.text == nil || self.txtUserName.text.length == 0)  {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
                                                        message:@"Please enter username"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    else{
        
        [[AppData sharedManager] setDeviceName:self.name];
        
        if (self.imgProfilePic.image != nil) {
            [[AppData sharedManager] saveUserProfileImage: self.imgProfilePic.image];
            [[APIClient sharedManager] saveASKBackgoundImage: self.imgProfilePic.image onCompletion:^(NSDictionary *resultData, bool success) {
                if (success) {
                    
                    [KVNProgress show];
                    
                    NSLog(@"%@",[[AppData sharedManager] getCurrentUserID]);
                    NSString *imageName = [resultData valueForKey:@"Request_ID"];
                    
                    self.param= @{@"ProfilePicture":imageName,@"Id": [[AppData sharedManager] getCurrentUserID],@"Name": self.name,@"Email":  self.email};
                    
                    [AppData startNetworkIndicator];
                    
                    [[APIClient sharedManager] updateUserProfile: self.param onCompletion:^(NSDictionary *resultData, bool success) {
                        
                        [KVNProgress dismiss];
                        
                        [AppData stopNetworkIndicator];
                        [self redirectTo];
                    }];
                }
            }];
        }
        else {
            [KVNProgress show];
            
            self.param= @{@"ProfilePicture":@"",@"Id": [[AppData sharedManager] getCurrentUserID],@"Name": self.name,@"Email":  self.email};
            
            [AppData startNetworkIndicator];
            
            [[APIClient sharedManager] updateUserProfile: self.param onCompletion:^(NSDictionary *resultData, bool success) {
                
                [KVNProgress dismiss];
                
                [AppData stopNetworkIndicator];
                [self redirectTo];
                
                //[self setTabBar];
            }];
        }
    }
}

- (void) setTabBar {
    AppDelegate *appdelegte =(AppDelegate*)[[UIApplication sharedApplication]delegate];
    appdelegte.window.rootViewController = appdelegte.container;
    [appdelegte.window makeKeyAndVisible];
}

- (void) redirectTo {
    FAGoToScreenVC *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"FAGoToScreenVC"];
    vc.user = [NSString stringWithFormat:@"Welcome %@!",self.txtUserName.text];
    [self.navigationController pushViewController:vc animated:YES];
}

- (IBAction)btnProfilePicClicked:(id)sender {
    
    UIImagePickerController *picker = [[UIImagePickerController alloc] init];
    picker.delegate = self;
    picker.allowsEditing = YES;
    picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    
    [self presentViewController:picker animated:YES completion:NULL];

}

#pragma mark - image picker Delagate/Datasource

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    
    UIImage *chosenImage = info[UIImagePickerControllerEditedImage];
    self.imgProfilePic.image = chosenImage;
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void) updateProfilePicture {
    
    [KVNProgress show];
    UIImage *compressedImage  = [[AppData sharedManager]compressImage:self.imgProfilePic.image];
    [[AppData sharedManager] saveUserProfileImage:compressedImage];
    [[APIClient sharedManager] saveASKBackgoundImage:compressedImage onCompletion:^(NSDictionary *resultData, bool success) {
        [KVNProgress dismiss];
        if (success) {
            NSString *imageName = [resultData valueForKey:@"Request_ID"];
            self.param = @{@"ProfilePicture":imageName,@"Id": [[AppData sharedManager] getCurrentUserID]};
        
        }
    }];
}


@end
