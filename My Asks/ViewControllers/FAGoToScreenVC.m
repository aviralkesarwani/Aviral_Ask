//
//  FAGoToScreenVC.m
//  My Asks
//
//  Created by MAC on 22/06/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "FAGoToScreenVC.h"

@interface FAGoToScreenVC ()

@end

@implementation FAGoToScreenVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [AppData configurePushNotifications];
    
    self.lblUserName.text = self.user;
    
    self.btnPlaces.layer.cornerRadius = 5.0;
    self.btnPlaces.clipsToBounds = YES;
    
    self.btnAskFriends.layer.cornerRadius = 5.0;
    self.btnAskFriends.clipsToBounds = YES;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnStartPlaces:(id)sender {
    [self setTabBar];
}

- (IBAction)btnAskFriends:(id)sender {
    AppDelegate *appdelegte =(AppDelegate*)[[UIApplication sharedApplication]delegate];
    UITabBarController *tabBarController = appdelegte.container.centerViewController;
    [tabBarController setSelectedIndex:2];
    appdelegte.window.rootViewController = appdelegte.container;
    [appdelegte.window makeKeyAndVisible];
}

- (void) setTabBar {
    AppDelegate *appdelegte =(AppDelegate*)[[UIApplication sharedApplication]delegate];
    appdelegte.window.rootViewController = appdelegte.container;
    [appdelegte.window makeKeyAndVisible];
}

@end
