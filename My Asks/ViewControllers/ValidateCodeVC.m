//
//  ValidateCodeVC.m
//  My Asks
//
//  Created by MAC on 08/06/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "ValidateCodeVC.h"
#import <QuickBlox/Quickblox.h>
#import <QMServices/QMServices.h>
#import <KVNProgress.h>

@interface ValidateCodeVC ()

@end

@implementation ValidateCodeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self checkValidationCode];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

#pragma mark - Custom Methods

- (void) checkValidationCode {

    [AppData startNetworkIndicator];
    [KVNProgress show];

        [[APIClient sharedManager] VerifyUser:self.validateCode onCompletion:^(NSDictionary *resultData, bool success) {

            if ([[resultData valueForKey:@"Success"]boolValue]) {
    
                [[AppData sharedManager] setQbUserName:[resultData valueForKey:MAQBUserName]];
                [[AppData sharedManager] setQbPassword:[resultData valueForKey:MAQBPassword]];
                
                // QuickBlox Login
                QBUUser *loginUser = [QBUUser user];
                
                loginUser.login = [resultData valueForKey:MAQBUserName];
                loginUser.password = [resultData valueForKey:MAQBPassword];
                
                QMServicesManager *servicesManagerObject = [QMServicesManager instance];
                
                [servicesManagerObject logInWithUser:loginUser completion:^(BOOL success, NSString *errorMessage) {
                    
                    [AppData stopNetworkIndicator];
                    [KVNProgress dismiss];
                    
                    if (success) {
                        
                        NSDictionary *userData = @{MAUserID:[AppData sharedManager].userID,MAUserMobileNumber:[AppData sharedManager].userMobileNumber,MAUserCoutryCode:[AppData sharedManager].userCountryCode};
                                                
                        [[AppData sharedManager] saveUserInformation:userData];
                        
                        [[ChatClient sharedManager] setUpPubnubClientWithConfiguration];
                        
                        [self.delegate codeVarifiedSuccessfully];
                        [self dismissViewControllerAnimated:true completion:nil];
                        
                    }else {
                        [self.delegate codeVarificationFailed];
                        [self dismissViewControllerAnimated:true completion:nil];
                    }
                }];
            }
            else{
                [KVNProgress dismiss];
                [self.delegate codeVarificationFailed];
                [self dismissViewControllerAnimated:true completion:nil];
            }
        }];
}

@end
