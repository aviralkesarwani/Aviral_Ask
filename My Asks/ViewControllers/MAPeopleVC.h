//
//  MAPeopleVC.h
//  My Asks
//
//  Created by artis on 18/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <KVNProgress.h>
#import "MAPeopletableCell.h"
#import "MAPeopleInviteFriendsVC.h"
#import "MAPeopleDetailVC.h"
#import "APIClient.h"
#import "MAFriendListModel.h"

@interface MAPeopleVC : UIViewController<UITableViewDataSource,UITableViewDelegate,UISearchBarDelegate>
{
    MAFriendListModel *friendModel;
    
    CNContactStore *contactsStrore;
    NSMutableArray *contactDetails;
    MAInviteFriendsModel *inviteFriendsModel;
    NSMutableArray *ContactList;
}

#pragma mark - Outlets
@property (weak, nonatomic) IBOutlet UITableView *tblFriendsList;
@property (weak, nonatomic) IBOutlet UISearchBar *friendsSearchBar;

#pragma mark - Variables
@property (assign, nonatomic) BOOL isSearching;
@property (strong, nonatomic) NSMutableArray *searchContactArray;
@property (strong, nonatomic)NSMutableArray *friendList;

@property (strong, nonatomic) UIRefreshControl *refreshControl;

#pragma mark - Methods

- (IBAction)btnBackClicked:(id)sender;
- (IBAction)btnInviteFriendsClicked:(id)sender;

@end
