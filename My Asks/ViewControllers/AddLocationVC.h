//
//  AddLocationVC.h
//  My Asks
//
//  Created by artis on 31/03/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <GoogleMaps/GoogleMaps.h>
#import <MapKit/MapKit.h>
#import <CoreLocation/CoreLocation.h>
#import "MVPlaceSearchTextField.h"
#import "MALocationModel.h"

@protocol MAAddLocationsDelegate <NSObject>

@required
- (void)addLocations:(NSMutableArray *)locationsArray;
@end

@interface AddLocationVC : UIViewController <UITextFieldDelegate ,CLLocationManagerDelegate ,UITableViewDataSource , UITableViewDelegate,UIGestureRecognizerDelegate,MKMapViewDelegate,UIAlertViewDelegate,PlaceSearchTextFieldDelegate,PlaceSearchTextFieldDelegate,GMSMapViewDelegate>
{
    NSMutableArray *locationDetails;
    MALocationModel *addLocationVC;
}

@property (weak, nonatomic) IBOutlet GMSMapView *googleMapView;
@property (weak, nonatomic) IBOutlet MVPlaceSearchTextField *placeSearchTextField;
@property (weak, nonatomic) IBOutlet UITableView *locationTableVIew;

@property (weak, nonatomic) IBOutlet UIView *autoComplete;


@property (nonatomic, assign) id<MAAddLocationsDelegate> delegate;

- (IBAction)btnAddLocationCllicked:(id)sender;
- (IBAction)btnBackClicked:(id)sender;
@end
