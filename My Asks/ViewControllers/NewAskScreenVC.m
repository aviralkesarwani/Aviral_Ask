//
//  NewAskScreenVC.m
//  My Asks
//
//  Created by MAC on 08/07/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "NewAskScreenVC.h"
#import "MAFriendListModel.h"
#import <QuickBlox/Quickblox.h>

@interface NewAskScreenVC ()

@property (weak, nonatomic) IBOutlet UITextView *txtAskDetails;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectFriends;
@property (weak, nonatomic) IBOutlet UIButton *btn_save;

@property (weak, nonatomic) IBOutlet UIButton *btnCreateAsk;
@property (weak, nonatomic) IBOutlet UIView *editView;
@property (weak, nonatomic) IBOutlet UITableView *tblSelectFriend;
@property (weak, nonatomic) IBOutlet UILabel *lblAskTitle;

- (IBAction)btnDeleteQuestionClicked:(id)sender;
- (IBAction)btnSaveQuestionClicked:(id)sender;
- (IBAction)btnCreateAskClicked:(id)sender;
- (IBAction)btnSelectFriendsClicked:(id)sender;

@end


@implementation NewAskScreenVC

- (void)viewDidLoad {
    [super viewDidLoad];

    contactsStrore = [[CNContactStore alloc] init];

    self.tblSelectFriend.delegate = self;
    self.tblSelectFriend.dataSource = self;
    self.txtAskDetails.delegate = self;
    
    if(self.isForEdit) {
        
        self.btn_save.hidden = NO;
        self.editView.hidden = false;
        self.txtAskDetails.text = self.selectedChatDialod.name;
        [self.tblSelectFriend reloadData];

       /* for (NSString* contactNumber in self.addedContacts) {
            
            NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"ContactNumber == %@",contactNumber];
            NSArray *filterArray = [[AppData sharedManager].invitedFriendsGlobalArray filteredArrayUsingPredicate:resultPredicate];
            
            if (filterArray.count > 0) {
                [self.contactsArray addObjectsFromArray:filterArray];
            }
        }*/
        
    }
    else {
        self.btn_save.hidden = YES;
        self.contactsArray = [[NSMutableArray alloc]init];
        self.editView.hidden = true;
    }
    
    self.btnCreateAsk.enabled = false;
}
- (void)viewWillAppear:(BOOL)animated
{
    [self UIChanges];
}

-(void)UIChanges
{
    //...Hanlding the ui and disable the tableview and button interaction
    [self.txtAskDetails.layer setCornerRadius:6.5f];
    [self.btnSelectFriends.layer setCornerRadius:5.0f];
    if ([self.contactsArray count] == 0 ) {
        self.btnCreateAsk.alpha  = 0.0;
        self.tblSelectFriend.hidden = YES;
    }
    else{
        self.btnCreateAsk.alpha  = 1.0;
        self.tblSelectFriend.hidden = NO;
    }
    //...
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)btnCancelClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:true];
}

- (IBAction)btnDeleteQuestionClicked:(id)sender {

    [KVNProgress show];
    [QBRequest deleteDialogsWithIDs:[NSSet setWithObject:self.selectedChatDialod.ID] forAllUsers:NO
                       successBlock:^(QBResponse *response, NSArray *deletedObjectsIDs, NSArray *notFoundObjectsIDs, NSArray *wrongPermissionsObjectsIDs) {
                           
                           [KVNProgress dismiss];

                           NSArray* vcArray = self.navigationController.viewControllers;
                           if (vcArray.count >= 3 ) {
                               [self.navigationController popToViewController:vcArray[vcArray.count - 3] animated:true];
                           }
                           
                       } errorBlock:^(QBResponse *response) {
                           [KVNProgress dismiss];

                       }];
}

- (IBAction)btnSaveQuestionClicked:(id)sender {

    NSMutableDictionary *paramDict = [NSMutableDictionary new];
    
    NSMutableArray *phoneContactsArray = [NSMutableArray new];
    
    for (MAFriendListModel *mdl in self.contactsArray) {
        
        [phoneContactsArray addObject:@{@"QuickbloxUserName" : mdl.ContactNumber, @"QuickbloxUserNickName" : mdl.ContactName}];
    }
    
    [paramDict setObject:self.txtAskDetails.text forKey:@"Question"];
    [paramDict setObject:[AppData sharedManager].userID forKey:@"UserId"];
    [paramDict setObject:phoneContactsArray forKey:@"InvitedContacts"];
    [paramDict setObject:self.selectedChatDialod.data[@"askId"] forKey:@"Id"];

    [KVNProgress show];
    [[APIClient sharedManager] updateAsk:paramDict onCompletion:^(NSDictionary *resultData, bool success) {
        
        if (success) {

            NSMutableArray *previousePeoples = [NSMutableArray new];
            for (NSNumber *ids in self.selectedChatDialod.occupantIDs) {
                if ([ids integerValue] != self.selectedChatDialod.userID) {
                    [previousePeoples addObject:[NSString stringWithFormat:@"%ld",[ids integerValue]]];
                }
            }
            
            NSMutableArray *allSelectedPeoples = [NSMutableArray new];
            
            for (NSDictionary *dict in resultData[@"InvitedContacts"]) {
                [allSelectedPeoples addObject:dict[@"QuickbloxUserId"]];
            }
            
            NSMutableArray *newSelectedPeoples = [NSMutableArray new];
            
            for (NSString *ID in allSelectedPeoples) {
                
                if (![previousePeoples containsObject:ID]) {
                    [newSelectedPeoples addObject:ID];
                }
            }
            
            NSMutableArray *removedPeoples = [NSMutableArray new];
            
            for (NSString *ID in previousePeoples) {
                
                if (![allSelectedPeoples containsObject:ID]) {
                    [removedPeoples addObject:ID];
                }
            }
            
            QBChatDialog *chatDialog = [[QBChatDialog alloc] initWithDialogID:self.selectedChatDialod.ID type:QBChatDialogTypeGroup];
            chatDialog.name = self.txtAskDetails.text;
            chatDialog.pushOccupantsIDs = newSelectedPeoples;
            chatDialog.pullOccupantsIDs = removedPeoples;
            chatDialog.data = @{ @"class_name": @"AskDetail" ,@"dialogOwnerName" : [[AppData sharedManager] getDeviceName],@"askId" : resultData[@"Id"]};
            
            [QBRequest updateDialog:chatDialog successBlock:^(QBResponse *responce, QBChatDialog *dialog) {
                
                //
                [self notifiyAllUsers:dialog];

                [KVNProgress dismiss];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
                                                                message:@"Updated Ask"
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
                
                NSArray* vcArray = self.navigationController.viewControllers;
                if (vcArray.count >= 3 ) {
                    [self.navigationController popToViewController:vcArray[vcArray.count - 3] animated:true];
                }

            } errorBlock:^(QBResponse *response) {
                
                [KVNProgress dismiss];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
                                                                message:@"Unable to Update Ask"
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }];
            
        } else{
                [KVNProgress dismiss];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
                                                                message:@"Unable to Update Ask"
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
        }];
}

- (IBAction)btnCreateAskClicked:(id)sender {
    
    NSMutableDictionary *paramDict = [NSMutableDictionary new];
    
    NSMutableArray *phoneContactsArray = [NSMutableArray new];
    
    for (MAFriendListModel *mdl in self.contactsArray) {
        
        [phoneContactsArray addObject:@{@"QuickbloxUserName" : mdl.ContactNumber, @"QuickbloxUserNickName" : mdl.ContactName}];
    }
    
    [paramDict setObject:self.txtAskDetails.text forKey:@"Question"];
    [paramDict setObject:[AppData sharedManager].userID forKey:@"UserId"];
    [paramDict setObject:phoneContactsArray forKey:@"InvitedContacts"];
    [paramDict setObject:@[] forKey:@"AskDetails"];

    [KVNProgress show];
    [[APIClient sharedManager] createAsk:paramDict onCompletion:^(NSDictionary *resultData, bool success) {
        
        if (success) {
            
            NSMutableArray *quickBloxUserIDsArray = [NSMutableArray new];
            
            for (NSDictionary *dict in resultData[@"InvitedContacts"]) {
                [quickBloxUserIDsArray addObject:dict[@"QuickbloxUserId"]];
            }
            
            QBChatDialog *chatDialog = [[QBChatDialog alloc] initWithDialogID:nil type:QBChatDialogTypeGroup];
            chatDialog.name = self.txtAskDetails.text;
            chatDialog.occupantIDs = quickBloxUserIDsArray;
            chatDialog.data = @{ @"class_name": @"AskDetail" ,@"dialogOwnerName" : [[AppData sharedManager] getDeviceName],@"askId" : resultData[@"Id"]};
            [QBRequest createDialog:chatDialog successBlock:^(QBResponse *response, QBChatDialog *createdDialog) {
                
                //
                [self notifiyAllUsers:createdDialog];
                
                [[APIClient sharedManager] updateQBChatDialogId:@{ @"askId" : resultData[@"Id"] , @"dialog" : createdDialog.ID} onCompletion:^(NSDictionary *resultData, bool success) {
                    
                    [KVNProgress dismiss];

                    if (success) {
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
                                                                        message:@"Ask created"
                                                                       delegate:nil
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                        [alert show];
                        
                        [self.navigationController popViewControllerAnimated:true];

                    }else{
                        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
                                                                        message:@"Unable to create Ask"
                                                                       delegate:nil
                                                              cancelButtonTitle:@"OK"
                                                              otherButtonTitles:nil];
                        [alert show];
                    }
                }];
                
            } errorBlock:^(QBResponse *response) {
                [KVNProgress dismiss];

                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
                                                                message:@"Unable to create Ask"
                                                               delegate:nil
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }];
           
        } else{
            [KVNProgress dismiss];
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
                                                            message:@"Unable to create Ask"
                                                           delegate:nil
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
        }
    }];
}

- (IBAction)btnSelectFriendsClicked:(id)sender {

    if ([[NSUserDefaults standardUserDefaults] valueForKey:@"OneTimeContactUploaded"] == nil) {
        ContactAccess *ContactAccessViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactAccess"];
        ContactAccessViewController.delegate = self;
        [self presentViewController:ContactAccessViewController animated:YES completion:nil];
    }else{
        [self requestContactsAccessWithHandler:^(BOOL grandted) {
            
            if (grandted) {
                [self checkContactsAccess];
            }else{
                ContactAccess *ContactAccessViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactAccess"];
                ContactAccessViewController.delegate = self;
                [self presentViewController:ContactAccessViewController animated:YES completion:nil];
            }
        }];
    }
}

-(void)contactAccessResult :(BOOL)result{
    if (result) {
        [self checkContactsAccess];
    }
    else{
        SelectingFriendsVC *contributeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectingFriendsVC"];
        contributeViewController.contactsAccessed = NO;
        contributeViewController.delegate = self;
        UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:contributeViewController];
        nav.navigationBarHidden = YES;
        [self presentViewController:nav animated:YES completion:nil];
    }
}

-(void)btnRemoveContact:(UIButton *)sender{
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:[sender tag] inSection:0];
    
    [self.contactsArray removeObjectAtIndex:sender.tag];
    
    [self.tblSelectFriend deleteRowsAtIndexPaths: [NSArray arrayWithObject:indexPath] withRowAnimation:UITableViewRowAnimationFade];
    
    [self.tblSelectFriend reloadData];
    
    if ([self.contactsArray count] == 0 ) {
        [self.btnSelectFriends setTitle:@"Select Friends to Ask" forState:UIControlStateNormal];
        self.btnCreateAsk.enabled = false;
        [self UIChanges];
    }
    else{
        [self.btnSelectFriends setTitle:@"Select More Friends to Ask" forState:UIControlStateNormal];
    }
}

#pragma mark - Sent notification to Group users after group created

-(void)notifiyAllUsers:(QBChatDialog*)dialog {
    
    for (NSString *occupantID in dialog.occupantIDs) {
        
        QBChatMessage *inviteMessage = [self createChatNotificationForGroupChatCreation:dialog];
        
        NSTimeInterval timestamp = (unsigned long)[[NSDate date] timeIntervalSince1970];
        inviteMessage.customParameters[@"date_sent"] = @(timestamp);
        
        // send notification
        //
        inviteMessage.recipientID = [occupantID integerValue];
        
        [[QBChat instance] sendSystemMessage:inviteMessage completion:^(NSError * _Nullable error) {
            
        }];
    }
}


- (QBChatMessage *)createChatNotificationForGroupChatCreation:(QBChatDialog *)dialog
{
    // create message:
    QBChatMessage *inviteMessage = [QBChatMessage message];
    
//    NSMutableDictionary *customParams = [NSMutableDictionary new];
//    customParams[@"xmpp_room_jid"] = dialog.roomJID;
//    customParams[@"name"] = dialog.name;
//    customParams[@"_id"] = dialog.ID;
//    customParams[@"type"] = @(dialog.type);
//    customParams[@"occupants_ids"] = [dialog.occupantIDs componentsJoinedByString:@","];
//    
//    
//    // Add notification_type=1 to extra params when you created a group chat
//    //
//    customParams[@"notification_type"] = @"1";
    
    NSMutableDictionary *customParams = [NSMutableDictionary new];
    customParams[@"xmpp_room_jid"] = dialog.roomJID;
    customParams[@"room_name"] = dialog.name;
    customParams[@"dialog_id"] = dialog.ID;
    customParams[@"type"] = @(dialog.type);
    customParams[@"current_occupant_ids"] = [dialog.occupantIDs componentsJoinedByString:@","];
    
    // Add notification_type=1 to extra params when you created a group chat
    //
    customParams[@"notification_type"] = @"1";
    
    
    inviteMessage.customParameters = customParams;
    
    return inviteMessage;
}

#pragma mark -

- (NSUInteger)quickBloxUserID {
    return [QBSession currentSession].currentUser.ID;
}

- (NSString *)quickBloxUserName {
    return [QBSession currentSession].currentUser.fullName;
}

#pragma mark - Contact

-(void)checkContactsAccess{

    [self requestContactsAccessWithHandler:^(BOOL grandted) {
        if (grandted) {
            SelectingFriendsVC *contributeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectingFriendsVC"];
            contributeViewController.contactsAccessed = YES;
            contributeViewController.delegate = self;
            if ([self.contactsArray count] == 0) {
                [[AppData sharedManager].invitedFriendsGlobalArray removeAllObjects];
            }
            contributeViewController.selectedContactsArray = self.contactsArray;
            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:contributeViewController];
            nav.navigationBarHidden = YES;
            [self presentViewController:nav animated:YES completion:nil];        }
        else{
            SelectingFriendsVC *contributeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"SelectingFriendsVC"];
            contributeViewController.contactsAccessed = NO;
            contributeViewController.delegate = self;
            if ([self.contactsArray count] == 0) {
                [[AppData sharedManager].invitedFriendsGlobalArray removeAllObjects];
            }
            contributeViewController.selectedContactsArray = self.contactsArray;

            UINavigationController *nav = [[UINavigationController alloc] initWithRootViewController:contributeViewController];
            nav.navigationBarHidden = YES;
            [self presentViewController:nav animated:YES completion:nil];
        }
    }];
    
}

-(void)requestContactsAccessWithHandler:(void (^)(BOOL grandted))handler{
    
    switch ([CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts]) {
        case CNAuthorizationStatusAuthorized:{
            handler(YES);
            break;
        }
        case CNAuthorizationStatusDenied:
        case CNAuthorizationStatusNotDetermined:{
            [contactsStrore requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
                handler(granted);
            }];
            break;
        }
        case CNAuthorizationStatusRestricted:
            handler(NO);
            break;
    }
}


#pragma mark - UITextView Delegate

- (void)textViewDidBeginEditing:(UITextView *)textView {
    if([textView.text  isEqual: @"Easily ask your friends about a place, where everyone wants to go for lunch, Ask anything you want. It’s fast and easy."]) {
        textView.text = @"";
    }
}

#pragma mark - InviteFriends button clicked delagate

-(void)inviteFriends:(NSMutableArray *)contactsArray{
    
    [self.contactsArray removeAllObjects];
    self.contactsArray = contactsArray;
    
    if ([self.contactsArray count] == 0 ) {
        [self.btnSelectFriends setTitle:@"Select Friends to Ask" forState:UIControlStateNormal];
    }else{
        [self.btnSelectFriends setTitle:@"Select More Friends to Ask" forState:UIControlStateNormal];
        self.btnCreateAsk.enabled = true;
        [self.tblSelectFriend reloadData];
    }
}

#pragma mark - Tableview Delegate/Datasource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    return [self.contactsArray count];
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    AskingFriendsTableViewCell *inviteContactsCell = [tableView dequeueReusableCellWithIdentifier:@"AskingFriendsTableViewCell" forIndexPath: indexPath];
    
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    
    inviteContactsCell.lblContactName.text = [self.contactsArray[indexPath.row] valueForKeyPath:@"ContactName"];
    
    inviteContactsCell.btnDeleteRecord.tag = indexPath.row;
    
    [inviteContactsCell.btnDeleteRecord addTarget:self action:@selector(btnRemoveContact:) forControlEvents:UIControlEventTouchUpInside];
    
    return  inviteContactsCell;
    
}


@end
