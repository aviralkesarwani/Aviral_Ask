//
//  MAPeopleInviteFriendsVC.m
//  My Asks
//
//  Created by artis on 19/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "MAPeopleInviteFriendsVC.h"

@interface MAPeopleInviteFriendsVC ()

@end

@implementation MAPeopleInviteFriendsVC

#pragma mark - Life Cycle

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    NSLog(@"%@",self.ContactsArray);
    
    self.filteredContactsArray = [[NSMutableArray alloc]init];
    self.searchContactArray = [[NSMutableArray alloc] init];
    contactsToinvite = [[NSMutableArray alloc]init];
    ContactList = [[NSString alloc]init];
    self.friendsSearchBar.delegate = self;
    
    [self.btnInvite addTarget:self action:@selector(btnInviteClicked:) forControlEvents:UIControlEventTouchUpInside];
    
    for (NSArray *list in self.ContactsArray) {
        
        NSLog(@"%@",list);
        MAPeopleInviteModel *peopleInviteModel = [MAPeopleInviteModel new];
        
        peopleInviteModel.contactFullName = [list valueForKey:@"ContactName"];
        peopleInviteModel.contactPhoneNumber = [list valueForKey:@"ContactNumber"];
        //peopleInviteModel.IsRexUser = [[list valueForKey:@"IsRexUser"] boolValue];
    
        if (![[list valueForKey:@"IsRexUser"] boolValue]) {
            
            NSLog(@"selectedNumber = %@",self.selectedNumber);
            NSLog(@"Number = %@",peopleInviteModel.contactPhoneNumber);
            
            if ([self.selectedNumber isEqualToString:peopleInviteModel.contactPhoneNumber]) {
            
                NSLog(@"selectedNumber = %@",self.selectedNumber);
                NSLog(@"Number = %@",peopleInviteModel.contactPhoneNumber);
                NSLog(@"Number = %@",peopleInviteModel.contactFullName);
                
                NSString *phoneNumberSelected = [self.selectedNumber stringByReplacingOccurrencesOfString:@"\"" withString:@""];
//                if (phoneNumberSelected.length == 10) {
//                    
//                    NSString *dialingCode = [AppData sharedManager].userCountryCode;
//                    phoneNumberSelected = [NSString stringWithFormat:@"%@%@",dialingCode,phoneNumberSelected];
//                }
                [contactsToinvite addObject:phoneNumberSelected];
                peopleInviteModel.isSelectedNumber = true;
                
                [self.btnInvite setTitle:[NSString stringWithFormat:@"INVITE (%lu)",(unsigned long)[contactsToinvite count]] forState:UIControlStateNormal];
            }
             [self.filteredContactsArray addObject:peopleInviteModel];
        }
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Tableview  Delegate and Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (self.isSearching) {
        return [self.searchContactArray count];
    }
    else {
        return [self.filteredContactsArray count];
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

    if (self.isSearching) {
        inviteModel = [self.searchContactArray objectAtIndex:indexPath.row];
    }
    else {
        inviteModel =  [self.filteredContactsArray objectAtIndex:indexPath.row];
    }
    
    MAPeopleInviteFriendsTableCell * peopleCell = [tableView dequeueReusableCellWithIdentifier:@"MAPeopleInviteFriendsTableCell" forIndexPath: indexPath];
    peopleCell.lblName.text = inviteModel.contactFullName;
    peopleCell.lblContactNumber.text = inviteModel.contactPhoneNumber;
    
    [peopleCell.btnSelectDeselect addTarget:self action:@selector(btnSelectDeselectClicked:) forControlEvents:UIControlEventTouchUpInside];
    peopleCell.btnSelectDeselect.tag = indexPath.row;
    
    
    if ([inviteModel.contactPhoneNumber isEqualToString:self.selectedNumber]) {
        
         //inviteModel.isSelectedNumber = true;
         [peopleCell.btnSelectDeselect setImage:[UIImage imageNamed:@"Checkbox Selected"] forState:UIControlStateNormal];
    }
    else{
         //inviteModel.isSelectedNumber = false;
         [peopleCell.btnSelectDeselect setImage:[UIImage imageNamed:@"Checkbox normal"] forState:UIControlStateNormal];
    }
    
    if (inviteModel.isSelectedNumber) {
        [peopleCell.btnSelectDeselect setImage:[UIImage imageNamed:@"Checkbox Selected"] forState:UIControlStateNormal];
    }
    else{
        [peopleCell.btnSelectDeselect setImage:[UIImage imageNamed:@"Checkbox normal"] forState:UIControlStateNormal];
    }
   
    
    return peopleCell;
}

#pragma mark - button actions

- (void)btnSelectDeselectClicked:(UIButton*)sender{

    NSInteger index = sender.tag;

    if (self.isSearching) {
        inviteModel = [self.searchContactArray objectAtIndex:index];
    }
    else {
        inviteModel = [self.filteredContactsArray objectAtIndex:index];
    }
    
    if (inviteModel.isSelectedNumber == false) {
        
        inviteModel.isSelectedNumber = true;
        NSString *phoneNumber = [inviteModel.contactPhoneNumber stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        
        if (phoneNumber.length == 10) {
            NSString *countryCode = [[AppData sharedManager]getCurrentCountryCode];
            phoneNumber = [NSString stringWithFormat:@"%@%@",countryCode,phoneNumber];
        }
        
        [contactsToinvite addObject:phoneNumber];
    }
    else {
        inviteModel.isSelectedNumber = false;
        
        NSString *phoneNumber = [inviteModel.contactPhoneNumber stringByReplacingOccurrencesOfString:@"\"" withString:@""];
        if (phoneNumber.length == 10) {
//            NSString *countryCode = [[AppData sharedManager]getCurrentCountryCode];
//            phoneNumber = [NSString stringWithFormat:@"%@%@",countryCode,phoneNumber];

        }
        
        [contactsToinvite removeObject:phoneNumber];
    }
    
    [self.btnInvite setTitle:[NSString stringWithFormat:@"INVITE (%lu)",(unsigned long)[contactsToinvite count]] forState:UIControlStateNormal];
    
    [self.tblFriendsList reloadData];
}

-(void) btnInviteClicked:(UIButton *)sender{
    
    NSLog(@"%@",ContactList);

    ContactList = [contactsToinvite componentsJoinedByString:@","];
    NSLog(@"%@",ContactList);
    ContactList = [ContactList stringByReplacingOccurrencesOfString:@"\"" withString:@""];
    
    if ([contactsToinvite count]==0) {
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
                                                        message:@"Please select contact to invite"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    else{
        
        [KVNProgress show];
        [[APIClient sharedManager]InviteFriends:ContactList onCompletion:^(NSDictionary *resultData, bool success) {
            
            [KVNProgress dismiss];
            
            if (resultData == (id)[NSNull null] || [resultData count] == 0) {
                
                [self.navigationController popViewControllerAnimated:YES];
            }
            else{
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
                                                                message:@"Invite Failed"
                                                               delegate:self
                                                      cancelButtonTitle:@"OK"
                                                      otherButtonTitles:nil];
                [alert show];
            }
        }];
    }
}
- (IBAction)btnBackClicked:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - SearchBar Delegate

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)text {
    if (text.length > 0) {
        self.isSearching = YES;
        [self filterContentForSearchText:text scope:@""];
    }
    else {
        self.isSearching = NO;
        [searchBar performSelector:@selector(resignFirstResponder) withObject:nil afterDelay:0];
    }
    [self.tblFriendsList reloadData];
}

-(void)searchBarTextDidEndEditing:(UISearchBar *)searchBar
{
    if(searchBar.text.length == 0){
    }
}


- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    self.isSearching = NO;
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
    NSPredicate *resultPredicate = [NSPredicate
                                    predicateWithFormat:@"contactFullName contains[cd] %@",
                                    searchText];
    
    NSArray *filterArray;

    
    filterArray = [self.filteredContactsArray filteredArrayUsingPredicate:resultPredicate];
    
    self.searchContactArray = [NSMutableArray arrayWithArray:filterArray];
    
}


@end
