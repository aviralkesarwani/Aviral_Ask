//
//  MAHistoryCell.h
//  My Asks
//
//  Created by MY PC on 4/23/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MAHistoryCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgHistoryType;
@property (weak, nonatomic) IBOutlet UIView *verticalView;
@property (weak, nonatomic) IBOutlet UIView *circleView;
@property (weak, nonatomic) IBOutlet UILabel *lblTime;
@property (weak, nonatomic) IBOutlet UILabel *lblHistoryText;

@end
