//
//  MAAddContactVC.m
//  My Asks
//
//  Created by Rahul on 9/14/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "MAAddContactVC.h"
#import <DropDownListView.h>
#import <CoreTelephony/CTCarrier.h>
#import <CoreTelephony/CTTelephonyNetworkInfo.h>

#define kCountriesFileName  @"countries.json"
#define kCountryName        @"name"
#define kCountryCallingCode @"dial_code"
#define kCountryCode        @"code"



@interface MAAddContactVC ()<kDropDownListViewDelegate,UITextFieldDelegate>
{
    NSArray *countriesList;
    NSMutableArray *countryDataRows;
    DropDownListView * Dropobj;
    NSString *dialingCode;
    NSString *countryName;
}

@property (weak, nonatomic) IBOutlet UITextField *txtCountryCode;
@property (weak, nonatomic) IBOutlet UITextField *txtMobileNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblCountryCode;
@property (weak, nonatomic) IBOutlet UIView *fillDataView;
@property (weak, nonatomic) IBOutlet UIButton *btnDone;

@end

@implementation MAAddContactVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
   
    [self parseJSON];
    
    countryDataRows = [[NSMutableArray alloc]init];

    for (NSMutableDictionary *dic in countriesList) {
        
        NSString *country = [dic valueForKey:@"name"];
        NSString *Code = [dic valueForKey:@"dial_code"];
        
        NSString *data = [NSString stringWithFormat:@"%@ (%@)",country,Code];
        [countryDataRows addObject:data];
    }
    
    self.btnDone.userInteractionEnabled = false;
    [self.btnDone setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];

    // For geting country code information without using location services
    
    CTTelephonyNetworkInfo *networkInfo = [[CTTelephonyNetworkInfo alloc] init];
    CTCarrier *carrier = [networkInfo subscriberCellularProvider];
    
    // Get carrier name
    NSString *carrierName = [carrier carrierName];
    if (carrierName != nil)
        NSLog(@"Carrier: %@", carrierName);
    
    // Get mobile country code
    NSString *mcc = [carrier mobileCountryCode];
    if (mcc != nil)
        NSLog(@"Mobile Country Code (MCC): %@", mcc);
    
    // Get mobile network code
    NSString *mnc = [carrier isoCountryCode];
    
    if (mnc == nil || mnc.length == 0){
        
        NSLog(@"Mobile Network Code (MNC): %@", mnc);
        
        self.txtCountryCode.text = @"United States";
        self.lblCountryCode.text = @"+1";
        dialingCode = @"1";
        
    }
    else{
        
        for (NSMutableDictionary *dic in countriesList) {
            
            NSString *country = [dic valueForKey:@"name"];
            NSString *Code = [dic valueForKey:@"dial_code"];
            
            NSString *countryCode = [[NSString stringWithFormat:@"%@",mnc]uppercaseString];
            
            if ([countryCode isEqualToString:[dic valueForKey:kCountryCode]]) {
                
                self.txtCountryCode.text = [NSString stringWithFormat:@"%@",[dic valueForKey:kCountryName]];
                self.lblCountryCode.text = [NSString stringWithFormat:@"%@",[dic valueForKey:kCountryCallingCode]];
                
                dialingCode = [[[dic valueForKey:kCountryCallingCode] componentsSeparatedByCharactersInSet:
                                [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                               componentsJoinedByString:@""];
            }
            
            
            NSString *data = [NSString stringWithFormat:@"%@ (%@)",country,Code];
            [countryDataRows addObject:data];
        }
        
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)parseJSON {
    
    NSData *data = [NSData dataWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"countries" ofType:@"json"]];
    NSError *localError = nil;
    NSDictionary *parsedObject = [NSJSONSerialization JSONObjectWithData:data options:0 error:&localError];
    
    if (localError != nil) {
        NSLog(@"%@", [localError userInfo]);
    }
    NSArray *objectArray = (NSArray *)parsedObject;
    
    NSSortDescriptor *sortDescriptor =
    [NSSortDescriptor sortDescriptorWithKey:@"name"
                                  ascending:YES
                                   selector:@selector(caseInsensitiveCompare:)];
    countriesList = [objectArray sortedArrayUsingDescriptors:@[sortDescriptor]];
    
}

#pragma mark -

- (IBAction)btnCountryCodeClicked:(id)sender {
    
    [self.view endEditing:true];
    
    [Dropobj fadeOut];
    
    [self showPopUpWithTitle:@"Select Country" withOption:countryDataRows xy:CGPointMake(self.fillDataView.frame.origin.x,self.fillDataView.frame.origin.y) size:CGSizeMake(self.fillDataView.frame.size.width,self.fillDataView.frame.size.height) isMultiple:NO];
    
}

- (IBAction)btnCancelClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnDoneClicked:(id)sender {
    
    if(self.txtCountryCode.text.length == 0){
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
                                                        message:@"Enter valid country code"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    else if(self.txtMobileNumber.text.length == 0 || self.txtMobileNumber.text.length >10){
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"REX.Life"
                                                        message:@"Enter valid mobile number"
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
    
    else{
        
        if ([self.delegate respondsToSelector:@selector(addedContact:)]) {
            
            MAFriendListModel *friendListModel = [MAFriendListModel new];
            
            friendListModel.Id = @"";
            friendListModel.ContactNumber = [NSString stringWithFormat:@"%@%@",dialingCode,self.txtMobileNumber.text];
            friendListModel.ContactName = [NSString stringWithFormat:@"%@%@",dialingCode,self.txtMobileNumber.text];
            friendListModel.ListCount = @"";
            friendListModel.PictureCount = @"";
            friendListModel.PlacesCount = @"";
            friendListModel.IsRexUser = NO;
            friendListModel.ProfilePicture = @"";
            friendListModel.isSelected = YES;
            
            [self.delegate addedContact:friendListModel];
        }
        
        [self.navigationController popViewControllerAnimated:YES];
    }
}


#pragma  mark - textfield delegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    
    if (textField == self.txtMobileNumber) {
        
        NSUInteger newLength = [textField.text length] + [string length] - range.length;
        
        if (newLength == 10) {
            
            self.btnDone.userInteractionEnabled = true;
            [self.btnDone setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
        }
        else
        {
            self.btnDone.userInteractionEnabled = false;
            [self.btnDone setTitleColor:[UIColor grayColor] forState:UIControlStateNormal];
        }
        //return newLength <= 10;
    }
    return YES;
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if (textField == self.txtMobileNumber) {
        
        [self btnDoneClicked:nil];
    }
    return YES;
}

#pragma  mark - methods for dropDown

-(void)showPopUpWithTitle:(NSString*)popupTitle withOption:(NSArray*)arrOptions xy:(CGPoint)point size:(CGSize)size isMultiple:(BOOL)isMultiple{
    
    
    Dropobj = [[DropDownListView alloc] initWithTitle:popupTitle options:arrOptions xy:point size:size isMultiple:isMultiple];
    Dropobj.delegate = self;
    [Dropobj showInView:self.view animated:YES];
    
    /*----------------Set DropDown backGroundColor-----------------*/
    
    Dropobj.backgroundColor = [UIColor colorWithRed:64.0/255.0 green:88.0/255.0 blue:122.0/255.0 alpha:0.9];
    
}

- (void)DropDownListView:(DropDownListView *)dropdownListView didSelectedIndex:(NSInteger)anIndex{
    /*----------------Get Selected Value[Single selection]-----------------*/
    
    dialingCode = [[[[countriesList objectAtIndex:anIndex]valueForKey:kCountryCallingCode] componentsSeparatedByCharactersInSet:
                    [[NSCharacterSet decimalDigitCharacterSet] invertedSet]]
                   componentsJoinedByString:@""];
    
    NSLog(@"%@ is chosen!",dialingCode);
    
    countryName = [[countriesList objectAtIndex:anIndex]valueForKey:kCountryName];
    
    self.txtCountryCode.text = [NSString stringWithFormat:@"%@",countryName];
    self.lblCountryCode.text = [NSString stringWithFormat:@"+%@",dialingCode];
    
}

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event{
    
    [Dropobj fadeOut];
}

@end
