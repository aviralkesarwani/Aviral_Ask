//
//  DiscussionDetailView.h
//  My Asks
//
//  Created by _ on 8/17/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DiscussionDetailView : UIView

@property (weak, nonatomic) IBOutlet UIView *bgView;

@property (weak, nonatomic) IBOutlet UILabel *lbl_name;
@property (weak, nonatomic) IBOutlet UILabel *lbl_address;
@property (weak, nonatomic) IBOutlet UIButton *btn_call;
@property (weak, nonatomic) IBOutlet UIButton *btn_direction;
@property (weak, nonatomic) IBOutlet UIButton *btn_share;
@property (weak, nonatomic) IBOutlet UIButton *btn_save;
@property (weak, nonatomic) IBOutlet UIButton *btn_cross;


-(void)configureViewWithData:(id)data;

@end
