//
//  JSQMessageWithExtraData.h
//  My Asks
//
//  Created by _ on 8/30/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "JSQMessage.h"
#import <QuickBlox/Quickblox.h>


@interface JSQMessageWithExtraData : JSQMessage

@property QBChatMessage *qbChatMessage;

@end
