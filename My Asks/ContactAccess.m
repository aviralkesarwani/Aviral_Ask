//
//  ContactAccess.m
//  My Asks
//
//  Created by Iton, LLC on 22/07/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "ContactAccess.h"

@interface ContactAccess ()

@end

@implementation ContactAccess

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}
- (IBAction)nahTapped:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
    [self.delegate contactAccessResult:NO];
}
- (IBAction)sureTapped:(id)sender {
    [self dismissViewControllerAnimated:NO completion:nil];
    [self.delegate contactAccessResult:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
