//
//  ContactAccess.h
//  My Asks
//
//  Created by Iton, LLC on 22/07/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol ContactAccessDelegate <NSObject>
-(void)contactAccessResult :(BOOL)result;
@end

@interface ContactAccess : UIViewController{
    
}

@property (nonatomic, weak) id <ContactAccessDelegate> delegate;
@end
