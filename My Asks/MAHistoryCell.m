//
//  MAHistoryCell.m
//  My Asks
//
//  Created by MY PC on 4/23/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "MAHistoryCell.h"

@implementation MAHistoryCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    self.circleView.layer.cornerRadius = self.circleView.bounds.size.width / 2.0;
//    self.circleView.clipsToBounds = YES;
    self.circleView.layer.borderWidth = 1.0;
    self.circleView.layer.borderColor = [UIColor colorWithRed:205.0/255.0 green:209.0/255.0 blue:214.0/255.0 alpha:1].CGColor;
    // Configure the view for the selected state
}

@end
