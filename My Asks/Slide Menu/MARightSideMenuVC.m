//
//  MARightSideMenuVC.m
//  My Asks
//
//  Created by Mac on 31/03/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "MARightSideMenuVC.h"
#import "MARightSideMenuTableCell.h"

@interface MARightSideMenuVC ()
{
    AppDelegate *appDelegate;
}
@end



@implementation MARightSideMenuVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tblChatContact.delegate = self;
    self.tblChatContact.dataSource = self;
    
    appDelegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];

    self.invitedContactsArray = [[NSMutableArray alloc] init];
    
    //contactsStrore = [[CNContactStore alloc] init];
    
    contactDetails = [[NSMutableArray alloc]init];
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(sideMenuState:) name:MFSideMenuStateNotificationEvent object:nil];
//    self.tblChatContact.backgroundColor = [UIColor redColor];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}

- (void)viewWillAppear:(BOOL)animated {
}

- (void) sideMenuState:(NSNotification *) notification
{
//    NSLog(@"%@",notification.userInfo);
    if ([[notification.userInfo valueForKey:@"eventType"] integerValue] == 1) {
        [self.invitedContactsArray removeAllObjects];
        
        if ([AppData sharedManager].userAskModelForSideMenu != nil) {
            self.userAskModel = [AppData sharedManager].userAskModelForSideMenu;
            [self checkContactsAccess];
            [self setInviteFriends];
        }
    }
}


- (void)viewDidAppear:(BOOL)animated {
    NSLog(@"viewDidAppear");
}

#pragma mark - UITableView Delegate & Datasrouce -

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [self.invitedContactsArray count];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.tblChatContact.frame.size.width, 20)];
    UILabel *headerLable = [[UILabel alloc] initWithFrame:CGRectMake(20, 0, self.tblChatContact.frame.size.width - 40, 20)];
    
    headerLable.textColor = [UIColor whiteColor];
    
    [headerLable setFont:[UIFont fontWithName:@"HelveticaNeue-Regular" size:15.0f]];
    [headerLable setText:@"People in chat"];
    
    [view addSubview:headerLable];
    
    view.backgroundColor = [UIColor clearColor];
    return view;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 20;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    MARightSideMenuTableCell *cell = [tableView dequeueReusableCellWithIdentifier:@"MARightSideMenuTableCell"];
    
//    switch (indexPath.row)
//    {
//        case 0:
//            cell.lblContactName.text = @"Sunil";
//            break;
//            
//        case 1:
//            cell.lblContactName.text = @"Yashwanth";
//            break;
//            
//        case 2:
//            cell.lblContactName.text = @"Ashok";
//            break;
//            
//        case 3:
//            cell.lblContactName.text = @"Mahendra";
//            break;
//    }
    
//    NSDictionary *contact = @{@"ContactName":contactString,@"PhoneNumber":contactString};
//    [self.invitedContactsArray addObject:contact];

    NSDictionary *contact = [self.invitedContactsArray objectAtIndex:indexPath.row];
    cell.lblContactName.text = [contact valueForKey:@"ContactName"];
    
    
    cell.backgroundColor = [UIColor clearColor];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
//    id <SlideNavigationContorllerAnimator> revealAnimator;
//    CGFloat animationDuration = 0;
//    
//    switch (indexPath.row)
//    {
//        case 0:
//            revealAnimator = nil;
//            animationDuration = .19;
//            break;
//            
//        case 1:
//            revealAnimator = [[SlideNavigationContorllerAnimatorSlide alloc] init];
//            animationDuration = .19;
//            break;
//            
//        case 2:
//            revealAnimator = [[SlideNavigationContorllerAnimatorFade alloc] init];
//            animationDuration = .18;
//            break;
//            
//        case 3:
//            revealAnimator = [[SlideNavigationContorllerAnimatorSlideAndFade alloc] initWithMaximumFadeAlpha:.8 fadeColor:[UIColor blackColor] andSlideMovement:100];
//            animationDuration = .19;
//            break;
//            
//        case 4:
//            revealAnimator = [[SlideNavigationContorllerAnimatorScale alloc] init];
//            animationDuration = .22;
//            break;
//            
//        case 5:
//            revealAnimator = [[SlideNavigationContorllerAnimatorScaleAndFade alloc] initWithMaximumFadeAlpha:.6 fadeColor:[UIColor blackColor] andMinimumScale:.8];
//            animationDuration = .22;
//            break;
//            
//        default:
//            return;
//    }
//    
//    [[SlideNavigationController sharedInstance] closeMenuWithCompletion:^{
//        [SlideNavigationController sharedInstance].menuRevealAnimationDuration = animationDuration;
//        [SlideNavigationController sharedInstance].menuRevealAnimator = revealAnimator;
//    }];
}

#pragma mark - Button Actions

- (IBAction)btnBackClicked:(id)sender {
    
//    id <SlideNavigationContorllerAnimator> revealAnimator;
//    CGFloat animationDuration = 0;
//    
//    revealAnimator = [[SlideNavigationContorllerAnimatorSlide alloc] init];
//    animationDuration = .19;
//
//
//    [[SlideNavigationController sharedInstance] closeMenuWithCompletion:^{
//        [SlideNavigationController sharedInstance].menuRevealAnimationDuration = animationDuration;
//        [SlideNavigationController sharedInstance].menuRevealAnimator = revealAnimator;
//    }];
    [appDelegate openRightSlideDrawer];
}

- (IBAction)btnInviteFriendsClicked:(id)sender {
    MAInviteFriendsVC *contributeViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"MAInviteFriendsVC"];
    CGRect fullScreenRect = [[UIScreen mainScreen] bounds];
    
    contributeViewController.delegate = self;
    
    BIZPopupViewController *popupViewController = [[BIZPopupViewController alloc] initWithContentViewController:contributeViewController contentSize:CGSizeMake(fullScreenRect.size.width , fullScreenRect.size.height)];
    popupViewController.showDismissButton = NO;
    
    [self presentViewController:popupViewController animated:NO completion:nil];

}

#pragma mark - Invite Friends Deleagte

-(void)inviteFriends:(NSMutableArray *)contactsArray{
    if (contactsArray.count > 0) {
        [self inviteFriendsForInsertAsk:contactsArray];
    }
}


#pragma mark - Custom Methods

- (void) setInvitedContactsArray {
    
    for (NSString *contactString in self.userAskModel.InvitedContactsArray) {
        
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"ANY arrPhoneNumbers IN %@", contactString];
        NSArray *filtered  = [contactDetails filteredArrayUsingPredicate:predicate];
        NSLog(@"%@",filtered);
        
        if ([filtered count] > 0) {
            MAInviteFriendsModel *inviteFriends = [filtered firstObject];
            NSDictionary *contact = @{@"ContactName":inviteFriends.contactFullName,@"PhoneNumber":contactString};
            [self.invitedContactsArray addObject:contact];
        }
        else {
            NSDictionary *contact = @{@"ContactName":contactString,@"PhoneNumber":contactString};
            [self.invitedContactsArray addObject:contact];
        }
    }
    [self.tblChatContact reloadData];
}

- (void) setInviteFriends {
    if (self.userAskModel.IsOwner) {
        self.btnInviteFriends.hidden = false;
    }
    else {
        self.btnInviteFriends.hidden = true;
    }
}

- (void) inviteFriendsForInsertAsk : (NSArray *) contactsArray {
    
    NSArray *filteredArray = [self filteredContacts:contactsArray];
    
    [KVNProgress show];
    NSDictionary *param = @{@"Id":self.userAskModel.UserAsksID,@"UserId":self.userAskModel.UserId,@"InvitedContacts":filteredArray};
    [[APIClient sharedManager] insertAskInvite:param onCompletion:^(NSDictionary *resultData, bool success) {
        [KVNProgress dismiss];
        
        if (success) {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Rex.Life" message:@"Invited Friends successfully" preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
                [[NSNotificationCenter defaultCenter] postNotificationName:@"invitedFriendsFromChat" object:self];

                [appDelegate openRightSlideDrawer];
            }];
            [alert addAction:okAction];
            [self presentViewController:alert animated:true completion:nil];
        }
    }];
    
}

-(NSMutableArray *)filteredContacts : (NSArray * ) contactsArray {
    
    //    NSArray *phoneNumberArray = [[AppData sharedManager].ContactsArray valueForKey:@"arrPhoneNumbers"];
    
    NSMutableArray *filteredArray = [[NSMutableArray alloc]init];
    //
    //    for (NSArray *phoneNumber in phoneNumberArray) {
    //
    //        if (phoneNumber.count > 0) {
    //            NSString *number = [phoneNumber firstObject];
    //            NSLog(@"%@",number);
    //
    //            [filteredArray addObject:number];
    //        }
    //    }
    
    NSMutableArray *contactArray = [NSMutableArray arrayWithArray:contactsArray];
    
    for (MAInviteFriendsModel *inviteFriend in contactArray) {
        
        NSString *phoneNumber = inviteFriend.selectedPhoneNumber;
        if (phoneNumber.length == 10) {
            
            phoneNumber = [NSString stringWithFormat:@"91%@",phoneNumber];
        }
        
        [filteredArray addObject:phoneNumber];
    }
    
    NSLog(@"%@",filteredArray);
    
    return filteredArray;
}


#pragma mark - Fetch Contact

-(void)checkContactsAccess{
    
    /*[self requestContactsAccessWithHandler:^(BOOL grandted) {
        
        if (grandted) {
            
            //keys with fetching properties
            NSArray *keys = @[CNContactFamilyNameKey, CNContactGivenNameKey, CNContactNamePrefixKey, CNContactMiddleNameKey, CNContactPhoneNumbersKey,CNContactImageDataKey,CNContactImageDataAvailableKey,CNContactThumbnailImageDataKey];
            NSString *containerId = contactsStrore.defaultContainerIdentifier;
            NSPredicate *predicate = [CNContact predicateForContactsInContainerWithIdentifier:containerId];
            NSError *error;
            NSArray *cnContacts = [contactsStrore unifiedContactsMatchingPredicate:predicate keysToFetch:keys error:&error];
            if (error) {
                NSLog(@"error fetching contacts %@", error);
            } else {
                for (CNContact *contact in cnContacts) {
                    // copy data to my custom Contacts class.
                    MAInviteFriendsModel *inviteFriends = [[MAInviteFriendsModel alloc]init];
                    
                    inviteFriends.contactFullName = [NSString stringWithFormat:@"%@ %@",contact.givenName,contact.familyName];
                    inviteFriends.arrPhoneNumbers = [[contact.phoneNumbers valueForKey:@"value"] valueForKey:@"digits"];
                    inviteFriends.isSelected = NO;
                    
                    if (contact.imageDataAvailable) {
                        
                        inviteFriends.contactImage = [UIImage imageWithData:contact.imageData];
                    }
                    else {
                        inviteFriends.contactImage = [UIImage imageNamed:@"Photo.png"];
                    }
                    [contactDetails addObject:inviteFriends];
                }
            }
            
            dispatch_async(dispatch_get_main_queue(), ^{
                [self setInvitedContactsArray];
            });
        }
    }];*/
}

/*

-(void)requestContactsAccessWithHandler:(void (^)(BOOL grandted))handler{
    
    switch ([CNContactStore authorizationStatusForEntityType:CNEntityTypeContacts]) {
        case CNAuthorizationStatusAuthorized:
            handler(YES);
            break;
        case CNAuthorizationStatusDenied:
        case CNAuthorizationStatusNotDetermined:{
            [contactsStrore requestAccessForEntityType:CNEntityTypeContacts completionHandler:^(BOOL granted, NSError * _Nullable error) {
                
                handler(granted);
            }];
            break;
        }
        case CNAuthorizationStatusRestricted:
            handler(NO);
            break;
    }
}
 */


@end
