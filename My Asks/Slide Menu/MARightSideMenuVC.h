//
//  MARightSideMenuVC.h
//  My Asks
//
//  Created by Mac on 31/03/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"
#import "MARightSideMenuTableCell.h"
#import "AppData.h"
#import "MAUserAsksModel.h"
#import "MAInviteFriendsModel.h"
//#import <Contacts/Contacts.h>
#import "MAInviteFriendsVC.h"
#import "APIClient.h"
#import <KVNProgress.h>


@interface MARightSideMenuVC : UIViewController <UITableViewDataSource, UITableViewDelegate,MAInviteFriendsDelegate>
{
    //CNContactStore *contactsStrore;
    NSMutableArray *contactDetails;
}
@property (weak, nonatomic) IBOutlet UITableView *tblChatContact;
@property (nonatomic, assign) BOOL slideOutAnimationEnabled;

@property (strong,nonatomic) NSMutableArray *invitedContactsArray;
@property (strong, nonatomic) MAUserAsksModel *userAskModel;
@property (weak, nonatomic) IBOutlet UIButton *btnInviteFriends;

- (IBAction)btnBackClicked:(id)sender;

- (IBAction)btnInviteFriendsClicked:(id)sender;

@end
