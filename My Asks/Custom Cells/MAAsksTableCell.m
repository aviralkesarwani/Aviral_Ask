//
//  MAAsksTableCell.m
//  My Asks
//
//  Created by Mac on 31/03/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "MAAsksTableCell.h"

@implementation MAAsksTableCell

- (void)awakeFromNib {
    // Initialization code
    self.imgBackground.crossfadeDuration = 0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
