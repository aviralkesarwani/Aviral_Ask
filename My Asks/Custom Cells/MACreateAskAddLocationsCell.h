//
//  MACreateAskAddLocationsCell.h
//  My Asks
//
//  Created by artis on 04/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MACreateAskAddLocationsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblPlaceName;
@property (weak, nonatomic) IBOutlet UILabel *lblPlaceDetails;


@end
