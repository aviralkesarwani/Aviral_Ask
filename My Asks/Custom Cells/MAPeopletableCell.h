//
//  MAPeopletableCell.h
//  My Asks
//
//  Created by artis on 18/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AsyncImageView.h>

@interface MAPeopletableCell : UITableViewCell

@property (weak, nonatomic) IBOutlet AsyncImageView *imgPeople;
@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UIButton *btnInvite;
@property (weak, nonatomic) IBOutlet UILabel *lblListPlaces;
@property (weak, nonatomic) IBOutlet UIButton *btnForward;


@end
