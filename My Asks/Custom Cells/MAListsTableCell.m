//
//  MAListsTableCell.m
//  My Asks
//
//  Created by MY PC on 4/17/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "MAListsTableCell.h"

@implementation MAListsTableCell

- (void)awakeFromNib {
    // Initialization code
    self.imgBackground.crossfadeDuration = 0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
