//
//  MARightSideMenuTableCell.h
//  My Asks
//
//  Created by Mac on 31/03/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AsyncImageView.h>
#import "MAUserAsksModel.h"

@interface MARightSideMenuTableCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblContactName;
@property (weak, nonatomic) IBOutlet UIView *viewOnline;
@property (weak, nonatomic) IBOutlet AsyncImageView *imgProfilePic;

@property (strong, nonatomic) MAUserAsksModel *userAskModel;
@end
