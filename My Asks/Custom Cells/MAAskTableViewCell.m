//
//  MAAskTableViewCell.m
//  My Asks
//
//  Created by MAC on 28/06/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "MAAskTableViewCell.h"

@implementation MAAskTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.btnTotalMessage.layer.cornerRadius = 5;
    self.btnTotalMessage.clipsToBounds = true;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
