//
//  MAPeopleInviteFriendsTableCell.h
//  My Asks
//
//  Created by artis on 19/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MAPeopleInviteFriendsTableCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblContactNumber;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectDeselect;

@end
