//
//  MAAsksTableCell.h
//  My Asks
//
//  Created by Mac on 31/03/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AsyncImageView.h>


@interface MAAsksTableCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblAsk;
@property (weak, nonatomic) IBOutlet UILabel *lblNoOfRecomendAsk;
@property (weak, nonatomic) IBOutlet UILabel *lblDateTimeOfAsk;
@property (weak, nonatomic) IBOutlet AsyncImageView *imgBackground;

@property (weak, nonatomic) IBOutlet UIButton *btnTotalMessage;

@end
