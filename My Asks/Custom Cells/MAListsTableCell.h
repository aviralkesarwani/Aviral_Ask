//
//  MAListsTableCell.h
//  My Asks
//
//  Created by MY PC on 4/17/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AsyncImageView.h>

@interface MAListsTableCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblName;
@property (weak, nonatomic) IBOutlet UILabel *lblPlace;
@property (weak, nonatomic) IBOutlet UILabel *lblPlaceCount;
@property (weak, nonatomic) IBOutlet UILabel *lblLikeCount;
@property (weak, nonatomic) IBOutlet UILabel *lblCommentCount;
@property (weak, nonatomic) IBOutlet AsyncImageView *imgBackground;

@property (weak, nonatomic) IBOutlet UIButton *btnAdd;

@end
