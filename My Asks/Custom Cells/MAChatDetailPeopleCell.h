//
//  MAChatDetailPeopleCell.h
//  My Asks
//
//  Created by MAC on 07/07/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MAChatDetailPeopleCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblPeopleName;
@end
