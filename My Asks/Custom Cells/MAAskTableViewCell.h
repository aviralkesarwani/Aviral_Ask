//
//  MAAskTableViewCell.h
//  My Asks
//
//  Created by MAC on 28/06/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AsyncImageView.h>

@interface MAAskTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblAsk;
@property (weak, nonatomic) IBOutlet UILabel *lblNoOfRecomendAsk;
@property (weak, nonatomic) IBOutlet UILabel *lblDateTimeOfAsk;
@property (weak, nonatomic) IBOutlet AsyncImageView *imgBackground;

@property (weak, nonatomic) IBOutlet UIButton *btnTotalMessage;


@end
