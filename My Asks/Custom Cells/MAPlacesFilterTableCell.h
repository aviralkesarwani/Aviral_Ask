//
//  MAPlacesFilterTableCell.h
//  My Asks
//
//  Created by Mac on 22/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <AsyncImageView.h>

@interface MAPlacesFilterTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet AsyncImageView *imgPlaces;
@property (weak, nonatomic) IBOutlet UILabel *lblPlaceName;
@property (weak, nonatomic) IBOutlet UILabel *lblPlaceDetails;
@property (strong, nonatomic) IBOutlet UIButton *btnIsFavorite;
@property (strong, nonatomic) IBOutlet UILabel *lblDistance;
@property (strong, nonatomic) IBOutlet UIButton *btnShare;

@end
