//
//  MAInviteFriendsTableCell.h
//  My Asks
//
//  Created by artis on 02/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MAInviteFriendsTableCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *btnSelectDeselect;
@property (weak, nonatomic) IBOutlet UILabel *lblContactName;
@property (weak, nonatomic) IBOutlet UIImageView *imgContact;

@property (strong, nonatomic) IBOutlet UIButton *btnselected;
@property (weak, nonatomic) IBOutlet UILabel *placeNameLbl;
@property (weak, nonatomic) IBOutlet UILabel *placeAddresslbl;

@end
