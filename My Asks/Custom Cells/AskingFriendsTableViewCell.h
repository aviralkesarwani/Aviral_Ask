//
//  AskingFriendsTableViewCell.h
//  My Asks
//
//  Created by MAC on 08/07/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AskingFriendsTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lblContactName;


@property (weak, nonatomic) IBOutlet UIButton *btnDeleteRecord;

@end
