//
//  MACreateAskInviteContactsCell.h
//  My Asks
//
//  Created by artis on 04/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MACreateAskInviteContactsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgContactImage;
@property (weak, nonatomic) IBOutlet UILabel *lblContactName;
@property (weak, nonatomic) IBOutlet UIButton *btnRemoveContact;

@end
