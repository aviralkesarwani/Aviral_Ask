//
//  MAPlacesFilterTableCell.m
//  My Asks
//
//  Created by Mac on 22/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "MAPlacesFilterTableCell.h"

@implementation MAPlacesFilterTableCell

- (void)awakeFromNib {
    // Initialization code
    self.imgPlaces.crossfadeDuration = 0;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

