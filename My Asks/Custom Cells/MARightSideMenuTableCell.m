//
//  MARightSideMenuTableCell.m
//  My Asks
//
//  Created by Mac on 31/03/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "MARightSideMenuTableCell.h"

@implementation MARightSideMenuTableCell

- (void)awakeFromNib {
    // Initialization code
    
    self.imgProfilePic.layer.cornerRadius = self.imgProfilePic.bounds.size.height/2;
    self.imgProfilePic.clipsToBounds = YES;
    
    self.viewOnline.layer.cornerRadius = self.viewOnline.bounds.size.height/2;
    self.viewOnline.clipsToBounds = YES;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
