//
//  MAAddLocationTableCell.h
//  My Asks
//
//  Created by artis on 01/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MAAddLocationTableCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnSelectDeselect;
@property (weak, nonatomic) IBOutlet UILabel *lblLocationName;
@property (weak, nonatomic) IBOutlet UILabel *lblLocationDetail;

@end
