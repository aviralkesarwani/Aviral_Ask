//
//  MAPlacesSearchTableCell.h
//  My Asks
//
//  Created by Mac on 20/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MAPlacesSearchTableCell : UITableViewCell
@property (strong, nonatomic) IBOutlet UILabel *lblCategoryName;

@end
