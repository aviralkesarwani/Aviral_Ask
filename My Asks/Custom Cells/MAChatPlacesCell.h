//
//  MAChatPlacesCell.h
//  My Asks
//
//  Created by Mac on 17/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MAChatPlacesCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblPlaceName;

@property (weak, nonatomic) IBOutlet UILabel *lblPlaceDetail;

@property (weak, nonatomic) IBOutlet UILabel *lblDistance;

@property (weak, nonatomic) IBOutlet UIButton *btnFavorite;

@end
