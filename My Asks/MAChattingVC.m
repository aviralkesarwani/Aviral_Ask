//
//  MAChattingVC.m
//  My Asks
//
//  Created by _ on 8/30/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "MAChattingVC.h"
#import "newAskSelectLocationVC.h"
#import "MAMapVC.h"
#import "DiscussionDetailView.h"
#import "DCPathButton.h"
#import "JSQMessageWithExtraData.h"
#import "MASearchPlacesModel.h"

#define kConstantURL @"http://MyAsk.com/data="

@interface MAChattingVC ()<MAMapViewDelegate,JSQMessagesComposerTextViewPasteDelegate,SelectedLocationDelegate,QBChatDelegate,TTTAttributedLabelDelegate>
{
    NSIndexPath *expandedIndexPath;
}
@property NSMutableArray *placeIDArray;
@property NSMutableArray *messagesArray;
@property NSMutableArray *selectedLocationsArray;
@property NSMutableDictionary *placeDict;

@end

@implementation MAChattingVC

#pragma mark - View lifecycle

/**
 *  Override point for customization.
 *
 *  Customize your view.
 *  Look at the properties on `JSQMessagesViewController` and `JSQMessagesCollectionView` to see what is possible.
 *
 *  Customize your layout.
 *  Look at the properties on `JSQMessagesCollectionViewFlowLayout` to see what is possible.
 */

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.title = @"JSQMessages";
    [[QBChat instance] addDelegate:self];
    
     MAChattingVC* __weak Obj = self;
    
    [self.selectedChatDialod setOnUserIsTyping:^(NSUInteger userID) {
        
        if ([QBSession currentSession].currentUser.ID != userID) {
            Obj.showTypingIndicator = YES;
        }

    }];
    
    [self.selectedChatDialod setOnUserStoppedTyping:^(NSUInteger userID) {
        
        if ([QBSession currentSession].currentUser.ID != userID) {
            Obj.showTypingIndicator = NO;
        }
    }];
    
    self.placeIDArray = [NSMutableArray new];
    self.messagesArray = [NSMutableArray new];
    self.senderId = [NSString stringWithFormat:@"%ld",[QBSession currentSession].currentUser.ID];
    self.senderDisplayName = [[AppData sharedManager] getDeviceName];
    
    self.inputToolbar.contentView.textView.pasteDelegate = self;
    self.inputToolbar.contentView.leftBarButtonItem = [JSQMessagesToolbarButtonFactory defaultLocationButtonItem];
    self.inputToolbar.contentView.leftBarButtonItem.enabled = true;

    /**
     *  You can set custom avatar sizes
     */
//    self.collectionView.collectionViewLayout.incomingAvatarViewSize = CGSizeZero;
    
//    self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeZero;
    
//    self.showLoadEarlierMessagesHeader = YES;
    
      /**
     *  Register custom menu actions for cells.
     */
//    [JSQMessagesCollectionViewCell registerMenuAction:@selector(customAction:)];
    
    /**
     *  OPT-IN: allow cells to be deleted
     */
    [JSQMessagesCollectionViewCell registerMenuAction:@selector(delete:)];
    
    /**
     *  Customize your toolbar buttons
     *
     *  self.inputToolbar.contentView.leftBarButtonItem = custom button or nil to remove
     *  self.inputToolbar.contentView.rightBarButtonItem = custom button or nil to remove
     */
    
    /**
     *  Set a maximum height for the input toolbar
     *
     *  self.inputToolbar.maximumHeight = 150;
     */
    
    
    // fetch all msg
    [self getChatsMessage];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    /**
     *  Enable/disable springy bubbles, default is NO.
     *  You must set this from `viewDidAppear:`
     *  Note: this feature is mostly stable, but still experimental
     */
}

#pragma mark - fetch Chat Data

-(void)getChatsMessage {
    
    [QBRequest messagesWithDialogID:self.selectedChatDialod.ID successBlock:^(QBResponse * _Nonnull response, NSArray<QBChatMessage *> * _Nullable messages) {
        
        for (QBChatMessage *chatMsg in messages) {
            
            NSString *senderId = [NSString stringWithFormat:@"%ld",chatMsg.senderID];
            NSString *senderDisplayName = chatMsg.customParameters[@"senderNickName"];
            
            JSQMessageWithExtraData *jsqMessage = [[JSQMessageWithExtraData alloc] initWithSenderId:senderId senderDisplayName:senderDisplayName date:chatMsg.createdAt text:chatMsg.text];
            jsqMessage.qbChatMessage = chatMsg;
            [self.messagesArray addObject:jsqMessage];
        }
        
        [self finishReceivingMessage];
        
    } errorBlock:^(QBResponse * _Nonnull response) {
        
    }];

}

#pragma mark - QBChatDelegate

- (void)chatRoomDidReceiveMessage:(QB_NONNULL QBChatMessage *)message fromDialogID:(QB_NONNULL NSString *)dialogID {
    
    if ([message.text rangeOfString:@"#"].location != NSNotFound) {// get location's off msg
        
        __weak  MAChattingVC *weakSelf = self;
        
        [self.parentVC fetchDialogDetailFromRexServer:^{
            
            [weakSelf.collectionView reloadData];
        }];
    }
    
    
    if ([dialogID isEqualToString:self.selectedChatDialod.ID]) {
        
        NSString *senderId = [NSString stringWithFormat:@"%ld",message.senderID];
        NSString *senderDisplayName = message.customParameters[@"senderNickName"];
        
        JSQMessageWithExtraData *jsqMessage = [[JSQMessageWithExtraData alloc] initWithSenderId:senderId senderDisplayName:senderDisplayName date:message.dateSent text:message.text];
        jsqMessage.qbChatMessage = message;
        [self.messagesArray addObject:jsqMessage];
        
        [self finishReceivingMessageAnimated:YES];
    }
}

#pragma mark - Messages view controller

- (void)didPressSendButton:(UIButton *)button
           withMessageText:(NSString *)text
                  senderId:(NSString *)senderId
         senderDisplayName:(NSString *)senderDisplayName
                      date:(NSDate *)date
{
    
    if ([self.inputToolbar.contentView.textView hasText]) {
        [JSQSystemSoundPlayer jsq_playMessageSentSound];
        
        QBChatMessage *message = [QBChatMessage markableMessage];
        [message setText:text];
        
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        params[@"save_to_history"] = @YES;
        params[@"senderNickName"] = [[AppData sharedManager] getDeviceName];
        [message setCustomParameters:params];
        
        [self.selectedChatDialod sendMessage:message completionBlock:^(NSError * _Nullable error) {
            
        }];
    
        if (self.placeIDArray.count > 0) {
            [KVNProgress show];
            
            NSMutableArray *arr = [NSMutableArray new];
            
            for (NSString *strID in self.placeIDArray) {
                [arr addObject:@{ @"AskId" : @([self.selectedChatDialod.data[@"askId"] integerValue]),@"PlaceId" : strID, @"UserID" :  @([[AppData sharedManager].userID integerValue])}];
            }
            
            [[APIClient sharedManager] addPlaceToAsk:arr onCompletion:^(NSDictionary *resultData, bool success) {
                
                [KVNProgress dismiss];
                
                if (success) {
                }
            }];
        }
        
        [self.placeIDArray removeAllObjects];
        
        
        [self finishSendingMessageAnimated:YES];
    }
    else {
        
        MAMapVC *mapVC = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"MAMapVC"];
        mapVC.delegate = self;
        [self.parentVC.navigationController pushViewController:mapVC animated:YES];
    }
}

- (void)didPressAccessoryButton:(UIButton *)sender
{
    [self.inputToolbar.contentView.textView resignFirstResponder];
    
    newAskSelectLocationVC *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"newAskSelectLocationVC"];
    vc.delegate = self;
    [self.parentVC.navigationController pushViewController:vc animated:YES];
    
}

#pragma mark - JSQMessages collection view data source

- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.messagesArray objectAtIndex:indexPath.item];
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didDeleteMessageAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    JSQMessageWithExtraData *message  = [self.messagesArray objectAtIndex:indexPath.item];
    
    NSSet *mesagesIDs = [NSSet setWithObjects:message.qbChatMessage.ID, nil];
    
    [QBRequest deleteMessagesWithIDs:mesagesIDs forAllUsers:YES successBlock:^(QBResponse *response) {
        

    } errorBlock:^(QBResponse *response) {
        
    }];
    
    //
    [self.messagesArray removeObjectAtIndex:indexPath.item];
}

- (id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  You may return nil here if you do not want bubbles.
     *  In this case, you should set the background color of your collection view cell's textView.
     *
     *  Otherwise, return your previously created bubble image data objects.
     */
    
    JSQMessage *message = [self.messagesArray objectAtIndex:indexPath.item];
    JSQMessagesBubbleImageFactory *bubbleFactory = [[JSQMessagesBubbleImageFactory alloc] init];
    
    if ([message.senderId isEqualToString:self.senderId]) {
        return [bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleLightGrayColor]];
    }
    
    return [bubbleFactory incomingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleGreenColor]];
}

- (id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  Return `nil` here if you do not want avatars.
     *  If you do return `nil`, be sure to do the following in `viewDidLoad`:
     *
     *  self.collectionView.collectionViewLayout.incomingAvatarViewSize = CGSizeZero;
     *  self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeZero;
     *
     *  It is possible to have only outgoing avatars or only incoming avatars, too.
     */
    
    /**
     *  Return your previously created avatar image data objects.
     *
     *  Note: these the avatars will be sized according to these values:
     *
     *  self.collectionView.collectionViewLayout.incomingAvatarViewSize
     *  self.collectionView.collectionViewLayout.outgoingAvatarViewSize
     *
     *  Override the defaults in `viewDidLoad`
     */
    
    JSQMessage *message = [self.messagesArray objectAtIndex:indexPath.item];
    
    if ([message.senderId isEqualToString:self.senderId]) {
        
        JSQMessagesAvatarImage *senderImage = [JSQMessagesAvatarImageFactory avatarImageWithImage:[UIImage imageNamed:@"Photo"]
                                                                                         diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
        
        return senderImage;
    }
    else {
        JSQMessagesAvatarImage *senderImage = [JSQMessagesAvatarImageFactory avatarImageWithImage:[UIImage imageNamed:@"Photo"]
                                                                                         diameter:kJSQMessagesCollectionViewAvatarSizeDefault];
        return senderImage;
    }
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  This logic should be consistent with what you return from `heightForCellTopLabelAtIndexPath:`
     *  The other label text delegate methods should follow a similar pattern.
     *
     *  Show a timestamp for every 3rd message
     */

    JSQMessage *message = [self.messagesArray objectAtIndex:indexPath.item];
    return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:message.date];
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessage *message = [self.messagesArray objectAtIndex:indexPath.item];
    
    /**
     *  iOS7-style sender name labels
     */

    if ([message.senderId isEqualToString:self.senderId]) {
        return nil;
    }

    if (indexPath.item - 1 > 0) {
        JSQMessage *previousMessage = [self.messagesArray objectAtIndex:indexPath.item - 1];
        if ([[previousMessage senderId] isEqualToString:message.senderDisplayName]) {
            return nil;
        }
    }
    
    /**
     *  Don't specify attributes to use the defaults.
     */
    return [[NSAttributedString alloc] initWithString:message.senderDisplayName];
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
    return nil;
}

#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return [self.messagesArray count];
}

- (UICollectionViewCell *)collectionView:(JSQMessagesCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  Override point for customizing cells
     */
    JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
    
    /**
     *  Configure almost *anything* on the cell
     *
     *  Text colors, label text, label colors, etc.
     *
     *
     *  DO NOT set `cell.textView.font` !
     *  Instead, you need to set `self.collectionView.collectionViewLayout.messageBubbleFont` to the font you want in `viewDidLoad`
     *
     *
     *  DO NOT manipulate cell layout information!
     *  Instead, override the properties you want on `self.collectionView.collectionViewLayout` from `viewDidLoad`
     */
    
    cell.textView.hidden = YES;
    
    JSQMessage *msg = [self.messagesArray objectAtIndex:indexPath.item];

    NSString *completeStr = msg.text;
    
    NSMutableAttributedString *temp = [[NSMutableAttributedString alloc] initWithString:completeStr];
    [temp addAttribute:NSFontAttributeName value:self.collectionView.collectionViewLayout.messageBubbleFont range:NSMakeRange(0, completeStr.length)];
    
    if (!msg.isMediaMessage) {
        
        if ([msg.senderId isEqualToString:self.senderId]) {
            [temp addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, completeStr.length)];
        }
        else {
            [temp addAttribute:NSForegroundColorAttributeName value:[UIColor whiteColor] range:NSMakeRange(0, completeStr.length)];
        }
    }

    if (self.collectionView.collectionViewLayout.messageBubbleFont != nil) {
        [temp addAttribute:NSFontAttributeName value:self.collectionView.collectionViewLayout.messageBubbleFont range:NSMakeRange(0, completeStr.length)];
    }
    
    NSInteger stringLength = completeStr.length;
    
    for (NSDictionary *locationDict in self.parentVC.chatPlaceArray) {
        
        NSString *strLocation = [NSString stringWithFormat:@"#%@",locationDict[@"Name"]];
        
        NSRange range = NSMakeRange(0,  stringLength);
        
        while(range.location != NSNotFound)
        {
            range = [completeStr rangeOfString: strLocation options:0 range:range];
            
            if(range.location != NSNotFound)
            {
                //[cell.ttattributedLabel addLinkToURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kConstantURL,strLocation]] withRange:range];

                [temp addAttribute:NSForegroundColorAttributeName value:[UIColor orangeColor] range:range];
                range = NSMakeRange(range.location + range.length,  stringLength - (range.location + range.length));
            }
        }
    }

    cell.ttattributedLabel.enabledTextCheckingTypes = NSTextCheckingTypeLink;
    cell.ttattributedLabel.delegate = self;
    
    
   // cell.textView.attributedText = temp;
    cell.ttattributedLabel.attributedText = temp;
    
    
    for (NSDictionary *locationDict in self.parentVC.chatPlaceArray) {
        
        NSString *strLocation = [NSString stringWithFormat:@"#%@",locationDict[@"Name"]];
        
        NSRange range = NSMakeRange(0,  stringLength);
        
        while(range.location != NSNotFound)
        {
            range = [completeStr rangeOfString: strLocation options:0 range:range];
            
            if(range.location != NSNotFound)
            {
                [cell.ttattributedLabel addLinkToURL:[NSURL URLWithString:[NSString stringWithFormat:@"%@%@",kConstantURL,locationDict[@"PlaceID"]]] withRange:range];
                
                range = NSMakeRange(range.location + range.length,  stringLength - (range.location + range.length));
            }
        }
    }

    if (self.placeDict != nil) {
        
        cell.discussionDetailView.lbl_name.text = self.placeDict[@"Name"];
        cell.discussionDetailView.lbl_address.text = self.placeDict[@"Address"];
    }
    
    cell.ttattributedLabel.tag = indexPath.item;
    
    // detail
    //[cell.discussionDetailView configureViewWithData:nil];
    [cell.discussionDetailView.btn_call addTarget:self action:@selector(callBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell.discussionDetailView.btn_direction addTarget:self action:@selector(directionBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell.discussionDetailView.btn_share addTarget:self action:@selector(shareBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell.discussionDetailView.btn_save addTarget:self action:@selector(saveBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    [cell.discussionDetailView.btn_cross addTarget:self action:@selector(crossBtnAction:) forControlEvents:UIControlEventTouchUpInside];
    //

    
    return cell;
}

- (BOOL)shouldShowAccessoryButtonForMessage:(id<JSQMessageData>)message
{
    return NO;
}

#pragma mark - UICollectionView Delegate

#pragma mark - Custom menu items

///*
- (BOOL)collectionView:(UICollectionView *)collectionView canPerformAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{
    if (action == @selector(customAction:)) {
        return YES;
    }
    
    if (action == @selector(delete:)) {
        
        JSQMessage *msg = [self.messagesArray objectAtIndex:indexPath.item];

        if (([msg.senderId isEqualToString:self.senderId])) {
            return YES;
        }
        return NO;
    }
    
    return [super collectionView:collectionView canPerformAction:action forItemAtIndexPath:indexPath withSender:sender];
}

- (void)collectionView:(UICollectionView *)collectionView performAction:(SEL)action forItemAtIndexPath:(NSIndexPath *)indexPath withSender:(id)sender
{
    if (action == @selector(customAction:)) {
        [self customAction:sender];
        return;
    }
    
    [super collectionView:collectionView performAction:action forItemAtIndexPath:indexPath withSender:sender];
}

- (void)customAction:(id)sender
{
    NSLog(@"Custom action received! Sender: %@", sender);
    
    [[[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Custom Action", nil)
                                message:nil
                               delegate:nil
                      cancelButtonTitle:NSLocalizedString(@"OK", nil)
                      otherButtonTitles:nil]
     show];
}
//*/
#pragma mark - JSQMessages collection view flow layout delegate

#pragma mark - Adjusting cell label heights

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    return 20.0f;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    return 20.0f;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
    if (expandedIndexPath != nil && expandedIndexPath.item == indexPath.item) {
        return 165;
    }
    return 0.0f;
}

#pragma mark - Responding to collection view tap events

- (void)collectionView:(JSQMessagesCollectionView *)collectionView
                header:(JSQMessagesLoadEarlierHeaderView *)headerView didTapLoadEarlierMessagesButton:(UIButton *)sender
{
    NSLog(@"Load earlier messages!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapAvatarImageView:(UIImageView *)avatarImageView atIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Tapped avatar!");
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapMessageBubbleAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"Tapped message bubble!");

    JSQMessage *msg = [self.messagesArray objectAtIndex:indexPath.item];

    if ([msg.text rangeOfString:@"#"].location == NSNotFound) {// do not expand cell if it does't contain # char
        
        return;
    }
    
    expandedIndexPath = indexPath;
    [collectionView reloadData];
    [collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionBottom animated:YES];

}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapCellAtIndexPath:(NSIndexPath *)indexPath touchLocation:(CGPoint)touchLocation
{
    NSLog(@"Tapped cell at %@!", NSStringFromCGPoint(touchLocation));
}

#pragma mark - JSQMessagesComposerTextViewPasteDelegate methods

- (BOOL)composerTextView:(JSQMessagesComposerTextView *)textView shouldPasteWithSender:(id)sender
{
    if ([UIPasteboard generalPasteboard].string) {
       
        return YES;
    }
    return NO;
}

#pragma mark - JSQMessagesViewAccessoryDelegate methods

- (void)messageView:(JSQMessagesCollectionView *)view didTapAccessoryButtonAtIndexPath:(NSIndexPath *)path
{
    NSLog(@"Tapped accessory button!");
}

#pragma mark - Text view delegate

- (void)textViewDidBeginEditing:(UITextView *)textView
{
    if (textView != self.inputToolbar.contentView.textView) {
        return;
    }
    
    [textView becomeFirstResponder];
    
    if (self.automaticallyScrollsToMostRecentMessage) {
        
        [self performSelector:@selector(scrollToBottomAnimated:) withObject:@YES afterDelay:1.0];
    }
}

- (void)textViewDidChange:(UITextView *)textView
{
    if (textView != self.inputToolbar.contentView.textView) {
        return;
    }
    
    NSRange textViewCurrentRange =  textView.selectedRange;
    [textView setScrollEnabled:NO];
    [self setTextViewAttribute];
    [textView setScrollEnabled:YES];
    textView.selectedRange = textViewCurrentRange;
    
    [self.inputToolbar toggleSendButtonEnabled];
    self.inputToolbar.contentView.rightBarButtonItem = [JSQMessagesToolbarButtonFactory defaultSendButtonItem];

}

- (void)textViewDidEndEditing:(UITextView *)textView
{
    // finish typing
    [self.selectedChatDialod sendUserStoppedTyping];
    
    if (textView != self.inputToolbar.contentView.textView) {
        return;
    }
    
    [textView resignFirstResponder];
}

-(bool)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text{
    
    // send 'is typing' notification to opponent
    [self.selectedChatDialod sendUserIsTyping];
    
    if ([text isEqualToString:@"#"]) {
        [textView resignFirstResponder];
        newAskSelectLocationVC *vc = [[UIStoryboard storyboardWithName:@"Main" bundle:nil] instantiateViewControllerWithIdentifier:@"newAskSelectLocationVC"];
        vc.delegate = self;
        [self.parentVC.navigationController pushViewController:vc animated:YES];
        return false;
    }
    else{
        return true;
    }
}
#pragma mark -

- (void)attributedLabel:(TTTAttributedLabel *)label
   didSelectLinkWithURL:(NSURL *)url{
    
    NSString *placeID = [url.absoluteString stringByReplacingOccurrencesOfString:kConstantURL withString:@""];
    
    for (NSMutableDictionary *locationDict in self.parentVC.chatPlaceArray) {
        
        if ([locationDict[@"PlaceID"] isEqualToString:placeID]) {
            self.placeDict = locationDict;
            break;
        }
    }
    
    NSIndexPath *indexPath = [NSIndexPath indexPathForItem:label.tag inSection:0];
    
    if (indexPath != nil && self.placeDict != nil) {
        expandedIndexPath = indexPath;
        [self.collectionView reloadData];
        [self.collectionView scrollToItemAtIndexPath:indexPath atScrollPosition:UICollectionViewScrollPositionBottom animated:YES];
    }
}
#pragma mark - SelectedLocationDelegate

-(void)selectedLocationDetails:(id)selectedlocationModal{
    
    MASearchPlacesModel *mdl = selectedlocationModal;
    
    NSString *selectedlocation = [NSString stringWithFormat:@"#%@ ",mdl.PlaceName];
    
    if (self.selectedLocationsArray == nil) {
        self.selectedLocationsArray = [NSMutableArray new];
    }
    
    [self.selectedLocationsArray addObject:selectedlocationModal];
    
    NSString *totalStr;
    
    if (self.inputToolbar.contentView.textView.text.length==0) {
        totalStr = selectedlocation;
    }
    else{
        totalStr = [NSString stringWithFormat:@"%@ %@",self.inputToolbar.contentView.textView.text,selectedlocation];
    }
    
    self.inputToolbar.contentView.textView.text = totalStr;
    //[self setTextViewAttribute];
    [self.inputToolbar toggleSendButtonEnabled];

    //[self.inputToolbar.contentView.textView resignFirstResponder];
    
    [self textViewDidChange:self.inputToolbar.contentView.textView];
}

-(void)setTextViewAttribute{
    
    NSString *completeStr = self.inputToolbar.contentView.textView.text;
    
    NSMutableAttributedString *temp = [[NSMutableAttributedString alloc] initWithString:completeStr];
    [temp addAttribute:NSFontAttributeName value:[UIFont systemFontOfSize:16] range:NSMakeRange(0, completeStr.length)];
    [temp addAttribute:NSForegroundColorAttributeName value:[UIColor blackColor] range:NSMakeRange(0, completeStr.length)];
    
    NSInteger stringLength = completeStr.length;
    
    [self.placeIDArray removeAllObjects];
    
    for (MASearchPlacesModel *locationMdl in self.selectedLocationsArray) {
        
        NSString *strLocation = [NSString stringWithFormat:@"#%@",locationMdl.PlaceName];
        
        NSRange range = NSMakeRange(0,  stringLength);
        
        while(range.location != NSNotFound)
        {
            range = [completeStr rangeOfString: strLocation options:0 range:range];
            
            if(range.location != NSNotFound)
            {
                [self.placeIDArray addObject:locationMdl.PlaceId];
                [temp addAttribute:NSForegroundColorAttributeName value:[UIColor orangeColor] range:range];
                range = NSMakeRange(range.location + range.length,  stringLength - (range.location + range.length));
            }
        }
    }
    
    self.inputToolbar.contentView.textView.attributedText = temp;
    NSRange textViewCurrentRange =  self.inputToolbar.contentView.textView.selectedRange;
    self.inputToolbar.contentView.textView.selectedRange = textViewCurrentRange;
}

#pragma mark - MAMapViewDelegate

- (void)getLocationFromMap : (NSDictionary *) location {
    
}

#pragma mark - Discussion Detail Btn Action

-(void)callBtnAction:(UIButton*)sender {
    NSLog(@"callBtnAction");
}
-(void)directionBtnAction:(UIButton*)sender {
    NSLog(@"directionBtnAction");
}
-(void)shareBtnAction:(UIButton*)sender {
    NSLog(@"shareBtnAction");
}
-(void)saveBtnAction:(UIButton*)sender {
    NSLog(@"saveBtnAction");
}
-(void)crossBtnAction:(UIButton*)sender {
    
    expandedIndexPath = nil;
    [self.collectionView reloadData];
}


@end
