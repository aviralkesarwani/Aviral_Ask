//
//  AppDelegate.m
//  My Asks
//
//  Created by artis on 30/03/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import "AppDelegate.h"
#import "MAListDetailVC.h"
#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import "SelectingFriendsVC.h"

#include <sys/socket.h>
#include <sys/sysctl.h>
#include <net/if.h>
#include <net/if_dl.h>
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <QuickBlox/Quickblox.h>
#import <QMServices/QMServices.h>


@import HockeySDK;

@interface AppDelegate ()

@end

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.

    [QBSettings setApplicationID:45775];
    [QBSettings setAuthKey:@"ts5yYQnnJJmxqCN"];
    [QBSettings setAuthSecret:@"j4Mz-aQuROvYUQL"];
    [QBSettings setAccountKey:@"fbMdXp3SzcxskNrJW6Qq"];
    [QBSettings setChatDNSLookupCacheEnabled:YES];
    
    [QBSettings setKeepAliveInterval:30];
    [QBSettings setAutoReconnectEnabled:YES];
    
    // enabling carbons for chat
    [QBSettings setCarbonsEnabled:YES];
    // Enables Quickblox REST API calls debug console output
    [QBSettings setLogLevel:QBLogLevelNothing];
    // Enables detailed XMPP logging in console output
    [QBSettings enableXMPPLogging];

    [self registerForRemoteNotifications];

    [Fabric with:@[[Crashlytics class]]];
    [[IQKeyboardManager sharedManager] setShouldResignOnTouchOutside:YES];
    [IQKeyboardManager sharedManager].enableAutoToolbar = NO;
    
    [GMSServices provideAPIKey:@"AIzaSyCh0u-NfLq2M2acazVQEXF-g2REEsiJDRQ"];
    [PSLocation setApiKey:@"h3B0j6lgda1VUaK9fLUnh1WhSg5rh2cK11wUXYqp" andClientID:@"2Jcoc4aHy7plHPXHMSuXsL9B3wqc86ctvYWFBKh6"];
    
    [[FBSDKApplicationDelegate sharedInstance] application:application
                             didFinishLaunchingWithOptions:launchOptions];
    
    
    self.locationManager = [[CLLocationManager alloc] init];
    self.locationManager.delegate = self;
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    //[self.locationManager requestWhenInUseAuthorization];
   // [self.locationManager startUpdatingLocation];
    
//    //Right Slide Menu
//    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
//    
//    MARightSideMenuVC *rightMenu = (MARightSideMenuVC*)[mainStoryboard instantiateViewControllerWithIdentifier: @"MARightSideMenuVC"];
//    
//    [SlideNavigationController sharedInstance].rightMenu = rightMenu;
//    
//    [SlideNavigationController sharedInstance].menuRevealAnimationDuration = .18;
//    
//    [SlideNavigationController sharedInstance].portraitSlideOffset = 120;


    UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
    
    self.tabBarController = (UITabBarController *)[mainStoryboard instantiateViewControllerWithIdentifier: @"tabbarController"];
    UITabBar *tabBar = self.tabBarController.tabBar;
    UITabBarItem *tabBarItem1 = [tabBar.items objectAtIndex:0];
    UITabBarItem *tabBarItem2 = [tabBar.items objectAtIndex:1];
    UITabBarItem *tabBarItem3 = [tabBar.items objectAtIndex:2];
    UITabBarItem *tabBarItem4 = [tabBar.items objectAtIndex:3];
    UITabBarItem *tabBarItem5 = [tabBar.items objectAtIndex:4];
    tabBar.translucent = NO;
    
    [tabBar setBackgroundColor:[UIColor colorWithRed:16/255.0 green:39/255.0 blue:72/255.0 alpha:1.0]];
    
    tabBarItem1.title = @"Places";
    tabBarItem2.title = @"People";
    tabBarItem3.title = @"Asks";
    tabBarItem4.title = @"Lists";
    tabBarItem5.title = @"Settings";
    
    [tabBarItem1 setImage:[[UIImage imageNamed:@"Places_normal.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabBarItem1 setSelectedImage:[[UIImage imageNamed:@"Places_active.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    [tabBarItem2 setImage:[[UIImage imageNamed:@"People_normal.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabBarItem2 setSelectedImage:[[UIImage imageNamed:@"People_active.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    [tabBarItem3 setImage:[[UIImage imageNamed:@"Asks_normal.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabBarItem3 setSelectedImage:[[UIImage imageNamed:@"Asks_active.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    [tabBarItem4 setImage:[[UIImage imageNamed:@"Lists_normal.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabBarItem4 setSelectedImage:[[UIImage imageNamed:@"Lists_active.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    [tabBarItem5 setImage:[[UIImage imageNamed:@"Settings_normal.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    [tabBarItem5 setSelectedImage:[[UIImage imageNamed:@"Settings_active.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal]];
    
    [self.tabBarController setSelectedIndex:0];
    
    // Change the tab bar background
    UIImage* tabBarBackground = [UIImage imageNamed:@"tabbar.png"];
    [[UITabBar appearance] setBackgroundImage:tabBarBackground];
    
    //  Normal State,
    [UITabBarItem.appearance setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor whiteColor]}forState:UIControlStateNormal];
    
    // Selected State
    [UITabBarItem.appearance setTitleTextAttributes:@{NSForegroundColorAttributeName : [UIColor colorWithRed:253.0/255.0 green:174.0/255.0 blue:42.0/255.0 alpha:1.0]}forState:UIControlStateSelected];
    
    MARightSideMenuVC *rightMenu = (MARightSideMenuVC*)[mainStoryboard instantiateViewControllerWithIdentifier: @"MARightSideMenuVC"];
    
    self.container = [MFSideMenuContainerViewController
                      containerWithCenterViewController:self.tabBarController
                      leftMenuViewController:nil
                      rightMenuViewController:rightMenu];
    
    self.mainNavigationCntl = (UINavigationController*)[mainStoryboard
                                                        instantiateViewControllerWithIdentifier: @"launchingNVCtr"];
    self.container.leftMenuWidth = 200.0f;
    self.container.rightMenuWidth = 230.0f;
    [self slideMenuOpenWithPan:NO];
    
    if (![[[AppData sharedManager] getCurrentUserID] isEqualToString:@""]) {
        
        [[ChatClient sharedManager] setUpPubnubClientWithConfiguration];
        self.window.rootViewController = self.container;
        [AppData sharedManager].userID = [[AppData sharedManager] getCurrentUserID];
        
        // QuickBlox Login
        QBUUser *loginUser = [QBUUser user];
        
        loginUser.login = [AppData sharedManager].qbUserName;
        loginUser.password = [AppData sharedManager].qbPassword;
        
        QMServicesManager *servicesManagerObject = [QMServicesManager instance];
        
        [servicesManagerObject logInWithUser:loginUser completion:^(BOOL success, NSString *errorMessage) {

            NSLog(@"Login Successfull");
        }];
    }
    
    
//     Creating a custom bar button for right menu
    
//    UIButton *button  = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, 30, 30)];
//    [button setImage:[UIImage imageNamed:@"gear"] forState:UIControlStateNormal];
//    [button addTarget:[SlideNavigationController sharedInstance] action:@selector(toggleRightMenu) forControlEvents:UIControlEventTouchUpInside];
//    UIBarButtonItem *rightBarButtonItem = [[UIBarButtonItem alloc] initWithCustomView:button];
//    [SlideNavigationController sharedInstance].rightBarButtonItem = rightBarButtonItem;

//    if ([[[UIDevice currentDevice] systemVersion]floatValue] >=8.0) {
//        
//        [[UIApplication sharedApplication]registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
//        
//        [[UIApplication sharedApplication]registerForRemoteNotifications];
//    }
//    else
//    {
//        [[UIApplication sharedApplication] registerForRemoteNotificationTypes: (UIRemoteNotificationTypeNewsstandContentAvailability|UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound | UIRemoteNotificationTypeAlert)];
//    }
    // Hockey App integration
    
    [[BITHockeyManager sharedHockeyManager] configureWithIdentifier:@"1828548c877c4d859bb603045dfb8e20"];
    // Do some additional configuration if needed here
    [[BITHockeyManager sharedHockeyManager] startManager];
    [[BITHockeyManager sharedHockeyManager].authenticator
     authenticateInstallation];
    
    return YES;
}

- (void)application:(UIApplication *)app didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken
{
    NSString *token = [[deviceToken description] stringByTrimmingCharactersInSet: [NSCharacterSet characterSetWithCharactersInString:@"<>"]];
    token = [token stringByReplacingOccurrencesOfString:@" " withString:@""];
    NSLog(@"content---%@", token);
    [AppData sharedManager].deviceToken = token;
    [AppData checkDeviceToken:token];

//    [QMCore instance].pushNotificationManager.deviceToken = deviceToken;

}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error
{
    NSLog(@"Failed to get token, error: %@", error);
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
    NSLog(@"%@",userInfo);
    
    if ( application.applicationState == UIApplicationStateActive )
    {
    }
    else {
        NSDictionary *askDict = [userInfo valueForKey:@"Asks"];
        NSString *askId = [NSString stringWithFormat:@"%ld",[[askDict valueForKey:@"Id"] integerValue]] ;
        
        UITabBarController *tabBarController = self.container.centerViewController;
        
        [tabBarController setSelectedIndex:2];
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        
        MAChatScreenVC *chatScreenVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"MAChatScreenVC"];
//        chatScreenVC.askId = askId;
        [(UINavigationController *)tabBarController.selectedViewController pushViewController:chatScreenVC animated:YES];
    }
}

#pragma mark -

- (void)registerForRemoteNotifications{
    
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 80000
    if ([[UIApplication sharedApplication] respondsToSelector:@selector(registerUserNotificationSettings:)]) {
        
        [[UIApplication sharedApplication] registerUserNotificationSettings:[UIUserNotificationSettings settingsForTypes:(UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge) categories:nil]];
        [[UIApplication sharedApplication] registerForRemoteNotifications];
    }
    else{
        [[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound];
    }
#else
    [[UIApplication sharedApplication] registerForRemoteNotificationTypes:UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeSound];
#endif
}


#pragma mark -

- (BOOL)tabBarController:(UITabBarController *)tabBarController shouldSelectViewController:(UIViewController *)viewController{
    NSUInteger index=[[tabBarController viewControllers] indexOfObject:viewController];
    
    switch (index) {
        case 0:
            self.imgV.image=[UIImage imageNamed:@"People_active.png"];
            break;
        case 1:
            self.imgV.image=[UIImage imageNamed:@"tBar2.png"];
            break;
        case 2:
            self.imgV.image=[UIImage imageNamed:@"tBar3.png"];
            break;
        case 3:
            self.imgV.image=[UIImage imageNamed:@"tBar4.png"];
            break;
        case 4:
            self.imgV.image=[UIImage imageNamed:@"tBar5.png"];
            break;
        default:
            break;
    }
    return YES;
}

#pragma mark - Side Menu

- (void) slideMenuOpenWithPan : (BOOL) flag {
    if (flag) {
        self.container.panMode = MFSideMenuPanModeDefault;
    }
    else {
        self.container.panMode = MFSideMenuPanModeNone;
    }
}

-(void)openRightSlideDrawer {
    [self.container toggleRightSideMenuCompletion:^{}];
}

-(void)changeRootControllOfNavigation:(UIViewController*)vc{
    self.mainNavigationCntl = (UINavigationController*)[[self.tabBarController viewControllers] objectAtIndex:1];
    [self.mainNavigationCntl setViewControllers:@[vc] animated:NO];
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
//    [[QBChat instance] disconnectWithCompletionBlock:^(NSError * _Nullable error) {
//        
//    }];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
//    [[QBChat instance] disconnectWithCompletionBlock:^(NSError * _Nullable error) {
//        
//    }];
}

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation fromLocation:(CLLocation *)oldLocation {
    
    [AppData sharedManager].latitude = [[NSNumber numberWithDouble:newLocation.coordinate.latitude]stringValue];
    [AppData sharedManager].longitude =  [[NSNumber numberWithDouble:newLocation.coordinate.longitude]stringValue];
    [self.locationManager stopUpdatingLocation];
}

+ (NSString *)getMacAddress
{
    int                 mgmtInfoBase[6];
    char                *msgBuffer = NULL;
    NSString            *errorFlag = NULL;
    size_t              length;
    
    // Setup the management Information Base (mib)
    mgmtInfoBase[0] = CTL_NET;        // Request network subsystem
    mgmtInfoBase[1] = AF_ROUTE;       // Routing table info
    mgmtInfoBase[2] = 0;
    mgmtInfoBase[3] = AF_LINK;        // Request link layer information
    mgmtInfoBase[4] = NET_RT_IFLIST;  // Request all configured interfaces
    
    // With all configured interfaces requested, get handle index
    if ((mgmtInfoBase[5] = if_nametoindex("en0")) == 0)
        errorFlag = @"if_nametoindex failure";
    // Get the size of the data available (store in len)
    else if (sysctl(mgmtInfoBase, 6, NULL, &length, NULL, 0) < 0)
        errorFlag = @"sysctl mgmtInfoBase failure";
    // Alloc memory based on above call
    else if ((msgBuffer = malloc(length)) == NULL)
        errorFlag = @"buffer allocation failure";
    // Get system information, store in buffer
    else if (sysctl(mgmtInfoBase, 6, msgBuffer, &length, NULL, 0) < 0)
    {
        free(msgBuffer);
        errorFlag = @"sysctl msgBuffer failure";
    }
    else
    {
        // Map msgbuffer to interface message structure
        struct if_msghdr *interfaceMsgStruct = (struct if_msghdr *) msgBuffer;
        
        // Map to link-level socket structure
        struct sockaddr_dl *socketStruct = (struct sockaddr_dl *) (interfaceMsgStruct + 1);
        
        // Copy link layer address data in socket structure to an array
        unsigned char macAddress[6];
        memcpy(&macAddress, socketStruct->sdl_data + socketStruct->sdl_nlen, 6);
        
        // Read from char array into a string object, into traditional Mac address format
        NSString *macAddressString = [NSString stringWithFormat:@"%02X:%02X:%02X:%02X:%02X:%02X",
                                      macAddress[0], macAddress[1], macAddress[2], macAddress[3], macAddress[4], macAddress[5]];
        NSLog(@"Mac Address: %@", macAddressString);
        
        // Release the buffer memory
        free(msgBuffer);
        
        return macAddressString;
    }
    
    // Error...
    NSLog(@"Error: %@", errorFlag);
    
    return nil;
}

- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error {
    
    NSLog(@"%@",error);
}

-(BOOL)application:(UIApplication *)app openURL:(NSURL *)url options:(NSDictionary<NSString *,id> *)options {
    
    NSString *string = [url query];
    NSMutableDictionary *queryStringDictionary = [[NSMutableDictionary alloc] init];
    
    NSArray *urlComponents = [string componentsSeparatedByString:@"&"];
    for (NSString *keyValuePair in urlComponents)
    {
        NSArray *pairComponents = [keyValuePair componentsSeparatedByString:@"="];
        NSString *key = [[pairComponents firstObject] stringByRemovingPercentEncoding];
        NSString *value = [[pairComponents lastObject] stringByRemovingPercentEncoding];
        
        [queryStringDictionary setObject:value forKey:key];
    }
    
    if ([string rangeOfString:@"ListID"].location != NSNotFound) {
//        [KVNProgress show];
        
        UITabBarController *tabBarController = self.container.centerViewController;
        
        [tabBarController setSelectedIndex:3];
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        
        MAListDetailVC *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"MAListDetailVC"];
        vc.userListId = [queryStringDictionary valueForKey:@"ListID"];
        vc.userId = [queryStringDictionary valueForKey:@"UserID"];
        [(UINavigationController *)tabBarController.selectedViewController pushViewController:vc animated:YES];
        
        return YES;
    }
    else if ([string rangeOfString:@"PlaceID"].location != NSNotFound) {
        
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"URL error"
//                                                        message:[NSString stringWithFormat:
//                                                                 @"PlaceID %@ and UserID %@", [queryStringDictionary valueForKey:@"PlaceID"],[queryStringDictionary valueForKey:@"UserID"]]
//                                                       delegate:self cancelButtonTitle:@"Ok"
//                                              otherButtonTitles:nil];
//        
//        
//        [alert show];
        
        UITabBarController *tabBarController = self.container.centerViewController;
        
        [tabBarController setSelectedIndex:0];
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        
        MAPlacesDetailVC *vc = [mainStoryboard instantiateViewControllerWithIdentifier:@"MAPlacesDetailVC"];
        vc.PlaceID = [queryStringDictionary valueForKey:@"PlaceID"];
        [(UINavigationController *)tabBarController.selectedViewController pushViewController:vc animated:YES];

        
        return YES;
    }
    else if ([string rangeOfString:@"Code"].location != NSNotFound) {
    }
    return NO;
}

- (BOOL)application:(UIApplication *)application openURL:(NSURL *)url sourceApplication:(NSString *)sourceApplication annotation:(id)annotation {
    BOOL handled = [[FBSDKApplicationDelegate sharedInstance] application:application
                                                                  openURL:url
                                                        sourceApplication:sourceApplication
                                                               annotation:annotation
                    ];
    return handled;
}

- (NSString *) getLocalDateStringFrmGMT : (NSString *) createdDateString {
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
    NSDate *sourceDate = [formatter dateFromString:createdDateString];
    NSDateFormatter* dateFormat = [[NSDateFormatter alloc] init];
    dateFormat.timeZone = [NSTimeZone systemTimeZone];
    [dateFormat setDateFormat:@"yyyy/MM/dd hh:mm a"];
    NSString* localTime = [dateFormat stringFromDate:sourceDate];
    
    return localTime;
}

- (NSString *) getDateStringFromDate : (NSDate *) createdDateWithTime {
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *createdDateComp = [calendar components:NSCalendarUnitYear | NSCalendarUnitMonth | NSCalendarUnitDay fromDate:createdDateWithTime];
    NSDate *createdDate = [calendar dateFromComponents:createdDateComp];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"yyyy/MM/dd";
    return [dateFormatter stringFromDate:createdDate];
}

- (NSString *) getTimeStringFromDate : (NSDate *) createdDateWithTime {
    
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *createdDateComp = [calendar components:NSCalendarUnitHour | NSCalendarUnitMinute fromDate:createdDateWithTime];
    NSDate *createdDate = [calendar dateFromComponents:createdDateComp];
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    dateFormatter.dateFormat = @"hh:mm a";
    return [dateFormatter stringFromDate:createdDate];
}


@end
