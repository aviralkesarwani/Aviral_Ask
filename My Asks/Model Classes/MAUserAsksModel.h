//
//  MAUserAsksModel.h
//  My Asks
//
//  Created by Mac on 06/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface MAUserAsksModel : NSObject

@property (strong, nonatomic) NSString *UserAsksID;
@property (strong, nonatomic) NSString *UserId;
@property (strong, nonatomic) NSString *Question;
@property (strong, nonatomic) NSString *InvitedContactsCount;
@property (strong, nonatomic) NSString *BackgroundImage;
@property (strong, nonatomic) NSString *ChannelName;
@property (strong, nonatomic) NSString *CreatedDate;
@property (strong, nonatomic) NSString *TotalMessages;
@property (assign, nonatomic) BOOL IsOwner;
@property (strong, nonatomic) NSMutableArray *InvitedContactsArray;
@property (strong, nonatomic) NSMutableArray *AskDetailsArray;

@end
