//
//  MAPeopleInviteModel.h
//  My Asks
//
//  Created by artis on 26/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MAPeopleInviteModel : NSObject

@property (strong, nonatomic) NSString *contactFullName;
@property (strong, nonatomic) NSString *contactPhoneNumber;
@property (nonatomic, assign) BOOL isSelectedNumber;

@end
