//
//  MAInviteFriendsModel.h
//  My Asks
//
//  Created by artis on 03/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <Foundation/Foundation.h>
#import  <UIKit/UIKit.h>

@interface MAInviteFriendsModel : NSObject

@property (strong, nonatomic) NSString *contactFullName;
@property (strong, nonatomic) NSArray *arrPhoneNumbers;
@property (strong, nonatomic) UIImage *contactImage;
@property (strong, nonatomic) NSString *selectedPhoneNumber;
@property (nonatomic, assign) BOOL isSelected;
@end
