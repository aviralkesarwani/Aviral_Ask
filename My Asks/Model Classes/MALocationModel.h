//
//  MALocationModel.h
//  My Asks
//
//  Created by artis on 01/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MALocationModel : NSObject

@property (strong, nonatomic) NSString *placeName;
@property (strong, nonatomic) NSString *placeAddress;
@property (strong, nonatomic) NSString *latitude;
@property (strong, nonatomic) NSString *longitude;
@property (strong, nonatomic) NSString *placeID;
@property (nonatomic, assign) BOOL isSelected;

@end
