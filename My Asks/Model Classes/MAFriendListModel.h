//
//  MAFriendListModel.h
//  My Asks
//
//  Created by artis on 22/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MAFriendListModel : NSObject

@property (strong, nonatomic) NSString *ContactName;
@property (strong, nonatomic) NSString *ContactNumber;
@property (strong, nonatomic) NSString *Id;
@property (nonatomic, assign) BOOL IsRexUser;
@property (strong, nonatomic) NSString *ListCount;
@property (strong, nonatomic) NSString *PictureCount;
@property (strong, nonatomic) NSString *PlacesCount;
@property (strong, nonatomic) NSString *ProfilePicture;
@property (nonatomic, assign) BOOL isSelected;


@end
