//
//  MAUserListsModel.h
//  My Asks
//
//  Created by MY PC on 4/24/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MAUserListsModel : NSObject


@property (strong, nonatomic) NSString *UserListID;
@property (strong, nonatomic) NSString *UserId;
@property (strong, nonatomic) NSString *Name;
@property (strong, nonatomic) NSString *Description;
@property (strong, nonatomic) NSString *LikesCount;
@property (strong, nonatomic) NSString *CommmentsCount;
@property (strong, nonatomic) NSString *PicturesCount;
@property (strong, nonatomic) NSString *LastmodifiedDate;

@property (assign, nonatomic) bool IsPublic;
@property (nonatomic, assign) BOOL IsDefault;
@property (nonatomic, assign) BOOL isSelected;

@property (strong, nonatomic) NSMutableArray *Pictures;
@property (strong, nonatomic) NSMutableArray *Places;

@end
