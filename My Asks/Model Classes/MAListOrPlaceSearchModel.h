//
//  MAListOrPlaceSearch.h
//  My Asks
//
//  Created by MY PC on 4/24/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MAListOrPlaceSearchModel : NSObject

@property (strong, nonatomic) NSString *UserID;
@property (strong, nonatomic) NSString *PlaceLatitude;
@property (strong, nonatomic) NSString *PlaceLongitude;
@property (strong, nonatomic) NSString *PlaceTypes;
@property (strong, nonatomic) NSString *PlaceName;
@property (strong, nonatomic) NSString *PlaceId;
@property (strong, nonatomic) NSString *DistanceinMiles;
@property (strong, nonatomic) NSString *DistanceinKMs;
@property (nonatomic, assign) BOOL isFavorite;
@property (strong, nonatomic) NSString *LikesCount;
@property (strong, nonatomic) NSString *CommentsCount;
@property (strong, nonatomic) NSString *PlaceAddress;
@property (strong, nonatomic) NSString *PlaceImage;
@property (strong, nonatomic) NSString *PlaceImageURL;
@property (strong, nonatomic) NSString *InternationalPhoneNumber;
@property (strong, nonatomic) NSString *OpeningHours;
@property (nonatomic, assign) BOOL isSelected;
@end
