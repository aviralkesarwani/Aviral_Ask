//
//  MACreateAskDetailsModel.h
//  My Asks
//
//  Created by artis on 06/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MACreateAskDetailsModel : NSObject

@property (strong, nonatomic) NSString *Message;
@property (strong, nonatomic) NSString *Type;
@property (assign, nonatomic) bool IsOwner;
@property (strong, nonatomic) NSString *Number;
@property (strong, nonatomic) NSString *AskId;

@end
