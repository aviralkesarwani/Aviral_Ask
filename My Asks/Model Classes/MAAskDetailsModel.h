//
//  MAAskDetailsModel.h
//  My Asks
//
//  Created by Mac on 06/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MAAskDetailsModel : NSObject

@property (strong, nonatomic) NSString *Name;
@property (strong, nonatomic) NSString *AskDetailID;
@property (strong, nonatomic) NSString *AskId;
@property (strong, nonatomic) NSString *Number;
@property (strong, nonatomic) NSString *Type;
@property (strong, nonatomic) NSString *MessageTime;
@property (strong, nonatomic) NSString *Message;
@property (strong, nonatomic) NSString *placeName;
@property (strong, nonatomic) NSString *placeAddress;
@property (strong, nonatomic) NSString *latitude;
@property (strong, nonatomic) NSString *longitude;
@property (strong, nonatomic) NSString *placeID;
@property (assign, nonatomic) BOOL isFavorite;

@end
