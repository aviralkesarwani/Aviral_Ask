//
//  MASettingModelClass.h
//  My Asks
//
//  Created by Mac on 24/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MAUserListsModel.h"
#import "MAUserAsksModel.h"
#import "MAMyProfileModelClass.h"

@interface MASettingModelClass : NSObject

@property (strong, nonatomic) NSString *AsksCount;
@property (strong, nonatomic) NSString *CommentsCount;
@property (strong, nonatomic) NSString *LikesCount;
@property (strong, nonatomic) NSString *ListsCount;
@property (strong, nonatomic) NSString *PlacesCount;
@property (strong, nonatomic) NSMutableArray *Asks;
@property (strong, nonatomic) NSMutableArray *Lists;
@property (strong, nonatomic) MAMyProfileModelClass *Profile;
@property (strong, nonatomic) NSString *SharesCount;
@property (strong, nonatomic) NSString *FriendsCount;

@end
