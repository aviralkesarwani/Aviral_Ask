//
//  MAUserListsPlacesModel.h
//  My Asks
//
//  Created by MY PC on 4/24/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MAUserListsPlacesModel : NSObject

@property (strong, nonatomic) NSString *ID;
@property (strong, nonatomic) NSString *Name;
@property (strong, nonatomic) NSString *Address;
@property (strong, nonatomic) NSString *City;
@property (strong, nonatomic) NSString *State;
@property (strong, nonatomic) NSString *Latitude;
@property (strong, nonatomic) NSString *Longitude;
@property (strong, nonatomic) NSString *DistanceFromUserLocation;
@property (strong, nonatomic) NSString *PlaceID;
@property (strong, nonatomic) NSString *PhoneNumber;
@property (strong, nonatomic) NSString *OpenHours;
@property (strong, nonatomic) NSString *UserId;
@property (strong, nonatomic) NSString *ListId;
@property (strong, nonatomic) NSString *PlaceImage;
@property (strong, nonatomic) NSString *Category;
@property (assign, nonatomic) bool IsUserFavorite;

@end
