//
//  MACreateAskModel.h
//  My Asks
//
//  Created by artis on 06/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MACreateAskModel : NSObject

@property (strong, nonatomic) NSString *Question;
@property (strong, nonatomic) NSString *BackgroundImage;
@property (strong, nonatomic) NSString *UserID;
@property (strong, nonatomic) NSMutableArray *InvitedContacts;
@property (strong, nonatomic) NSMutableArray *AskDetails;
@end
