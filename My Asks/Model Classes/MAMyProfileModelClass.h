//
//  MAMyProfileModelClass.h
//  My Asks
//
//  Created by Mac on 24/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface MAMyProfileModelClass : NSObject

@property (strong, nonatomic) NSString *BackgroundImage;
@property (strong, nonatomic) NSString *CountryCode;
@property (strong, nonatomic) NSString *CreatedDate;
@property (strong, nonatomic) NSString *DeviceToken;
@property (strong, nonatomic) NSString *Email;
@property (strong, nonatomic) NSString *Gender;
@property (strong, nonatomic) NSString *Id;
@property (assign, nonatomic) BOOL IsActive;
@property (assign, nonatomic) NSInteger MobileNumber;
@property (strong, nonatomic) NSString *Name;
@property (strong, nonatomic) NSString *ProfilePicture;
@property (strong, nonatomic) NSString *UpdatedDate;
@property (strong, nonatomic) NSString *request_id;


@end
