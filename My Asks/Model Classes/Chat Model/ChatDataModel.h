//
//  ChatDataModel.h
//  My Asks
//
//  Created by Mac on 07/04/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import <CoreLocation/CoreLocation.h>

#import "JSQMessages.h"

static NSString * const kJSQDemoAvatarDisplayNameSquires = @"Mahendra";
static NSString * const kJSQDemoAvatarDisplayNameCook = @"Yashwanth";
static NSString * const kJSQDemoAvatarDisplayNameJobs = @"Sunil";
static NSString * const kJSQDemoAvatarDisplayNameWoz = @"Ashok";

static NSString * const kJSQDemoAvatarIdSquires = @"9429504142";
static NSString * const kJSQDemoAvatarIdCook = @"919900062090";
static NSString * const kJSQDemoAvatarIdJobs = @"9722744637";
static NSString * const kJSQDemoAvatarIdWoz = @"919900062626";

@interface ChatDataModel : NSObject

@property (strong, nonatomic) NSMutableArray *messages;

@property (strong, nonatomic) NSDictionary *avatars;

@property (strong, nonatomic) JSQMessagesBubbleImage *outgoingBubbleImageData;

@property (strong, nonatomic) JSQMessagesBubbleImage *incomingBubbleImageData;

@property (strong, nonatomic) NSDictionary *users;

- (void)addPhotoMediaMessage;

- (void)addLocationMediaMessageCompletion:(JSQLocationMediaItemCompletionBlock)completion;

- (void)addVideoMediaMessage;


@end
