//
//  MAInsertUpdateUserLists.h
//  My Asks
//
//  Created by MY PC on 4/25/16.
//  Copyright © 2016 OrangeAppSol. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface MAInsertUpdateUserListsModel : NSObject

@property (strong, nonatomic) NSString *PlaceLatitude;
@property (strong, nonatomic) NSString *PlaceLongitude;
@property (strong, nonatomic) NSString *PlaceName;

@end
